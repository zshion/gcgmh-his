<?php

use Illuminate\Support\Facades\Route;
use Spatie\Activitylog\Models\Activity;
use App\MedicineIssuance;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   //return Activity::all();
    return view('welcome');
});


Route::get('admin/equipment/reports','EquipmentController@reports');
Route::get('admin/purchase-orders/reports','PurchaseOrderController@reports');
Route::get('admin/issuances_report','MedicineIssuanceController@reports');
Route::get('admin/issuances_reports', 'MedicineIssuanceController@get_reports')->name('admin.issuances_reports');
Route::get('admin/medicine-issuances/creates', 'MedicineIssuanceController@Creates');
Route::post("admin/medicine-issuances/add-field", ['as' => 'post_dynamic_fields', 'uses' => 'MedicineIssuanceController@postAddFields']);
Route::post("admin/medicine-issuances/clone-fields", ['as' => 'clone_fields', 'uses' => 'MedicineIssuanceController@getCloneFields']);
Route::get("admin/medicine-issuances/add-field", ['as' => 'dynamic_fields', 'uses' => 'MedicineIssuanceController@Creates']);
Route::get('admin/get-issuances', 'MedicineIssuanceController@get_issuances');
Route::get('admin/total-issuances', 'MedicineIssuanceController@total_issuances');
Route::get('admin/view-issuances', 'MedicineIssuanceController@get_total_issuances');
Route::get('admin/medicine-issuances/searchajax', ['as'=>'searchajax','uses'=>'MedicineIssuanceController@searchResponse']);
Route::get('admin/medicine-issuances/fetch', ['as'=>'fetched','uses'=>'MedicineIssuanceController@fetch'])->name('admin.medicine-issuances.fetch');
Route::get('admin/medicine-issuances/export', 'MedicineIssuanceController@medicine_issuance_reports');
Route::get('admin/medicine-issuances/views', 'MedicineIssuanceController@total_issuances');
Route::get('admin/medicine-stockbalances/export', 'MedicineStockbalanceController@export');
Route::get('admin/medicine-deliveries/export', 'MedicineDeliveryController@export');
Route::get('admin/medicine-deliveries-export', 'MedicineDeliveryController@get_export');
Route::get('admin/pharmacy-medicine-transaction-report', 'PharmacyMedicineTransactionController@get_report');
Route::get('admin/pharmacy-medicine-transactions/reports', 'PharmacyMedicineTransactionController@report');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    
});

Route::get('admin/buttons', 'MedicineIssuanceController@buttons');
Route::post('admin/buttons/export', 'MedicineIssuanceController@buttons');

Route::get ( '/admin/table', function () {
    $data = MedicineIssuance::find([1, 2, 3]);
    return view('/vendor/voyager/Medicine-Issuances/reports')->withData ( $data );
} );
Route::get('admin/tablereport','MedicineIssuanceController@tablereport')->name('admin.tablereport');
