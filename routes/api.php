<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*Route::resource('Equipment', 'Api/EquipmentController', ['except' => ['create', 'edit']]);*/




Route::resource('equipment', 'EquipmentAPIController');
Route::resource('purchase_orders', 'PurchaseOrderAPIController');
Route::resource('issuances_report', 'MedicineIssuanceAPIController');


Route::resource('medicine_credit_notes', 'MedicineCreditNoteAPIController');

Route::resource('medicine_adjustments', 'MedicineAdjustmentAPIController');

Route::resource('medicine_deliveries', 'MedicineDeliveryAPIController');

Route::resource('medicine_credit_memos', 'MedicineCreditMemoAPIController');

Route::resource('medicine_expired_damages', 'MedicineExpiredDamageAPIController');

Route::resource('medicine_stockbalances', 'MedicineStockbalanceAPIController');