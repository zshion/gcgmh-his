<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class PurchaseOrders extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \App\PurchaseOrder::count();
        $string = 'Purchase Orders';


        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => __('voyager::dimmer.purchase_orders_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => 'Purchase Orders',
                'link' => route('voyager.purchase-orders.index'),
            ],
            'image' => '/PO.jpg',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
        return Auth::user()->all();
        //return Auth::user()->can('browse', Voyager::model('User'));
    }
}
