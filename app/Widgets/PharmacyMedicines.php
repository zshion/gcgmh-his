<?php

namespace App\Widgets;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Widgets\BaseDimmer;

class PharmacyMedicines extends BaseDimmer
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $count = \App\PharmacyMedicine::count();
        $string = 'Medicines';


        return view('voyager::dimmer', array_merge($this->config, [
            'icon'   => 'voyager-group',
            'title'  => "{$count} {$string}",
            'text'   => __('voyager::dimmer.medicines_text', ['count' => $count, 'string' => Str::lower($string)]),
            'button' => [
                'text' => 'Medicines',
                'link' => route('voyager.pharmacy-medicines.index'),
            ],
            'image' => '/medicines.jfif',
        ]));
    }

    /**
     * Determine if the widget should be displayed.
     *
     * @return bool
     */
    public function shouldBeDisplayed()
    {
       return Auth::user()->all();
        //return Auth::user()->can('browse', Voyager::model('User'));
    }
}
