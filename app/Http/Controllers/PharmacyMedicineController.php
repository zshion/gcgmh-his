<?php

namespace App\Http\Controllers;
use App\GenericName;
use App\PharmacyMedicine;
use App\MedicineStockbalance;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PharmacyMedicineController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request){

        $rules = [
             'pharmacy_medicine_sku' => 'required|unique:pharmacy_medicines',
             'pharmacy_item_type_key'=>'required'
        ];

        $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/pharmacy-medicines/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                    try{

                        $genericKey = DB::table('generic_names')->where('generic_name_key', $data['generic_name_key'])->value('generic_name');
                        $strengthKey = DB::table('pharmacy_item_strengths')->where('pharmacy_item_strength_key', $data['pharmacy_item_strength_key'])->value('pharmacy_item_strength_name');
                        $routeKey = DB::table('pharmacy_item_routes')->where('pharmacy_item_route_key', $data['pharmacy_item_route_key'])->value('pharmacy_item_route_name');
                        $formKey = DB::table('pharmacy_item_forms')->where('pharmacy_item_form_key', $data['pharmacy_item_form_key'])->value('pharmacy_item_form_name');
                        $typeKey = DB::table('pharmacy_item_types')->where('pharmacy_item_type_key',$data['pharmacy_item_type_key'])->value('pharmacy_item_type_name');
                        //$unitKey = DB::table('units')->where('unit_key',$data['unit_key'])->value('unit_name');
                        $pndfKey = DB::table('pndf_classes')->where('pndf_class_key',$data['pndf_class_key'])->value('pndf_class_name');

                        $medicine = new PharmacyMedicine;
                            if($typeKey=='Donated'){
                                $medicine->pharmacy_medicine_name = $genericKey .' '. $data['pharmacy_medicine_strength_value'] .' '.$strengthKey .' '.$routeKey .' '.$formKey.' '.'('.$typeKey.')';
                                $medicine->pharmacy_medicine_sku = $data['pharmacy_medicine_sku'];
                                $medicine->pharmacy_medicine_strength_value = $data['pharmacy_medicine_strength_value'];
                                //$medicine->pharmacy_medicine_qty = $data['pharmacy_medicine_qty'];
                                $medicine->generic_name_key = $data['generic_name_key'];
                                $medicine->pharmacy_item_strength_key = $data['pharmacy_item_strength_key'];
                                $medicine->pharmacy_item_form_key = $data['pharmacy_item_form_key'];
                                $medicine->pharmacy_item_route_key = $data['pharmacy_item_route_key'];
                                $medicine->pharmacy_medicine_antimicrobial = $data['pharmacy_medicine_antimicrobial'];
                                $medicine->pharmacy_item_type_key = $data['pharmacy_item_type_key'];
                                $medicine->pndf_class_key = $data['pndf_class_key'];
                            }
                            else{
                                $medicine->pharmacy_medicine_name = $genericKey .' '. $data['pharmacy_medicine_strength_value'] .' '.$strengthKey .' '.$routeKey .' '.$formKey;
                                $medicine->pharmacy_medicine_sku = $data['pharmacy_medicine_sku'];
                                $medicine->pharmacy_medicine_strength_value = $data['pharmacy_medicine_strength_value'];
                                //$medicine->pharmacy_medicine_qty = $data['pharmacy_medicine_qty'];
                                $medicine->generic_name_key = $data['generic_name_key'];
                                $medicine->pharmacy_item_strength_key = $data['pharmacy_item_strength_key'];
                                $medicine->pharmacy_item_form_key = $data['pharmacy_item_form_key'];
                                $medicine->pharmacy_item_route_key = $data['pharmacy_item_route_key'];
                                $medicine->pharmacy_medicine_antimicrobial = $data['pharmacy_medicine_antimicrobial'];
                                $medicine->pharmacy_item_type_key = $data['pharmacy_item_type_key'];
                                $medicine->pndf_class_key = $data['pndf_class_key'];
                            }
                            
                            if($medicine->save()){
                                $pharmacyMedicine_id = $medicine->pharmacy_medicine_key;
                                $medicineStockbalance = new MedicineStockbalance();
 
                                $medicineStockbalance->pharmacy_medicine_key = $pharmacyMedicine_id;
                                $medicineStockbalance->medicine_stockbalance_beginning = 0;
                                $medicineStockbalance->medicine_stockbalance_current = 0;
                                $medicineStockbalance->medicine_stockbalance_reorder = 0;
                                $medicineStockbalance->pharmacy_medicine_sku = $data['pharmacy_medicine_sku'];
                                $medicineStockbalance->pharmacy_item_type_key = $data['pharmacy_item_type_key'];
                                $medicineStockbalance->save();
                            }
                            else{
                                return redirect('admin/ppharmacy-medicines')->with([
                                    'message' => "Error Saving",
                                    'alert-type' => 'error']);
                            }

                            return redirect('admin/pharmacy-medicines')->with([
                                'message' => __('voyager::generic.successfully_added_new')." Pharmacy Medicine",
                                'alert-type' => 'success']);

                    }
                    catch(Exception $e){
                        return redirect('path')->with([
                            'message' => "Error Saving",
                            'alert-type' => 'error']);
                    }
            }
    }

    public function update(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $data = $request->input();
            try{
                $genericKey = DB::table('generic_names')->where('generic_name_key', $data['generic_name_key'])->value('generic_name');
                $strengthKey = DB::table('pharmacy_item_strengths')->where('pharmacy_item_strength_key', $data['pharmacy_item_strength_key'])->value('pharmacy_item_strength_name');
                $routeKey = DB::table('pharmacy_item_routes')->where('pharmacy_item_route_key', $data['pharmacy_item_route_key'])->value('pharmacy_item_route_name');
                $formKey = DB::table('pharmacy_item_forms')->where('pharmacy_item_form_key', $data['pharmacy_item_form_key'])->value('pharmacy_item_form_name');
                $typeKey = DB::table('pharmacy_item_types')->where('pharmacy_item_type_key',$data['pharmacy_item_type_key'])->value('pharmacy_item_type_name');
                $pndfKey = DB::table('pndf_classes')->where('pndf_class_key',$data['pndf_class_key'])->value('pndf_class_name');

                    if($typeKey=='Donated'){
                    $pharmacyMedicine = DB::table('pharmacy_medicines')
                        ->where('pharmacy_medicine_key', $id)
                        ->update(array('pharmacy_medicine_strength_value'=> $data['pharmacy_medicine_strength_value'],
                            'pharmacy_medicine_name'=> $genericKey .' '. $data['pharmacy_medicine_strength_value'] .' '.$strengthKey .' '.$routeKey .' '.$formKey.' '.'('.$typeKey.')',
                            'generic_name_key'=> $data['generic_name_key'],
                            'pharmacy_item_strength_key'=> $data['pharmacy_item_strength_key'],
                            'pharmacy_item_route_key'=> $data['pharmacy_item_route_key'],
                            'pharmacy_item_form_key'=> $data['pharmacy_item_form_key'],
                            'pndf_class_key'=> $data['pndf_class_key'],
                            'pharmacy_medicine_antimicrobial' => $data['pharmacy_medicine_antimicrobial'],
                            'pharmacy_item_type_key' => $data['pharmacy_item_type_key']));
                    }
                    else{
                        $pharmacyMedicine = DB::table('pharmacy_medicines')
                        ->where('pharmacy_medicine_key', $id)
                        ->update(array('pharmacy_medicine_strength_value'=> $data['pharmacy_medicine_strength_value'],
                            'pharmacy_medicine_name'=> $genericKey .' '. $data['pharmacy_medicine_strength_value'] .' '.$strengthKey .' '.$routeKey .' '.$formKey,
                            'generic_name_key'=> $data['generic_name_key'],
                            'pharmacy_item_strength_key'=> $data['pharmacy_item_strength_key'],
                            'pharmacy_item_route_key'=> $data['pharmacy_item_route_key'],
                            'pharmacy_item_form_key'=> $data['pharmacy_item_form_key'],
                            'pndf_class_key'=> $data['pndf_class_key'],
                            'pharmacy_medicine_antimicrobial' => $data['pharmacy_medicine_antimicrobial'],
                            'pharmacy_item_type_key' => $data['pharmacy_item_type_key']));
                    }
            }
            catch(Exception $e){
                return redirect('path')->with([
                    'message' => "Error Saving",
                    'alert-type' => 'error']);
            }

            return redirect('admin/pharmacy-medicines')->with([
                'message' => __('voyager::generic.successfully_updated')." Pharmacy Medicine",
                'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect('admin/pharmacy-medicines')->with([
            'message' => __('voyager::generic.successfully_deleted')." Pharmacy Medicine",
            'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/pharmacy-medicines')->with([
            'message' => __('voyager::generic.successfully_restored')." Pharmacy Medicine",
            'alert-type' => 'success']);
    }

    public function reports()
    {
        return view('/vendor/voyager/Equipment/reports');
    }


    public function getCountries()
    {
        /*  return "{$this->purchase_order_key}";*/
        $countries = DB::table('purchase_orders')
            /* ->select('*')
             ->get();*/
            ->pluck("purchase_order_number","purchase_order_key");
        return view('my_view',compact( ['countries']));
    }

    // public $additional_attributes = ['countries'];

    public function getStates($id)
    {
        $states = DB::table("purchase_orders")->where("purchase_order_key",$id)->get();
        // $states->all();
        return json_encode($states);

    }

}
