<?php

namespace App\Http\Controllers\Api;

use App\Equipment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class EquipmentController extends Controller
{
    public function index()
    {
        return Equipment::all();
    }

    public function store(Request $request)
    {
        $Equipment = Equipment::create($request->all());

        return $Equipment;
    }

    public function show($id)
    {
        return Equipment::findOrFail($id);
    }

    public function update(Request $request, $id)
    {
        $Equipment = Equipment::findOrFail($id);
        $Equipment->update($request->all());

        return $Equipment;
    }

    public function destroy($id)
    {
        $Equipment = Equipment::findOrFail($id);
        $Equipment->delete();

        return '';
    }
}
