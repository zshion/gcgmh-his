<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicineStockbalanceAPIRequest;
use App\Http\Requests\API\UpdateMedicineStockbalanceAPIRequest;
use App\Models\MedicineStockbalance;
use App\Repositories\MedicineStockbalanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicineStockbalanceController
 * @package App\Http\Controllers\API
 */

class MedicineStockbalanceAPIController extends AppBaseController
{
    /** @var  MedicineStockbalanceRepository */
    private $medicineStockbalanceRepository;

    public function __construct(MedicineStockbalanceRepository $medicineStockbalanceRepo)
    {
        $this->medicineStockbalanceRepository = $medicineStockbalanceRepo;
    }

    /**
     * Display a listing of the MedicineStockbalance.
     * GET|HEAD /medicineStockbalances
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        /* $medicineStockbalances = $this->medicineStockbalanceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicineStockbalances->toArray(), 'Medicine Stockbalances retrieved successfully'); */

        $stockbalance = \Illuminate\Support\Facades\DB::table('medicine_stockbalances as stockbalance')
            ->select('*')

        ->leftjoin('pharmacy_medicines', function($join) {
            $join->on('stockbalance.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
        })
        ->leftjoin('pharmacy_item_types', function($join) {
            $join->on('stockbalance.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
        })
        ->get();

        return datatables()->of($stockbalance)
            ->make(true);
    }

    /**
     * Store a newly created MedicineStockbalance in storage.
     * POST /medicineStockbalances
     *
     * @param CreateMedicineStockbalanceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicineStockbalanceAPIRequest $request)
    {
        $input = $request->all();

        $medicineStockbalance = $this->medicineStockbalanceRepository->create($input);

        return $this->sendResponse($medicineStockbalance->toArray(), 'Medicine Stockbalance saved successfully');
    }

    /**
     * Display the specified MedicineStockbalance.
     * GET|HEAD /medicineStockbalances/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicineStockbalance $medicineStockbalance */
        $medicineStockbalance = $this->medicineStockbalanceRepository->find($id);

        if (empty($medicineStockbalance)) {
            return $this->sendError('Medicine Stockbalance not found');
        }

        return $this->sendResponse($medicineStockbalance->toArray(), 'Medicine Stockbalance retrieved successfully');
    }

    /**
     * Update the specified MedicineStockbalance in storage.
     * PUT/PATCH /medicineStockbalances/{id}
     *
     * @param int $id
     * @param UpdateMedicineStockbalanceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicineStockbalanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicineStockbalance $medicineStockbalance */
        $medicineStockbalance = $this->medicineStockbalanceRepository->find($id);

        if (empty($medicineStockbalance)) {
            return $this->sendError('Medicine Stockbalance not found');
        }

        $medicineStockbalance = $this->medicineStockbalanceRepository->update($input, $id);

        return $this->sendResponse($medicineStockbalance->toArray(), 'MedicineStockbalance updated successfully');
    }

    /**
     * Remove the specified MedicineStockbalance from storage.
     * DELETE /medicineStockbalances/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicineStockbalance $medicineStockbalance */
        $medicineStockbalance = $this->medicineStockbalanceRepository->find($id);

        if (empty($medicineStockbalance)) {
            return $this->sendError('Medicine Stockbalance not found');
        }

        $medicineStockbalance->delete();

        return $this->sendSuccess('Medicine Stockbalance deleted successfully');
    }
}
