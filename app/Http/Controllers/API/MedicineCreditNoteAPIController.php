<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicineCreditNoteAPIRequest;
use App\Http\Requests\API\UpdateMedicineCreditNoteAPIRequest;
use App\Models\MedicineCreditNote;
use App\Repositories\MedicineCreditNoteRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicineCreditNoteController
 * @package App\Http\Controllers\API
 */

class MedicineCreditNoteAPIController extends AppBaseController
{
    /** @var  MedicineCreditNoteRepository */
    private $medicineCreditNoteRepository;

    public function __construct(MedicineCreditNoteRepository $medicineCreditNoteRepo)
    {
        $this->medicineCreditNoteRepository = $medicineCreditNoteRepo;
    }

    /**
     * Display a listing of the MedicineCreditNote.
     * GET|HEAD /medicineCreditNotes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       /* $medicineCreditNotes = $this->medicineCreditNoteRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicineCreditNotes->toArray(), 'Medicine Credit Notes retrieved successfully'); */

        $creditNotes = \Illuminate\Support\Facades\DB::table('medicine_credit_notes as credit_note')
            ->selectRaw('*, credit_note.created_at as created_date')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('credit_note.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->get();

        return datatables()->of($creditNotes)
            ->addColumn('action', function($row) {
                return '<a href="medicine-credit-notes/'. $row->medicine_credit_note_key .'/edit" class="btn btn-primary">Edit</a>';
            })

            ->rawColumns(['action' => 'action'])
            ->make(true);

    }

    /**
     * Store a newly created MedicineCreditNote in storage.
     * POST /medicineCreditNotes
     *
     * @param CreateMedicineCreditNoteAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicineCreditNoteAPIRequest $request)
    {
        $input = $request->all();

        $medicineCreditNote = $this->medicineCreditNoteRepository->create($input);

        return $this->sendResponse($medicineCreditNote->toArray(), 'Medicine Credit Note saved successfully');
    }

    /**
     * Display the specified MedicineCreditNote.
     * GET|HEAD /medicineCreditNotes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicineCreditNote $medicineCreditNote */
        $medicineCreditNote = $this->medicineCreditNoteRepository->find($id);

        if (empty($medicineCreditNote)) {
            return $this->sendError('Medicine Credit Note not found');
        }

        return $this->sendResponse($medicineCreditNote->toArray(), 'Medicine Credit Note retrieved successfully');
    }

    /**
     * Update the specified MedicineCreditNote in storage.
     * PUT/PATCH /medicineCreditNotes/{id}
     *
     * @param int $id
     * @param UpdateMedicineCreditNoteAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicineCreditNoteAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicineCreditNote $medicineCreditNote */
        $medicineCreditNote = $this->medicineCreditNoteRepository->find($id);

        if (empty($medicineCreditNote)) {
            return $this->sendError('Medicine Credit Note not found');
        }

        $medicineCreditNote = $this->medicineCreditNoteRepository->update($input, $id);

        return $this->sendResponse($medicineCreditNote->toArray(), 'MedicineCreditNote updated successfully');
    }

    /**
     * Remove the specified MedicineCreditNote from storage.
     * DELETE /medicineCreditNotes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicineCreditNote $medicineCreditNote */
        $medicineCreditNote = $this->medicineCreditNoteRepository->find($id);

        if (empty($medicineCreditNote)) {
            return $this->sendError('Medicine Credit Note not found');
        }

        $medicineCreditNote->delete();

        return $this->sendSuccess('Medicine Credit Note deleted successfully');
    }
}
