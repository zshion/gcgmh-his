<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicineDeliveryAPIRequest;
use App\Http\Requests\API\UpdateMedicineDeliveryAPIRequest;
use App\Models\MedicineDelivery;
use App\Repositories\MedicineDeliveryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicineDeliveryController
 * @package App\Http\Controllers\API
 */

class MedicineDeliveryAPIController extends AppBaseController
{
    /** @var  MedicineDeliveryRepository */
    private $medicineDeliveryRepository;

    public function __construct(MedicineDeliveryRepository $medicineDeliveryRepo)
    {
        $this->medicineDeliveryRepository = $medicineDeliveryRepo;
    }

    /**
     * Display a listing of the MedicineDelivery.
     * GET|HEAD /medicineDeliveries
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       /* $medicineDeliveries = $this->medicineDeliveryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicineDeliveries->toArray(), 'Medicine Deliveries retrieved successfully'); */

        $deliveries = \Illuminate\Support\Facades\DB::table('medicine_deliveries as delivery')
            ->selectRaw('*, delivery.created_at as created_date')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('delivery.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('delivery.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            })
            ->leftjoin('suppliers', function($join) {
                $join->on('delivery.supplier_key', '=', 'suppliers.supplier_key');
            })
            ->get();

        return datatables()->of($deliveries)
            ->addColumn('action', function($row) {
                return '<a href="medicine-deliveries/'. $row->medicine_delivery_key .'/edit" class="btn btn-primary">Edit</a>';
            })

            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    /**
     * Store a newly created MedicineDelivery in storage.
     * POST /medicineDeliveries
     *
     * @param CreateMedicineDeliveryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicineDeliveryAPIRequest $request)
    {
        $input = $request->all();

        $medicineDelivery = $this->medicineDeliveryRepository->create($input);

        return $this->sendResponse($medicineDelivery->toArray(), 'Medicine Delivery saved successfully');
    }

    /**
     * Display the specified MedicineDelivery.
     * GET|HEAD /medicineDeliveries/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicineDelivery $medicineDelivery */
        $medicineDelivery = $this->medicineDeliveryRepository->find($id);

        if (empty($medicineDelivery)) {
            return $this->sendError('Medicine Delivery not found');
        }

        return $this->sendResponse($medicineDelivery->toArray(), 'Medicine Delivery retrieved successfully');
    }

    /**
     * Update the specified MedicineDelivery in storage.
     * PUT/PATCH /medicineDeliveries/{id}
     *
     * @param int $id
     * @param UpdateMedicineDeliveryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicineDeliveryAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicineDelivery $medicineDelivery */
        $medicineDelivery = $this->medicineDeliveryRepository->find($id);

        if (empty($medicineDelivery)) {
            return $this->sendError('Medicine Delivery not found');
        }

        $medicineDelivery = $this->medicineDeliveryRepository->update($input, $id);

        return $this->sendResponse($medicineDelivery->toArray(), 'MedicineDelivery updated successfully');
    }

    /**
     * Remove the specified MedicineDelivery from storage.
     * DELETE /medicineDeliveries/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicineDelivery $medicineDelivery */
        $medicineDelivery = $this->medicineDeliveryRepository->find($id);

        if (empty($medicineDelivery)) {
            return $this->sendError('Medicine Delivery not found');
        }

        $medicineDelivery->delete();

        return $this->sendSuccess('Medicine Delivery deleted successfully');
    }
}
