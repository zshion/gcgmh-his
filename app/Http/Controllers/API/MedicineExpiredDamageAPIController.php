<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicineExpiredDamageAPIRequest;
use App\Http\Requests\API\UpdateMedicineExpiredDamageAPIRequest;
use App\Models\MedicineExpiredDamage;
use App\Repositories\MedicineExpiredDamageRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicineExpiredDamageController
 * @package App\Http\Controllers\API
 */

class MedicineExpiredDamageAPIController extends AppBaseController
{
    /** @var  MedicineExpiredDamageRepository */
    private $medicineExpiredDamageRepository;

    public function __construct(MedicineExpiredDamageRepository $medicineExpiredDamageRepo)
    {
        $this->medicineExpiredDamageRepository = $medicineExpiredDamageRepo;
    }

    /**
     * Display a listing of the MedicineExpiredDamage.
     * GET|HEAD /medicineExpiredDamages
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       /* $medicineExpiredDamages = $this->medicineExpiredDamageRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicineExpiredDamages->toArray(), 'Medicine Expired Damages retrieved successfully'); */

        $expireDamage = \Illuminate\Support\Facades\DB::table('medicine_expired_damages as expired')
            ->selectRaw('*, expired.created_at as created_date')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('expired.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('expired.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            })
            ->get();

        return datatables()->of($expireDamage)
            ->addColumn('action', function($row) {
                return '<a href="medicine-expired-damages/'. $row->medicine_expired_damaged_key .'/edit" class="btn btn-primary">Edit</a>';
            })

            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    /**
     * Store a newly created MedicineExpiredDamage in storage.
     * POST /medicineExpiredDamages
     *
     * @param CreateMedicineExpiredDamageAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicineExpiredDamageAPIRequest $request)
    {
        $input = $request->all();

        $medicineExpiredDamage = $this->medicineExpiredDamageRepository->create($input);

        return $this->sendResponse($medicineExpiredDamage->toArray(), 'Medicine Expired Damage saved successfully');
    }

    /**
     * Display the specified MedicineExpiredDamage.
     * GET|HEAD /medicineExpiredDamages/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicineExpiredDamage $medicineExpiredDamage */
        $medicineExpiredDamage = $this->medicineExpiredDamageRepository->find($id);

        if (empty($medicineExpiredDamage)) {
            return $this->sendError('Medicine Expired Damage not found');
        }

        return $this->sendResponse($medicineExpiredDamage->toArray(), 'Medicine Expired Damage retrieved successfully');
    }

    /**
     * Update the specified MedicineExpiredDamage in storage.
     * PUT/PATCH /medicineExpiredDamages/{id}
     *
     * @param int $id
     * @param UpdateMedicineExpiredDamageAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicineExpiredDamageAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicineExpiredDamage $medicineExpiredDamage */
        $medicineExpiredDamage = $this->medicineExpiredDamageRepository->find($id);

        if (empty($medicineExpiredDamage)) {
            return $this->sendError('Medicine Expired Damage not found');
        }

        $medicineExpiredDamage = $this->medicineExpiredDamageRepository->update($input, $id);

        return $this->sendResponse($medicineExpiredDamage->toArray(), 'MedicineExpiredDamage updated successfully');
    }

    /**
     * Remove the specified MedicineExpiredDamage from storage.
     * DELETE /medicineExpiredDamages/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicineExpiredDamage $medicineExpiredDamage */
        $medicineExpiredDamage = $this->medicineExpiredDamageRepository->find($id);

        if (empty($medicineExpiredDamage)) {
            return $this->sendError('Medicine Expired Damage not found');
        }

        $medicineExpiredDamage->delete();

        return $this->sendSuccess('Medicine Expired Damage deleted successfully');
    }
}
