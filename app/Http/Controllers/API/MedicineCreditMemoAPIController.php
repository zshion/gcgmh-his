<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicineCreditMemoAPIRequest;
use App\Http\Requests\API\UpdateMedicineCreditMemoAPIRequest;
use App\Models\MedicineCreditMemo;
use App\Repositories\MedicineCreditMemoRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicineCreditMemoController
 * @package App\Http\Controllers\API
 */

class MedicineCreditMemoAPIController extends AppBaseController
{
    /** @var  MedicineCreditMemoRepository */
    private $medicineCreditMemoRepository;

    public function __construct(MedicineCreditMemoRepository $medicineCreditMemoRepo)
    {
        $this->medicineCreditMemoRepository = $medicineCreditMemoRepo;
    }

    /**
     * Display a listing of the MedicineCreditMemo.
     * GET|HEAD /medicineCreditMemos
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
       /* $medicineCreditMemos = $this->medicineCreditMemoRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicineCreditMemos->toArray(), 'Medicine Credit Memos retrieved successfully'); */

        $creditMemos = \Illuminate\Support\Facades\DB::table('medicine_credit_memos as credit_memo')
            ->selectRaw('*, credit_memo.created_at as created_date')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('credit_memo.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('suppliers', function($join) {
                $join->on('credit_memo.supplier_key', '=', 'suppliers.supplier_key');
            })
            ->get();

        return datatables()->of($creditMemos)
            ->addColumn('action', function($row) {
                return '<a href="medicine-credit-memos/'. $row->medicine_credit_memo_key .'/edit" class="btn btn-primary">Edit</a>';
            })

            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    /**
     * Store a newly created MedicineCreditMemo in storage.
     * POST /medicineCreditMemos
     *
     * @param CreateMedicineCreditMemoAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicineCreditMemoAPIRequest $request)
    {
        $input = $request->all();

        $medicineCreditMemo = $this->medicineCreditMemoRepository->create($input);

        return $this->sendResponse($medicineCreditMemo->toArray(), 'Medicine Credit Memo saved successfully');
    }

    /**
     * Display the specified MedicineCreditMemo.
     * GET|HEAD /medicineCreditMemos/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicineCreditMemo $medicineCreditMemo */
        $medicineCreditMemo = $this->medicineCreditMemoRepository->find($id);

        if (empty($medicineCreditMemo)) {
            return $this->sendError('Medicine Credit Memo not found');
        }

        return $this->sendResponse($medicineCreditMemo->toArray(), 'Medicine Credit Memo retrieved successfully');
    }

    /**
     * Update the specified MedicineCreditMemo in storage.
     * PUT/PATCH /medicineCreditMemos/{id}
     *
     * @param int $id
     * @param UpdateMedicineCreditMemoAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicineCreditMemoAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicineCreditMemo $medicineCreditMemo */
        $medicineCreditMemo = $this->medicineCreditMemoRepository->find($id);

        if (empty($medicineCreditMemo)) {
            return $this->sendError('Medicine Credit Memo not found');
        }

        $medicineCreditMemo = $this->medicineCreditMemoRepository->update($input, $id);

        return $this->sendResponse($medicineCreditMemo->toArray(), 'MedicineCreditMemo updated successfully');
    }

    /**
     * Remove the specified MedicineCreditMemo from storage.
     * DELETE /medicineCreditMemos/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicineCreditMemo $medicineCreditMemo */
        $medicineCreditMemo = $this->medicineCreditMemoRepository->find($id);

        if (empty($medicineCreditMemo)) {
            return $this->sendError('Medicine Credit Memo not found');
        }

        $medicineCreditMemo->delete();

        return $this->sendSuccess('Medicine Credit Memo deleted successfully');
    }
}
