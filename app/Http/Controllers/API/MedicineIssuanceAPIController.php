<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicineIssuanceAPIRequest;
use App\Http\Requests\API\UpdateMedicineIssuanceAPIRequest;
use App\MedicineIssuance;
use App\Repositories\MedicineIssuanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class MedicineIssuanceController
 * @package App\Http\Controllers\API
 */

class MedicineIssuanceAPIController extends AppBaseController
{
    /** @var  MedicineIssuanceRepository */
    private $medicineIssuanceRepository;

    public function __construct(MedicineIssuanceRepository $medicineIssuanceRepo)
    {
        $this->medicineIssuanceRepository = $medicineIssuanceRepo;
    }

    /**
     * Display a listing of the MedicineIssuance.
     * GET|HEAD /medicineIssuances
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {

        $medicineIssuances = DB::table('pharmacy_medicines as medicines')
            ->selectRaw('*,medicines.pharmacy_medicine_key as medicine_key,
            COALESCE(SUM(damage.medicine_expired_damaged_qty), 0) AS damagesum')

            ->leftjoin('medicine_issuances as issuances', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'issuances.pharmacy_medicine_key');
            })
            ->leftJoin('medicine_stockbalances as beginning', function($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'beginning.pharmacy_medicine_key');
            })

            ->leftJoin('medicine_deliveries as delivery', function($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'delivery.pharmacy_medicine_key');
            })

            ->leftjoin('delivery as medicine_name', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'medicine_name.pharmacy_medicine_key');
            })
            ->leftJoin('medicine_adjustments as adjustment', function ($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'adjustment.pharmacy_medicine_key');
            })
            ->leftJoin('medicine_expired_damages as damage', function ($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'damage.pharmacy_medicine_key');
            })
            ->leftJoin('issued', function ($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'issued.pharmacy_medicine_key');
            })

            ->groupBy('medicines.pharmacy_medicine_key')
        ->get();


       /* $medicineIssuances = DB::table('medicine_issuances')
            ->leftJoin('medicine_stockbalances', 'medicine_issuances.pharmacy_medicine_key', '=', 'medicine_stockbalances.pharmacy_medicine_key')
            ->leftJoin('pharmacy_medicines', 'medicine_issuances.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key')
            ->select('*')
            ->groupBy('pharmacy_medicine_key')
            ->get();*/

        return ($medicineIssuances);

        //return $this->sendResponse($medicineIssuances->toArray(), 'Medicine Issuances retrieved successfully');
    }

    /**
     * Store a newly created MedicineIssuance in storage.
     * POST /medicineIssuances
     *
     * @param CreateMedicineIssuanceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicineIssuanceAPIRequest $request)
    {
        $input = $request->all();

        $medicineIssuance = $this->medicineIssuanceRepository->create($input);

        return $this->sendResponse($medicineIssuance->toArray(), 'Medicine Issuance saved successfully');
    }

    /**
     * Display the specified MedicineIssuance.
     * GET|HEAD /medicineIssuances/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicineIssuance $medicineIssuance */
        $medicineIssuance = $this->medicineIssuanceRepository->find($id);

        if (empty($medicineIssuance)) {
            return $this->sendError('Medicine Issuance not found');
        }

        return $this->sendResponse($medicineIssuance->toArray(), 'Medicine Issuance retrieved successfully');
    }

    /**
     * Update the specified MedicineIssuance in storage.
     * PUT/PATCH /medicineIssuances/{id}
     *
     * @param int $id
     * @param UpdateMedicineIssuanceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicineIssuanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicineIssuance $medicineIssuance */
        $medicineIssuance = $this->medicineIssuanceRepository->find($id);

        if (empty($medicineIssuance)) {
            return $this->sendError('Medicine Issuance not found');
        }

        $medicineIssuance = $this->medicineIssuanceRepository->update($input, $id);

        return $this->sendResponse($medicineIssuance->toArray(), 'MedicineIssuance updated successfully');
    }

    /**
     * Remove the specified MedicineIssuance from storage.
     * DELETE /medicineIssuances/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicineIssuance $medicineIssuance */
        $medicineIssuance = $this->medicineIssuanceRepository->find($id);

        if (empty($medicineIssuance)) {
            return $this->sendError('Medicine Issuance not found');
        }

        $medicineIssuance->delete();

        return $this->sendSuccess('Medicine Issuance deleted successfully');
    }
}
