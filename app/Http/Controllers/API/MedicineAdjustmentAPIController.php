<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicineAdjustmentAPIRequest;
use App\Http\Requests\API\UpdateMedicineAdjustmentAPIRequest;
use App\Models\MedicineAdjustment;
use App\Repositories\MedicineAdjustmentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicineAdjustmentController
 * @package App\Http\Controllers\API
 */

class MedicineAdjustmentAPIController extends AppBaseController
{
    /** @var  MedicineAdjustmentRepository */
    private $medicineAdjustmentRepository;

    public function __construct(MedicineAdjustmentRepository $medicineAdjustmentRepo)
    {
        $this->medicineAdjustmentRepository = $medicineAdjustmentRepo;
    }

    /**
     * Display a listing of the MedicineAdjustment.
     * GET|HEAD /medicineAdjustments
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
      /*  $medicineAdjustments = $this->medicineAdjustmentRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicineAdjustments->toArray(), 'Medicine Adjustments retrieved successfully'); */

        $adjustments = \Illuminate\Support\Facades\DB::table('medicine_adjustments as adjustment')
            ->selectRaw('*, adjustment.created_at as created_date')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('adjustment.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('adjustment.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            })
            ->leftjoin('medicine_adjustment_lists', function($join) {
                $join->on('adjustment.medicine_adjustment_name', '=', 'medicine_adjustment_lists.medicine_adjustment_list_key');
            })
            ->get();

        return datatables()->of($adjustments)
            ->addColumn('action', function($row) {
                return '<a href="medicine-adjustments/'. $row->medicine_adjustment_key .'/edit" class="btn btn-primary">Edit</a>';
            })

            ->rawColumns(['action' => 'action'])
            ->make(true);
    }

    /**
     * Store a newly created MedicineAdjustment in storage.
     * POST /medicineAdjustments
     *
     * @param CreateMedicineAdjustmentAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicineAdjustmentAPIRequest $request)
    {
        $input = $request->all();

        $medicineAdjustment = $this->medicineAdjustmentRepository->create($input);

        return $this->sendResponse($medicineAdjustment->toArray(), 'Medicine Adjustment saved successfully');
    }

    /**
     * Display the specified MedicineAdjustment.
     * GET|HEAD /medicineAdjustments/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicineAdjustment $medicineAdjustment */
        $medicineAdjustment = $this->medicineAdjustmentRepository->find($id);

        if (empty($medicineAdjustment)) {
            return $this->sendError('Medicine Adjustment not found');
        }

        return $this->sendResponse($medicineAdjustment->toArray(), 'Medicine Adjustment retrieved successfully');
    }

    /**
     * Update the specified MedicineAdjustment in storage.
     * PUT/PATCH /medicineAdjustments/{id}
     *
     * @param int $id
     * @param UpdateMedicineAdjustmentAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicineAdjustmentAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicineAdjustment $medicineAdjustment */
        $medicineAdjustment = $this->medicineAdjustmentRepository->find($id);

        if (empty($medicineAdjustment)) {
            return $this->sendError('Medicine Adjustment not found');
        }

        $medicineAdjustment = $this->medicineAdjustmentRepository->update($input, $id);

        return $this->sendResponse($medicineAdjustment->toArray(), 'MedicineAdjustment updated successfully');
    }

    /**
     * Remove the specified MedicineAdjustment from storage.
     * DELETE /medicineAdjustments/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicineAdjustment $medicineAdjustment */
        $medicineAdjustment = $this->medicineAdjustmentRepository->find($id);

        if (empty($medicineAdjustment)) {
            return $this->sendError('Medicine Adjustment not found');
        }

        $medicineAdjustment->delete();

        return $this->sendSuccess('Medicine Adjustment deleted successfully');
    }
}
