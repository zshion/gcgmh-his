<?php

namespace App\Http\Controllers;

use App\User;
use App\PharmacyEndBalance;
use App\MedicineStockbalance;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

class PharmacyEndBalanceController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_end_balance_date' => 'required|unique:pharmacy_end_balances'
         ];
         $validator = Validator::make($request->all(),$rules);

            if ($validator->fails()) {
                return redirect('admin/pharmacy-end-balances/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                if(Auth::check()) {
                    $data = $request->input();
                    $user_id = Auth::user()->id;
                        try{
                            $medicineBalance = MedicineStockbalance::all();
                                foreach($medicineBalance as $medicine_balance) {
                                    $endBalance =  PharmacyEndBalance::insert([
                                        'pharmacy_medicine_key' => $medicine_balance->pharmacy_medicine_key,
                                        'pharmacy_medicine_sku' => $medicine_balance->pharmacy_medicine_sku,
                                        'pharmacy_item_type_key' =>  $medicine_balance->pharmacy_item_type_key,
                                        'pharmacy_end_balance_qty' => $medicine_balance->medicine_stockbalance_current,
                                        'pharmacy_end_balance_date' => $data['pharmacy_end_balance_date'],
                                        'pharmacy_beginning_balance' => $medicine_balance->medicine_stockbalance_beginning,
                                        'user_id' => $user_id
                                    ]);  
                                    $medicine_Stockbalances = DB::table('medicine_stockbalances')
                                            ->where('pharmacy_medicine_key', $medicine_balance->pharmacy_medicine_key)
                                            ->update(array('medicine_stockbalance_beginning' => $medicine_balance->medicine_stockbalance_current));

                                }
                            //dd ($endBalance);       
                                return redirect('admin/pharmacy-end-balances/');  
                        }
                        catch(Exception $e){
                            return redirect('path')->with([
                                'message' => "Error Saving",
                                'alert-type' => 'error']);
                        }
                } else {
                    return redirect('admin/medicine-stockbalances')
                        ->withErrors([
                            'message' => "User need to log in."]);
                }
            }
    }
}
