<?php

namespace App\Http\Controllers;
use App\SubDescription;
use App\PharmacySupplyDescription;
use App\PharmacySupply;
use App\PharmacySupplyStockbalance;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PharmacySupplyController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
        'pharmacy_supply_sku' => 'required|unique:pharmacy_supplies',
        'pharmacy_item_type_key' => 'required'
        ];

        $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/pharmacy-supplies/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                    try{
                        $descriptionKey = DB::table('pharmacy_supply_descriptions')->where('pharmacy_supply_description_key', $data['pharmacy_supply_description_key'])->value('pharmacy_supply_description_name');
                        $subDescriptionKey = DB::table('sub_descriptions')->where('sub_description_key', $data['sub_description_key'])->value('sub_description_name');
                        $typeKey = DB::table('pharmacy_item_types')->where('pharmacy_item_type_key',$data['pharmacy_item_type_key'])->value('pharmacy_item_type_name');

                        $supply = new PharmacySupply;

                            if($typeKey=='Donated'){
                                $supply->pharmacy_supply_name = $descriptionKey .', '. $subDescriptionKey .' '.'('.$typeKey.')';
                                $supply->pharmacy_supply_sku = $data['pharmacy_supply_sku'];
                                //$supply->pharmacy_supply_qty = $data['pharmacy_supply_qty'];
                                $supply->pharmacy_supply_description_key = $data['pharmacy_supply_description_key'];
                                $supply->sub_description_key = $data['sub_description_key'];
                                $supply->pharmacy_item_type_key = $data['pharmacy_item_type_key'];
                                //$supply->unit_key = $data['unit_key']
                            }
                            else{
                                $supply->pharmacy_supply_name = $descriptionKey .', '. $subDescriptionKey;
                                $supply->pharmacy_supply_sku = $data['pharmacy_supply_sku'];
                                //$supply->pharmacy_supply_qty = $data['pharmacy_supply_qty'];
                                $supply->pharmacy_supply_description_key = $data['pharmacy_supply_description_key'];
                                $supply->sub_description_key = $data['sub_description_key'];
                                $supply->pharmacy_item_type_key = $data['pharmacy_item_type_key'];
                                //$supply->unit_key = $data['unit_key'];
                            }
                            
                                if($supply->save()){
                                    $pharmacySupply_id = $supply->pharmacy_supply_key;
                                    $supplyStockbalance = new PharmacySupplyStockbalance();
                                    
                                    $supplyStockbalance->pharmacy_supply_key = $pharmacySupply_id;
                                    $supplyStockbalance->pharmacy_supply_stockbalance_beginning = 0;
                                    $supplyStockbalance->pharmacy_supply_stockbalance_current = 0;
                                    $supplyStockbalance->pharmacy_supply_stockbalance_reorder = 0;                           
                                    $supplyStockbalance->pharmacy_supply_sku = $data['pharmacy_supply_sku'];
                                    $supplyStockbalance->pharmacy_item_type_key = $data['pharmacy_item_type_key'];
                                    $supplyStockbalance->save();
                                }
                                else{
                                    return redirect('admin/pharmacy-supplies')->with([
                                        'message' => "Error Saving",
                                        'alert-type' => 'error']);
                                }

                            return redirect('admin/pharmacy-supplies')->with([
                                'message' => __('voyager::generic.successfully_added_new')." Pharmacy Supply",
                                'alert-type' => 'success']);
                    }
                    catch(Exception $e){
                        return redirect('path')->with([
                            'message' => "Error Saving",
                            'alert-type' => 'error']);
                    }
            }
    }
    
    public function update(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $data = $request->input();

        try{
            $descriptionKey = DB::table('pharmacy_supply_descriptions')->where('pharmacy_supply_description_key', $data['pharmacy_supply_description_key'])->value('pharmacy_supply_description_name');
            $subDescriptionKey = DB::table('sub_descriptions')->where('sub_description_key', $data['sub_description_key'])->value('sub_description_name');
            $typeKey = DB::table('pharmacy_item_types')->where('pharmacy_item_type_key',$data['pharmacy_item_type_key'])->value('pharmacy_item_type_name');

            if($typeKey=='Donated'){
                $pharmacySupply = DB::table('pharmacy_supplies')
                ->where('pharmacy_supply_key', $id)
                ->update(array('pharmacy_supply_name' => $descriptionKey .', '. $subDescriptionKey .' '.'('.$typeKey.')',
                'pharmacy_supply_description_key' => $data['pharmacy_supply_description_key'],
                'sub_description_key' => $data['sub_description_key'],
                'pharmacy_item_type_key' => $data['pharmacy_item_type_key']));
            }
            else{
                $pharmacySupply = DB::table('pharmacy_supplies')
                ->where('pharmacy_supply_key', $id)
                ->update(array('pharmacy_supply_name' => $descriptionKey .', '. $subDescriptionKey,
                'pharmacy_supply_description_key' => $data['pharmacy_supply_description_key'],
                'sub_description_key' => $data['sub_description_key'],
                'pharmacy_item_type_key' => $data['pharmacy_item_type_key']));
            }
        }
        catch(Exception $e){
            return redirect('path')->with([
                'message' => "Error Saving",
                'alert-type' => 'error']);
        }
        return redirect('admin/pharmacy-supplies')->with([
            'message' => __('voyager::generic.successfully_updated')." Pharmacy Supply",
            'alert-type' => 'success']);

    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect('admin/pharmacy-supplies')->with([
            'message' => __('voyager::generic.successfully_deleted')." Pharmacy Supply",
            'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/pharmacy-supplies')->with([
            'message' => __('voyager::generic.successfully_restored')." Pharmacy Supply",
            'alert-type' => 'success']);
    }
    
}