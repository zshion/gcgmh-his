<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class PurchaseOrderController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function reports()
    {
        return view('/vendor/voyager/Purchase-Orders/reports');
    }

}
