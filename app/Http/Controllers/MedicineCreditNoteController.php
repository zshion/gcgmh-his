<?php

namespace App\Http\Controllers;
use App\MedicineCreditNote;
use App\MedicineStockbalance;
use App\MedicineTransaction;
use App\PharmacyMedicineTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MedicineCreditNoteController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_medicine_key' => 'required',
            'medicine_credit_note_qty'  => 'required',
            'medicine_credit_note_date'  => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
         if ($validator->fails()) {
             return redirect('admin/medicine-credit-notes/create')
                 ->withInput()
                 ->withErrors($validator);
         }
         else{
            $data = $request->input();
                try{
                    $pharmacyMedicineKey = $data['pharmacy_medicine_key'];
                    $itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');
                    $checkStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->doesntExist();

                        if($checkStockBalance){
                            return redirect('admin/medicine-credit-notes/create')
                                ->withInput()
                                ->withErrors([
                                    'message' => "No stock balance on the item."]);
                        }
                        else{
                            $medicineCreditNote = new MedicineCreditNote();
    
                            $medicineCreditNote ->medicine_credit_note_date = $data['medicine_credit_note_date'];
                            $medicineCreditNote ->medicine_credit_note_qty = $data['medicine_credit_note_qty'];
                            $medicineCreditNote ->medicine_credit_note_remarks = $data['medicine_credit_note_remarks'];
                            $medicineCreditNote ->medicine_credit_note_area = $data['medicine_credit_note_area'];
                            $medicineCreditNote ->employee_name = $data['employee_name'];
                            $medicineCreditNote ->patient_hospital_no = $data['patient_hospital_no'];
                            $medicineCreditNote ->pharmacy_medicine_key = $data['pharmacy_medicine_key'];
                            
                            $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');
                            
                                if($medicineCreditNote ->save()){
                                    $medicineCreditNote_id = $medicineCreditNote->medicine_credit_note_key;

                                    $medicineTransaction = new MedicineTransaction();
                                    $medicineTransactionType = "Credit Note";
                                    $medicineTransaction->medicine_transaction_type_key = $medicineCreditNote_id;
                                    $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                                    $medicineTransaction->medicine_transaction_qty = $data['medicine_credit_note_qty'];
                                    $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                    $medicineTransaction->medicine_transaction_status = "Added";

                                        if($medicineTransaction->save()){
                                            $balance = MedicineStockbalance::where('pharmacy_medicine_key','=',$pharmacyMedicineKey)->firstOrFail();
                                            $balance_current = $balance->medicine_stockbalance_current;
                                            $balance->medicine_stockbalance_current = $balance_current + $data['medicine_credit_note_qty'];
                                        
                                                if($balance->save()) {
                                                    $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');
                                                    $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_reorder');
                                                        if($currentBalance<=$reOrderLevel){
                                                            $status = "Re-order";
                                                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                            ->update(array('medicine_stockbalance_status'=>$status) );
                                                        }
                                                        else{
                                                            $status = "";
                                                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                            ->update(array('medicine_stockbalance_status'=>$status) );
                                                        }

                                                            $transaction = new PharmacyMedicineTransaction;
                                                            $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                            $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                            $transaction->pharmacy_medicine_transaction_date = $data['medicine_credit_note_date'];
                                                            $transaction->medicine_credit_note_qty = $data['medicine_credit_note_qty'];
                                                            $transaction->pharmacy_transaction_id = $medicineCreditNote_id;
                                                            $transaction->pharmacy_transaction_type = "Credit_Note";
                                                            $transaction->save();
                                                            
                                                                return redirect('admin/medicine-credit-notes')->with([
                                                                    'message' => __('voyager::generic.successfully_added_new')." Credit Note",
                                                                    'alert-type' => 'success']);
                                                }
                                                else{
                                                    return redirect('admin/medicine-credit-notes')->with([
                                                        'message' => "Stock balance not change.",
                                                        'alert-type' => 'error']);
                                                }
                                        }   
                                        else{
                                            return redirect('admin/medicine-credit-notes')->with([
                                                'message' => "Medicine transaction not save.",
                                                'alert-type' => 'error']);
                                        } 
                                }
                                else{
                                    return redirect('admin/medicine-credit-notes/create')->with([
                                        'message' => "Error Saving",
                                        'alert-type' => 'error']);
                                }
                        }                   
                }
                catch(Exception $e){
                    return redirect('path')->with([
                        'message' => "Error Saving",
                        'alert-type' => 'error']);
                }   
         }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

            $medicineCreditNote  = DB::table('medicine_credit_notes')->where('medicine_credit_note_key', $id)->value('medicine_credit_note_qty');
            $medicineKey = DB::table('medicine_credit_notes')->where('medicine_credit_note_key', $id)->value('pharmacy_medicine_key');
            $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

            $retStockQUantity = $StockBalance - $medicineCreditNote;
            $newStockQuantity = $retStockQUantity + $request->input('medicine_credit_note_qty');
        
            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                ->update(array('medicine_stockbalance_current'=>$newStockQuantity) );

            $medicine_credit_note = DB::table('medicine_credit_notes')
                ->where('medicine_credit_note_key', $id)
                ->update(array('medicine_credit_note_date'=> $request->input('medicine_credit_note_date'),
                    'medicine_credit_note_qty'=> $request->input('medicine_credit_note_qty'),
                    'medicine_credit_note_remarks'=> $request->input('medicine_credit_note_remarks'),
                    'employee_name'=> $request->input('employee_name'),
                    'medicine_credit_note_area'=>$request->input('medicine_credit_note_area'),
                    'patient_hospital_no'=> $request->input('patient_hospital_no')));

                    $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                    $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                        if($currentBalance<=$reOrderLevel){
                            $status = "Re-order";
                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                            ->update(array('medicine_stockbalance_status'=>$status) );
                        }
                        else{
                            $status = "";
                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                            ->update(array('medicine_stockbalance_status'=>$status) );
                        }

                            $medicineTransaction = new MedicineTransaction();
                            $medicineTransactionType = "Credit Note";
                            $medicineTransaction->medicine_transaction_type_key = $id;
                            $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                            $medicineTransaction->medicine_transaction_qty = $request->input('medicine_credit_note_qty');
                            $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                            $medicineTransaction->medicine_transaction_status = "Updated"; 

                                if($medicineTransaction->save()){
                                    $transaction = DB::table('pharmacy_medicine_transactions')
                                    ->where('pharmacy_transaction_id', $id)
                                    ->where('pharmacy_transaction_type', "Credit_Note")
                                        ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_credit_note_date'),
                                        'medicine_credit_note_qty'=> $request->input('medicine_credit_note_qty') ));
                                }
                                else{
                                    return redirect('admin/medicine-credit-notes')->with([
                                        'message' => "Transaction not save.",
                                        'alert-type' => 'error']); 
                                }

                                    return redirect('admin/medicine-credit-notes')->with([
                                        'message' => __('voyager::generic.successfully_updated')." Credit Note",
                                        'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $medicineCreditNoteQty  = DB::table('medicine_credit_notes')->where('medicine_credit_note_key', $id)->value('medicine_credit_note_qty');
        $medicineKey = DB::table('medicine_credit_notes')->where('medicine_credit_note_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $newStockQUantity = $StockBalance - $medicineCreditNoteQty;

        $deleted_creditNote = DB::table('medicine_credit_notes')->where('medicine_credit_note_key', $id)->delete();

        $newMedicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQUantity) );

            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
            $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                if($currentBalance<=$reOrderLevel){
                    $status = "Re-order";
                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_status'=>$status) );
                }
                else{
                    $status = "";
                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_status'=>$status) );
                }

                    $medicineTransaction = new MedicineTransaction();
                    $medicineTransactionType = "Credit Note";
                    $medicineTransaction->medicine_transaction_type_key = $id;
                    $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                    $medicineTransaction->medicine_transaction_qty = $medicineCreditNoteQty;
                    $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                    $medicineTransaction->medicine_transaction_status = "Deleted";

                        if($medicineTransaction->save()){
                            $deleted_transaction = DB::table('pharmacy_medicine_transactions')
                            ->where('pharmacy_transaction_id', $id)
                            ->where('pharmacy_transaction_type', "Credit_Note")
                            ->delete();
                        }
                        else{
                            return redirect('admin/medicine-credit-notes')->with([
                                'message' => "Transaction not save.",
                                'alert-type' => 'error']); 
                        }

                            return redirect('admin/medicine-credit-notes')->with([
                                'message' => __('voyager::generic.successfully_deleted')." Credit Note",
                                'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/medicine-credit-notes')->with([
            'message' => __('voyager::generic.successfully_restored')." Credit Note",
            'alert-type' => 'success']);
    }

    public function get_credit_notes(){
        $issuances = \Illuminate\Support\Facades\DB::table('medicine_credit_notes as credit_note')
            ->selectRaw('*, credit_note.created_at as created_date')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('credit_note.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->get();

        return datatables()->of($issuances)
            ->addColumn('action', function($row) {
                return '<a href="medicine-credit-notes/'. $row->medicine_credit_note_key .'/edit" class="btn btn-primary">Edit</a>';
            })

            ->rawColumns(['action' => 'action'])
            ->make(true);

    }
}