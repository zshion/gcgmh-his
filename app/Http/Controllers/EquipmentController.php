<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use DB;
use Illuminate\View\View;


class EquipmentController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{


    public function reports()
    {
        return view('/vendor/voyager/Equipment/reports');
    }


    public function getCountries()
    {
      /*  return "{$this->purchase_order_key}";*/
        $countries = DB::table('purchase_orders')
           /* ->select('*')
            ->get();*/
            ->pluck("purchase_order_number","purchase_order_key");
        return view('my_view',compact( ['countries']));
    }

   // public $additional_attributes = ['countries'];

    public function getStates($id)
    {
        $states = DB::table("purchase_orders")->where("purchase_order_key",$id)->get();
       // $states->all();
        return json_encode($states);

    }

}
