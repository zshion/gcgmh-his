<?php

namespace App\Http\Controllers;

use App\User;
use App\DataTables\IssuancesReportsDataTable;
use App\DataTables\MedicineIssuanceReportsDataTable;
use App\DataTables\PharmacyMedicineTransactionReportsDataTable;
use App\MedicineIssuance;
use App\MedicineStockbalance;
use App\MedicineTransaction;
use App\PharmacyMedicineTransaction;
use App\Patient;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Input;
use function MongoDB\BSON\toJSON;


class MedicineIssuanceController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request){

        $rules = [
            'pharmacy_medicine_key' => 'required',
            'medicine_issuance_qty'  => 'required',
            'patient_hospital_no' => 'required',
            'medicine_issuance_date' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
         if ($validator->fails()) {
             return redirect('admin/medicine-issuances/create')
                 ->withInput()
                 ->withErrors($validator);
         }
         else{
            $data = $request->input();
            $user_id = Auth::user()->id;

            try{
                /*$hospID = $data['patient_hospital_no'];
                $connect = odbc_connect("MSSQLSERVER", "sa", "p@ssw0rd");
                    if( $connect === false ) {
                        return redirect('admin/medicine-issuances/create')
                        ->withInput()
                        ->withErrors([
                            'message' => "Connection to iHOMIS Failed"]);
                    }

                    $sql = "SELECT hpercode, patlast, patfirst, patmiddle, patbdate FROM dbo.hperson WHERE hpercode = '$hospID' ";
                    $results = odbc_exec($connect, $sql);
                    
                        if($results === false){
                            echo "Query Failed" .odbc_error();
                        }
                        else{
                            while( $row = odbc_fetch_array($results) ) {
                                $HospitalID = $row['hpercode'];
                                $firstname = $row['patfirst'];
                                $middlename = $row['patmiddle'];
                                $lastname = $row['patlast'];
                                $birthDate = $row['patbdate'];
                            }
                        } */


                $serverName = "192.168.7.1";
                $connectionInfo = array("Database"=>"GCGMH_dbo", "UID"=>"sa", "PWD"=>"p@ssw0rd");
                $conn = sqlsrv_connect( $serverName, $connectionInfo );
                    if( $conn === false ) {
                        //die( print_r( sqlsrv_errors(), true));
                        return redirect('admin/medicine-issuances/create')
                        ->withInput()
                        ->withErrors([
                            'message' => "Connection to iHOMIS Failed"]);
                    }
                $hospID = $data['patient_hospital_no'];

                $sql = "SELECT hpercode, patlast, patfirst, patmiddle, patbdate FROM dbo.hperson WHERE hpercode = '$hospID' ";
                $stmt = sqlsrv_query( $conn, $sql );
                    if( $stmt === false) {
                        die( print_r( sqlsrv_errors(), true) );
                    }
                    if(sqlsrv_has_rows($stmt)){
                       while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {

                            //$string=$row['hpercode'];
                            //$test = str_replace("-", "", $string);

                            $HospitalID = $row['hpercode'];
                            $firstname = $row['patfirst'];
                            $middlename = $row['patmiddle'];
                            $lastname = $row['patlast'];
                            $birthDate = $row['patbdate'];

                            //echo $HospitalID." ". $row['patlast'].", ".$row['patfirst']."<br />";
                        }
                    }
                    else{
                        return redirect('admin/medicine-issuances/create')
                        ->withInput()
                        ->withErrors([
                            'message' => "Hospital number not found."]);
                    }
                        sqlsrv_free_stmt( $stmt);


                    $checkPatient = DB::table('patients')->where('patient_hospital_no', $hospID)->doesntExist();

                        if($checkPatient){
                            $newPatient = new Patient();
                            $newPatient->patient_hospital_no = $HospitalID ;
                            $newPatient->patient_last_name = $lastname;
                            $newPatient->patient_first_name = $firstname;
                            $newPatient->patient_middle_name = $middlename;
                            $newPatient->patient_birth_date = $birthDate;
                            $newPatient->patient_full_name = $lastname .', '. $firstname .' '. $middlename;
                            $newPatient->save();
                        }
                            $pharmacyMedicineKey = $data['pharmacy_medicine_key'];
                            $itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');

                            $checkStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->doesntExist();

                                if($checkStockBalance){
                                    return redirect('admin/medicine-issuances/create')
                                        ->withInput()
                                        ->withErrors([
                                            'message' => "No stock balance on the item."]);
                                }
                                else{
                                    $medicineIssuance = new MedicineIssuance();

                                    $medicineIssuance->medicine_issuance_charge_slip = $data['medicine_issuance_charge_slip'];
                                    $medicineIssuance->medicine_issuance_qty = $data['medicine_issuance_qty'];
                                    $medicineIssuance->medicine_issuance_remarks = $data['medicine_issuance_remarks'];
                                    $medicineIssuance->medicine_issuance_date = $data['medicine_issuance_date'];
                                    $medicineIssuance->employee_name = $data['employee_name'];
                                    $medicineIssuance->patient_hospital_no = $data['patient_hospital_no'];
                                    $medicineIssuance->pharmacy_medicine_key = $data['pharmacy_medicine_key'];
                                    $medicineIssuance->pharmacy_item_type_key = $itemTypeKey;
                                    $medicineIssuance->user_id = $user_id;

                                    $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');

                                        if($medicineIssuance->save()){
                                            $medicineIssuance_id = $medicineIssuance->medicine_issuance_key;

                                            $medicineTransaction = new MedicineTransaction();
                                            $medicineTransactionType = "Issuance";
                                            $medicineTransaction->medicine_transaction_type_key = $medicineIssuance_id;
                                            $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                                            $medicineTransaction->medicine_transaction_qty = $data['medicine_issuance_qty'];
                                            $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                            $medicineTransaction->medicine_transaction_status = "Added";

                                                if($medicineTransaction->save()){
                                                    $balance = MedicineStockbalance::where('pharmacy_medicine_key','=',$pharmacyMedicineKey)->firstOrFail();
                                                    $balance_current = $balance->medicine_stockbalance_current;
                                                    $balance->medicine_stockbalance_current = $balance_current - $data['medicine_issuance_qty'];

                                                        if($balance->save()) {
                                                            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');
                                                            $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_reorder');
                                                                if($currentBalance<=$reOrderLevel){
                                                                    $status = "Re-order";
                                                                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                                    ->update(array('medicine_stockbalance_status'=>$status) );
                                                                }
                                                                else{
                                                                    $status = "";
                                                                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                                    ->update(array('medicine_stockbalance_status'=>$status) );
                                                                }

                                                                    $transaction = new PharmacyMedicineTransaction;
                                                                    $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                    $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                    $transaction->pharmacy_medicine_transaction_date = $data['medicine_issuance_date'];
                                                                    $transaction->medicine_issuance_qty = $data['medicine_issuance_qty'];
                                                                    $transaction->pharmacy_transaction_id = $medicineIssuance_id;
                                                                    $transaction->pharmacy_transaction_type = "Issuance";
                                                                    $transaction->save();

                                                                        return redirect('admin/medicine-issuances')->with([
                                                                            'message' => __('voyager::generic.successfully_added_new')." Issuance",
                                                                            'alert-type' => 'success']);
                                                        }
                                                        else{
                                                            return redirect('admin/medicine-issuances')
                                                                ->with([
                                                                    'message' => "Stock balance not change. ",
                                                                    'alert-type' => 'error']);
                                                        }
                                                }
                                                else{
                                                    return redirect('admin/medicine-issuances')->with([
                                                        'message' => "Medicine transaction not save.",
                                                        'alert-type' => 'error']);
                                                }
                                        }
                                        else{
                                            return redirect('admin/medicine-issuances/create')
                                                ->withInput()
                                                ->withErrors([
                                                    'message' => "Error Saving",
                                                    'alert-type' => 'error']);
                                        }
                                }
            }
            catch(Exception $e){
                return redirect('path')->with([
                    'message' => "Error Saving",
                    'alert-type' => 'error']);
             }
         }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;
        //$pharmacyMedicineKey = $request->input('pharmacy_medicine_key');
        //$itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');

        $medicineIssuance = DB::table('medicine_issuances')->where('medicine_issuance_key', $id)->value('medicine_issuance_qty');
        $medicineKey = DB::table('medicine_issuances')->where('medicine_issuance_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $retStockQUantity = $StockBalance + $medicineIssuance;
        $newStockQuantity = $retStockQUantity - $request->input('medicine_issuance_qty');

        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQuantity) );

        $medicine_issuance = DB::table('medicine_issuances')
            ->where('medicine_issuance_key', $id)
            ->update(array('medicine_issuance_charge_slip'=> $request->input('medicine_issuance_charge_slip'),
                'medicine_issuance_qty'=> $request->input('medicine_issuance_qty'),
                'medicine_issuance_remarks'=> $request->input('medicine_issuance_remarks'),
                'medicine_issuance_date'=> $request->input('medicine_issuance_date'),
                'employee_name'=> $request->input('employee_name'),
                'patient_hospital_no'=> $request->input('patient_hospital_no')));
                //'pharmacy_item_type_key'=> $itemTypeKey,
                //'pharmacy_medicine_key'=> $request->input('pharmacy_medicine_key')

                $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }

                        $medicineTransaction = new MedicineTransaction();
                        $medicineTransactionType = "Issuance";
                        $medicineTransaction->medicine_transaction_type_key = $id;
                        $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                        $medicineTransaction->medicine_transaction_qty = $request->input('medicine_issuance_qty');
                        $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                        $medicineTransaction->medicine_transaction_status = "Updated";
                        $medicineTransaction->save();

                            if($medicineTransaction->save()){
                                $transaction = DB::table('pharmacy_medicine_transactions')
                                ->where('pharmacy_transaction_id', $id)
                                ->where('pharmacy_transaction_type', "Issuance")
                                    ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_issuance_date'),
                                    'medicine_issuance_qty'=> $request->input('medicine_issuance_qty') ));
                            }
                            else{
                                return redirect('admin/medicine-credit-notes')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => 'error']);
                            }

                                return redirect('admin/medicine-issuances')->with([
                                    'message' => __('voyager::generic.successfully_updated')." Issuance",
                                    'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $medicineIssuanceQty = DB::table('medicine_issuances')->where('medicine_issuance_key', $id)->value('medicine_issuance_qty');
        $medicineKey = DB::table('medicine_issuances')->where('medicine_issuance_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $newStockQUantity = $StockBalance + $medicineIssuanceQty;

        $deleted_issuance = DB::table('medicine_issuances')->where('medicine_issuance_key', $id)->delete();

        $newMedicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQUantity) );

            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
            $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                if($currentBalance<=$reOrderLevel){
                    $status = "Re-order";
                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_status'=>$status) );
                }
                else{
                    $status = "";
                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_status'=>$status) );
                }

                    $medicineTransaction = new MedicineTransaction();
                    $medicineTransactionType = "Issuance";
                    $medicineTransaction->medicine_transaction_type_key = $id;
                    $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                    $medicineTransaction->medicine_transaction_qty = $medicineIssuanceQty;
                    $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                    $medicineTransaction->medicine_transaction_status = "Deleted";

                        if($medicineTransaction->save()){
                            $deleted_transaction = DB::table('pharmacy_medicine_transactions')
                            ->where('pharmacy_transaction_id', $id)
                            ->where('pharmacy_transaction_type', "Issuance")
                            ->delete();
                        }
                        else{
                            return redirect('admin/medicine-issuances')->with([
                                'message' => "Transaction not save.",
                                'alert-type' => 'error']);
                        }

                            return redirect('admin/medicine-issuances')->with([
                                'message' => __('voyager::generic.successfully_deleted')." Issuance",
                                'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/medicine-issuances')->with([
            'message' => __('voyager::generic.successfully_restored')." Issuance",
            'alert-type' => 'success']);
    }

    public function get_reports(){
        $dt = date("Y-m-d");
        $startdate =date("Y-m-27", strtotime("first day of last month"));
        $enddate = date("Y-m-26", strtotime($dt));

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $medicineIssuances = \Illuminate\Support\Facades\DB::table('pharmacy_medicine_transactions as medicines')
            ->selectRaw('*,SUM(medicine_delivery_qty) AS delivery,
            SUM(medicine_issuance_qty) AS issuance,
            SUM(adjustment_unrecorded_iar_qty) AS unrecorded,
            SUM(adjustment_cancelled_iar_qty) AS cancelled,
            SUM(adjustment_overissued_qty) AS overissued,
            SUM(adjustment_unissued_qty) AS unissued,
            SUM(adjustment_returned_ward_qty) AS ward,
            SUM(adjustment_issued_employee_qty) AS employee,
            SUM(adjustment_replacement_qty) AS replacement,
            SUM(medicine_expired_qty) AS expired,
            SUM(medicine_damage_qty) AS damaged,
            SUM(medicine_credit_memo_qty) AS memo,
            SUM(medicine_credit_note_qty) AS note,
            medicine_stockbalance_beginning AS beginning,
            medicine_stockbalance_current AS ending


             ')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('medicine_stockbalances', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'medicine_stockbalances.pharmacy_medicine_key');
            })

            ->whereBetween('pharmacy_medicine_transaction_date',array($startdate,$enddate))
            ->groupBy('medicines.pharmacy_medicine_key')
            ->get();



        if($start_date && $end_date){

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));

            $medicineIssuances = \Illuminate\Support\Facades\DB::table('pharmacy_medicine_transactions as medicines')
                ->selectRaw('*,SUM(medicine_delivery_qty) AS delivery,
            SUM(medicine_issuance_qty) AS issuance,
            SUM(adjustment_unrecorded_iar_qty) AS unrecorded,
            SUM(adjustment_cancelled_iar_qty) AS cancelled,
            SUM(adjustment_overissued_qty) AS overissued,
            SUM(adjustment_unissued_qty) AS unissued,
            SUM(adjustment_returned_ward_qty) AS ward,
            SUM(adjustment_issued_employee_qty) AS employee,
            SUM(adjustment_replacement_qty) AS replacement,
            SUM(medicine_expired_qty) AS expired,
            SUM(medicine_damage_qty) AS damaged,
            SUM(medicine_credit_memo_qty) AS memo,
            SUM(medicine_credit_note_qty) AS note,
            pharmacy_beginning_balance AS beginning,

            ( (pharmacy_beginning_balance + SUM(medicine_delivery_qty) + SUM(medicine_credit_note_qty) + SUM(adjustment_returned_ward_qty) + SUM(adjustment_overissued_qty) + 
            SUM(adjustment_replacement_qty) + SUM(adjustment_unrecorded_iar_qty) ) - ( SUM(medicine_issuance_qty) + SUM(adjustment_cancelled_iar_qty) + SUM(adjustment_unissued_qty)
            + SUM(adjustment_issued_employee_qty) + SUM(medicine_damage_qty) + SUM(medicine_expired_qty) + SUM(medicine_credit_memo_qty) )) AS ending

             ')
                ->leftjoin('pharmacy_medicines', function($join) {
                    $join->on('medicines.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                })
                ->leftjoin('pharmacy_end_balances', function($join) {
                    $join->on('medicines.pharmacy_medicine_key', '=','pharmacy_end_balances.pharmacy_medicine_key');
                })

                ->whereBetween('medicines.pharmacy_medicine_transaction_date',array($start_date,$end_date))
                ->where('medicines.pharmacy_medicine_type_key',$type)
                ->groupBy('medicines.pharmacy_medicine_key')
                ->get();

        }
      //
        return datatables()->of($medicineIssuances)
            ->addColumn('action', function($row) {
                return '<a href="/admin/pharmacy-medicines/'. $row->pharmacy_medicine_key .'/edit" class="btn btn-primary">Edit</a>';
            })
            ->editColumn('delete', function ($row) {
                return '<a href="#">delete</a>';
            })
            ->rawColumns(['delete' => 'delete','action' => 'action'])
            //->make(true);
            ->make(true);

        //return $medicineIssuances;


    }

    public function buttons(IssuancesReportsDataTable $dataTable)
    {
        return $dataTable->render('/vendor/voyager/medicine-issuances/button');
    }

    /* return $dataTable->render('users');*/


    public function medicine_issuance_reports(MedicineIssuanceReportsDataTable $dataTable)
    {
        return $dataTable->render('/vendor/voyager/medicine-issuances/export');
    }


    public function total_issuances(){
        return view('/vendor/voyager/medicine-issuances/views');
    }

    public function get_total_issuances(){
        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $totalIssuances = \Illuminate\Support\Facades\DB::table('medicine_issuances as issuance')
            ->selectRaw('*,SUM(medicine_issuance_qty) AS total')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('issuance.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->groupBy('issuance.pharmacy_medicine_key')
            ->get();
           // return $totalIssuances;

           if($start_date && $end_date){
                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));

                $totalIssuances = \Illuminate\Support\Facades\DB::table('medicine_issuances as issuance')
            ->selectRaw('*,SUM(medicine_issuance_qty) AS total')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('issuance.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })

            ->whereBetween('issuance.medicine_issuance_date',array($start_date,$end_date))
            ->where('issuance.pharmacy_item_type_key',$type)
            ->groupBy('issuance.pharmacy_medicine_key')
            ->get();

           }
            return datatables()->of($totalIssuances)
            ->make(true);
    }

    public function get_issuances(){

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $issuances = \Illuminate\Support\Facades\DB::table('medicine_issuances as issuance')
            ->selectRaw('*, issuance.created_at as created_date')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('issuance.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('issuance.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            })
            ->leftjoin('users', function($join) {
                $join->on('issuance.user_id', '=', 'users.id');
            })
            ->get();

                if($start_date && $end_date){
                    $start_date = date('Y-m-d', strtotime($start_date));
                    $end_date = date('Y-m-d', strtotime($end_date));

                    $issuances = \Illuminate\Support\Facades\DB::table('medicine_issuances as issuance')
                        ->selectRaw('*, issuance.created_at as created_date')

                        ->leftjoin('pharmacy_medicines', function($join) {
                            $join->on('issuance.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                        })
                        ->leftjoin('pharmacy_item_types', function($join) {
                            $join->on('issuance.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
                        })
                        ->leftjoin('users', function($join) {
                            $join->on('issuance.user_id', '=', 'users.id');
                        })
                        ->whereBetween('issuance.medicine_issuance_date',array($start_date,$end_date))
                        ->where('issuance.pharmacy_item_type_key',$type)
                        ->get();
                }

        return datatables()->of($issuances)
            ->addColumn('action', function($row) {
                return '<a href="medicine-issuances/'. $row->medicine_issuance_key .'/edit" class="btn btn-primary">Edit</a>';
            })

            ->rawColumns(['action' => 'action'])
            //->make(true);
            ->make(true);

    }

    public function Creates() {
        return view("/vendor/voyager/medicine-issuances/add");
    }

    public function fetch(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = DB::table('pharmacy_medicines')
                ->select('pharmacy_medicine_name')
                ->where('pharmacy_medicine_name', 'LIKE', "%{$query}%")
                ->orderby('pharmacy_medicine_name')
                ->take(5)
                ->get();
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach($data as $row)
            {
                $output .= '<li><a href="#">'.$row->pharmacy_medicine_name.'</a></li>';
            }
            $output .= '</ul>';
            echo $output;
        }
    }
    public function searchResponse(Request $request){
        $query = $request->get('term','');
        $countries=\DB::table('pharmacy_medicines');
        if($request->type=='countryname'){
            $countries->where('pharmacy_medicine_name','LIKE','%'.$query.'%');
        }

        $countries=$countries->get();
        $data=array();
        foreach ($countries as $country) {
            $data[]=array('pharmacy_medicine_name'=>$country->pharmacy_medicine_name);
        }
        if(count($data))
            return $data;
        else
            return ['pharmacy_medicine_name'=>''];
    }

    public function postAddFields(Request $request) {
        $inputData = $request->input();
        for($i=0; $i<count($inputData['name']); $i++) {
            $data['name'] = $inputData['name'][$i];
            $data['language'] = $inputData['language'][$i];
            $data['age'] = $inputData['age'][$i];
            $saveData = new DynamicFields($data);
            $saveData->save();
        }
        return back()->with('success', 'Record Created Successfully.');
    }



    public function getCloneFields(Request $request) {
        $items = PharmacyMedicine::all('pharmacy_medicine_name', 'pharmacy_medicine_key');
        $selectedID = 1;
        //return view('items.edit', compact('pharmacy_medicine_key', 'items'));

        $input = $request->input();
        $id = $input['div_count'];
        $varId = 'removeId'. $id;
        $data = '
        <div class="clonedInput" id="'. $varId .'">
            <div class="row" id="clonedInput">
                <div class="col-md-3">
                    <input type="text" name="name[]" id="name'. $id .'" placeholder="Enter Your Name" class="form-control" required/>
                    <label class="control-label col-md-8"></label>
                </div>
               <select class="form-control" name="product_id">
                    <option>Select Item</option>
                </select>

                <div class="col-md-3">
                    <input type="text" name="language" id="language'. $id .'" placeholder="Enter Your Language"  class="form-control" required/>
                    <label class="control-label col-md-8"></label>
                </div>
                <div class="col-md-3">
                    <input type="number" name="age[]" id="age'. $id .'" placeholder="Enter Your Age"  class="form-control" required/>
                    <label class="control-label col-md-8"></label>
                </div>
                <div class="col-md-1">
                    <input type="button" class="btn btn-danger" name="del_item" value="Delete" onClick="removedClone('. $varId .');" />
                </div>
            </div>
        </div>
        ';
        echo json_encode($data);
    }
   /* public function get_reports(){

        //$medicineIssuances = \Illuminate\Support\Facades\DB::table('delivery as medicines');

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        $medicineIssuances = \Illuminate\Support\Facades\DB::table('pharmacy_medicines as medicines')
            ->selectRaw('*,medicines.pharmacy_medicine_key as medicine_key,
            COALESCE(SUM(damage.medicine_expired_damaged_qty), 0) AS damagesum
             ')

            ->leftjoin('totalissued', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'totalissued.pharmacy_medicine_key');
            })
            ->leftjoin('totaladjustment', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'totaladjustment.pharmacy_medicine_key');
            })

            ->leftjoin('medicine_issuances as issuances', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'issuances.pharmacy_medicine_key');
            })
            ->leftJoin('medicine_stockbalances as beginning', function($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'beginning.pharmacy_medicine_key');
            })

            ->leftJoin('medicine_deliveries as delivery', function($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'delivery.pharmacy_medicine_key');
            })


            ->leftJoin('medicine_adjustments as adjustment', function ($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'adjustment.pharmacy_medicine_key');
            })
            ->leftJoin('medicine_expired_damages as damage', function ($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'damage.pharmacy_medicine_key');
            })
            ->leftJoin('issued', function ($join){
                $join->on('medicines.pharmacy_medicine_key', '=', 'issued.pharmacy_medicine_key');
            })

            ->groupBy('medicines.pharmacy_medicine_key')
            ->get();

        if($start_date && $end_date){

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));


            $medicineIssuances = \Illuminate\Support\Facades\DB::table('pharmacy_medicines as medicines')
                ->selectRaw('*,medicines.pharmacy_medicine_key as medicine_key,
            COALESCE(SUM(damage.medicine_expired_damaged_qty), 0) AS damagesum
             ')

                ->leftjoin('totalissued', function($join) {
                    $join->on('medicines.pharmacy_medicine_key', '=', 'totalissued.pharmacy_medicine_key');
                })
                ->leftjoin('totaladjustment', function($join) {
                    $join->on('medicines.pharmacy_medicine_key', '=', 'totaladjustment.pharmacy_medicine_key');
                })

                ->leftjoin('medicine_issuances as issuances', function($join) {
                    $join->on('medicines.pharmacy_medicine_key', '=', 'issuances.pharmacy_medicine_key');
                })
                ->leftJoin('medicine_stockbalances as beginning', function($join){
                    $join->on('medicines.pharmacy_medicine_key', '=', 'beginning.pharmacy_medicine_key');
                })

                ->leftJoin('medicine_deliveries as delivery', function($join){
                    $join->on('medicines.pharmacy_medicine_key', '=', 'delivery.pharmacy_medicine_key');
                })


                ->leftJoin('medicine_adjustments as adjustment', function ($join){
                    $join->on('medicines.pharmacy_medicine_key', '=', 'adjustment.pharmacy_medicine_key');
                })
                ->leftJoin('medicine_expired_damages as damage', function ($join){
                    $join->on('medicines.pharmacy_medicine_key', '=', 'damage.pharmacy_medicine_key');
                })
                ->leftJoin('issued', function ($join){
                    $join->on('medicines.pharmacy_medicine_key', '=', 'issued.pharmacy_medicine_key');
                })

                ->whereBetween('adjustment.medicine_adjustment_date',array($start_date,$end_date))
                ->whereRaw("issuances.medicine_issuance_date >= '" . $start_date . "' AND issuances.medicine_issuance_date <= '" . $end_date . "'")
                ->groupBy('medicines.pharmacy_medicine_key')
                ->get();

           // $medicineIssuances->whereRaw("date(posts.created_at) >= '" . $start_date . "' AND date(posts.created_at) <= '" . $end_date . "'");
        }
       $posts = $medicineIssuances;
        return datatables()->of($posts)
            ->make(true);
    }*/

    public function tablereport(Request $request)
    {

        $columns = array(
            0 =>'medicine_issuance_key',
            1 =>'medicine_issuance_charge_slip',
            2=> 'employee_name',
            3=>'medicine_issuance_key'

        );

        $totalData = MedicineIssuance::count();

        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $posts = MedicineIssuance::offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();
        }
        else {
            $search = $request->input('search.value');

            $posts =  MedicineIssuance::where('medicine_issuance_key','LIKE',"%{$search}%")
                ->orWhere('employee_name', 'LIKE',"%{$search}%")
                ->offset($start)
                ->limit($limit)
                ->orderBy($order,$dir)
                ->get();

            $totalFiltered = MedicineIssuance::where('medicine_issuance_key','LIKE',"%{$search}%")
                ->orWhere('employee_name', 'LIKE',"%{$search}%")
                ->count();
        }

        $data = array();
        if(!empty($posts))
        {
            foreach ($posts as $post)
            {
                $show =  route('posts.show',$post->medicine_issuance_key);
                $edit =  route('posts.edit',$post->medicine_issuance_key);

                $nestedData['medicine_issuance_key'] = $post->medicine_issuance_key;
                $nestedData['employee_name'] = $post->employee_name;

                $nestedData['options'] = "&emsp;<a href='{$show}' title='SHOW' ><span class='glyphicon glyphicon-list'></span></a>
                                          &emsp;<a href='{$edit}' title='EDIT' ><span class='glyphicon glyphicon-edit'></span></a>";
                $data[] = $nestedData;

            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),
            "recordsTotal"    => intval($totalData),
            "recordsFiltered" => intval($totalFiltered),
            "data"            => $data
        );

        echo json_encode($json_data);

    }
}
