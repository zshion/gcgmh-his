<?php

namespace App\Http\Controllers;

use App\PharmacySupplyCreditMemo;
use App\PharmacySupplyStockbalance;
use App\PharmacySupplyTransaction;
use App\SupplyTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PharmacySupplyCreditMemoController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_supply_key' => 'required',
            'pharmacy_supply_credit_memo_qty'  => 'required',
            'pharmacy_supply_credit_memo_date' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/pharmacy-supply-credit-memos/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                    try{
                        $supplyCreditMemo = new PharmacySupplyCreditMemo();
        
                        $supplyCreditMemo->pharmacy_supply_credit_memo_docno = $data['pharmacy_supply_credit_memo_docno'];
                        $supplyCreditMemo->pharmacy_supply_credit_memo_doc_date = $data['pharmacy_supply_credit_memo_doc_date'];
                        $supplyCreditMemo->pharmacy_supply_credit_memo_qty = $data['pharmacy_supply_credit_memo_qty'];
                        $supplyCreditMemo->pharmacy_supply_credit_memo_unit_cost = $data['pharmacy_supply_credit_memo_unit_cost'];
                        $supplyCreditMemo->pharmacy_supply_credit_memo_total_cost = $data['pharmacy_supply_credit_memo_unit_cost'] * $data['pharmacy_supply_credit_memo_qty'];
                        $supplyCreditMemo->pharmacy_supply_credit_memo_remarks = $data['pharmacy_supply_credit_memo_remarks'];
                        $supplyCreditMemo->pharmacy_supply_key = $data['pharmacy_supply_key'];
                        $supplyCreditMemo->pharmacy_supply_credit_memo_date = $data['pharmacy_supply_credit_memo_date'];
                        $supplyCreditMemo->supplier_key = $data['supplier_key'];

                            if($supplyCreditMemo->save()){
                                $supplyCreditMemo_id = $supplyCreditMemo->pharmacy_supply_credit_memo_key;
                                $pharmacySupplyKey = $data['pharmacy_supply_key'];
                                $itemTypeKey = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$pharmacySupplyKey)->value('pharmacy_item_type_key');
                                $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_current');
                                
                                $supplyTransaction = new PharmacySupplyTransaction();
                                $supplyTransactionType = "Credit Memo";
                                $supplyTransaction->pharmacy_supply_transaction_type_key = $supplyCreditMemo_id;
                                $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                                $supplyTransaction->pharmacy_supply_transaction_qty = $data['pharmacy_supply_credit_memo_qty'];
                                $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                                $supplyTransaction->pharmacy_supply_transaction_status = "Added";
                                
                                    if($supplyTransaction->save()){
                                        $balance = PharmacySupplyStockbalance::findOrFail($pharmacySupplyKey);
                                        $balance_current = $balance->pharmacy_supply_stockbalance_current;
                                        $balance->pharmacy_supply_stockbalance_current = $balance_current - $data['pharmacy_supply_credit_memo_qty'];
                                            if($balance->save()){
                                                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_current');
                                                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_reorder');
                                                    if($currentBalance<=$reOrderLevel){
                                                        $status = "Re-order";
                                                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)
                                                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                                    }
                                                    else{
                                                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)
                                                        ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                                                    }

                                                        $transaction = new SupplyTransaction;
                                                        $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                        $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                        $transaction->supply_transaction_date = $data['pharmacy_supply_credit_memo_date'];
                                                        $transaction->pharmacy_supply_credit_memo_qty = $data['pharmacy_supply_credit_memo_qty'];
                                                        $transaction->pharmacy_supply_transaction_id = $supplyCreditMemo_id;
                                                        $transaction->supply_transaction_type = "Credit Memo";
                                                        $transaction->save();
                                                    
                                                    return redirect('admin/pharmacy-supply-credit-memos')->with([
                                                        'message' => __('voyager::generic.successfully_added_new')." Credit Memo",
                                                        'alert-type' => 'success']);
                                            }
                                            else{
                                                return redirect('admin/pharmacy-supply-credit-memos')
                                                ->with([
                                                    'message' => "Stock balance not change.",
                                                    'alert-type' => 'error'
                                                    ]);
                                            }
                                    }  
                                    else{
                                        return redirect('admin/pharmacy-supply-credit-memos')
                                        ->with([
                                            'message' => "Transaction not save.",
                                            'alert-type' => 'error'
                                            ]);
                                    }
                            }
                            else{
                                return redirect('admin/pharmacy-supply-credit-memos/create')
                                ->with([
                                    'message' => "Error Saving",
                                    'alert-type' => 'error'
                                    ]);
                            }
                    }
                    catch(Exception $e){
                        return redirect('path')->with([
                            'message' => "Error Saving",
                            'alert-type' => 'error']);
                    }
            }
    }

    public function update(Request $request, $id){
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyCreditMemo = DB::table('pharmacy_supply_credit_memos')->where('pharmacy_supply_credit_memo_key', $id)->value('pharmacy_supply_credit_memo_qty');
        $supplyKey = DB::table('pharmacy_supply_credit_memos')->where('pharmacy_supply_credit_memo_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $retStockQUantity =  $supplyCreditMemo + $StockBalance;
        $newStockQuantity = $retStockQUantity - $request->input('pharmacy_supply_credit_memo_qty');

        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQuantity) );
        
        $supply_credit_memo = DB::table('pharmacy_supply_credit_memos')
            ->where('pharmacy_supply_credit_memo_key', $id)
            ->update(array('pharmacy_supply_credit_memo_qty'=> $request->input('pharmacy_supply_credit_memo_qty'),
                'pharmacy_supply_credit_memo_docno'=> $request->input('pharmacy_supply_credit_memo_docno'),
                'pharmacy_supply_credit_memo_doc_date'=> $request->input('pharmacy_supply_credit_memo_doc_date'),
                'pharmacy_supply_credit_memo_unit_cost'=> $request->input('pharmacy_supply_credit_memo_unit_cost'),
                'pharmacy_supply_credit_memo_total_cost'=> $request->input('pharmacy_supply_credit_memo_qty') * $request->input('pharmacy_supply_credit_memo_unit_cost'),
                'pharmacy_supply_credit_memo_remarks'=> $request->input('pharmacy_supply_credit_memo_remarks'),
                'pharmacy_supply_credit_memo_date' => $request->input('pharmacy_supply_credit_memo_date'),
                'supplier_key'=> $request->input('supplier_key')));

                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                    }

                        $supplyTransaction = new PharmacySupplyTransaction();
                        $supplyTransactionType = "Credit Memo";
                        $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                        $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                        $supplyTransaction->pharmacy_supply_transaction_qty = $request->input('pharmacy_supply_credit_memo_qty');
                        $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                        $supplyTransaction->pharmacy_supply_transaction_status = "Updated";
                        
                            if($supplyTransaction->save()){
                                $trasaction = DB::table('supply_transactions')
                                ->where('pharmacy_supply_transaction_id',$id)
                                ->where('supply_transaction_type',"Credit Memo")
                                    ->update(array('supply_transaction_date'=> $request->input('pharmacy_supply_credit_memo_date'),
                                        'pharmacy_supply_credit_memo_qty'=> $request->input('pharmacy_supply_credit_memo_qty')));
                            }  
                            else{
                                return redirect('admin/pharmacy-supply-credit-memos/create')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => "error"
                                ]);
                            } 
                    
                            return redirect('admin/pharmacy-supply-credit-memos')->with([
                                'message' => __('voyager::generic.successfully_updated')." Credit Memo",
                                'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyCreditMemoQty = DB::table('pharmacy_supply_credit_memos')->where('pharmacy_supply_credit_memo_key', $id)->value('pharmacy_supply_credit_memo_qty');
        $supplyKey = DB::table('pharmacy_supply_credit_memos')->where('pharmacy_supply_credit_memo_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $newStockQuantity =  $supplyCreditMemoQty + $StockBalance;

        $deleted_credit_memo = DB::table('pharmacy_supply_credit_memos')->where('pharmacy_supply_credit_memo_key', $id)->delete();

        $newStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQuantity) );
        
            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
            $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                if($currentBalance<=$reOrderLevel){
                    $status = "Re-order";
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                }
                else{
                    $status = "";
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                }

                    $supplyTransaction = new PharmacySupplyTransaction();
                    $supplyTransactionType = "Credit Memo";
                    $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                    $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                    $supplyTransaction->pharmacy_supply_transaction_qty = $supplyCreditMemoQty;
                    $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                    $supplyTransaction->pharmacy_supply_transaction_status = "Deleted";
                    $supplyTransaction->save();

                    return redirect('admin/pharmacy-supply-credit-memos')->with([
                        'message' => __('voyager::generic.successfully_deleted')." RMA",
                        'alert-type' => 'success']);
    }
}
