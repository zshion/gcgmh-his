<?php

namespace App\Http\Controllers;
use App\PharmacySupplyStockbalance;
use App\PharmacySupplyExpiredDamage;
use App\PharmacySupplyTransaction;
use App\SupplyTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PharmacySupplyExpiredDamageController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request){

        $rules = [
            'pharmacy_supply_key' => 'required',
            'pharmacy_supply_expired_damaged_qty'  => 'required',
            'pharmacy_supply_expired_damaged_date' => 'required',
            'pharmacy_supply_expired_damaged_remarks' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('insert')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                try{
                    $supplyKey = $data['pharmacy_supply_key'];
                    $itemTypeKey = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$supplyKey)->value('pharmacy_item_type_key');
                    $checkStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->doesntExist();
                        
                        if($checkStockBalance){
                            return redirect('admin/pharmacy-supply-expired-damages/create')
                                ->withInput()
                                ->withErrors([
                                    'message' => "No stock balance on the item."]);
                        }
                        else{
                            $supply_expired_damaged = new PharmacySupplyExpiredDamage();
                
                            $supply_expired_damaged->pharmacy_supply_expired_damaged_qty = $data['pharmacy_supply_expired_damaged_qty'];
                            $supply_expired_damaged->pharmacy_supply_expired_damaged_date = $data['pharmacy_supply_expired_damaged_date'];
                            $supply_expired_damaged->pharmacy_supply_expired_damaged_location = $data['pharmacy_supply_expired_damaged_location'];
                            $supply_expired_damaged->pharmacy_supply_expired_damaged_remarks = $data['pharmacy_supply_expired_damaged_remarks'];
                            $supply_expired_damaged->pharmacy_supply_expired_damaged_lot_no = $data['pharmacy_supply_expired_damaged_lot_no'];
                            $supply_expired_damaged->pharmacy_supply_expired_damaged_expiry = $data['pharmacy_supply_expired_damaged_expiry'];
                            $supply_expired_damaged->pharmacy_item_type_key = $itemTypeKey;
                            $supply_expired_damaged->pharmacy_supply_key = $data['pharmacy_supply_key'];
                            
                            $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                                if($supply_expired_damaged->save()){
                                    $supplyExpiredDamage_id = $supply_expired_damaged->pharmacy_supply_expired_damaged_key;

                                    $supplyTransaction = new PharmacySupplyTransaction();
                                    $supplyTransactionType = "Expired_Damaged";
                                    $supplyTransaction->pharmacy_supply_transaction_type_key = $supplyExpiredDamage_id;
                                    $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                                    $supplyTransaction->pharmacy_supply_transaction_qty = $data['pharmacy_supply_expired_damaged_qty'];
                                    $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                                    $supplyTransaction->pharmacy_supply_transaction_status = "Added";
                                    
                                        if($supplyTransaction->save()){
                                            $balance = PharmacySupplyStockbalance::findOrFail($supplyKey);
                                            $balance_current = $balance->pharmacy_supply_stockbalance_current;
                                            $balance->pharmacy_supply_stockbalance_current = $balance_current - $request->input('pharmacy_supply_expired_damaged_qty');

                                            if($balance->save()) {
                                                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                                                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                                                    if($currentBalance<=$reOrderLevel){
                                                        $status = "Re-order";
                                                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                                    }
                                                    else{
                                                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                                        ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                                                    }

                                                        if($data['pharmacy_supply_expired_damaged_remarks'] == 1){
                                                            $transaction = new SupplyTransaction;
                                                            $transaction->pharmacy_supply_key = $supplyKey;
                                                            $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                            $transaction->supply_transaction_date = $data['pharmacy_supply_expired_damaged_date'];
                                                            $transaction->pharmacy_supply_expired_qty = $data['pharmacy_supply_expired_damaged_qty'];
                                                            $transaction->pharmacy_supply_transaction_id = $supplyExpiredDamage_id;
                                                            $transaction->supply_transaction_type = "Expired_Damaged";
                                                            $transaction->save();
                                                        }
                                                        else{
                                                            $transaction = new SupplyTransaction;
                                                            $transaction->pharmacy_supply_key = $supplyKey;
                                                            $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                            $transaction->supply_transaction_date = $data['pharmacy_supply_expired_damaged_date'];
                                                            $transaction->pharmacy_supply_damage_qty = $data['pharmacy_supply_expired_damaged_qty'];
                                                            $transaction->pharmacy_supply_transaction_id = $supplyExpiredDamage_id;
                                                            $transaction->supply_transaction_type = "Expired_Damaged";
                                                            $transaction->save();
                                                        }
                                                        return redirect('admin/pharmacy-supply-expired-damages')->with([
                                                            'message' => __('voyager::generic.successfully_added_new')." Expired/Damaged Supply",
                                                            'alert-type' => 'success']);
                                            }
                                            else{
                                                return redirect('admin/pharmacy-supply-expired-damages')->with([
                                                    'message' => "Stock balance not change.",
                                                    'alert-type' => 'error']);
                                            } 
                                        }
                                        else{
                                            return redirect('admin/pharmacy-supply-expired-damages')->with([
                                                'message' => "Supply transaction not save.",
                                                'alert-type' => 'error']);
                                        }
                                }
                                else{
                                    return redirect('admin/pharmacy-supply-expired-damages/create')->with([
                                        'message' => "Error Saving",
                                        'alert-type' => 'error']);
                                }
                        }
                }
                catch(Exception $e){
                    return redirect('path')->with([
                        'message' => "Error Saving",
                        'alert-type' => 'error']);
                }                    
            } 
    }

    public function update(Request $request, $id){
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyExpiredamage = DB::table('pharmacy_supply_expired_damages')->where('pharmacy_supply_expired_damaged_key', $id)->value('pharmacy_supply_expired_damaged_qty');
        $supplyKey = DB::table('pharmacy_supply_expired_damages')->where('pharmacy_supply_expired_damaged_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $retStockQUantity =  $supplyExpiredamage + $StockBalance ;
        $newStockQuantity = $retStockQUantity - $request->input('pharmacy_supply_expired_damaged_qty');

        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQuantity) );
        
        $supplyExpiredDamaged = DB::table('pharmacy_supply_expired_damages')
            ->where('pharmacy_supply_expired_damaged_key', $id)
            ->update(array('pharmacy_supply_expired_damaged_qty'=> $request->input('pharmacy_supply_expired_damaged_qty'),
                'pharmacy_supply_expired_damaged_date'=> $request->input('pharmacy_supply_expired_damaged_date'),
                'pharmacy_supply_expired_damaged_remarks'=>$request->input('pharmacy_supply_expired_damaged_remarks'),
                'pharmacy_supply_expired_damaged_lot_no'=>$request->input('pharmacy_supply_expired_damaged_lot_no'),
                'pharmacy_supply_expired_damaged_expiry'=>$request->input('pharmacy_supply_expired_damaged_expiry'),
                'pharmacy_supply_expired_damaged_location'=> $request->input('pharmacy_supply_expired_damaged_location')));
        
                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                    }

                    $supplyTransaction = new PharmacySupplyTransaction();
                    $supplyTransactionType = "Expired_Damaged";
                    $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                    $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                    $supplyTransaction->pharmacy_supply_transaction_qty = $request->input('pharmacy_supply_expired_damaged_qty');
                    $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                    $supplyTransaction->pharmacy_supply_transaction_status = "Updated";
                    
                        if($supplyTransaction->save()){
                            if($request->input('pharmacy_supply_expired_damaged_remarks') == 1){
                                $transaction = DB::table('supply_transactions')
                                    ->where('pharmacy_supply_transaction_id', $id)
                                    ->where('supply_transaction_type', "Expired_Damaged")
                                     ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_expired_damaged_date'),
                                        'pharmacy_supply_expired_qty'=> $request->input('pharmacy_supply_expired_damaged_qty'),
                                        'pharmacy_supply_damage_qty'=> 0));
                            }
                            else{
                                $transaction = DB::table('supply_transactions')
                                    ->where('pharmacy_supply_transaction_id', $id)
                                    ->where('supply_transaction_type', "Expired_Damaged")
                                     ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_expired_damaged_date'),
                                        'pharmacy_supply_expired_qty'=> 0,
                                        'pharmacy_supply_damage_qty'=> $request->input('pharmacy_supply_expired_damaged_qty')));
                            }
                            
                        }
                        else{
                            return redirect('admin/pharmacy-supply-expired-damages')->with([
                                'message' => "Transaction not save.",
                                'alert-type' => 'error']); 
                        }
        
                        return redirect('admin/pharmacy-supply-expired-damages')->with([
                            'message' => __('voyager::generic.successfully_updated')." Expired/Damaged Supply",
                            'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyExpiredamageQty = DB::table('pharmacy_supply_expired_damages')->where('pharmacy_supply_expired_damaged_key', $id)->value('pharmacy_supply_expired_damaged_qty');
        $supplyKey = DB::table('pharmacy_supply_expired_damages')->where('pharmacy_supply_expired_damaged_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $newtStockQUantity =  $supplyExpiredamageQty + $StockBalance;

        $deleted_delivery = DB::table('pharmacy_supply_expired_damages')->where('pharmacy_supply_expired_damaged_key', $id)->delete();
        
        $newSupplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
        ->update(array('pharmacy_supply_stockbalance_current'=>$newtStockQUantity) );

            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }

                    $supplyTransaction = new PharmacySupplyTransaction();
                    $supplyTransactionType = "Expired/Damaged";
                    $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                    $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                    $supplyTransaction->pharmacy_supply_transaction_qty = $supplyExpiredamageQty;
                    $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                    $supplyTransaction->pharmacy_supply_transaction_status = "Deleted";
                    $supplyTransaction->save();

                return redirect('admin/pharmacy-supply-expired-damages')->with([
                    'message' => __('voyager::generic.successfully_deleted')." Expired/Damaged Supply",
                    'alert-type' => 'success']);
    }

}