<?php

namespace App\Http\Controllers;

use App\PharmacySupplyDelivery;
use App\PharmacySupplyStockbalance;
use App\PharmacySupplyTransaction;
use App\SupplyTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PharmacySupplyDeliveryController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_supply_key' => 'required',
            'pharmacy_supply_delivery_qty'  => 'required',
            'pharmacy_supply_delivery_date' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/pharmacy-supply-deliveries/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                    try{
                        $pharmacySupplyKey = $data['pharmacy_supply_key'];
                        $itemTypeKey = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$pharmacySupplyKey)->value('pharmacy_item_type_key');
                        $checkStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->doesntExist();
                            
                            if ($checkStockBalance()) {
                                return redirect('admin/pharmacy-supply-deliveries/create')
                                    ->withInput()
                                    ->withErrors([
                                        'message' => "Item not found on stockbalance."]);
                            }
                            else{
                                $supplyDelivery = new PharmacySupplyDelivery();
                
                                $supplyDelivery->pharmacy_supply_delivery_iar = $data['pharmacy_supply_delivery_iar'];
                                $supplyDelivery->pharmacy_supply_delivery_qty = $data['pharmacy_supply_delivery_qty'];
                                $supplyDelivery->pharmacy_supply_delivery_unit_cost = $data['pharmacy_supply_delivery_unit_cost'];
                                $supplyDelivery->pharmacy_supply_delivery_total_cost = $data['pharmacy_supply_delivery_unit_cost'] * $data['pharmacy_supply_delivery_qty'];
                                $supplyDelivery->pharmacy_supply_delivery_expiry_date = $data['pharmacy_supply_delivery_expiry_date'];
                                $supplyDelivery->pharmacy_supply_delivery_lotno = $data['pharmacy_supply_delivery_lotno'];
                                $supplyDelivery->pharmacy_supply_delivery_guarantee = $data['pharmacy_supply_delivery_guarantee'];
                                $supplyDelivery->pharmacy_supply_delivery_location = $data['pharmacy_supply_delivery_location'];
                                $supplyDelivery->pharmacy_supply_delivery_remarks = $data['pharmacy_supply_delivery_remarks'];
                                $supplyDelivery->pharmacy_supply_delivery_po_mode = $data['pharmacy_supply_delivery_po_mode'];
                                $supplyDelivery->pharmacy_supply_delivery_iar_date = $data['pharmacy_supply_delivery_iar_date'];
                                $supplyDelivery->pharmacy_supply_delivery_po_no = $data['pharmacy_supply_delivery_po_no'];
                                $supplyDelivery->pharmacy_supply_delivery_po_date = $data['pharmacy_supply_delivery_po_date'];
                                $supplyDelivery->pharmacy_supply_delivery_brand_name = $data['pharmacy_supply_delivery_brand_name'];
                                $supplyDelivery->pharmacy_supply_delivery_batch_no = $data['pharmacy_supply_delivery_batch_no'];
                                $supplyDelivery->pharmacy_supply_key = $data['pharmacy_supply_key'];
                                $supplyDelivery->supplier_key = $data['supplier_key'];
                                $supplyDelivery->pharmacy_item_type_key = $itemTypeKey;
                                $supplyDelivery->pharmacy_supply_delivery_date = $data['pharmacy_supply_delivery_date'];
                                
                                $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_current');

                                    if($supplyDelivery->save()){
                                        $pharmacySupplyDelivery_id = $supplyDelivery->pharmacy_supply_delivery_key;

                                        $supplyTransaction = new PharmacySupplyTransaction();
                                        $supplyTransactionType = "Delivery";
                                        $supplyTransaction->pharmacy_supply_transaction_type_key = $pharmacySupplyDelivery_id;
                                        $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                                        $supplyTransaction->pharmacy_supply_transaction_qty = $data['pharmacy_supply_delivery_qty'];
                                        $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                                        $supplyTransaction->pharmacy_supply_transaction_status = "Added";
                                        
                                            if($supplyTransaction->save()){
                                                $balance = PharmacySupplyStockbalance::findOrFail($pharmacySupplyKey);
                                                $balance_current = $balance->pharmacy_supply_stockbalance_current;
                                                $balance->pharmacy_supply_stockbalance_current = $balance_current + $data['pharmacy_supply_delivery_qty'];
                                                    
                                                    if($balance->save()){
                                                            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_current');
                                                            $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_reorder');
                                                                if($currentBalance<=$reOrderLevel){
                                                                    $status = "Re-order";
                                                                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)
                                                                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                                                }
                                                                else{
                                                                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)
                                                                    ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                                                                }
                                                                    $transaction = new SupplyTransaction;
                                                                    $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                    $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                    $transaction->supply_transaction_date = $data['pharmacy_supply_delivery_date'];
                                                                    $transaction->pharmacy_supply_delivery_qty = $data['pharmacy_supply_delivery_qty'];
                                                                    $transaction->pharmacy_supply_transaction_id = $pharmacySupplyDelivery_id;
                                                                    $transaction->supply_transaction_type = "Delivery";
                                                                    $transaction->save();

                                                                    return redirect('admin/pharmacy-supply-deliveries')->with([
                                                                    'message' => __('voyager::generic.successfully_added_new')." Delivery",
                                                                    'alert-type' => 'success']); 
                                                    }
                                                    else{
                                                        return redirect('admin/pharmacy-supply-deliveries')->with([
                                                            'message' => "Stock balance not change",
                                                            'alert-type' => 'error']);
                                                    }
                                            }
                                            else{
                                                return redirect('admin/pharmacy-supply-deliveries')->with([
                                                    'message' => "Supply transaction not save.",
                                                    'alert-type' => 'error']);
                                            } 
                                    }
                                    else{
                                        return redirect('admin/pharmacy-supply-deliveries/create')->with([
                                            'message' => "Error Saving",
                                            'alert-type' => 'error']);
                                    } 
                                                
                            }  
                    }
                    catch(Exception $e){
                        return redirect('path')->with([
                            'message' => "Error Saving",
                            'alert-type' => 'error']);
                    }
            }
    }

    public function update(Request $request, $id){
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyDelivery = DB::table('pharmacy_supply_deliveries')->where('pharmacy_supply_delivery_key', $id)->value('pharmacy_supply_delivery_qty');
        $supplyKey = DB::table('pharmacy_supply_deliveries')->where('pharmacy_supply_delivery_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $retStockQUantity = $StockBalance - $supplyDelivery ;
        $newStockQuantity = $retStockQUantity + $request->input('pharmacy_supply_delivery_qty');

        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQuantity) );

            $supply_delivery = DB::table('pharmacy_supply_deliveries')
                ->where('pharmacy_supply_delivery_key', $id)
                ->update(array('pharmacy_supply_delivery_iar'=> $request->input('pharmacy_supply_delivery_iar'),
                    'pharmacy_supply_delivery_qty'=> $request->input('pharmacy_supply_delivery_qty'),
                    'pharmacy_supply_delivery_unit_cost'=> $request->input('pharmacy_supply_delivery_unit_cost'),
                    'pharmacy_supply_delivery_total_cost'=> $request->input('pharmacy_supply_delivery_qty') * $request->input('pharmacy_supply_delivery_unit_cost'),
                    'pharmacy_supply_delivery_expiry_date'=> $request->input('pharmacy_supply_delivery_expiry_date'),
                    'pharmacy_supply_delivery_lotno'=> $request->input('pharmacy_supply_delivery_lotno'),
                    'pharmacy_supply_delivery_guarantee'=> $request->input('pharmacy_supply_delivery_guarantee'),
                    'pharmacy_supply_delivery_location'=> $request->input('pharmacy_supply_delivery_location'),
                    'pharmacy_supply_delivery_remarks'=> $request->input('pharmacy_supply_delivery_remarks'),
                    'pharmacy_supply_delivery_po_mode' => $request->input('pharmacy_supply_delivery_po_mode'),
                    'pharmacy_supply_delivery_iar_date' => $request->input('pharmacy_supply_delivery_iar_date'),
                    'pharmacy_supply_delivery_po_no' => $request->input('pharmacy_supply_delivery_po_no'),
                    'pharmacy_supply_delivery_po_date' => $request->input('pharmacy_supply_delivery_po_date'),
                    'pharmacy_supply_delivery_brand_name' => $request->input('pharmacy_supply_delivery_brand_name'),
                    'pharmacy_supply_delivery_batch_no' => $request->input('pharmacy_supply_delivery_batch_no'),
                    'pharmacy_supply_delivery_date'=> $request->input('pharmacy_supply_delivery_date'),
                    'supplier_key'=> $request->input('supplier_key')));
            
                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                            ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                            ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                    }
                        $supplyTransaction = new PharmacySupplyTransaction();
                        $supplyTransactionType = "Delivery";
                        $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                        $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                        $supplyTransaction->pharmacy_supply_transaction_qty = $request->input('pharmacy_supply_delivery_qty');
                        $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                        $supplyTransaction->pharmacy_supply_transaction_status = "Updated";

                            if($supplyTransaction->save()){
                                $trasaction = DB::table('supply_transactions')
                                ->where('pharmacy_supply_transaction_id',$id)
                                ->where('supply_transaction_type',"Delivery")
                                    ->update(array('supply_transaction_date'=> $request->input('pharmacy_supply_delivery_date'),
                                        'pharmacy_supply_delivery_qty'=> $request->input('pharmacy_supply_delivery_qty')));
                            }
                            else{
                                return redirect('admin/pharmacy-supply-deliveries')
                                    ->with([
                                        'message' => "Transaction not save.",
                                        'alert-type' => "error"
                                    ]);
                            }

                    return redirect('admin/pharmacy-supply-deliveries')->with([
                        'message' => __('voyager::generic.successfully_updated')." Delivery",
                        'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyDeliveryQty = DB::table('pharmacy_supply_deliveries')->where('pharmacy_supply_delivery_key', $id)->value('pharmacy_supply_delivery_qty');
        $supplyKey = DB::table('pharmacy_supply_deliveries')->where('pharmacy_supply_delivery_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $newStockQUantity = $StockBalance - $supplyDeliveryQty;

        $deleted_credit_note = DB::table('pharmacy_supply_deliveries')->where('pharmacy_supply_delivery_key', $id)->delete();

        $newsupplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
        ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQUantity) );        

            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                            ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                            ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                        $supplyTransaction = new PharmacySupplyTransaction();
                        $supplyTransactionType = "Delivery";
                        $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                        $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                        $supplyTransaction->pharmacy_supply_transaction_qty = $supplyDeliveryQty;
                        $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                        $supplyTransaction->pharmacy_supply_transaction_status = "Deleted";
                        $supplyTransaction->save();

        return redirect('admin/pharmacy-supply-deliveries')->with([
            'message' => __('voyager::generic.successfully_deleted')." Delivery",
            'alert-type' => 'success']);
    }
}
