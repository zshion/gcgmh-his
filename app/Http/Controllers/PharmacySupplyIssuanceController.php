<?php

namespace App\Http\Controllers;
use App\PharmacySupplyIssuance;
use App\PharmacySupplyStockbalance;
use App\PharmacySupplyTransaction;
use App\Patient;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PharmacySupplyIssuanceController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request){
        
        $rules = [
            'pharmacy_supply_key' => 'required',
            'pharmacy_supply_issuance_qty'  => 'required',
            'patient_hospital_no' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
         if ($validator->fails()) {
             return redirect('admin/pharmacy-supply-issuances/create')
                 ->withInput()
                 ->withErrors($validator);
         }
         else{
            $data = $request->input();
            try{
                         /*   $connect = odbc_connect("MSSQLSERVER", "sa", "p@ssw0rd");
                            if( $connect === false ) {
                                return redirect('admin/pharmacy-supply-issuances/create')
                                ->withInput()
                                ->withErrors([
                                    'message' => "Connection to iHOMIS Failed"]);
                            }
                                
                            $sql = "SELECT hpercode, patlast, patfirst, patmiddle, patbdate FROM dbo.hperson WHERE hpercode = '$hospID' ";
                            $results = odbc_exec($connect, $sql);
                                if($results===false){
                                    echo "Query Failed" .odbc_error();
                                }
                                if(odbc_fetch_row($results) === false){
                                    return redirect('admin/pharmacy-supply-issuances/create')
                                    ->withInput()
                                    ->withErrors([
                                        'message' => "Hospital number not found."]);
                                }
                                    while( $row = odbc_fetch_array($results) ) {           
                                        $HospitalID = $row['hpercode'];
                                        $firstname = $row['patfirst'];
                                        $middlename = $row['patmiddle'];
                                        $lastname = $row['patlast'];
                                        $birthDate = $row['patbdate'];
                                    }*/


                $serverName = "DESKTOP-1VHQV8I";
                $connectionInfo = array("Database"=>"demo5", "UID"=>"sa", "PWD"=>"123@gatekeeper");
                //$serverName = "192.168.7.1";
                //$connectionInfo = array("Database"=>"GCGMH_dbo", "UID"=>"sa", "PWD"=>"p@ssw0rd");
                $conn = sqlsrv_connect( $serverName, $connectionInfo );
                    if( $conn === false ) {
                       // die( print_r( sqlsrv_errors(), true));
                       return redirect('admin/pharmacy-supply-issuances/create')
                        ->withInput()
                        ->withErrors([
                            'message' => "Connection to iHOMIS Failed"]);
                    }
                $hospID = $data['patient_hospital_no'];

                $sql = "SELECT hpercode, patlast, patfirst, patmiddle, patbdate FROM dbo.hperson WHERE hpercode = '$hospID' ";
                $stmt = sqlsrv_query( $conn, $sql );
                    if( $stmt === false) {
                        die( print_r( sqlsrv_errors(), true) );
                    }
                    if(sqlsrv_has_rows($stmt)){
                        while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
                            $HospitalID = $row['hpercode'];
                            $firstname = $row['patfirst'];
                            $middlename = $row['patmiddle'];
                            $lastname = $row['patlast'];
                            $birthDate = $row['patbdate'];
                        }
                    }
                    else{
                        return redirect('admin/pharmacy-supply-issuances/create')
                        ->withInput()
                        ->withErrors([
                            'message' => "Hospital number not found."]);
                    }   


                        sqlsrv_free_stmt( $stmt);
               
                $checkPatient = DB::table('patients')->where('patient_hospital_no', $hospID)->doesntExist();

                if($checkPatient){
                    $newPatient = new Patient();
                    $newPatient->patient_hospital_no = $HospitalID ;
                    $newPatient->patient_last_name = $lastname;
                    $newPatient->patient_first_name = $firstname;
                    $newPatient->patient_middle_name = $middlename;
                    $newPatient->patient_birth_date = $birthDate;
                    $newPatient->save();
                }
                $supplyKey = $data['pharmacy_supply_key'];
                $itemTypeKey = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$supplyKey)->value('pharmacy_item_type_key');

                    $checkStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->doesntExist();   
                    
                    if($checkStockBalance){
                        return redirect('admin/pharmacy-supply-issuances/create')
                            ->withInput()
                            ->withErrors([
                                'message' => "No stock balance on the item."]);
                    }
                    else{
                        $supplyIssuance = new PharmacySupplyIssuance();
    
                        $supplyIssuance->pharmacy_supply_issuance_charge_slip = $data['pharmacy_supply_issuance_charge_slip'];
                        $supplyIssuance->pharmacy_supply_issuance_qty = $data['pharmacy_supply_issuance_qty'];
                        $supplyIssuance->pharmacy_supply_issuance_remarks = $data['pharmacy_supply_issuance_remarks'];
                        $supplyIssuance->pharmacy_supply_issuance_date = $data['pharmacy_supply_issuance_date'];
                        $supplyIssuance->employee_name = $data['employee_name'];
                        $supplyIssuance->patient_hospital_no = $data['patient_hospital_no'];
                        $supplyIssuance->pharmacy_supply_key = $data['pharmacy_supply_key'];
                        $supplyIssuance->pharmacy_item_type_key = $itemTypeKey;

                        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                        
                        if($supplyIssuance->save()){
                            $supplyIssuance_id = $supplyIssuance->pharmacy_supply_issuance_key;

                            $supplyTransaction = new PharmacySupplyTransaction();
                            $supplyTransactionType = "Issuance";
                            $supplyTransaction->pharmacy_supply_transaction_type_key = $supplyIssuance_id;
                            $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                            $supplyTransaction->pharmacy_supply_transaction_qty = $data['pharmacy_supply_issuance_qty'];
                            $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                            $supplyTransaction->pharmacy_supply_transaction_status = "Added";
                            $supplyTransaction->save();
                            try{
                            
                                $balance = PharmacySupplyStockbalance::findOrFail($supplyKey);
                                $balance_current = $balance->pharmacy_supply_stockbalance_current;
                                $balance->pharmacy_supply_stockbalance_current = $balance_current - $data['pharmacy_supply_issuance_qty'];;
                            
                                if($balance->save()) {
                                    $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                                    $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                                        if($currentBalance<=$reOrderLevel){
                                            $status = "Re-order";
                                            $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                            ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                        }
                                        else{
                                            $status = "";
                                            $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                            ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                        }

                                    return redirect('admin/pharmacy-supply-issuances')->with([
                                        'message' => __('voyager::generic.successfully_added_new')." Issuance",
                                        'alert-type' => 'success']);
                                }
                            }
                            catch(Exception $e){
                                return redirect('path')->with([
                                    'message' => "Error Saving",
                                    'alert-type' => 'error']);
                            }
                        }
                    }      
            }
            catch(Exception $e){
                return redirect('path')->with([
                    'message' => "Error Saving",
                    'alert-type' => 'error']);
             }
           
         }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyIssuance = DB::table('pharmacy_supply_issuances')->where('pharmacy_supply_issuance_key', $id)->value('pharmacy_supply_issuance_qty');
        $supplyKey = DB::table('pharmacy_supply_issuances')->where('pharmacy_supply_issuance_key', $id)->value('pharmacy_supply_key');
        
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $retStockQUantity = $StockBalance + $supplyIssuance ;
        $newStockQuantity = $retStockQUantity - $request->input('pharmacy_supply_issuance_qty');
    
        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQuantity) );

        $supplyIssuance = DB::table('pharmacy_supply_issuances')
            ->where('pharmacy_supply_issuance_key', $id)
            ->update(array('pharmacy_supply_issuance_charge_slip'=> $request->input('pharmacy_supply_issuance_charge_slip'),
                'pharmacy_supply_issuance_qty'=> $request->input('pharmacy_supply_issuance_qty'),
                'pharmacy_supply_issuance_remarks'=> $request->input('pharmacy_supply_issuance_remarks'),
                'pharmacy_supply_issuance_date'=> $request->input('pharmacy_supply_issuance_date'),
                'employee_name'=> $request->input('employee_name'),
                'patient_hospital_no'=> $request->input('patient_hospital_no'),
                'pharmacy_item_type_key'=> $request->input('pharmacy_item_type_key'),
                'pharmacy_supply_key'=> $request->input('pharmacy_supply_key')));

                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }

            $supplyTransaction = new PharmacySupplyTransaction();
            $supplyTransactionType = "Issuance";
            $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
            $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
            $supplyTransaction->pharmacy_supply_transaction_qty = $request->input('pharmacy_supply_issuance_qty');
            $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
            $supplyTransaction->pharmacy_supply_transaction_status = "Updated";
            $supplyTransaction->save();

                return redirect('admin/pharmacy-supply-issuances')->with([
                    'message' => __('voyager::generic.successfully_updated')." Issuance",
                    'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyIssuanceQty = DB::table('pharmacy_supply_issuances')->where('pharmacy_supply_issuance_key', $id)->value('pharmacy_supply_issuance_qty');
        $supplyKey = DB::table('pharmacy_supply_issuances')->where('pharmacy_supply_issuance_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $newStockQUantity = $StockBalance + $supplyIssuanceQty;

        $deleted_issuance = DB::table('pharmacy_supply_issuances')->where('pharmacy_supply_issuance_key', $id)->delete();

        $newSupplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQUantity) );
        
            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
            $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                if($currentBalance<=$reOrderLevel){
                    $status = "Re-order";
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                }
                else{
                    $status = "";
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                }

                $supplyTransaction = new PharmacySupplyTransaction();
                $supplyTransactionType = "Issuance";
                $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                $supplyTransaction->pharmacy_supply_transaction_qty = $supplyIssuanceQty;
                $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                $supplyTransaction->pharmacy_supply_transaction_status = "Deleted";
                $supplyTransaction->save();

        return redirect('admin/pharmacy-supply-issuances')->with([
            'message' => __('voyager::generic.successfully_deleted')." Issuance",
            'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/pharmacy-supply-issuances')->with([
            'message' => __('voyager::generic.successfully_restored')." Issuance",
            'alert-type' => 'success']);
    }
}