<?php

namespace App\Http\Controllers;
use App\MedicineDelivery;
use App\MedicineStockbalance;
use App\MedicineTransaction;
use App\PharmacyMedicineTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\DataTables\MedicineDeliveryReportsDataTable;


class MedicineDeliveryController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request){
        $rules = [
            'pharmacy_medicine_key' => 'required',
            'medicine_delivery_qty'  => 'required', 
            'medicine_delivery_date' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/medicine-deliveries/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                    try{
                        $pharmacyMedicineKey = $data['pharmacy_medicine_key'];
                        $itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');
                        $checkStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->doesntExist();

                            if($checkStockBalance){
                                return redirect('admin/medicine-deliveries/create')
                                    ->withInput()
                                    ->withErrors([
                                        'message' => "Item not found on stockbalance."]);
                            }
                            else{
                                $medicineDelivery = new MedicineDelivery();
                
                                $medicineDelivery->medicine_delivery_iar = $data['medicine_delivery_iar'];
                                $medicineDelivery->medicine_delivery_qty = $data['medicine_delivery_qty'];
                                $medicineDelivery->medicine_delivery_unit_cost = $data['medicine_delivery_unit_cost'];
                                $medicineDelivery->medicine_delivery_total_cost = $data['medicine_delivery_unit_cost'] * $data['medicine_delivery_qty'];
                                $medicineDelivery->medicine_delivery_expiry_date = $data['medicine_delivery_expiry_date'];
                                $medicineDelivery->medicine_delivery_lotno = $data['medicine_delivery_lotno'];
                                $medicineDelivery->medicine_delivery_guarantee = $data['medicine_delivery_guarantee'];
                                $medicineDelivery->medicine_delivery_location = $data['medicine_delivery_location'];
                                $medicineDelivery->medicine_delivery_remarks = $data['medicine_delivery_remarks'];
                                $medicineDelivery->medicine_delivery_po_mode = $data['medicine_delivery_po_mode'];
                                $medicineDelivery->medicine_delivery_iar_date = $data['medicine_delivery_iar_date'];
                                $medicineDelivery->medicine_delivery_po_no = $data['medicine_delivery_po_no'];
                                $medicineDelivery->medicine_delivery_po_date = $data['medicine_delivery_po_date'];
                                $medicineDelivery->medicine_delivery_brand_name = $data['medicine_delivery_brand_name'];
                                $medicineDelivery->medicine_delivery_batch_no = $data['medicine_delivery_batch_no'];
                                $medicineDelivery->pharmacy_medicine_key = $data['pharmacy_medicine_key'];
                                $medicineDelivery->supplier_key = $data['supplier_key'];
                                $medicineDelivery->pharmacy_item_type_key = $itemTypeKey;
                                $medicineDelivery->medicine_delivery_date = $data['medicine_delivery_date'];
                                /* if($data['medicine_delivery_date'] == null){
                                        $medicineDelivery->medicine_delivery_date = date("Y-m-d");
                                    }
                                    else{
                                        $medicineDelivery->medicine_delivery_date = $data['medicine_delivery_date'];
                                    } */

                                $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');
                                
                                    if($medicineDelivery->save()){
                                        $medicineDelivery_id = $medicineDelivery->medicine_delivery_key;

                                        $medicineTransaction = new MedicineTransaction();
                                        $medicineTransactionType = "Delivery";
                                        $medicineTransaction->medicine_transaction_type_key = $medicineDelivery_id;
                                        $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                                        $medicineTransaction->medicine_transaction_qty = $data['medicine_delivery_qty'];
                                        $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                        $medicineTransaction->medicine_transaction_status = "Added";

                                            if( $medicineTransaction->save()){
                                                $balance = MedicineStockbalance::where('pharmacy_medicine_key','=',$pharmacyMedicineKey)->firstOrFail();
                                                $balance_current = $balance->medicine_stockbalance_current;
                                                $balance->medicine_stockbalance_current = $balance_current + $data['medicine_delivery_qty'];
                                            
                                                    if($balance->save()) {
                                                        $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');
                                                        $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_reorder');
                                                            if($currentBalance<=$reOrderLevel){
                                                                $status = "Re-order";
                                                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                                ->update(array('medicine_stockbalance_status'=>$status) );
                                                            }
                                                            else{
                                                                $status = "";
                                                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                                ->update(array('medicine_stockbalance_status'=>$status) );
                                                            }

                                                                $transaction = new PharmacyMedicineTransaction;
                                                                $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                $transaction->pharmacy_medicine_transaction_date = $data['medicine_delivery_date'];
                                                                $transaction->medicine_delivery_qty = $data['medicine_delivery_qty'];
                                                                $transaction->pharmacy_transaction_id = $medicineDelivery_id;
                                                                $transaction->pharmacy_transaction_type = "Delivery";
                                                                $transaction->save();

                                                                    return redirect('admin/medicine-deliveries')->with([
                                                                        'message' => __('voyager::generic.successfully_added_new')." Delivery",
                                                                        'alert-type' => 'success']);
                                                    }
                                                    else{
                                                        return redirect('admin/medicine-deliveries')->with([
                                                            'message' => "Stock balance not change",
                                                            'alert-type' => 'error']);
                                                    }
                                            }
                                            else{
                                                return redirect('admin/medicine-deliveries')->with([
                                                    'message' => "Medicine transaction not save.",
                                                    'alert-type' => 'error']);
                                            }             
                                    }
                                    else{
                                        return redirect('admin/medicine-deliveries/create')->with([
                                            'message' => "Error Saving",
                                            'alert-type' => 'error']);
                                    }
                            }
                        
                    }
                    catch(Exception $e){
                        return redirect('path')->with([
                            'message' => "Error Saving",
                            'alert-type' => 'error']);
                    }
            }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;
        //$pharmacyMedicineKey = $request->input('pharmacy_medicine_key');
        //$itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');

                $medicineDelivery = DB::table('medicine_deliveries')->where('medicine_delivery_key', $id)->value('medicine_delivery_qty');
                $medicineKey = DB::table('medicine_deliveries')->where('medicine_delivery_key', $id)->value('pharmacy_medicine_key');
                $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

                $retStockQUantity = $StockBalance - $medicineDelivery ;
                $newStockQuantity = $retStockQUantity + $request->input('medicine_delivery_qty');
            
                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_current'=>$newStockQuantity) );

                $medicine_delivery = DB::table('medicine_deliveries')
                    ->where('medicine_delivery_key', $id)
                    ->update(array('medicine_delivery_iar'=> $request->input('medicine_delivery_iar'),
                        'medicine_delivery_qty'=> $request->input('medicine_delivery_qty'),
                        'medicine_delivery_unit_cost'=> $request->input('medicine_delivery_unit_cost'),
                        'medicine_delivery_total_cost'=> $request->input('medicine_delivery_qty') * $request->input('medicine_delivery_unit_cost'),
                        'medicine_delivery_expiry_date'=> $request->input('medicine_delivery_expiry_date'),
                        'medicine_delivery_lotno'=> $request->input('medicine_delivery_lotno'),
                        'medicine_delivery_guarantee'=> $request->input('medicine_delivery_guarantee'),
                        'medicine_delivery_location'=> $request->input('medicine_delivery_location'),
                        'medicine_delivery_remarks'=> $request->input('medicine_delivery_remarks'),
                        'medicine_delivery_po_mode' => $request->input('medicine_delivery_po_mode'),
                        'medicine_delivery_iar_date' => $request->input('medicine_delivery_iar_date'),
                        'medicine_delivery_po_no' => $request->input('medicine_delivery_po_no'),
                        'medicine_delivery_po_date' => $request->input('medicine_delivery_po_date'),
                        'medicine_delivery_brand_name' => $request->input('medicine_delivery_brand_name'),
                        'medicine_delivery_batch_no' => $request->input('medicine_delivery_batch_no'),
                        'medicine_delivery_date'=> $request->input('medicine_delivery_date'),
                        'supplier_key'=> $request->input('supplier_key')));
              
                        $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                        $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                            if($currentBalance<=$reOrderLevel){
                                $status = "Re-order";
                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                ->update(array('medicine_stockbalance_status'=>$status) );
                            }
                            else{
                                $status = "";
                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                ->update(array('medicine_stockbalance_status'=>$status) );
                            }

                                $medicineTransaction = new MedicineTransaction();
                                $medicineTransactionType = "Delivery";
                                $medicineTransaction->medicine_transaction_type_key = $id;
                                $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                                $medicineTransaction->medicine_transaction_qty = $request->input('medicine_delivery_qty');
                                $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                $medicineTransaction->medicine_transaction_status = "Updated";

                                    if($medicineTransaction->save()){
                                        $transaction = DB::table('pharmacy_medicine_transactions')
                                        ->where('pharmacy_transaction_id', $id)
                                        ->where('pharmacy_transaction_type', "Delivery")
                                            ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_delivery_date'),
                                            'medicine_delivery_qty'=> $request->input('medicine_delivery_qty') ));
                                    }
                                    else{
                                        return redirect('admin/medicine-deliveries')->with([
                                            'message' => "Transaction not save.",
                                            'alert-type' => 'error']); 
                                    }

                                return redirect('admin/medicine-deliveries')->with([
                                    'message' => __('voyager::generic.successfully_updated')." Delivery",
                                    'alert-type' => 'success']);            
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;
 
        $medicineDeliveryQty = DB::table('medicine_deliveries')->where('medicine_delivery_key', $id)->value('medicine_delivery_qty');
        $medicineKey = DB::table('medicine_deliveries')->where('medicine_delivery_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $newStockQUantity = $StockBalance - $medicineDeliveryQty ;

        $deleted_delivery = DB::table('medicine_deliveries')->where('medicine_delivery_key', $id)->delete();

        $newMedicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQUantity) );

            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }

                        $medicineTransaction = new MedicineTransaction();
                        $medicineTransactionType = "Delivery";
                        $medicineTransaction->medicine_transaction_type_key = $id;
                        $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                        $medicineTransaction->medicine_transaction_qty = $medicineDeliveryQty;
                        $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                        $medicineTransaction->medicine_transaction_status = "Deleted";

                            if($medicineTransaction->save()){
                                $deleted_transaction = DB::table('pharmacy_medicine_transactions')
                                ->where('pharmacy_transaction_id', $id)
                                ->where('pharmacy_transaction_type', "Delivery")
                                ->delete();
                            }
                            else{
                                return redirect('admin/medicine-deliveries')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => 'error']); 
                            }

                            return redirect('admin/medicine-deliveries')->with([
                                'message' => __('voyager::generic.successfully_deleted')." Delivery",
                                'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/medicine-deliveries')->with([
            'message' => __('voyager::generic.successfully_restored')." Delivery",
            'alert-type' => 'success']);
    }

    public function export()
    {
        return view('/vendor/voyager/medicine-deliveries/export');
    }

    public function get_export(){

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $deliveries = \Illuminate\Support\Facades\DB::table('medicine_deliveries as delivery')
            ->select('*')

            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('delivery.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('delivery.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            })
            ->leftjoin('suppliers', function($join) {
                $join->on('delivery.supplier_key', '=', 'suppliers.supplier_key');
            })
            ->get();

            if($start_date && $end_date){
                $start_date = date('Y-m-d', strtotime($start_date));
                $end_date = date('Y-m-d', strtotime($end_date));

                $deliveries = \Illuminate\Support\Facades\DB::table('medicine_deliveries as delivery')
                    ->select('*')

                    ->leftjoin('pharmacy_medicines', function($join) {
                        $join->on('delivery.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                    })
                    ->leftjoin('pharmacy_item_types', function($join) {
                        $join->on('delivery.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
                    })
                    ->leftjoin('suppliers', function($join) {
                        $join->on('delivery.supplier_key', '=', 'suppliers.supplier_key');
                    })

                    ->whereBetween('delivery.medicine_delivery_date',array($start_date,$end_date))
                    ->where('delivery.pharmacy_item_type_key',$type)
                    ->get();
            }      

        return datatables()->of($deliveries)
            ->make(true);
    }


}