<?php

namespace App\Http\Controllers;
use App\MedicineExpiredDamage;
use App\MedicineStockbalance;
use App\MedicineTransaction;
use App\PharmacyMedicineTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class MedicineExpiredDamageController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{

    public function store(Request $request){

               /* return response()->json([
            'message' => $request->input('medicine_expired_damaged_qty')
        ]); */

        $rules = [
            'pharmacy_medicine_key' => 'required',
            'medicine_expired_damaged_qty'  => 'required',
            'medicine_expired_damaged_date' => 'required',
            'medicine_expired_damaged_remarks' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/medicine-expired-damages/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                try{
                    $pharmacy_medicine_key = $request->input('pharmacy_medicine_key');
                    $itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacy_medicine_key)->value('pharmacy_item_type_key');
                    $checkStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->doesntExist();

                        if($checkStockBalance){
                            return redirect('admin/medicine-expired-damages/create')
                                ->withInput()
                                ->withErrors([
                                    'message' => "No stock balance on the item."]);
                        }
                        else{
                            $medicine_expired_damaged = new MedicineExpiredDamage();
                
                            $medicine_expired_damaged->medicine_expired_damaged_qty = $request->input('medicine_expired_damaged_qty');
                            $medicine_expired_damaged->medicine_expired_damaged_date = $request->input('medicine_expired_damaged_date');
                            $medicine_expired_damaged->medicine_expired_damaged_location = $request->input('medicine_expired_damaged_location');
                            $medicine_expired_damaged->medicine_expired_damaged_remarks = $request->input('medicine_expired_damaged_remarks');
                            $medicine_expired_damaged->medicine_expired_damaged_lot_no = $request->input('medicine_expired_damaged_lot_no');
                            $medicine_expired_damaged->medicine_expired_damaged_expiry = $request->input('medicine_expired_damaged_expiry');
                            $medicine_expired_damaged->pharmacy_item_type_key = $itemTypeKey;
                            $medicine_expired_damaged->pharmacy_medicine_key = $request->input('pharmacy_medicine_key');
                            
                            $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->value('medicine_stockbalance_current');
                            
                                if($medicine_expired_damaged->save()){
                                    $medicineExpiredDamage_id = $medicine_expired_damaged->medicine_expired_damaged_key;

                                    $medicineTransaction = new MedicineTransaction();
                                    $medicineTransactionType = "Expired/Damaged";
                                    $medicineTransaction->medicine_transaction_type_key = $medicineExpiredDamage_id;
                                    $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                                    $medicineTransaction->medicine_transaction_qty = $request->input('medicine_expired_damaged_qty');
                                    $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                    $medicineTransaction->medicine_transaction_status = "Added";
                                       
                                         if($medicineTransaction->save()){
                                            $balance = MedicineStockbalance::where('pharmacy_medicine_key','=',$pharmacy_medicine_key)->firstOrFail();
                                            $balance_current = $balance->medicine_stockbalance_current;
                                            $balance->medicine_stockbalance_current = $balance_current - $request->input('medicine_expired_damaged_qty');
                                        
                                                if($balance->save()) {
                                                    $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->value('medicine_stockbalance_current');
                                                    $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->value('medicine_stockbalance_reorder');
                                                        if($currentBalance<=$reOrderLevel){
                                                            $status = "Re-order";
                                                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)
                                                            ->update(array('medicine_stockbalance_status'=>$status) );
                                                        }
                                                        else{
                                                            $status = "";
                                                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)
                                                            ->update(array('medicine_stockbalance_status'=>$status) );
                                                        }

                                                            if($request->input('medicine_expired_damaged_remarks') == 1){
                                                                $transaction = new PharmacyMedicineTransaction;
                                                                $transaction->pharmacy_medicine_key = $pharmacy_medicine_key;
                                                                $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                $transaction->pharmacy_medicine_transaction_date = $request->input('medicine_expired_damaged_date');
                                                                $transaction->medicine_expired_qty = $request->input('medicine_expired_damaged_qty');
                                                                $transaction->pharmacy_transaction_id = $medicineExpiredDamage_id;
                                                                $transaction->pharmacy_transaction_type = "Expired_Damage";
                                                                $transaction->save();
                                                            }
                                                            else{
                                                                $transaction = new PharmacyMedicineTransaction;
                                                                $transaction->pharmacy_medicine_key = $pharmacy_medicine_key;
                                                                $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                $transaction->pharmacy_medicine_transaction_date = $request->input('medicine_expired_damaged_date');
                                                                $transaction->medicine_damage_qty = $request->input('medicine_expired_damaged_qty');
                                                                $transaction->pharmacy_transaction_id = $medicineExpiredDamage_id;
                                                                $transaction->pharmacy_transaction_type = "Expired_Damage";
                                                                $transaction->save();
                                                            }
                                                                
                                                                return redirect('admin/medicine-expired-damages')->with([
                                                                    'message' => __('voyager::generic.successfully_added_new')." Expired/Damaged Medicine",
                                                                    'alert-type' => 'success']);
                                                }
                                                else{
                                                    return redirect('admin/medicine-expired-damages')->with([
                                                        'message' => "Stock balance not change.",
                                                        'alert-type' => 'error']);
                                                } 
                                         }   
                                         else{
                                            return redirect('admin/medicine-expired-damages')->with([
                                                'message' => "Medicine transaction not save.",
                                                'alert-type' => 'error']);
                                         }                
                                }
                                else{
                                    return redirect('admin/medicine-expired-damages/create')->with([
                                        'message' => "Error Saving",
                                        'alert-type' => 'error']);
                                }
                        }
                }
                catch(Exception $e){
                    return redirect('path')->with([
                        'message' => "Error Saving",
                        'alert-type' => 'error']);
                }
            }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;
        //$pharmacyMedicineKey = $request->input('pharmacy_medicine_key');
        //$itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');

                $medicineExpiredamage = DB::table('medicine_expired_damages')->where('medicine_expired_damaged_key', $id)->value('medicine_expired_damaged_qty');
                $medicineKey = DB::table('medicine_expired_damages')->where('medicine_expired_damaged_key', $id)->value('pharmacy_medicine_key');
                $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

                $retStockQUantity =  $medicineExpiredamage + $StockBalance ;
                $newStockQuantity = $retStockQUantity - $request->input('medicine_expired_damaged_qty');
            
                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_current'=>$newStockQuantity) );

                $medicine_expired_damaged = DB::table('medicine_expired_damages')
                    ->where('medicine_expired_damaged_key', $id)
                    ->update(array('medicine_expired_damaged_qty'=> $request->input('medicine_expired_damaged_qty'),
                        'medicine_expired_damaged_date'=> $request->input('medicine_expired_damaged_date'),
                        'medicine_expired_damaged_remarks'=>$request->input('medicine_expired_damaged_remarks'),
                        'medicine_expired_damaged_lot_no'=>$request->input('medicine_expired_damaged_lot_no'),
                        'medicine_expired_damaged_expiry' =>$request->input('medicine_expired_damaged_expiry'),
                        'medicine_expired_damaged_location'=> $request->input('medicine_expired_damaged_location')));

                        $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                        $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                            if($currentBalance<=$reOrderLevel){
                                $status = "Re-order";
                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                ->update(array('medicine_stockbalance_status'=>$status) );
                            }
                            else{
                                $status = "";
                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                ->update(array('medicine_stockbalance_status'=>$status) );
                            }

                                $medicineTransaction = new MedicineTransaction();
                                $medicineTransactionType = "Expired/Damaged";
                                $medicineTransaction->medicine_transaction_type_key = $id;
                                $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                                $medicineTransaction->medicine_transaction_qty = $request->input('medicine_expired_damaged_qty');
                                $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                $medicineTransaction->medicine_transaction_status = "Updated";

                                    if($medicineTransaction->save()){
                                        if($request->input('medicine_expired_damaged_remarks') == 1){
                                             $transaction = DB::table('pharmacy_medicine_transactions')
                                             ->where('pharmacy_transaction_id', $id)
                                             ->where('pharmacy_transaction_type', "Expired_Damage")
                                                ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_expired_damaged_date'),
                                                'medicine_expired_qty'=> $request->input('medicine_expired_damaged_qty'),
                                                'medicine_damage_qty'=> 0));
                                        }
                                        else{
                                            $transaction = DB::table('pharmacy_medicine_transactions')
                                                ->where('pharmacy_transaction_id', $id)
                                                ->where('pharmacy_transaction_type', "Expired_Damage")
                                                    ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_expired_damaged_date'),
                                                        'medicine_damage_qty'=> $request->input('medicine_expired_damaged_qty'),
                                                        'medicine_expired_qty'=> 0 ));
                                        }
                                       
                                    }
                                    else{
                                        return redirect('admin/medicine-expired-damages')->with([
                                            'message' => "Transaction not save.",
                                            'alert-type' => 'error']); 
                                    }
                                
                                    return redirect('admin/medicine-expired-damages')->with([
                                        'message' => __('voyager::generic.successfully_updated')." Expired/Damaged Medicine",
                                        'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;
        
        $medicineExpiredamageQty = DB::table('medicine_expired_damages')->where('medicine_expired_damaged_key', $id)->value('medicine_expired_damaged_qty');
        $medicineKey = DB::table('medicine_expired_damages')->where('medicine_expired_damaged_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $newStockQUantity =  $medicineExpiredamageQty + $StockBalance;

        $deleted_ExpiredDamage = DB::table('medicine_expired_damages')->where('medicine_expired_damaged_key', $id)->delete();

        $newMedicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQUantity) );

            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }

                        $medicineTransaction = new MedicineTransaction();
                        $medicineTransactionType = "Expired/Damaged";
                        $medicineTransaction->medicine_transaction_type_key = $id;
                        $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                        $medicineTransaction->medicine_transaction_qty = $medicineExpiredamageQty;
                        $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                        $medicineTransaction->medicine_transaction_status = "Deleted";
                        $medicineTransaction->save();

                            if($medicineTransaction->save()){
                                $deleted_transaction = DB::table('pharmacy_medicine_transactions')
                                    ->where('pharmacy_transaction_id', $id)
                                    ->where('pharmacy_transaction_type', "Expired_Damage")
                                    ->delete();
                            }
                            else{
                                return redirect('admin/medicine-expired-damages')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => 'error']); 
                            }

                        return redirect('admin/medicine-expired-damages')->with([
                            'message' => __('voyager::generic.successfully_deleted')." Expired/Damaged Medicine",
                            'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/medicine-expired-damages')->with([
            'message' => __('voyager::generic.successfully_restored')." Expired/Damaged Medicine",
            'alert-type' => 'success']);
    }
}
