<?php

namespace App\Http\Controllers;

use App\PharmacySupplyCreditNote;
use App\PharmacySupplyStockbalance;
use App\PharmacySupplyTransaction;
use App\SupplyTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PharmacySupplyCreditNoteController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_supply_key' => 'required',
            'pharmacy_supply_credit_note_qty'  => 'required',
            'pharmacy_supply_credit_note_date' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/pharmacy-supply-credit-notes/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                    try{
                        $supplyKey = $data['pharmacy_supply_key'];
                        $itemTypeKey = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$supplyKey)->value('pharmacy_item_type_key');
                        $checkStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->doesntExist();
                            
                            if($checkStockBalance){
                                return redirect('admin/pharmacy-supply-credit-notes/create')
                                    ->withInput()
                                    ->withErrors([
                                        'message' => "No stock balance on the item."]);
                            }
                            else{
                                $supplyCreditNote = new PharmacySupplyCreditNote();
                
                                $supplyCreditNote ->pharmacy_supply_credit_note_date = $data['pharmacy_supply_credit_note_date'];
                                $supplyCreditNote ->pharmacy_supply_credit_note_qty = $data['pharmacy_supply_credit_note_qty'];
                                $supplyCreditNote ->pharmacy_supply_credit_note_remarks = $data['pharmacy_supply_credit_note_remarks'];
                                $supplyCreditNote ->pharmacy_supply_credit_note_area = $data['pharmacy_supply_credit_note_area'];
                                $supplyCreditNote ->employee_name = $data['employee_name'];
                                $supplyCreditNote ->patient_hospital_no = $data['patient_hospital_no'];
                                $supplyCreditNote ->pharmacy_supply_key = $data['pharmacy_supply_key'];

                                $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
    
                                if($supplyCreditNote->save()){
                                    $supplyCreditNote_id = $supplyCreditNote->pharmacy_supply_credit_note_key;

                                    $supplyTransaction = new PharmacySupplyTransaction();
                                    $supplyTransactionType = "Credit Note";
                                    $supplyTransaction->pharmacy_supply_transaction_type_key = $supplyCreditNote_id;
                                    $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                                    $supplyTransaction->pharmacy_supply_transaction_qty = $data['pharmacy_supply_credit_note_qty'];
                                    $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                                    $supplyTransaction->pharmacy_supply_transaction_status = "Added";
                                    
                                        if($supplyTransaction->save()){
                                            $balance = PharmacySupplyStockbalance::findOrFail($supplyKey);
                                            $balance_current = $balance->pharmacy_supply_stockbalance_current;
                                            $balance->pharmacy_supply_stockbalance_current = $balance_current + $data['pharmacy_supply_credit_note_qty'];

                                            if($balance->save()){
                                                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                                                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                                                    if($currentBalance<=$reOrderLevel){
                                                        $status = "Re-order";
                                                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                                    }
                                                    else{
                                                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                                        ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                                                    }
                                                        $transaction = new SupplyTransaction;
                                                        $transaction->pharmacy_supply_key = $supplyKey;
                                                        $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                        $transaction->supply_transaction_date = $data['pharmacy_supply_credit_note_date'];
                                                        $transaction->pharmacy_supply_credit_note_qty = $data['pharmacy_supply_credit_note_qty'];
                                                        $transaction->pharmacy_supply_transaction_id = $supplyCreditNote_id;
                                                        $transaction->supply_transaction_type = "Credit Note";
                                                        $transaction->save();

                                                        return redirect('admin/pharmacy-supply-credit-notes')->with([
                                                            'message' => __('voyager::generic.successfully_added_new')." Credit Note",
                                                            'alert-type' => 'success']);
                                            }
                                            else{
                                                return redirect('admin/pharmacy-supply-credit-notes')->with([
                                                    'message' => "Stock balance not change.",
                                                    'alert-type' => 'error']);
                                            }
                                        }
                                        else{
                                            return redirect('admin/pharmacy-supply-credit-notes')->with([
                                                'message' => "Supply transaction not save.",
                                                'alert-type' => 'error']);
                                        }
                                }
                                else{
                                    return redirect('admin/pharmacy-supply-credit-notes/create')->with([
                                        'message' => "Error Saving",
                                        'alert-type' => 'error']);
                                }

                            }                        
                    }
                    catch(Exception $e){
                        return redirect('path')->with([
                            'message' => "Error Saving",
                            'alert-type' => 'error']);
                    }
            }
    }

    public function update(Request $request, $id){
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyCreditNote  = DB::table('pharmacy_supply_credit_notes')->where('pharmacy_supply_credit_note_key', $id)->value('pharmacy_supply_credit_note_qty');
        $supplyKey = DB::table('pharmacy_supply_credit_notes')->where('pharmacy_supply_credit_note_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $retStockQUantity = $StockBalance - $supplyCreditNote;
        $newStockQuantity = $retStockQUantity + $request->input('pharmacy_supply_credit_note_qty');
    
        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQuantity) );

        $supply_credit_note = DB::table('pharmacy_supply_credit_notes')
            ->where('pharmacy_supply_credit_note_key', $id)
            ->update(array('pharmacy_supply_credit_note_date'=> $request->input('pharmacy_supply_credit_note_date'),
                'pharmacy_supply_credit_note_qty'=> $request->input('pharmacy_supply_credit_note_qty'),
                'pharmacy_supply_credit_note_remarks'=> $request->input('pharmacy_supply_credit_note_remarks'),
                'employee_name'=> $request->input('employee_name'),
                'pharmacy_supply_credit_note_area'=>$request->input('pharmacy_supply_credit_note_area'),
                'patient_hospital_no'=> $request->input('patient_hospital_no')));
        
                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                    }

                        $supplyTransaction = new PharmacySupplyTransaction();
                        $supplyTransactionType = "Credit Note";
                        $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                        $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                        $supplyTransaction->pharmacy_supply_transaction_qty = $request->input('pharmacy_supply_credit_note_qty');
                        $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                        $supplyTransaction->pharmacy_supply_transaction_status = "Updated";
                        
                            if($supplyTransaction->save()){
                                $trasaction = DB::table('supply_transactions')
                                    ->where('pharmacy_supply_transaction_id',$id)
                                    ->where('supply_transaction_type',"Credit Note")
                                        ->update(array('supply_transaction_date'=> $request->input('pharmacy_supply_credit_note_date'),
                                            'pharmacy_supply_credit_note_qty'=> $request->input('pharmacy_supply_credit_note_qty')));
                            }
                            else{
                                return redirect('admin/pharmacy-supply-credit-notes')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => 'error']); 
                            }

                        return redirect('admin/pharmacy-supply-credit-notes')->with([
                            'message' => __('voyager::generic.successfully_updated')." Credit Note",
                            'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyCreditNoteQty  = DB::table('pharmacy_supply_credit_notes')->where('pharmacy_supply_credit_note_key', $id)->value('pharmacy_supply_credit_note_qty');
        $supplyKey = DB::table('pharmacy_supply_credit_notes')->where('pharmacy_supply_credit_note_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $newStockQUantity = $StockBalance - $supplyCreditNoteQty;

        $deleted_credit_note = DB::table('pharmacy_supply_credit_notes')->where('pharmacy_supply_credit_note_key', $id)->delete();

        $newStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQUantity) );

            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
            $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                if($currentBalance<=$reOrderLevel){
                    $status = "Re-order";
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                }
                else{
                    $status = "";
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                }

                    $supplyTransaction = new PharmacySupplyTransaction();
                    $supplyTransactionType = "Credit Note";
                    $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                    $supplyTransaction->pharmacy_supply_transaction_type = $supplyTransactionType;
                    $supplyTransaction->pharmacy_supply_transaction_qty = $supplyCreditNoteQty;
                    $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                    $supplyTransaction->pharmacy_supply_transaction_status = "Deleted";
                    $supplyTransaction->save();

                    return redirect('admin/pharmacy-supply-credit-notes')->with([
                        'message' => __('voyager::generic.successfully_deleted')." Credit Note",
                        'alert-type' => 'success']);
    }
}
