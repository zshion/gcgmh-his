<?php

namespace App\Http\Controllers;
use App\PharmacySupplyStockbalance;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class PharmacySupplyStockbalanceController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_supply_key' => 'required',
            'pharmacy_supply_stockbalance_beginning'  => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
         if ($validator->fails()) {
             return redirect('admin/pharmacy-supply-stockbalances/create')
                 ->withInput()
                 ->withErrors($validator);
         }
         else{
            $data = $request->input();
            try{
                $pharmacy_supply_key = $data['pharmacy_supply_key'];
                $itemTypeKey = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$pharmacy_supply_key)->value('pharmacy_item_type_key');
                $itemSKU = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$pharmacy_supply_key)->value('pharmacy_supply_sku');
                
                $supplyStockbalance = new PharmacySupplyStockbalance();
 
                $supplyStockbalance->pharmacy_supply_stockbalance_beginning = $data['pharmacy_supply_stockbalance_beginning'];
                $supplyStockbalance->pharmacy_supply_stockbalance_current = $data['pharmacy_supply_stockbalance_beginning'];
                $supplyStockbalance->pharmacy_supply_stockbalance_reorder = $data['pharmacy_supply_stockbalance_reorder'];
                $supplyStockbalance->pharmacy_supply_key = $data['pharmacy_supply_key'];
                $supplyStockbalance->pharmacy_supply_sku = $itemSKU;
                $supplyStockbalance->pharmacy_item_type_key = $itemTypeKey;

                $checkPatient = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $data['pharmacy_supply_key'])->doesntExist();
                    if($checkPatient){
                        if($supplyStockbalance->save()){
                            $supplyKey = $data['pharmacy_supply_key'];
                            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                            $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');
                                if($currentBalance<=$reOrderLevel){
                                    $status = "Re-order";
                                    $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                }
                                else{
                                    $status = "";
                                    $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                }
                                return redirect('admin/pharmacy-supply-stockbalances')->with([
                                'message' => __('voyager::generic.successfully_updated')." Stock Balance",
                                'alert-type' => 'success']);
                        }   
                    }
                    else{
                        return redirect('admin/pharmacy-supply-stockbalances/create')->with([
                            'message' => "Item Description Already Exist",
                            'alert-type' => 'error']);
                    }
            }
            catch(Exception $e){
                return redirect('path')->with([
                    'message' => "Error Saving",
                    'alert-type' => 'error']);
             }
           
         }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_stockbalance_key', $id)->value('pharmacy_supply_stockbalance_current');
        $reOrderLevel = $request->input('pharmacy_supply_stockbalance_reorder');
            if($currentBalance<=$reOrderLevel){
                $status = "Re-order";
                $supply_Stockbalances = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_stockbalance_key', $id)
                    ->update(array('pharmacy_supply_stockbalance_reorder'=> $request->input('pharmacy_supply_stockbalance_reorder'),
                            //'pharmacy_supply_stockbalance_beginning'=> $request->input('pharmacy_supply_stockbalance_beginning'),
                            //'pharmacy_supply_stockbalance_current'=> $request->input('pharmacy_supply_stockbalance_beginning'),
                            'pharmacy_supply_stockbalance_status'=>$status));
            }
            else{
                $supply_Stockbalances = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_stockbalance_key', $id)
                    ->update(array('pharmacy_supply_stockbalance_reorder'=> $request->input('pharmacy_supply_stockbalance_reorder'),
                        //'pharmacy_supply_stockbalance_beginning'=> $request->input('pharmacy_supply_stockbalance_beginning'),
                        //'pharmacy_supply_stockbalance_current'=> $request->input('pharmacy_supply_stockbalance_beginning'),
                        'pharmacy_supply_stockbalance_status'=>null));
            }
        
            return redirect('admin/pharmacy-supply-stockbalances')->with([
                'message' => __('voyager::generic.successfully_updated')." Stock Balance",
                'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect('admin/pharmacy-supply-stockbalances')->with([
            'message' => __('voyager::generic.successfully_deleted')." Stock Balance",
            'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/pharmacy-supply-stockbalances')->with([
            'message' => __('voyager::generic.successfully_restored')." Stock Balance",
            'alert-type' => 'success']);
    }
}