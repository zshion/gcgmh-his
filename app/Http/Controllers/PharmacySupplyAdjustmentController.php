<?php

namespace App\Http\Controllers;

use App\PharmacySupplyAdjustment;
use App\PharmacySupplyStockbalance;
use App\PharmacySupplyTransaction;
use App\SupplyTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class PharmacySupplyAdjustmentController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_supply_key' => 'required',
            'pharmacy_supply_adjustment_qty'  => 'required',
            'pharmacy_supply_adjustment_date' => 'required',
            'pharmacy_supply_adjustment_name' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
         if ($validator->fails()) {
             return redirect('admin/pharmacy-supply-adjustments/create')
                 ->withInput()
                 ->withErrors($validator);
         }
         else{
            $data = $request->input();
                try{
                    $pharmacySupplyKey = $data['pharmacy_supply_key'];
                    $itemTypeKey = DB::table('pharmacy_supplies')->where('pharmacy_supply_key',$pharmacySupplyKey)->value('pharmacy_item_type_key');

                    $checkStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->doesntExist();
                        if($checkStockBalance){
                            return redirect('admin/pharmacy-supply-adjustments/create')
                                ->withInput()
                                ->withErrors([
                                    'message' => "No stock balance on the item."]);
                        }
                        else{
                            $supplyAdjusment = new PharmacySupplyAdjustment();
            
                            $supplyAdjusment->pharmacy_supply_adjustment_docno = $data['pharmacy_supply_adjustment_docno'];
                            $supplyAdjusment->pharmacy_supply_adjustment_name = $data['pharmacy_supply_adjustment_name'];
                            $supplyAdjusment->pharmacy_supply_adjustment_qty = $data['pharmacy_supply_adjustment_qty'];
                            $supplyAdjusment->pharmacy_supply_adjustment_unit_cost = $data['pharmacy_supply_adjustment_unit_cost'];
                            $supplyAdjusment->pharmacy_supply_adjustment_total_cost = $data['pharmacy_supply_adjustment_unit_cost'] * $data['pharmacy_supply_adjustment_qty'];
                            $supplyAdjusment->pharmacy_supply_adjustment_remarks = $data['pharmacy_supply_adjustment_remarks'];
                            $supplyAdjusment->pharmacy_supply_key = $data['pharmacy_supply_key'];
                            $supplyAdjusment->pharmacy_supply_adjustment_date = $data['pharmacy_supply_adjustment_date'];
                            $supplyAdjusment->pharmacy_item_type_key = $itemTypeKey;
                                
                                $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_current');
                                    if($supplyAdjusment->save()){
                                        $supplyAdjustment_id = $supplyAdjusment->pharmacy_supply_adjustment_key;

                                        $pharmacySupplyTransaction = new PharmacySupplyTransaction();
                                        $pharmacySupplyTransaction->pharmacy_supply_transaction_type_key = $supplyAdjustment_id;
                                        $pharmacySupplyTransaction->pharmacy_supply_transaction_type = "Adjustment";
                                        $pharmacySupplyTransaction->pharmacy_supply_transaction_qty = $data['pharmacy_supply_adjustment_qty'];
                                        $pharmacySupplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                                        $pharmacySupplyTransaction->pharmacy_supply_transaction_status = "Added";
                                        
                                            if($pharmacySupplyTransaction->save()){
                                                $balance = PharmacySupplyStockbalance::where('pharmacy_supply_key','=',$pharmacySupplyKey)->firstOrFail();
                                                $balance_current = $balance->pharmacy_supply_stockbalance_current;
                                                $balance->pharmacy_supply_stockbalance_current = $balance_current + $data['pharmacy_supply_adjustment_qty'];
                                                    
                                                if($balance->save()){
                                                        $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_current');
                                                        $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)->value('pharmacy_supply_stockbalance_reorder');
                                                            if($currentBalance<=$reOrderLevel){
                                                                $status = "Re-order";
                                                                $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)
                                                                ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                                                            }
                                                            else{
                                                                $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $pharmacySupplyKey)
                                                                ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                                                            }
                                                                $transactionType = "Adjustment";
                                                                    if($data['pharmacy_supply_adjustment_name'] == 1){
                                                                        $transaction = new SupplyTransaction;
                                                                        $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                        $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                        $transaction->supply_transaction_date = $data['pharmacy_supply_adjustment_date'];
                                                                        $transaction->adjustment_cancelled_iar_qty = $data['pharmacy_supply_adjustment_qty'];
                                                                        $transaction->pharmacy_supply_transaction_id = $supplyAdjustment_id;
                                                                        $transaction->supply_transaction_type = $transactionType;
                                                                        $transaction->save();
                                                                    }
                                                                        elseif($data['pharmacy_supply_adjustment_name'] == 2){
                                                                            $transaction = new SupplyTransaction;
                                                                            $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                            $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                            $transaction->supply_transaction_date = $data['pharmacy_supply_adjustment_date'];
                                                                            $transaction->adjustment_issued_employee_qty = $data['pharmacy_supply_adjustment_qty'];
                                                                            $transaction->pharmacy_supply_transaction_id = $supplyAdjustment_id;
                                                                            $transaction->supply_transaction_type = $transactionType;
                                                                            $transaction->save();
                                                                        }
                                                                            elseif($data['pharmacy_supply_adjustment_name'] == 3){
                                                                                $transaction = new SupplyTransaction;
                                                                                $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                                $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                                $transaction->supply_transaction_date = $data['pharmacy_supply_adjustment_date'];
                                                                                $transaction->adjustment_overissued_qty = $data['pharmacy_supply_adjustment_qty'];
                                                                                $transaction->pharmacy_supply_transaction_id = $supplyAdjustment_id;
                                                                                $transaction->supply_transaction_type = $transactionType;
                                                                                $transaction->save();
                                                                            }
                                                                                elseif($data['pharmacy_supply_adjustment_name'] == 4){
                                                                                    $transaction = new SupplyTransaction;
                                                                                    $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                                    $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                                    $transaction->supply_transaction_date = $data['pharmacy_supply_adjustment_date'];
                                                                                    $transaction->adjustment_replacement_qty = $data['pharmacy_supply_adjustment_qty'];
                                                                                    $transaction->pharmacy_supply_transaction_id = $supplyAdjustment_id;
                                                                                    $transaction->supply_transaction_type = $transactionType;
                                                                                    $transaction->save();
                                                                                }
                                                                                    elseif($data['pharmacy_supply_adjustment_name'] == 5){
                                                                                        $transaction = new SupplyTransaction;
                                                                                        $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                                        $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                                        $transaction->supply_transaction_date = $data['pharmacy_supply_adjustment_date'];
                                                                                        $transaction->adjustment_returned_ward_qty = $data['pharmacy_supply_adjustment_qty'];
                                                                                        $transaction->pharmacy_supply_transaction_id = $supplyAdjustment_id;
                                                                                        $transaction->supply_transaction_type = $transactionType;
                                                                                        $transaction->save();
                                                                                    }
                                                                                        elseif($data['pharmacy_supply_adjustment_name'] == 6){
                                                                                            $transaction = new SupplyTransaction;
                                                                                            $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                                            $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                                            $transaction->supply_transaction_date = $data['pharmacy_supply_adjustment_date'];
                                                                                            $transaction->adjustment_unissued_qty = $data['pharmacy_supply_adjustment_qty'];
                                                                                            $transaction->pharmacy_supply_transaction_id = $supplyAdjustment_id;
                                                                                            $transaction->supply_transaction_type = $transactionType;
                                                                                            $transaction->save();
                                                                                        }
                                                                                            elseif($data['pharmacy_supply_adjustment_name'] == 7){
                                                                                                $transaction = new SupplyTransaction;
                                                                                                $transaction->pharmacy_supply_key = $pharmacySupplyKey;
                                                                                                $transaction->pharmacy_item_type_key = $itemTypeKey;
                                                                                                $transaction->supply_transaction_date = $data['pharmacy_supply_adjustment_date'];
                                                                                                $transaction->adjustment_unrecorded_iar_qty = $data['pharmacy_supply_adjustment_qty'];
                                                                                                $transaction->pharmacy_supply_transaction_id = $supplyAdjustment_id;
                                                                                                $transaction->supply_transaction_type = $transactionType;
                                                                                                $transaction->save();
                                                                                            }

                                                                                            return redirect('admin/pharmacy-supply-adjustments')->with([
                                                                                                'message' => __('voyager::generic.successfully_added_new')." Adjustment",
                                                                                                'alert-type' => 'success']);
                                                    }
                                                    else{
                                                        return redirect('pharmacy-supply-adjustments')->with([
                                                            'message' => "Stock balance not change.",
                                                            'alert-type' => 'error']);
                                                    }
                                            }
                                            else{
                                                return redirect('pharmacy-supply-adjustments')->with([
                                                    'message' => "Supply transaction not save.",
                                                    'alert-type' => 'error']);
                                            }            
                                    }
                                    else{
                                        return redirect('pharmacy-supply-adjustments')->with([
                                            'message' => "Adjustment not save.",
                                            'alert-type' => 'error']);
                                    } 
                        }
                }
                catch(Exception $e){
                    return redirect('path')->with([
                        'message' => "Error Saving",
                        'alert-type' => 'error']);
                }
         }
    }

    public function update(Request $request, $id){
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyAdjustment = DB::table('pharmacy_supply_adjustments')->where('pharmacy_supply_adjustment_key', $id)->value('pharmacy_supply_adjustment_qty');
        $supplyKey = DB::table('pharmacy_supply_adjustments')->where('pharmacy_supply_adjustment_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $retStockQUantity = $StockBalance - $supplyAdjustment;
        $newStockQuantity = $retStockQUantity + $request->input('pharmacy_supply_adjustment_qty');

        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQuantity) );
        
        $supply_adjustment = DB::table('pharmacy_supply_adjustments')
            ->where('pharmacy_supply_adjustment_key', $id)
            ->update(array('pharmacy_supply_adjustment_qty'=> $request->input('pharmacy_supply_adjustment_qty'),
                'pharmacy_supply_adjustment_docno'=> $request->input('pharmacy_supply_adjustment_docno'),
                'pharmacy_supply_adjustment_name'=> $request->input('pharmacy_supply_adjustment_name'),
                'pharmacy_supply_adjustment_unit_cost'=> $request->input('pharmacy_supply_adjustment_unit_cost'),
                'pharmacy_supply_adjustment_total_cost'=> $request->input('pharmacy_supply_adjustment_qty') * $request->input('pharmacy_supply_adjustment_unit_cost'),
                'pharmacy_supply_adjustment_date'=> $request->input('pharmacy_supply_adjustment_date'),
                'pharmacy_supply_adjustment_remarks'=> $request->input('pharmacy_supply_adjustment_remarks')));
    
                $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
                $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');

                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                    }
                    else{
                        $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                        ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                    }
                        $pharmacySupplyTransaction = new PharmacySupplyTransaction();

                        $pharmacySupplyTransaction->pharmacy_supply_transaction_type_key = $id;
                        $pharmacySupplyTransaction->pharmacy_supply_transaction_type = "Adjustment";
                        $pharmacySupplyTransaction->pharmacy_supply_transaction_qty = $request->input('pharmacy_supply_adjustment_qty');
                        $pharmacySupplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                        $pharmacySupplyTransaction->pharmacy_supply_transaction_status = "Updated";
                        
                            if($pharmacySupplyTransaction->save()){
                                $transactionType = "Adjustment";
                                if($request->input('pharmacy_supply_adjustment_name') == 1){
                                    $transaction = DB::table('supply_transactions')
                                    ->where('pharmacy_supply_transaction_id', $id)
                                    ->where('supply_transaction_type', $transactionType)
                                        ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_adjustment_date'),
                                        'adjustment_cancelled_iar_qty'=> $request->input('pharmacy_supply_adjustment_qty'),
                                        'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                        'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                        'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                }
                                    elseif($request->input('pharmacy_supply_adjustment_name') == 2){
                                        $transaction = DB::table('supply_transactions')
                                        ->where('pharmacy_supply_transaction_id', $id)
                                        ->where('supply_transaction_type', $transactionType)
                                            ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_adjustment_date'),
                                            'adjustment_cancelled_iar_qty'=> 0 ,
                                            'adjustment_issued_employee_qty'=> $request->input('pharmacy_supply_adjustment_qty'), 
                                            'adjustment_overissued_qty' => 0,
                                            'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                            'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                    }
                                        elseif($request->input('pharmacy_supply_adjustment_name') == 3){
                                            $transaction = DB::table('supply_transactions')
                                            ->where('pharmacy_supply_transaction_id', $id)
                                            ->where('supply_transaction_type', $transactionType)
                                                ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_adjustment_date'),
                                                'adjustment_cancelled_iar_qty'=> 0 ,
                                                'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => $request->input('pharmacy_supply_adjustment_qty'),
                                                'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                                'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                        }
                                            elseif($request->input('pharmacy_supply_adjustment_name') == 4){
                                                $transaction = DB::table('supply_transactions')
                                                ->where('pharmacy_supply_transaction_id', $id)
                                                ->where('supply_transaction_type', $transactionType)
                                                    ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_adjustment_date'),
                                                    'adjustment_cancelled_iar_qty'=> 0 ,
                                                    'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                    'adjustment_replacement_qty'=> $request->input('pharmacy_supply_adjustment_qty'),
                                                    'adjustment_returned_ward_qty'=> 0,
                                                    'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                            }
                                                elseif($request->input('pharmacy_supply_adjustment_name') == 5){
                                                    $transaction = DB::table('supply_transactions')
                                                    ->where('pharmacy_supply_transaction_id', $id)
                                                    ->where('supply_transaction_type', $transactionType)
                                                        ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_adjustment_date'),
                                                        'adjustment_cancelled_iar_qty'=> 0 ,
                                                        'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                        'adjustment_replacement_qty'=> 0, 
                                                        'adjustment_returned_ward_qty'=> $request->input('pharmacy_supply_adjustment_qty'),
                                                        'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                                }
                                                    elseif($request->input('pharmacy_supply_adjustment_name') == 6){
                                                        $transaction = DB::table('supply_transactions')
                                                        ->where('pharmacy_supply_transaction_id', $id)
                                                        ->where('supply_transaction_type', $transactionType)
                                                            ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_adjustment_date'),
                                                            'adjustment_cancelled_iar_qty'=> 0 ,
                                                            'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                            'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                                            'adjustment_unissued_qty'=> $request->input('pharmacy_supply_adjustment_qty'), 
                                                            'adjustment_unrecorded_iar_qty'=> 0));
                                                    }
                                                        elseif($request->input('pharmacy_supply_adjustment_name') == 7){
                                                            $transaction = DB::table('supply_transactions')
                                                            ->where('pharmacy_supply_transaction_id', $id)
                                                            ->where('supply_transaction_type', $transactionType)
                                                                ->update( array('supply_transaction_date'=> $request->input('pharmacy_supply_adjustment_date'),
                                                                'adjustment_cancelled_iar_qty'=> 0 ,
                                                                'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                                'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                                                'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> $request->input('pharmacy_supply_adjustment_qty')));
                                                        } 
                            }
                            else{
                                return redirect('admin/pharmacy-supply-adjustments')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => 'error']); 
                            }
        
                            return redirect('admin/pharmacy-supply-adjustments')->with([
                                'message' => __('voyager::generic.successfully_updated')." Adjustment",
                                'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $supplyAdjustmentQty = DB::table('pharmacy_supply_adjustments')->where('pharmacy_supply_adjustment_key', $id)->value('pharmacy_supply_adjustment_qty');
        $supplyKey = DB::table('pharmacy_supply_adjustments')->where('pharmacy_supply_adjustment_key', $id)->value('pharmacy_supply_key');
        $StockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');

        $newStockQUantity = $StockBalance - $supplyAdjustmentQty;

        $deleted_adjustment = DB::table('pharmacy_supply_adjustments')->where('pharmacy_supply_adjustment_key', $id)->delete();

        $newSupplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
            ->update(array('pharmacy_supply_stockbalance_current'=>$newStockQUantity) );
        
            
            $currentBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_current');
            $reOrderLevel = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)->value('pharmacy_supply_stockbalance_reorder');

                if($currentBalance<=$reOrderLevel){
                    $status = "Re-order";
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>$status) );
                }
                else{
                    $supplyStockBalance = DB::table('pharmacy_supply_stockbalances')->where('pharmacy_supply_key', $supplyKey)
                    ->update(array('pharmacy_supply_stockbalance_status'=>null) );
                }
                    $supplyTransaction = new PharmacySupplyTransaction();
                    $supplyTransaction->pharmacy_supply_transaction_type_key = $id;
                    $supplyTransaction->pharmacy_supply_transaction_type = "Adjustment";
                    $supplyTransaction->pharmacy_supply_transaction_qty = $supplyAdjustmentQty;
                    $supplyTransaction->pharmacy_supply_transaction_beginning_qty = $StockBalance;
                    $supplyTransaction->pharmacy_supply_transaction_status = "Deleted";

                        if($supplyTransaction->save()){
                            $deleted_transaction = DB::table('supply_transactions')
                                    ->where('pharmacy_supply_transaction_id', $id)
                                    ->where('supply_transaction_type', "Adjustment")
                                    ->delete();
                        }
                        else{
                            return redirect('admin/pharmacy-supply-adjustments')->with([
                                'message' => "Transaction not save.",
                                'alert-type' => 'error']); 
                        }
                        return redirect('admin/pharmacy-supply-adjustments')->with([
                            'message' => __('voyager::generic.successfully_deleted')." Adjustment",
                            'alert-type' => 'success']);
    }
}
