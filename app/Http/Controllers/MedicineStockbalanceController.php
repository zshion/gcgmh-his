<?php

namespace App\Http\Controllers;
use App\MedicineStockbalance;

use App\DataTables\MedicineStockbalanceReportsDataTable;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class MedicineStockbalanceController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_medicine_key' => 'required',
            'medicine_stockbalance_beginning'  => 'required',
            'medicine_stockbalance_reorder'  => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
         if ($validator->fails()) {
             return redirect('admin/medicine-stockbalances/create')
                 ->withInput()
                 ->withErrors($validator);
         }
         else{
            $data = $request->input();
            try{
                $pharmacyMedicineKey = $data['pharmacy_medicine_key'];
                $itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');
                $itemSKU = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_medicine_sku');

                $medicineStockbalance = new MedicineStockbalance();
 
                $medicineStockbalance->medicine_stockbalance_beginning = $data['medicine_stockbalance_beginning'];
                $medicineStockbalance->medicine_stockbalance_current = $data['medicine_stockbalance_beginning'];
                $medicineStockbalance->medicine_stockbalance_reorder = $data['medicine_stockbalance_reorder'];
                $medicineStockbalance->pharmacy_medicine_key = $data['pharmacy_medicine_key'];
                $medicineStockbalance->pharmacy_medicine_sku = $itemSKU;
                $medicineStockbalance->pharmacy_item_type_key = $itemTypeKey;

                $checkMedicine = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $data['pharmacy_medicine_key'])->doesntExist();
                    if($checkMedicine){
                        if($medicineStockbalance->save()){
                            $medicineKey = $data['pharmacy_medicine_key'];
                            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                            $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                                if($currentBalance<=$reOrderLevel){
                                    $status = "Re-order";
                                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                    ->update(array('medicine_stockbalance_status'=>$status) );
                                }
                                else{
                                    $status = "";
                                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                    ->update(array('medicine_stockbalance_status'=>$status) );
                                }
                                return redirect('admin/medicine-stockbalances')->with([
                                'message' => __('voyager::generic.successfully_updated')." Stock Balance",
                                'alert-type' => 'success']);
                        }
                        
                    }
                    else{
                        return redirect('admin/medicine-stockbalances/create')->with([
                            'message' => "Item Description Already Exist",
                            'alert-type' => 'error']);
                    }
            }
            catch(Exception $e){
                return redirect('path')->with([
                    'message' => "Error Saving",
                    'alert-type' => 'error']);
             }
           
         }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $currentBalance = DB::table('medicine_stockbalances')->where('medicine_stockbalance_key', $id)->value('medicine_stockbalance_current');
        $reOrderLevel = $request->input('medicine_stockbalance_reorder');
            if($currentBalance<=$reOrderLevel){
                $status = "Re-order";
                $medicine_Stockbalances = DB::table('medicine_stockbalances')->where('medicine_stockbalance_key', $id)
                    ->update(array('medicine_stockbalance_reorder'=> $request->input('medicine_stockbalance_reorder'),
                        //'medicine_stockbalance_beginning'=> $request->input('medicine_stockbalance_beginning'),
                        //'medicine_stockbalance_current'=> $request->input('medicine_stockbalance_beginning'),
                        'medicine_stockbalance_status'=>$status));
            }
            else{
                $medicine_Stockbalances = DB::table('medicine_stockbalances')->where('medicine_stockbalance_key', $id)
                    ->update(array('medicine_stockbalance_reorder'=> $request->input('medicine_stockbalance_reorder'),
                        //'medicine_stockbalance_beginning'=> $request->input('medicine_stockbalance_beginning'),
                        //'medicine_stockbalance_current'=> $request->input('medicine_stockbalance_beginning'),
                        'medicine_stockbalance_status'=>null));
            }
        
            return redirect('admin/medicine-stockbalances')->with([
                'message' => __('voyager::generic.successfully_updated')." Stock Balance",
                'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Init array of IDs
        $ids = [];
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL
            $ids[] = $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

            // Check permission
            $this->authorize('delete', $data);

            $model = app($dataType->model_name);
            if (!($model && in_array(SoftDeletes::class, class_uses_recursive($model)))) {
                $this->cleanup($dataType, $data);
            }
        }

        $displayName = count($ids) > 1 ? $dataType->getTranslatedAttribute('display_name_plural') : $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_deleted')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_deleting')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect('admin/medicine-stockbalances')->with([
            'message' => __('voyager::generic.successfully_deleted')." Stock Balance",
            'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/medicine-stockbalances')->with([
            'message' => __('voyager::generic.successfully_restored')." Stock Balance",
            'alert-type' => 'success']);
    }

    public function export()
    {
        return view('/vendor/voyager/medicine-stockbalances/export');
    }
}