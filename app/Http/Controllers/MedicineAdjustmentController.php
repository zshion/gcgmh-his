<?php

namespace App\Http\Controllers;
use App\MedicineAdjustment;
use App\MedicineStockbalance;
use App\MedicineTransaction;
use App\PharmacyMedicineTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MedicineAdjustmentController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_medicine_key' => 'required',
            'medicine_adjustment_qty'  => 'required',
            'medicine_adjustment_date' => 'required',
            'medicine_adjustment_name' => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/medicine-adjustments/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                    try{
                        $pharmacyMedicineKey = $data['pharmacy_medicine_key'];
                        $itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacyMedicineKey)->value('pharmacy_item_type_key');
                        
                        $checkStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->doesntExist();

                            if($checkStockBalance){
                                return redirect('admin/medicine-adjustments/create')
                                    ->withInput()
                                    ->withErrors([
                                        'message' => "No stock balance on the item."]);
                            }
                            else{
                                $medicineAdjusment = new MedicineAdjustment();
        
                                $medicineAdjusment->medicine_adjustment_docno = $data['medicine_adjustment_docno'];
                                $medicineAdjusment->medicine_adjustment_name = $data['medicine_adjustment_name'];
                                $medicineAdjusment->medicine_adjustment_qty = $data['medicine_adjustment_qty'];
                                $medicineAdjusment->medicine_adjustment_unit_cost = $data['medicine_adjustment_unit_cost'];
                                $medicineAdjusment->medicine_adjustment_total_cost = $data['medicine_adjustment_unit_cost'] * $data['medicine_adjustment_qty'];
                                $medicineAdjusment->medicine_adjustment_remarks = $data['medicine_adjustment_remarks'];
                                $medicineAdjusment->pharmacy_medicine_key = $data['pharmacy_medicine_key'];
                                $medicineAdjusment->medicine_adjustment_date = $data['medicine_adjustment_date'];
                                $medicineAdjusment->pharmacy_item_type_key = $itemTypeKey;
                        
                                    $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');
                        
                                    if($medicineAdjusment->save()){
                                        $medicineAdjustment_id = $medicineAdjusment->medicine_adjustment_key;

                                        $medicineTransaction = new MedicineTransaction();
                                        $medicineTransaction->medicine_transaction_type_key = $medicineAdjustment_id;
                                        $medicineTransaction->medicine_transaction_type = "Adjustment";
                                        $medicineTransaction->medicine_transaction_qty = $data['medicine_adjustment_qty'];
                                        $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                        $medicineTransaction->medicine_transaction_status = "Added";

                                            if($medicineTransaction->save()){
                                                $balance = MedicineStockbalance::where('pharmacy_medicine_key','=',$pharmacyMedicineKey)->firstOrFail();
                                                $balance_current = $balance->medicine_stockbalance_current;
                                                $balance->medicine_stockbalance_current = $balance_current + $data['medicine_adjustment_qty'];
                                            
                                                    if($balance->save()) {
                                                        $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_current');
                                                        $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)->value('medicine_stockbalance_reorder');
                                                            if($currentBalance<=$reOrderLevel){
                                                                $status = "Re-order";
                                                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                                ->update(array('medicine_stockbalance_status'=>$status) );
                                                            }
                                                            else{
                                                                $status = "";
                                                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacyMedicineKey)
                                                                ->update(array('medicine_stockbalance_status'=>$status) );
                                                            }

                                                            $transactionType = "Adjustment";
                                                                if($data['medicine_adjustment_name'] == 1){
                                                                    $transaction = new PharmacyMedicineTransaction;
                                                                    $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                    $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                    $transaction->pharmacy_medicine_transaction_date = $data['medicine_adjustment_date'];
                                                                    $transaction->adjustment_cancelled_iar_qty = $data['medicine_adjustment_qty'];
                                                                    $transaction->pharmacy_transaction_id = $medicineAdjustment_id;
                                                                    $transaction->pharmacy_transaction_type = $transactionType;
                                                                    $transaction->save();
                                                                }
                                                                    elseif($data['medicine_adjustment_name'] == 2){
                                                                        $transaction = new PharmacyMedicineTransaction;
                                                                        $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                        $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                        $transaction->pharmacy_medicine_transaction_date = $data['medicine_adjustment_date'];
                                                                        $transaction->adjustment_issued_employee_qty = $data['medicine_adjustment_qty'];
                                                                        $transaction->pharmacy_transaction_id = $medicineAdjustment_id;
                                                                        $transaction->pharmacy_transaction_type = $transactionType;
                                                                        $transaction->save();
                                                                    }
                                                                        elseif($data['medicine_adjustment_name'] == 3){
                                                                            $transaction = new PharmacyMedicineTransaction;
                                                                            $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                            $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                            $transaction->pharmacy_medicine_transaction_date = $data['medicine_adjustment_date'];
                                                                            $transaction->adjustment_overissued_qty = $data['medicine_adjustment_qty'];
                                                                            $transaction->pharmacy_transaction_id = $medicineAdjustment_id;
                                                                            $transaction->pharmacy_transaction_type = $transactionType;
                                                                            $transaction->save();
                                                                        }
                                                                            elseif($data['medicine_adjustment_name'] == 4){
                                                                                $transaction = new PharmacyMedicineTransaction;
                                                                                $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                                $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                                $transaction->pharmacy_medicine_transaction_date = $data['medicine_adjustment_date'];
                                                                                $transaction->adjustment_replacement_qty = $data['medicine_adjustment_qty'];
                                                                                $transaction->pharmacy_transaction_id = $medicineAdjustment_id;
                                                                                $transaction->pharmacy_transaction_type = $transactionType;
                                                                                $transaction->save();
                                                                            }
                                                                                elseif($data['medicine_adjustment_name'] == 5){
                                                                                    $transaction = new PharmacyMedicineTransaction;
                                                                                    $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                                    $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                                    $transaction->pharmacy_medicine_transaction_date = $data['medicine_adjustment_date'];
                                                                                    $transaction->adjustment_returned_ward_qty = $data['medicine_adjustment_qty'];
                                                                                    $transaction->pharmacy_transaction_id = $medicineAdjustment_id;
                                                                                    $transaction->pharmacy_transaction_type = $transactionType;
                                                                                    $transaction->save();
                                                                                }
                                                                                    elseif($data['medicine_adjustment_name'] == 6){
                                                                                        $transaction = new PharmacyMedicineTransaction;
                                                                                        $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                                        $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                                        $transaction->pharmacy_medicine_transaction_date = $data['medicine_adjustment_date'];
                                                                                        $transaction->adjustment_unissued_qty = $data['medicine_adjustment_qty'];
                                                                                        $transaction->pharmacy_transaction_id = $medicineAdjustment_id;
                                                                                        $transaction->pharmacy_transaction_type = $transactionType;
                                                                                        $transaction->save();
                                                                                    }
                                                                                        elseif($data['medicine_adjustment_name'] == 7){
                                                                                            $transaction = new PharmacyMedicineTransaction;
                                                                                            $transaction->pharmacy_medicine_key = $pharmacyMedicineKey;
                                                                                            $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                                                            $transaction->pharmacy_medicine_transaction_date = $data['medicine_adjustment_date'];
                                                                                            $transaction->adjustment_unrecorded_iar_qty = $data['medicine_adjustment_qty'];
                                                                                            $transaction->pharmacy_transaction_id = $medicineAdjustment_id;
                                                                                            $transaction->pharmacy_transaction_type = $transactionType;
                                                                                            $transaction->save();
                                                                                        }

                                                                                    return redirect('admin/medicine-adjustments')->with([
                                                                                        'message' => __('voyager::generic.successfully_added_new')." Adjustment",
                                                                                        'alert-type' => 'success']);
                                                    }
                                                    else{
                                                        return redirect('admin/medicine-adjustments')->with([
                                                            'message' => "Stock balance not change.",
                                                            'alert-type' => 'error']);
                                                    }
                                            }
                                            else{
                                                return redirect('admin/medicine-adjustments')->with([
                                                    'message' => "Medicine transaction not save.",
                                                    'alert-type' => 'error']);
                                            }           
                                    }
                                    else{
                                        return redirect('admin/medicine-adjustments')->with([
                                            'message' => "Adjustment not save.",
                                            'alert-type' => 'error']);
                                    }  
                            }
                    }
                    catch(Exception $e){
                        return redirect('path')->with([
                            'message' => "Error Saving",
                            'alert-type' => 'error']);
                    }
            }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;
        
        $medicineAdjustment = DB::table('medicine_adjustments')->where('medicine_adjustment_key', $id)->value('medicine_adjustment_qty');
        $medicineKey = DB::table('medicine_adjustments')->where('medicine_adjustment_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $retStockQUantity = $StockBalance - $medicineAdjustment;
        $newStockQuantity = $retStockQUantity + $request->input('medicine_adjustment_qty');
    
        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQuantity) );

        $medicine_adjustment = DB::table('medicine_adjustments')
            ->where('medicine_adjustment_key', $id)
            ->update(array('medicine_adjustment_qty'=> $request->input('medicine_adjustment_qty'),
                'medicine_adjustment_docno'=> $request->input('medicine_adjustment_docno'),
                'medicine_adjustment_name'=> $request->input('medicine_adjustment_name'),
                'medicine_adjustment_unit_cost'=> $request->input('medicine_adjustment_unit_cost'),
                'medicine_adjustment_total_cost'=> $request->input('medicine_adjustment_qty') * $request->input('medicine_adjustment_unit_cost'),
                'medicine_adjustment_date'=> $request->input('medicine_adjustment_date'),
                'medicine_adjustment_remarks'=> $request->input('medicine_adjustment_remarks')));

                $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }

                        $medicineTransaction = new MedicineTransaction();
                        $medicineTransaction->medicine_transaction_type_key = $id;
                        $medicineTransaction->medicine_transaction_type = "Adjustment";
                        $medicineTransaction->medicine_transaction_qty = $request->input('medicine_adjustment_qty');
                        $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                        $medicineTransaction->medicine_transaction_status = "Updated";

                            if($medicineTransaction->save()){
                                $transactionType = "Adjustment";
                                if($request->input('medicine_adjustment_name') == 1){
                                    $transaction = DB::table('pharmacy_medicine_transactions')
                                    ->where('pharmacy_transaction_id', $id)
                                    ->where('pharmacy_transaction_type', $transactionType)
                                        ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_adjustment_date'),
                                        'adjustment_cancelled_iar_qty'=> $request->input('medicine_adjustment_qty'),
                                        'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                        'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                        'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                }
                                    elseif($request->input('medicine_adjustment_name') == 2){
                                        $transaction = DB::table('pharmacy_medicine_transactions')
                                        ->where('pharmacy_transaction_id', $id)
                                        ->where('pharmacy_transaction_type', $transactionType)
                                            ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_adjustment_date'),
                                            'adjustment_cancelled_iar_qty'=> 0 ,
                                            'adjustment_issued_employee_qty'=> $request->input('medicine_adjustment_qty'), 'adjustment_overissued_qty' => 0,
                                            'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                            'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                    }
                                        elseif($request->input('medicine_adjustment_name') == 3){
                                            $transaction = DB::table('pharmacy_medicine_transactions')
                                            ->where('pharmacy_transaction_id', $id)
                                            ->where('pharmacy_transaction_type', $transactionType)
                                                ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_adjustment_date'),
                                                'adjustment_cancelled_iar_qty'=> 0 ,
                                                'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => $request->input('medicine_adjustment_qty'),
                                                'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                                'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                        }
                                            elseif($request->input('medicine_adjustment_name') == 4){
                                                $transaction = DB::table('pharmacy_medicine_transactions')
                                                ->where('pharmacy_transaction_id', $id)
                                                ->where('pharmacy_transaction_type', $transactionType)
                                                    ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_adjustment_date'),
                                                    'adjustment_cancelled_iar_qty'=> 0 ,
                                                    'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                    'adjustment_replacement_qty'=> $request->input('medicine_adjustment_qty'), 'adjustment_returned_ward_qty'=> 0,
                                                    'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                            }
                                                elseif($request->input('medicine_adjustment_name') == 5){
                                                    $transaction = DB::table('pharmacy_medicine_transactions')
                                                    ->where('pharmacy_transaction_id', $id)
                                                    ->where('pharmacy_transaction_type', $transactionType)
                                                        ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_adjustment_date'),
                                                        'adjustment_cancelled_iar_qty'=> 0 ,
                                                        'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                        'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> $request->input('medicine_adjustment_qty'),
                                                        'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> 0));
                                                }
                                                    elseif($request->input('medicine_adjustment_name') == 6){
                                                        $transaction = DB::table('pharmacy_medicine_transactions')
                                                        ->where('pharmacy_transaction_id', $id)
                                                        ->where('pharmacy_transaction_type', $transactionType)
                                                            ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_adjustment_date'),
                                                            'adjustment_cancelled_iar_qty'=> 0 ,
                                                            'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                            'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                                            'adjustment_unissued_qty'=> $request->input('medicine_adjustment_qty'), 'adjustment_unrecorded_iar_qty'=> 0));
                                                    }
                                                        elseif($request->input('medicine_adjustment_name') == 7){
                                                            $transaction = DB::table('pharmacy_medicine_transactions')
                                                            ->where('pharmacy_transaction_id', $id)
                                                            ->where('pharmacy_transaction_type', $transactionType)
                                                                ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_adjustment_date'),
                                                                'adjustment_cancelled_iar_qty'=> 0 ,
                                                                'adjustment_issued_employee_qty'=> 0, 'adjustment_overissued_qty' => 0,
                                                                'adjustment_replacement_qty'=> 0, 'adjustment_returned_ward_qty'=> 0,
                                                                'adjustment_unissued_qty'=> 0, 'adjustment_unrecorded_iar_qty'=> $request->input('medicine_adjustment_qty')));
                                                        }                            
                            }
                            else{
                                return redirect('admin/medicine-adjustments')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => 'error']); 
                            }
                
                        return redirect('admin/medicine-adjustments')->with([
                            'message' => __('voyager::generic.successfully_updated')." Adjustment",
                            'alert-type' => 'success']);
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $medicineAdjustmentQty = DB::table('medicine_adjustments')->where('medicine_adjustment_key', $id)->value('medicine_adjustment_qty');
        $medicineKey = DB::table('medicine_adjustments')->where('medicine_adjustment_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $newStockQUantity = $StockBalance - $medicineAdjustmentQty;

        $deleted_adjustment = DB::table('medicine_adjustments')->where('medicine_adjustment_key', $id)->delete();

        $newMedicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQUantity) );

            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                    if($currentBalance<=$reOrderLevel){
                        $status = "Re-order";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }
                    else{
                        $status = "";
                        $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                        ->update(array('medicine_stockbalance_status'=>$status) );
                    }

                        $medicineTransaction = new MedicineTransaction();
                        $medicineTransaction->medicine_transaction_type_key = $id;
                        $medicineTransaction->medicine_transaction_type = "Adjustment";
                        $medicineTransaction->medicine_transaction_qty = $medicineAdjustmentQty;
                        $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                        $medicineTransaction->medicine_transaction_status = "Deleted";

                            if($medicineTransaction->save()){
                                $deleted_transaction = DB::table('pharmacy_medicine_transactions')
                                    ->where('pharmacy_transaction_id', $id)
                                    ->where('pharmacy_transaction_type', "Adjustment")
                                    ->delete();
                            }
                            else{
                                return redirect('admin/medicine-adjustments')->with([
                                    'message' => "Transaction not save.",
                                    'alert-type' => 'error']); 
                            }

                            return redirect('admin/medicine-adjustments')->with([
                                'message' => __('voyager::generic.successfully_deleted')." Adjustment",
                                'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/medicine-adjustments')->with([
            'message' => __('voyager::generic.successfully_restored')." Adjustment",
            'alert-type' => 'success']);
    }
}