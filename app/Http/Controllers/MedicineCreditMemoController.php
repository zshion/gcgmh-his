<?php

namespace App\Http\Controllers;
use App\MedicineCreditMemo;
use App\MedicineStockbalance;
use App\MedicineTransaction;
use App\PharmacyMedicineTransaction;
use Illuminate\Http\Request;
use TCG\Voyager\FormFields\AbstractHandler;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataRestored;
use TCG\Voyager\Facades\Voyager;
use DB;
use Illuminate\View\View;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MedicineCreditMemoController extends \TCG\Voyager\Http\Controllers\VoyagerBaseController
{
    public function store(Request $request){
        $rules = [
            'pharmacy_medicine_key' => 'required',
            'medicine_credit_memo_qty'  => 'required',
            'medicine_credit_memo_date'  => 'required'
         ];
         $validator = Validator::make($request->all(),$rules);
            if ($validator->fails()) {
                return redirect('admin/medicine-credit-memos/create')
                    ->withInput()
                    ->withErrors($validator);
            }
            else{
                $data = $request->input();
                try{
                    $pharmacy_medicine_key = $data['pharmacy_medicine_key'];
                    $itemTypeKey = DB::table('pharmacy_medicines')->where('pharmacy_medicine_key',$pharmacy_medicine_key)->value('pharmacy_item_type_key');
                    $checkStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->doesntExist();

                        if($checkStockBalance){
                            return redirect('admin/medicine-credit-memos/create')
                                ->withInput()
                                ->withErrors([
                                    'message' => "No stock balance on the item."]);
                        }
                        else{
                            $medicineCreditMemo = new MedicineCreditMemo();
    
                            $medicineCreditMemo->medicine_credit_memo_docno = $data['medicine_credit_memo_docno'];
                            $medicineCreditMemo->medicine_credit_memo_doc_date = $data['medicine_credit_memo_doc_date'];
                            $medicineCreditMemo->medicine_credit_memo_qty = $data['medicine_credit_memo_qty'];
                            $medicineCreditMemo->medicine_credit_memo_unit_cost = $data['medicine_credit_memo_unit_cost'];
                            $medicineCreditMemo->medicine_credit_memo_total_cost = $data['medicine_credit_memo_unit_cost'] * $data['medicine_credit_memo_qty'];
                            $medicineCreditMemo->medicine_credit_memo_remarks = $data['medicine_credit_memo_remarks'];
                            $medicineCreditMemo->pharmacy_medicine_key = $data['pharmacy_medicine_key'];
                            $medicineCreditMemo->medicine_credit_memo_date= $data['medicine_credit_memo_date'];
                            $medicineCreditMemo->supplier_key = $data['supplier_key'];

                            
                            $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->value('medicine_stockbalance_current');
                            
                                if($medicineCreditMemo->save()){
                                    $medicineCreditMemo_id = $medicineCreditMemo->medicine_credit_memo_key;

                                    $medicineTransacation = new MedicineTransaction();
                                    $medicineTransactionType = "Credit Memo";
                                    $medicineTransacation->medicine_transaction_type_key = $medicineCreditMemo_id;
                                    $medicineTransacation->medicine_transaction_type = $medicineTransactionType;
                                    $medicineTransacation->medicine_transaction_qty = $data['medicine_credit_memo_qty'];
                                    $medicineTransacation->medicine_transaction_beginning_qty = $StockBalance;
                                    $medicineTransacation->medicine_transaction_status = "Added";
                                        
                                        if($medicineTransacation->save()){
                                            $balance = MedicineStockbalance::where('pharmacy_medicine_key','=',$pharmacy_medicine_key)->firstOrFail();
                                            $balance_current = $balance->medicine_stockbalance_current;
                                            $balance->medicine_stockbalance_current = $balance_current - $data['medicine_credit_memo_qty'];
                                        
                                                if($balance->save()) {
                                                    $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->value('medicine_stockbalance_current');
                                                    $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)->value('medicine_stockbalance_reorder');
                                                        if($currentBalance<=$reOrderLevel){
                                                            $status = "Re-order";
                                                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)
                                                            ->update(array('medicine_stockbalance_status'=>$status) );
                                                        }
                                                        else{
                                                            $status = "";
                                                            $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $pharmacy_medicine_key)
                                                            ->update(array('medicine_stockbalance_status'=>$status) );
                                                        }
                                                        
                                                            $transaction = new PharmacyMedicineTransaction;
                                                            $transaction->pharmacy_medicine_key = $pharmacy_medicine_key;
                                                            $transaction->pharmacy_medicine_type_key = $itemTypeKey;
                                                            $transaction->pharmacy_medicine_transaction_date = $data['medicine_credit_memo_date'];
                                                            $transaction->medicine_credit_memo_qty = $data['medicine_credit_memo_qty'];
                                                            $transaction->pharmacy_transaction_id = $medicineCreditMemo_id;
                                                            $transaction->pharmacy_transaction_type = "Credit_Memo";
                                                            $transaction->save();

                                                                return redirect('admin/medicine-credit-memos')->with([
                                                                    'message' => __('voyager::generic.successfully_added_new')." Credit Memo",
                                                                    'alert-type' => 'success']);
                                                }  
                                        }
                                        else{
                                            return redirect('admin/medicine-credit-memos')->with([
                                                'message' => "Medicine transaction not save.",
                                                'alert-type' => 'error']);
                                        }
                                }
                                else{
                                    return redirect('admin/medicine-credit-memos/create')->with([
                                        'message' => "Error Saving",
                                        'alert-type' => 'error']);
                                }
                        }   
                }
                catch(Exception $e){
                    return redirect('path')->with([
                        'message' => "Error Saving",
                        'alert-type' => 'error']);
                } 
            }
    }

    public function update(Request $request, $id) {

        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

                $medicineCreditMemo = DB::table('medicine_credit_memos')->where('medicine_credit_memo_key', $id)->value('medicine_credit_memo_qty');
                $medicineKey = DB::table('medicine_credit_memos')->where('medicine_credit_memo_key', $id)->value('pharmacy_medicine_key');
                $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

                $retStockQUantity =  $medicineCreditMemo + $StockBalance ;
                $newStockQuantity = $retStockQUantity - $request->input('medicine_credit_memo_qty');
            
                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_current'=>$newStockQuantity) );

                $medicine_credit_memo = DB::table('medicine_credit_memos')
                    ->where('medicine_credit_memo_key', $id)
                    ->update(array('medicine_credit_memo_qty'=> $request->input('medicine_credit_memo_qty'),
                        'medicine_credit_memo_docno'=> $request->input('medicine_credit_memo_docno'),
                        'medicine_credit_memo_doc_date'=> $request->input('medicine_credit_memo_doc_date'),
                        'medicine_credit_memo_unit_cost'=> $request->input('medicine_credit_memo_unit_cost'),
                        'medicine_credit_memo_total_cost'=> $request->input('medicine_credit_memo_qty') * $request->input('medicine_credit_memo_unit_cost'),
                        'medicine_credit_memo_remarks'=> $request->input('medicine_credit_memo_remarks'),
                        'medicine_credit_memo_date' => $request->input('medicine_credit_memo_date'),
                        'supplier_key'=> $request->input('supplier_key')));

                        $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
                        $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                            if($currentBalance<=$reOrderLevel){
                                $status = "Re-order";
                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                ->update(array('medicine_stockbalance_status'=>$status) );
                            }
                            else{
                                $status = "";
                                $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                                ->update(array('medicine_stockbalance_status'=>$status) );
                            }

                                $medicineTransaction = new MedicineTransaction();
                                $medicineTransactionType = "Credit Memo";
                                $medicineTransaction->medicine_transaction_type_key = $id;
                                $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                                $medicineTransaction->medicine_transaction_qty = $request->input('medicine_credit_memo_qty');
                                $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                                $medicineTransaction->medicine_transaction_status = "Updated";    

                                    if($medicineTransaction->save()){
                                        $transaction = DB::table('pharmacy_medicine_transactions')
                                        ->where('pharmacy_transaction_id', $id)
                                        ->where('pharmacy_transaction_type', "Credit_Memo")
                                            ->update( array('pharmacy_medicine_transaction_date'=> $request->input('medicine_credit_memo_date'),
                                            'medicine_credit_memo_qty'=> $request->input('medicine_credit_memo_qty') ));
                                    }
                                    else{
                                        return redirect('admin/medicine-credit-memos')->with([
                                            'message' => "Transaction not save.",
                                            'alert-type' => 'error']); 
                                    }
                            
                                    return redirect('admin/medicine-credit-memos')->with([
                                        'message' => __('voyager::generic.successfully_updated')." Credit Memo",
                                        'alert-type' => 'success']);     
    }

    public function destroy(Request $request, $id)
    {
        $id = $id instanceof \Illuminate\Database\Eloquent\Model ? $id->{$id->getKeyName()} : $id;

        $medicineCreditMemoQty = DB::table('medicine_credit_memos')->where('medicine_credit_memo_key', $id)->value('medicine_credit_memo_qty');
        $medicineKey = DB::table('medicine_credit_memos')->where('medicine_credit_memo_key', $id)->value('pharmacy_medicine_key');
        $StockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');

        $newStockQUantity =  $medicineCreditMemoQty + $StockBalance;

        $deleted_creditMemo = DB::table('medicine_credit_memos')->where('medicine_credit_memo_key', $id)->delete();

        $newMedicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
            ->update(array('medicine_stockbalance_current'=>$newStockQUantity) );
        
            $currentBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_current');
            $reOrderLevel = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)->value('medicine_stockbalance_reorder');
                if($currentBalance<=$reOrderLevel){
                    $status = "Re-order";
                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_status'=>$status) );
                }
                else{
                    $status = "";
                    $medicineStockBalance = DB::table('medicine_stockbalances')->where('pharmacy_medicine_key', $medicineKey)
                    ->update(array('medicine_stockbalance_status'=>$status) );
                }

                    $medicineTransaction = new MedicineTransaction();
                    $medicineTransactionType = "Credit Memo";
                    $medicineTransaction->medicine_transaction_type_key = $id;
                    $medicineTransaction->medicine_transaction_type = $medicineTransactionType;
                    $medicineTransaction->medicine_transaction_qty = $medicineCreditMemoQty;
                    $medicineTransaction->medicine_transaction_beginning_qty = $StockBalance;
                    $medicineTransaction->medicine_transaction_status = "Deleted";

                        if($medicineTransaction->save()){
                            $deleted_transaction = DB::table('pharmacy_medicine_transactions')
                            ->where('pharmacy_transaction_id', $id)
                            ->where('pharmacy_transaction_type', "Credit_Memo")
                            ->delete();
                        }
                        else{
                            return redirect('admin/medicine-credit-memos')->with([
                                'message' => "Transaction not save.",
                                'alert-type' => 'error']); 
                        }

                            return redirect('admin/medicine-credit-memos')->with([
                                'message' => __('voyager::generic.successfully_deleted')." Credit Memo",
                                'alert-type' => 'success']);
    }

    public function restore(Request $request, $id)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Get record
        $model = call_user_func([$dataType->model_name, 'withTrashed']);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $model = $model->{$dataType->scope}();
        }
        $data = $model->findOrFail($id);

        $displayName = $dataType->getTranslatedAttribute('display_name_singular');

        $res = $data->restore($id);
        $data = $res
            ? [
                'message'    => __('voyager::generic.successfully_restored')." {$displayName}",
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('voyager::generic.error_restoring')." {$displayName}",
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataRestored($dataType, $data));
        }

        return redirect('admin/medicine-credit-memos')->with([
            'message' => __('voyager::generic.successfully_restored')." Credit Memo",
            'alert-type' => 'success']);
    }
}