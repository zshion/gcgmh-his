<?php

namespace App\Repositories;

use App\Models\MedicineAdjustment;
use App\Repositories\BaseRepository;

/**
 * Class MedicineAdjustmentRepository
 * @package App\Repositories
 * @version November 5, 2020, 3:59 pm HKT
*/

class MedicineAdjustmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicineAdjustment::class;
    }
}
