<?php

namespace App\Repositories;

use App\Models\MedicineStockbalance;
use App\Repositories\BaseRepository;

/**
 * Class MedicineStockbalanceRepository
 * @package App\Repositories
 * @version November 12, 2020, 2:11 pm HKT
*/

class MedicineStockbalanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicineStockbalance::class;
    }
}
