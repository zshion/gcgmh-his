<?php

namespace App\Repositories;

use App\Models\MedicineCreditMemo;
use App\Repositories\BaseRepository;

/**
 * Class MedicineCreditMemoRepository
 * @package App\Repositories
 * @version November 6, 2020, 9:12 am HKT
*/

class MedicineCreditMemoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicineCreditMemo::class;
    }
}
