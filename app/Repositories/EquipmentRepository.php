<?php

namespace App\Repositories;

use App\Models\Equipment;
use App\Repositories\BaseRepository;

/**
 * Class EquipmentRepository
 * @package App\Repositories
 * @version June 3, 2020, 9:05 am UTC
*/

class EquipmentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'equipment_name',
        'equipment_serial',
        'equipment_model',
        'equipment_quantity',
        'equipment_description',
        'equipment_brand',
        'equipment_country',
        'equipment_date_delivered',
        'equipment_date_inspected',
        'equipment_date_acquired',
        'equipment_supplier',
        'equipment_unit_price',
        'equipment_location',
        'equipment_tag_number',
        'equipment_card_number',
        'equipment_memorandum_receipt',
        'equipment_iar_number',
        'equipment_asset_categories',
        'equipment_account_code',
        'equipment_purchase_order'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Equipment::class;
    }
}
