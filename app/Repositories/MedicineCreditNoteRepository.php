<?php

namespace App\Repositories;

use App\Models\MedicineCreditNote;
use App\Repositories\BaseRepository;

/**
 * Class MedicineCreditNoteRepository
 * @package App\Repositories
 * @version October 30, 2020, 10:51 am HKT
*/

class MedicineCreditNoteRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicineCreditNote::class;
    }
}
