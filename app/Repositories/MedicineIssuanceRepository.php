<?php

namespace App\Repositories;

use App\Models\MedicineIssuance;
use App\Repositories\BaseRepository;

/**
 * Class MedicineIssuanceRepository
 * @package App\Repositories
 * @version September 11, 2020, 6:24 am UTC
*/

class MedicineIssuanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicineIssuance::class;
    }
}
