<?php

namespace App\Repositories;

use App\Models\MedicineDelivery;
use App\Repositories\BaseRepository;

/**
 * Class MedicineDeliveryRepository
 * @package App\Repositories
 * @version November 5, 2020, 5:00 pm HKT
*/

class MedicineDeliveryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicineDelivery::class;
    }
}
