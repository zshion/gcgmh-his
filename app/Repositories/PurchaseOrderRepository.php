<?php

namespace App\Repositories;

use App\Models\PurchaseOrder;
use App\Repositories\BaseRepository;

/**
 * Class PurchaseOrderRepository
 * @package App\Repositories
 * @version June 3, 2020, 5:24 am UTC
*/

class PurchaseOrderRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'purchase_order_number',
        'purchase_order_pr_number',
        'purchase_order_mode_of_procurement',
        'purchase_order_date_awarded',
        'purchase_order_date_of_po_received',
        'purchase_order_date_delivered',
        'purchase_order_inspected',
        'purchase_order_date_acquired'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PurchaseOrder::class;
    }
}
