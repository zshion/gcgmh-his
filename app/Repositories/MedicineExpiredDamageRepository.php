<?php

namespace App\Repositories;

use App\Models\MedicineExpiredDamage;
use App\Repositories\BaseRepository;

/**
 * Class MedicineExpiredDamageRepository
 * @package App\Repositories
 * @version November 6, 2020, 9:29 am HKT
*/

class MedicineExpiredDamageRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicineExpiredDamage::class;
    }
}
