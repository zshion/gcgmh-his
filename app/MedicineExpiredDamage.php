<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jahondust\ModelLog\Traits\ModelLogging;
use Spatie\Activitylog\Traits\LogsActivity;


class MedicineExpiredDamage extends Model
{
    use LogsActivity,ModelLogging,SoftDeletes;


   // use ModelLogging;

    protected $primaryKey = 'medicine_expired_damaged_key';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;


    protected $dates = ['deleted_at'];

}
