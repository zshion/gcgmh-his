<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jahondust\ModelLog\Traits\ModelLogging;
use Spatie\Activitylog\Traits\LogsActivity;

class PharmacyMedicine extends Model
{
    use LogsActivity,ModelLogging,SoftDeletes;

    /*protected $table = 'student_details';
    public $timestamps = true;

    protected $fillable = [
        'first_name', 'last_name','city_name', 'email',
    ];*/


   // use ModelLogging;

    protected $primaryKey = 'pharmacy_medicine_key';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;


    protected $dates = ['deleted_at'];
}
