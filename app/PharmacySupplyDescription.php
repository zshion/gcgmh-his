<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jahondust\ModelLog\Traits\ModelLogging;
use Spatie\Activitylog\Traits\LogsActivity;

class PharmacySupplyDescription extends Model
{
    use LogsActivity,ModelLogging,SoftDeletes;

   // use ModelLogging;

    protected $primaryKey = 'pharmacy_supply_description_key';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;


    protected $dates = ['deleted_at'];
}
