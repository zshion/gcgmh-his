<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MedicineCreditMemo
 * @package App\Models
 * @version November 6, 2020, 9:12 am HKT
 *
 */
class MedicineCreditMemo extends Model
{
    use SoftDeletes;

    public $table = 'medicine_credit_memos';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
