<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MedicineAdjustment
 * @package App\Models
 * @version November 5, 2020, 3:59 pm HKT
 *
 */
class MedicineAdjustment extends Model
{
    use SoftDeletes;

    public $table = 'medicine_adjustments';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
