<?php

namespace App\Models;

use Collective\Html\Eloquent;
//use Eloquent as Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Equipment
 * @package App\Models
 * @version June 3, 2020, 9:05 am UTC
 *
 * @property string $equipment_name
 * @property string $equipment_serial
 * @property string $equipment_model
 * @property integer $equipment_quantity
 * @property string $equipment_description
 * @property string $equipment_brand
 * @property string $equipment_country
 * @property string|\Carbon\Carbon $equipment_date_delivered
 * @property string|\Carbon\Carbon $equipment_date_inspected
 * @property string|\Carbon\Carbon $equipment_date_acquired
 * @property string $equipment_supplier
 * @property integer $equipment_unit_price
 * @property string $equipment_location
 * @property string $equipment_tag_number
 * @property string $equipment_card_number
 * @property string $equipment_memorandum_receipt
 * @property string $equipment_iar_number
 * @property string $equipment_asset_categories
 * @property string $equipment_account_code
 * @property string $equipment_purchase_order
 */
class Equipment extends Model
{
    use SoftDeletes;

    public $table = 'equipment';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];
    protected $primaryKey = 'equipment_key';



    public $fillable = [
        'equipment_name',
        'equipment_serial',
        'equipment_model',
        'equipment_quantity',
        'equipment_description',
        'equipment_brand',
        'equipment_country',
        'equipment_date_delivered',
        'equipment_date_inspected',
        'equipment_date_acquired',
        'equipment_supplier',
        'equipment_unit_price',
        'equipment_location',
        'equipment_tag_number',
        'equipment_card_number',
        'equipment_memorandum_receipt',
        'equipment_iar_number',
        'equipment_asset_categories',
        'equipment_account_code',
        'equipment_purchase_order'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'equipment_key' => 'integer',
        'equipment_name' => 'string',
        'equipment_serial' => 'string',
        'equipment_model' => 'string',
        'equipment_quantity' => 'integer',
        'equipment_description' => 'string',
        'equipment_brand' => 'string',
        'equipment_country' => 'string',
        'equipment_date_delivered' => 'datetime',
        'equipment_date_inspected' => 'datetime',
        'equipment_date_acquired' => 'datetime',
        'equipment_supplier' => 'string',
        'equipment_unit_price' => 'integer',
        'equipment_location' => 'string',
        'equipment_tag_number' => 'string',
        'equipment_card_number' => 'string',
        'equipment_memorandum_receipt' => 'string',
        'equipment_iar_number' => 'string',
        'equipment_asset_categories' => 'string',
        'equipment_account_code' => 'string',
        'equipment_purchase_order' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
