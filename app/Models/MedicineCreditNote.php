<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MedicineCreditNote
 * @package App\Models
 * @version October 30, 2020, 10:51 am HKT
 *
 */
class MedicineCreditNote extends Model
{
    use SoftDeletes;

    public $table = 'medicine_credit_notes';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
