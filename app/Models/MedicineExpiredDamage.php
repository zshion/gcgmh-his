<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MedicineExpiredDamage
 * @package App\Models
 * @version November 6, 2020, 9:29 am HKT
 *
 */
class MedicineExpiredDamage extends Model
{
    use SoftDeletes;

    public $table = 'medicine_expired_damages';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
