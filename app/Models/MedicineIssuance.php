<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MedicineIssuance
 * @package App\Models
 * @version September 11, 2020, 6:24 am UTC
 *
 */
class MedicineIssuance extends Model
{
    use SoftDeletes;

    public $table = 'medicine_issuances';
    protected $primaryKey = 'medicine_issuance_key';


    protected $dates = ['deleted_at'];



    public $fillable = [

    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];


}
