<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class PurchaseOrder
 * @package App\Models
 * @version June 3, 2020, 5:24 am UTC
 *
 * @property integer $purchase_order_number
 * @property integer $purchase_order_pr_number
 * @property string $purchase_order_mode_of_procurement
 * @property string|\Carbon\Carbon $purchase_order_date_awarded
 * @property string|\Carbon\Carbon $purchase_order_date_of_po_received
 * @property string|\Carbon\Carbon $purchase_order_date_delivered
 * @property string|\Carbon\Carbon $purchase_order_inspected
 * @property string|\Carbon\Carbon $purchase_order_date_acquired
 */
class PurchaseOrder extends Model
{
    use SoftDeletes;

    public $table = 'purchase_orders';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'purchase_order_number',
        'purchase_order_pr_number',
        'purchase_order_mode_of_procurement',
        'purchase_order_date_awarded',
        'purchase_order_date_of_po_received',
        'purchase_order_date_delivered',
        'purchase_order_inspected',
        'purchase_order_date_acquired'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'purchase_order_key' => 'integer',
        'purchase_order_number' => 'integer',
        'purchase_order_pr_number' => 'integer',
        'purchase_order_mode_of_procurement' => 'string',
        'purchase_order_date_awarded' => 'datetime',
        'purchase_order_date_of_po_received' => 'datetime',
        'purchase_order_date_delivered' => 'datetime',
        'purchase_order_inspected' => 'datetime',
        'purchase_order_date_acquired' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
