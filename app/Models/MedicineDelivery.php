<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MedicineDelivery
 * @package App\Models
 * @version November 5, 2020, 5:00 pm HKT
 *
 */
class MedicineDelivery extends Model
{
    use SoftDeletes;

    public $table = 'medicine_deliveries';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
