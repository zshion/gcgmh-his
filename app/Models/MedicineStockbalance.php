<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class MedicineStockbalance
 * @package App\Models
 * @version November 12, 2020, 2:11 pm HKT
 *
 */
class MedicineStockbalance extends Model
{
    use SoftDeletes;

    public $table = 'medicine_stockbalances';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
