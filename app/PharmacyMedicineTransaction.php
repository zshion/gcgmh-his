<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Jahondust\ModelLog\Traits\ModelLogging;
use Spatie\Activitylog\Traits\LogsActivity;

class PharmacyMedicineTransaction extends Model
{
    use LogsActivity,ModelLogging,SoftDeletes;

   // use ModelLogging;

    protected $primaryKey = 'pharmacy_medicine_transactions_key';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;


    protected $dates = ['deleted_at'];

    public function pharmacymedicine()
    {
        return $this->belongsTo('App\Models\PharmacyMedicine', 'pharmacy_medicine_key','pharmacy_medicine_key');
    }



}
