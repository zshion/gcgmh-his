<?php

namespace App\DataTables;

use App\MedicineDelivery;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MedicineDeliveryReportsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);

    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\MedicineDelivery $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MedicineDelivery $model)
    {
        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $data = MedicineDelivery::select('*')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('medicine_deliveries.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('medicine_deliveries.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            })
            ->leftjoin('suppliers', function($join) {
                $join->on('medicine_deliveries.supplier_key', '=', 'suppliers.supplier_key');
            });

            if($start_date && $end_date){
                $data = MedicineDelivery::select('*')
                    ->leftjoin('pharmacy_medicines', function($join) {
                        $join->on('medicine_deliveries.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                    })
                    ->leftjoin('pharmacy_item_types', function($join) {
                        $join->on('medicine_deliveries.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
                    })
                    ->leftjoin('suppliers', function($join) {
                        $join->on('medicine_deliveries.supplier_key', '=', 'suppliers.supplier_key');
                    })
                    ->whereBetween('medicine_deliveries.medicine_delivery_date',array($start_date,$end_date))
                    ->where('pharmacy_item_types.pharmacy_item_type_key',$type);
            }
            return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('medicinedeliveryreports-table')
                    ->columns($this->getColumns())
                    ->ajax([
                        'url' =>'',
                        'type' => 'GET',
                        'data' => 'function (d) {
                            d.start_date = $("#start_date").val();
                            d.end_date = $("#end_date").val();
                            d.type = $("#type").val();
                             console.log(d);
                        }',
                    ])
                    ->dom('Bfrtip')
                    ->orderBy([0, 'desc'])
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
           /* Column::computed('action')
                  ->exportable(false)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'), */
            'delivery date'   => ['data' => 'medicine_delivery_date', 'name' => 'medicine_deliveries.medicine_delivery_date'],
            'mode'   => ['data' => 'medicine_delivery_po_mode', 'name' => 'medicine_deliveries.medicine_delivery_po_mode'],
            'iar no.'   => ['data' => 'medicine_delivery_iar', 'name' => 'medicine_deliveries.medicine_delivery_iar'],
            'iar date'   => ['data' => 'medicine_delivery_iar_date', 'name' => 'medicine_deliveries.medicine_delivery_iar_date'],
            'po no.'   => ['data' => 'medicine_delivery_po_no', 'name' => 'medicine_deliveries.medicine_delivery_po_no'],
            'po date'   => ['data' => 'medicine_delivery_po_date', 'name' => 'medicine_deliveries.medicine_delivery_po_date'],
            'supplier'   => ['data' => 'supplier_name', 'name' => 'suppliers.supplier_name'],
            'item description'   => ['data' => 'pharmacy_medicine_name', 'name' => 'pharmacy_medicines.pharmacy_medicine_name'],
            'brand name'   => ['data' => 'medicine_delivery_brand_name', 'name' => 'medicine_deliveries.medicine_delivery_brand_name'],
            'qty'   => ['data' => 'medicine_delivery_qty', 'name' => 'medicine_deliveries.medicine_delivery_qty'],
            'unit cost'   => ['data' => 'medicine_delivery_unit_cost', 'name' => 'medicine_deliveries.medicine_delivery_unit_cost'],
            'total cost'   => ['data' => 'medicine_delivery_total_cost', 'name' => 'medicine_deliveries.medicine_delivery_total_cost'],
            'expiry date'   => ['data' => 'medicine_delivery_expiry_date', 'name' => 'medicine_deliveries.medicine_delivery_expiry_date'],
            'location'   => ['data' => 'medicine_delivery_location', 'name' => 'medicine_deliveries.medicine_delivery_location'],
            'lot no.'   => ['data' => 'medicine_delivery_lotno', 'name' => 'medicine_deliveries.medicine_delivery_lotno'],
            'batch no.'   => ['data' => 'medicine_delivery_batch_no', 'name' => 'medicine_deliveries.medicine_delivery_batch_no'],
            'with guarantee'   => ['data' => 'medicine_delivery_guarantee', 'name' => 'medicine_deliveries.medicine_delivery_guarantee'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'MedicineDeliveryReports_' . date('YmdHis');
    }
}
