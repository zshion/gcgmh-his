<?php

namespace App\DataTables;

use App\MedicineIssuance;
use App\PharmacyMedicineTransaction;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class IssuancesReportsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
       /* $dt = date("Y-m-d");
        $startdate =date("Y-m-27", strtotime("first day of last month"));
        $enddate = date("Y-m-26", strtotime($dt));

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $medicineIssuances = \Illuminate\Support\Facades\DB::table('pharmacy_medicine_transactions as medicines')
            ->selectRaw('*,SUM(medicine_delivery_qty) AS delivery,
            SUM(medicine_issuance_qty) AS issuance,
            SUM(adjustment_unrecorded_iar_qty) AS unrecorded,
            SUM(adjustment_cancelled_iar_qty) AS cancelled,
            SUM(adjustment_overissued_qty) AS overissued,
            SUM(adjustment_unissued_qty) AS unissued,
            SUM(adjustment_returned_ward_qty) AS ward,
            SUM(adjustment_issued_employee_qty) AS employee,
            SUM(adjustment_replacement_qty) AS replacement,
            SUM(medicine_expired_qty) AS expired,
            SUM(medicine_damage_qty) AS damaged,
            SUM(medicine_credit_memo_qty) AS memo,
            SUM(medicine_credit_note_qty) AS note,
            medicine_stockbalance_beginning AS beginning,
            medicine_stockbalance_current AS ending


             ')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('medicine_stockbalances', function($join) {
                $join->on('medicines.pharmacy_medicine_key', '=', 'medicine_stockbalances.pharmacy_medicine_key');
            })

            ->whereBetween('pharmacy_medicine_transaction_date',array($startdate,$enddate))
            ->groupBy('medicines.pharmacy_medicine_key')
            ->get();



        if($start_date && $end_date){

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));

            $medicineIssuances = \Illuminate\Support\Facades\DB::table('pharmacy_medicine_transactions as medicines')
                ->selectRaw('*,SUM(medicine_delivery_qty) AS delivery,
            SUM(medicine_issuance_qty) AS issuance,
            SUM(adjustment_unrecorded_iar_qty) AS unrecorded,
            SUM(adjustment_cancelled_iar_qty) AS cancelled,
            SUM(adjustment_overissued_qty) AS overissued,
            SUM(adjustment_unissued_qty) AS unissued,
            SUM(adjustment_returned_ward_qty) AS ward,
            SUM(adjustment_issued_employee_qty) AS employee,
            SUM(adjustment_replacement_qty) AS replacement,
            SUM(medicine_expired_qty) AS expired,
            SUM(medicine_damage_qty) AS damaged,
            SUM(medicine_credit_memo_qty) AS memo,
            SUM(medicine_credit_note_qty) AS note,
            pharmacy_beginning_balance AS beginning,

            ( (pharmacy_beginning_balance + (SUM(medicine_delivery_qty))) - ((SUM(adjustment_unrecorded_iar_qty))  + (SUM(adjustment_overissued_qty)) + (SUM(adjustment_returned_ward_qty))
            + (SUM(adjustment_replacement_qty)) + (SUM(adjustment_cancelled_iar_qty)) + (SUM(adjustment_unissued_qty)) + (SUM(adjustment_issued_employee_qty))
            + (SUM(medicine_issuance_qty)) + (SUM(medicine_credit_note_qty)) + (SUM(medicine_damage_qty)) + (SUM(medicine_expired_qty) + (SUM(medicine_credit_memo_qty))) )) AS ending

             ')
                ->leftjoin('pharmacy_medicines', function($join) {
                    $join->on('medicines.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                })
                ->leftjoin('pharmacy_end_balances', function($join) {
                    $join->on('medicines.pharmacy_medicine_key', '=','pharmacy_end_balances.pharmacy_medicine_key');
                })

                ->whereBetween('medicines.pharmacy_medicine_transaction_date',array($start_date,$end_date))
                ->where('medicines.pharmacy_medicine_type_key',$type)
                ->where('pharmacy_end_balances.pharmacy_end_balance_date',date("Y-m-26", strtotime($start_date)))
                ->groupBy('medicines.pharmacy_medicine_key')
                ->get();

        }
        //
        return datatables()->of($medicineIssuances)
            ->addColumn('action', function($row) {
                return '<a href="/admin/pharmacy-medicines/'. $row->pharmacy_medicine_key .'/edit" class="btn btn-primary">Edit</a>';
            })
            ->editColumn('delete', function ($row) {
                return '<a href="#">delete</a>';
            })
            ->rawColumns(['delete' => 'delete','action' => 'action'])
            //->make(true);
            ->make(true);*/
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'issuancesreports.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PharmacyMedicineTransaction $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PharmacyMedicineTransaction $model)
    {
/*
        return $model->newQuery()
                   //->orderby('pharmacy_medicine_transactions.pharmacy_medicine_key')
            ;*/

        $dt = date("Y-m-d");
        $startdate =date("Y-m-27", strtotime("first day of last month"));
        $enddate = date("Y-m-26", strtotime($dt));

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');



        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $data = PharmacyMedicineTransaction::select('pharmacy_medicine_transactions.pharmacy_medicine_key')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('pharmacy_medicine_transactions.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->orderby('pharmacy_medicine_transactions.pharmacy_medicine_transactions_key');

        if($start_date && $end_date) {
            $data = PharmacyMedicineTransaction::select('pharmacy_medicine_transactions.pharmacy_medicine_key')
                ->leftjoin('pharmacy_medicines', function($join) {
                    $join->on('pharmacy_medicine_transactions.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                })
                ->orderby('pharmacy_medicine_transactions.pharmacy_medicine_transactions_key')
                ->whereBetween('pharmacy_medicine_transactions.pharmacy_medicine_transaction_date',array($start_date,$end_date));

        }


        return $this->applyScopes($data);


       // return $this->applyScopes($medicineIssuances);

    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('issuancesreports-table')
                    ->columns($this->getColumns())
                    //->minifiedAjax(('/admin/issuances_reports'))
                    ->ajax([
                        'url' =>'', /*route('admin.issuances_reports')*/
                        'type' => 'GET',
                        'data' => 'function (d) {
                            d.start_date = $("#start_date").val();
                            d.end_date = $("#end_date").val();
                            d.type = $("#type").val();
                             console.log(d);
                        }',
                    ])
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('excel'),
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reset'),
                        Button::make('reload')
                    );
    }


    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::computed('action')
                  ->exportable(true)
                  ->printable(false)
                  ->width(60)
                  ->addClass('text-center'),
            Column::make('pharmacy_medicine_key'),
            
       /*     Column::make('add your columns'),
            Column::make('created_at'),
            Column::make('updated_at'),*/
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'IssuancesReports_' . date('YmdHis');
    }
}
