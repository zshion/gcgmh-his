<?php

namespace App\DataTables;

use App\MedicineIssuance;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MedicineIssuanceReportsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);
           // ->addColumn('action', 'medicineissuancereports.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\MedicineIssuance $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MedicineIssuance $model)
    {
      //  return $model->newQuery();

      $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
      $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');

      $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

      $data = MedicineIssuance::select('*')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('medicine_issuances.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('medicine_issuances.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            })
            ->leftjoin('users', function($join) {
                $join->on('medicine_issuances.user_id', '=', 'users.id');
            });
            

            if($start_date && $end_date){
            $data = MedicineIssuance::select('*')
                ->leftjoin('pharmacy_medicines', function($join) {
                    $join->on('medicine_issuances.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                })
                ->leftjoin('pharmacy_item_types', function($join) {
                    $join->on('medicine_issuances.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
                })
                ->leftjoin('users', function($join) {
                    $join->on('medicine_issuances.user_id', '=', 'users.id');
                })
                ->whereBetween('medicine_issuances.medicine_issuance_date',array($start_date,$end_date))
                ->where('pharmacy_item_types.pharmacy_item_type_key',$type);
            }

            return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('medicineissuancereports-table')
                    ->columns($this->getColumns())
                    ->ajax([
                        'url' =>'',
                        'type' => 'GET',
                        'data' => 'function (d) {
                            d.start_date = $("#start_date").val();
                            d.end_date = $("#end_date").val();
                            d.type = $("#type").val();
                             console.log(d);
                        }',
                    ])
                    ->dom('Bfrtip')
                    ->orderBy([0, 'desc'])
                    ->buttons(
                        Button::make('export'),
                        Button::make('print'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'date'   => ['data' => 'medicine_issuance_date', 'name' => 'medicine_issuances.medicine_issuance_date'],
            'charge slip' => ['data' => 'medicine_issuance_charge_slip', 'name' => 'medicine_issuances.medicine_issuance_charge_slip'],
            'hospital no.'   => ['data' => 'patient_hospital_no', 'name' => 'medicine_issuances.patient_hospital_no'],
            'item description'   => ['data' => 'pharmacy_medicine_name', 'name' => 'pharmacy_medicines.pharmacy_medicine_name'],
            'qty'   => ['data' => 'medicine_issuance_qty', 'name' => 'medicine_issuances.medicine_issuance_qty'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'MedicineIssuanceReports_' . date('YmdHis');
    }
}
