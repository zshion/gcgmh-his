<?php

namespace App\DataTables;

use App\PharmacyMedicineTransaction;
use App\MedicineIssuance;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class PharmacyMedicineTransactionReportsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);
           // ->addColumn('action', 'pharmacymedicinetransactionreports.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\PharmacyMedicineTransaction $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PharmacyMedicineTransaction $model)
    {
        //return $model->newQuery();

      /*  $dt = date("Y-m-d");
        $startdate =date("Y-m-27", strtotime("first day of last month"));
        $enddate = date("Y-m-26", strtotime($dt));

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');



        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $data = PharmacyMedicineTransaction::select('*')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('pharmacy_medicine_transactions.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('pharmacy_medicine_transactions.pharmacy_medicine_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            });


        return $this->applyScopes($data);*/
    
        $dt = date("Y-m-d");
        $startdate =date("Y-m-27", strtotime("first day of last month"));
        $enddate = date("Y-m-26", strtotime($dt));

        $start_date = (!empty($_GET["start_date"])) ? ($_GET["start_date"]) : ('');
        $end_date = (!empty($_GET["end_date"])) ? ($_GET["end_date"]) : ('');
        $type = (!empty($_GET["type"])) ? ($_GET["type"]) : ('');

        $data= PharmacyMedicineTransaction::selectRaw('*,SUM(medicine_delivery_qty) AS delivery,
            SUM(medicine_issuance_qty) AS issuance,
            SUM(adjustment_unrecorded_iar_qty) AS unrecorded,
            SUM(adjustment_cancelled_iar_qty) AS cancelled,
            SUM(adjustment_overissued_qty) AS overissued,
            SUM(adjustment_unissued_qty) AS unissued,
            SUM(adjustment_returned_ward_qty) AS ward,
            SUM(adjustment_issued_employee_qty) AS employee,
            SUM(adjustment_replacement_qty) AS replacement,
            SUM(medicine_expired_qty) AS expired,
            SUM(medicine_damage_qty) AS damaged,
            SUM(medicine_credit_memo_qty) AS memo,
            SUM(medicine_credit_note_qty) AS note


             ')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('pharmacy_medicine_transactions.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })

            ->whereBetween('pharmacy_medicine_transactions.pharmacy_medicine_transaction_date',array($startdate,$enddate))
            ->groupBy('pharmacy_medicine_transactions.pharmacy_medicine_key');
           



        if($start_date && $end_date){

            $start_date = date('Y-m-d', strtotime($start_date));
            $end_date = date('Y-m-d', strtotime($end_date));

            $data = PharmacyMedicineTransaction::selectRaw('SUM(medicine_delivery_qty) AS delivery,
                SUM(medicine_issuance_qty) AS issuance,
                SUM(adjustment_unrecorded_iar_qty) AS unrecorded,
                SUM(adjustment_cancelled_iar_qty) AS cancelled,
                SUM(adjustment_overissued_qty) AS overissued,
                SUM(adjustment_unissued_qty) AS unissued,
                SUM(adjustment_returned_ward_qty) AS ward,
                SUM(adjustment_issued_employee_qty) AS employee,
                SUM(adjustment_replacement_qty) AS replacement,
                SUM(medicine_expired_qty) AS expired,
                SUM(medicine_damage_qty) AS damaged,
                SUM(medicine_credit_memo_qty) AS memo,
                SUM(medicine_credit_note_qty) AS note

                ( (pharmacy_beginning_balance + SUM(medicine_delivery_qty) + SUM(medicine_credit_note_qty) + SUM(adjustment_returned_ward_qty) + SUM(adjustment_overissued_qty) + 
                SUM(adjustment_replacement_qty) + SUM(adjustment_unrecorded_iar_qty) ) - ( SUM(medicine_issuance_qty) + SUM(adjustment_cancelled_iar_qty) + SUM(adjustment_unissued_qty)
                + SUM(adjustment_issued_employee_qty) + SUM(medicine_damage_qty) + SUM(medicine_expired_qty) + SUM(medicine_credit_memo_qty) )) AS ending

                ')
                ->leftjoin('pharmacy_medicines', function($join) {
                    $join->on('pharmacy_medicine_transactions.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
                })

                ->whereBetween('pharmacy_medicine_transactions.pharmacy_medicine_transaction_date',array($start_date,$end_date))
                ->where('pharmacy_medicine_transactions.pharmacy_medicine_type_key',$type)
                ->groupBy('pharmacy_medicine_transactions.pharmacy_medicine_key');
                
        }
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('pharmacymedicinetransactionreports-table')
                    ->columns($this->getColumns())
                    ->ajax([
                        'url' =>'',
                        'type' => 'GET',
                        'data' => 'function (d) {
                            d.start_date = $("#start_date").val();
                            d.end_date = $("#end_date").val();
                            d.type = $("#type").val();
                             console.log(d);
                        }',
                    ])
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
          /*  Column::make('pharmacy_medicine_name')
                ->title('Item Description'),*/

            'item description'   => ['data' => 'pharmacy_medicine_name', 'name' => 'pharmacy_medicines.pharmacy_medicine_name'],
            'delivery'   => ['data' => 'delivery', 'name' => 'pharmacy_medicines.delivery'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'PharmacyMedicineTransactionReports_' . date('YmdHis');
    }
}
