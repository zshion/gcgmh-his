<?php

namespace App\DataTables;

use App\MedicineStockbalanceReport;
use App\MedicineStockbalance;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MedicineStockbalanceReportsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query);
           // ->addColumn('action', 'medicinestockbalancereports.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\MedicineStockbalance $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MedicineStockbalance $model)
    {
        //return $model->newQuery();

        $data = MedicineStockbalance::select('*')
            ->leftjoin('pharmacy_medicines', function($join) {
                $join->on('medicine_stockbalances.pharmacy_medicine_key', '=', 'pharmacy_medicines.pharmacy_medicine_key');
            })
            ->leftjoin('pharmacy_item_types', function($join) {
                $join->on('medicine_stockbalances.pharmacy_item_type_key', '=', 'pharmacy_item_types.pharmacy_item_type_key');
            });
            return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('medicinestockbalancereports-table')
                    ->columns($this->getColumns())
                    ->ajax([
                        'url' =>'',
                        'type' => 'GET',
                        'data' => 'function (d) {
                            d.type = $("#type").val();
                             console.log(d);
                        }',
                    ])
                    ->dom('Bfrtip')
                    ->orderBy([1, 'asc'])
                    ->buttons(
                        Button::make('excel'),
                        Button::make('csv'),
                        Button::make('reload')
                    );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            'SKU'   => ['data' => 'pharmacy_medicine_sku', 'name' => 'medicine_stockbalances.pharmacy_medicine_sku'],
            'item description'   => ['data' => 'pharmacy_medicine_name', 'name' => 'pharmacy_medicines.pharmacy_medicine_name'],
            'beginning qty'   => ['data' => 'medicine_stockbalance_beginning', 'name' => 'medicine_stockbalances.medicine_stockbalance_beginning'],
            'current qty'   => ['data' => 'medicine_stockbalance_current', 'name' => 'medicine_stockbalances.medicine_stockbalance_current'],
            're-order level'   => ['data' => 'medicine_stockbalance_reorder', 'name' => 'medicine_stockbalances.medicine_stockbalance_reorder'],
            'status'   => ['data' => 'medicine_stockbalance_status', 'name' => 'medicine_stockbalances.medicine_stockbalance_status'],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'MedicineStockbalanceReports_' . date('YmdHis');
    }
}
