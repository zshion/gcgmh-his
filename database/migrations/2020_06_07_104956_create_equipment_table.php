<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEquipmentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('equipment', function(Blueprint $table)
		{
			$table->increments('equipment_key');
			$table->string('equipment_name')->nullable();
			$table->string('equipment_serial')->nullable()->unique();
			$table->string('equipment_model')->nullable();
			$table->integer('equipment_quantity')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->string('equipment_description', 1000)->nullable();
			$table->string('equipment_brand')->nullable();
			$table->string('equipment_country')->nullable();
			$table->dateTime('equipment_date_delivered')->nullable();
			$table->dateTime('equipment_date_inspected')->nullable();
			$table->dateTime('equipment_date_acquired')->nullable();
			$table->string('equipment_supplier')->nullable();
			$table->integer('equipment_unit_price')->nullable();
			$table->string('equipment_location')->nullable();
			$table->string('equipment_tag_number')->nullable();
			$table->string('equipment_card_number')->nullable();
			$table->string('equipment_memorandum_receipt')->nullable();
			$table->string('equipment_iar_number')->nullable();
			$table->string('equipment_asset_categories')->nullable();
			$table->string('equipment_account_code')->nullable();
			$table->string('equipment_purchase_order')->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('equipment');
	}

}
