<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Equipment;
use Faker\Generator as Faker;

$factory->define(Equipment::class, function (Faker $faker) {

    return [
        'equipment_name' => $faker->word,
        'equipment_serial' => $faker->word,
        'equipment_model' => $faker->word,
        'equipment_quantity' => $faker->randomDigitNotNull,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s'),
        'equipment_description' => $faker->word,
        'equipment_brand' => $faker->word,
        'equipment_country' => $faker->word,
        'equipment_date_delivered' => $faker->date('Y-m-d H:i:s'),
        'equipment_date_inspected' => $faker->date('Y-m-d H:i:s'),
        'equipment_date_acquired' => $faker->date('Y-m-d H:i:s'),
        'equipment_supplier' => $faker->word,
        'equipment_unit_price' => $faker->randomDigitNotNull,
        'equipment_location' => $faker->word,
        'equipment_tag_number' => $faker->word,
        'equipment_card_number' => $faker->word,
        'equipment_memorandum_receipt' => $faker->word,
        'equipment_iar_number' => $faker->word,
        'equipment_asset_categories' => $faker->word,
        'equipment_account_code' => $faker->word,
        'equipment_purchase_order' => $faker->word,
        'equipment_remarks' => $faker->word,
        'equipment_date_of_remarks' => $faker->date('Y-m-d H:i:s'),
        'equipment_status' => $faker->word,
        'equipment_date_of_status' => $faker->date('Y-m-d H:i:s'),
        'equipment_par' => $faker->word
    ];
});
