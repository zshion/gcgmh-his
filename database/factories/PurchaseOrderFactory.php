<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\PurchaseOrder;
use Faker\Generator as Faker;

$factory->define(PurchaseOrder::class, function (Faker $faker) {

    return [
        'purchase_order_number' => $faker->randomDigitNotNull,
        'purchase_order_pr_number' => $faker->randomDigitNotNull,
        'purchase_order_mode_of_procurement' => $faker->word,
        'purchase_order_date_awarded' => $faker->date('Y-m-d H:i:s'),
        'purchase_order_date_of_po_received' => $faker->date('Y-m-d H:i:s'),
        'purchase_order_date_delivered' => $faker->date('Y-m-d H:i:s'),
        'purchase_order_inspected' => $faker->date('Y-m-d H:i:s'),
        'purchase_order_date_acquired' => $faker->date('Y-m-d H:i:s'),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
