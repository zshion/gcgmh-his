<?php

use Illuminate\Database\Seeder;

class GenericNamesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('generic_names')->delete();
        
        \DB::table('generic_names')->insert(array (
            0 => 
            array (
                'generic_key' => 1,
                'generic_name' => 'Acebutolol',
                'created_at' => '2020-08-05 07:00:36',
                'updated_at' => '2020-08-05 07:00:36',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'generic_key' => 2,
                'generic_name' => 'Acetaminophen',
                'created_at' => '2020-08-05 07:00:56',
                'updated_at' => '2020-08-05 07:00:56',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'generic_key' => 3,
                'generic_name' => 'Acetated Ringer\'s Solution',
                'created_at' => '2020-08-05 07:01:41',
                'updated_at' => '2020-08-05 07:01:41',
                'deleted_at' => NULL,
            ),
            3 => 
            array (
                'generic_key' => 4,
                'generic_name' => 'Acetazolamide',
                'created_at' => '2020-08-05 07:01:58',
                'updated_at' => '2020-08-05 07:01:58',
                'deleted_at' => NULL,
            ),
            4 => 
            array (
                'generic_key' => 5,
                'generic_name' => 'Acetohexamide',
                'created_at' => '2020-08-05 07:02:12',
                'updated_at' => '2020-08-05 07:02:12',
                'deleted_at' => NULL,
            ),
            5 => 
            array (
                'generic_key' => 6,
                'generic_name' => 'Acetylcysteine',
                'created_at' => '2020-08-05 07:02:29',
                'updated_at' => '2020-08-05 07:02:29',
                'deleted_at' => NULL,
            ),
            6 => 
            array (
                'generic_key' => 7,
                'generic_name' => 'Aciclovir',
                'created_at' => '2020-08-05 07:02:44',
                'updated_at' => '2020-08-05 07:02:44',
                'deleted_at' => NULL,
            ),
            7 => 
            array (
                'generic_key' => 8,
                'generic_name' => 'Actinomycin D',
                'created_at' => '2020-08-05 07:02:59',
                'updated_at' => '2020-08-05 07:02:59',
                'deleted_at' => NULL,
            ),
            8 => 
            array (
                'generic_key' => 9,
                'generic_name' => 'Activated Charcoal, Usp',
                'created_at' => '2020-08-05 07:03:17',
                'updated_at' => '2020-08-05 07:03:17',
                'deleted_at' => NULL,
            ),
            9 => 
            array (
                'generic_key' => 10,
                'generic_name' => 'Adrenaline',
                'created_at' => '2020-08-05 07:03:32',
                'updated_at' => '2020-08-05 07:03:32',
                'deleted_at' => NULL,
            ),
            10 => 
            array (
                'generic_key' => 11,
                'generic_name' => 'Albendazole',
                'created_at' => '2020-08-05 07:03:47',
                'updated_at' => '2020-08-05 07:03:47',
                'deleted_at' => NULL,
            ),
            11 => 
            array (
                'generic_key' => 12,
                'generic_name' => 'Albumin, Human',
                'created_at' => '2020-08-05 07:04:06',
                'updated_at' => '2020-08-05 07:04:06',
                'deleted_at' => NULL,
            ),
            12 => 
            array (
                'generic_key' => 13,
                'generic_name' => 'Alcohol, Ethyl',
                'created_at' => '2020-08-05 07:04:21',
                'updated_at' => '2020-08-05 07:04:21',
                'deleted_at' => NULL,
            ),
            13 => 
            array (
                'generic_key' => 14,
                'generic_name' => 'Alcohol, Isopropyl',
                'created_at' => '2020-08-05 07:04:38',
                'updated_at' => '2020-08-05 07:04:38',
                'deleted_at' => NULL,
            ),
            14 => 
            array (
                'generic_key' => 15,
                'generic_name' => 'Allopurinol',
                'created_at' => '2020-08-05 07:05:04',
                'updated_at' => '2020-08-05 07:05:04',
                'deleted_at' => NULL,
            ),
            15 => 
            array (
                'generic_key' => 16,
                'generic_name' => 'Alpha-Tocopherol',
                'created_at' => '2020-08-05 07:05:27',
                'updated_at' => '2020-08-05 07:05:27',
                'deleted_at' => NULL,
            ),
            16 => 
            array (
                'generic_key' => 17,
                'generic_name' => 'Alprazolam',
                'created_at' => '2020-08-05 07:05:42',
                'updated_at' => '2020-08-05 07:05:42',
                'deleted_at' => NULL,
            ),
            17 => 
            array (
                'generic_key' => 18,
                'generic_name' => 'Aluminum Hydroxide',
                'created_at' => '2020-08-05 07:05:58',
                'updated_at' => '2020-08-05 07:05:58',
                'deleted_at' => NULL,
            ),
            18 => 
            array (
                'generic_key' => 19,
                'generic_name' => 'Aluminum Hydroxide + Magnesium Hydroxide',
                'created_at' => '2020-08-05 07:06:29',
                'updated_at' => '2020-08-05 07:06:29',
                'deleted_at' => NULL,
            ),
            19 => 
            array (
                'generic_key' => 20,
                'generic_name' => 'Amantadine',
                'created_at' => '2020-08-05 07:13:11',
                'updated_at' => '2020-08-05 07:13:11',
                'deleted_at' => NULL,
            ),
            20 => 
            array (
                'generic_key' => 21,
                'generic_name' => 'Amidotrizoate',
                'created_at' => '2020-08-05 07:13:28',
                'updated_at' => '2020-08-05 07:13:28',
                'deleted_at' => NULL,
            ),
            21 => 
            array (
                'generic_key' => 22,
                'generic_name' => 'Amikacin Sulfate',
                'created_at' => '2020-08-05 07:13:50',
                'updated_at' => '2020-08-05 07:13:50',
                'deleted_at' => NULL,
            ),
            22 => 
            array (
                'generic_key' => 23,
                'generic_name' => 'Amineptine',
                'created_at' => '2020-08-05 07:14:10',
                'updated_at' => '2020-08-05 07:14:10',
                'deleted_at' => NULL,
            ),
            23 => 
            array (
                'generic_key' => 24,
                'generic_name' => 'Amino Acid 3.5%',
                'created_at' => '2020-08-05 07:14:27',
                'updated_at' => '2020-08-05 07:14:27',
                'deleted_at' => NULL,
            ),
            24 => 
            array (
                'generic_key' => 25,
                'generic_name' => 'Aminophylline',
                'created_at' => '2020-08-05 07:14:49',
                'updated_at' => '2020-08-05 07:14:49',
                'deleted_at' => NULL,
            ),
            25 => 
            array (
                'generic_key' => 26,
                'generic_name' => 'Amiodarone',
                'created_at' => '2020-08-06 02:31:20',
                'updated_at' => '2020-08-06 02:31:20',
                'deleted_at' => NULL,
            ),
            26 => 
            array (
                'generic_key' => 27,
                'generic_name' => 'Amoxicillin',
                'created_at' => '2020-08-06 02:31:34',
                'updated_at' => '2020-08-06 02:31:34',
                'deleted_at' => NULL,
            ),
            27 => 
            array (
                'generic_key' => 28,
                'generic_name' => 'Amoxicillin + Potassium Clavulanate',
                'created_at' => '2020-08-06 02:31:46',
                'updated_at' => '2020-08-06 02:31:46',
                'deleted_at' => NULL,
            ),
            28 => 
            array (
                'generic_key' => 29,
                'generic_name' => 'Amphotericin B non-lipid complex',
                'created_at' => '2020-08-06 02:31:58',
                'updated_at' => '2020-08-06 02:31:58',
                'deleted_at' => NULL,
            ),
            29 => 
            array (
                'generic_key' => 30,
                'generic_name' => 'Amphotericin B non-lipid complex',
                'created_at' => '2020-08-06 02:32:21',
                'updated_at' => '2020-08-06 02:32:21',
                'deleted_at' => NULL,
            ),
            30 => 
            array (
                'generic_key' => 31,
                'generic_name' => 'Ampicillin',
                'created_at' => '2020-08-06 02:32:36',
                'updated_at' => '2020-08-06 02:32:36',
                'deleted_at' => NULL,
            ),
            31 => 
            array (
                'generic_key' => 32,
                'generic_name' => 'Ampicillin + Sulbactam',
                'created_at' => '2020-08-06 02:32:49',
                'updated_at' => '2020-08-06 02:32:49',
                'deleted_at' => NULL,
            ),
            32 => 
            array (
                'generic_key' => 33,
                'generic_name' => 'Anti-D Immunoglobulin',
                'created_at' => '2020-08-06 02:33:00',
                'updated_at' => '2020-08-06 02:33:00',
                'deleted_at' => NULL,
            ),
            33 => 
            array (
                'generic_key' => 34,
                'generic_name' => 'Anti-Rabies Vaccine',
                'created_at' => '2020-08-06 02:33:31',
                'updated_at' => '2020-08-06 02:33:31',
                'deleted_at' => NULL,
            ),
            34 => 
            array (
                'generic_key' => 35,
                'generic_name' => 'Antitetanus Serum',
                'created_at' => '2020-08-06 02:33:45',
                'updated_at' => '2020-08-06 02:33:45',
                'deleted_at' => NULL,
            ),
            35 => 
            array (
                'generic_key' => 36,
                'generic_name' => 'Ascorbic Acid',
                'created_at' => '2020-08-06 02:33:56',
                'updated_at' => '2020-08-06 02:33:56',
                'deleted_at' => NULL,
            ),
            36 => 
            array (
                'generic_key' => 37,
                'generic_name' => 'Aspirin',
                'created_at' => '2020-08-06 02:34:09',
                'updated_at' => '2020-08-06 02:34:09',
                'deleted_at' => NULL,
            ),
            37 => 
            array (
                'generic_key' => 38,
                'generic_name' => 'Atenolol',
                'created_at' => '2020-08-06 02:34:24',
                'updated_at' => '2020-08-06 02:34:24',
                'deleted_at' => NULL,
            ),
            38 => 
            array (
                'generic_key' => 39,
                'generic_name' => 'Atracurium Besylate',
                'created_at' => '2020-08-06 02:35:02',
                'updated_at' => '2020-08-06 02:35:02',
                'deleted_at' => NULL,
            ),
            39 => 
            array (
                'generic_key' => 40,
                'generic_name' => 'Atropine Sulfate',
                'created_at' => '2020-08-06 02:35:22',
                'updated_at' => '2020-08-06 02:35:22',
                'deleted_at' => NULL,
            ),
            40 => 
            array (
                'generic_key' => 41,
                'generic_name' => 'Auranofin',
                'created_at' => '2020-08-06 02:35:36',
                'updated_at' => '2020-08-06 02:35:36',
                'deleted_at' => NULL,
            ),
            41 => 
            array (
                'generic_key' => 42,
                'generic_name' => 'Aurothiomalate',
                'created_at' => '2020-08-06 02:35:50',
                'updated_at' => '2020-08-06 02:35:50',
                'deleted_at' => NULL,
            ),
            42 => 
            array (
                'generic_key' => 43,
                'generic_name' => 'Azathioprine',
                'created_at' => '2020-08-06 02:36:03',
                'updated_at' => '2020-08-06 02:36:03',
                'deleted_at' => NULL,
            ),
            43 => 
            array (
                'generic_key' => 44,
                'generic_name' => 'Aztreonam',
                'created_at' => '2020-08-06 02:36:31',
                'updated_at' => '2020-08-06 02:36:31',
                'deleted_at' => NULL,
            ),
            44 => 
            array (
                'generic_key' => 45,
                'generic_name' => '0.9% Sodium Chloride',
                'created_at' => '2020-08-06 04:37:28',
                'updated_at' => '2020-08-06 04:37:28',
                'deleted_at' => NULL,
            ),
            45 => 
            array (
                'generic_key' => 46,
                'generic_name' => '0.9% Sodium Chloride for Irrigation',
                'created_at' => '2020-08-06 04:37:59',
                'updated_at' => '2020-08-06 04:37:59',
                'deleted_at' => NULL,
            ),
            46 => 
            array (
                'generic_key' => 47,
                'generic_name' => 'Bacampicillin',
                'created_at' => '2020-08-12 02:49:20',
                'updated_at' => '2020-08-12 02:49:20',
                'deleted_at' => NULL,
            ),
            47 => 
            array (
                'generic_key' => 48,
                'generic_name' => 'Baclofen',
                'created_at' => '2020-08-12 02:49:52',
                'updated_at' => '2020-08-12 02:49:52',
                'deleted_at' => NULL,
            ),
            48 => 
            array (
                'generic_key' => 49,
                'generic_name' => 'BMRS',
                'created_at' => '2020-08-12 02:50:22',
                'updated_at' => '2020-08-12 02:50:22',
                'deleted_at' => NULL,
            ),
            49 => 
            array (
                'generic_key' => 50,
                'generic_name' => 'Balanced Multiple Replacement Solution',
                'created_at' => '2020-08-12 02:50:45',
                'updated_at' => '2020-08-12 02:50:45',
                'deleted_at' => NULL,
            ),
            50 => 
            array (
                'generic_key' => 51,
                'generic_name' => 'Balanced Salt Solution',
                'created_at' => '2020-08-12 02:52:26',
                'updated_at' => '2020-08-12 02:52:26',
                'deleted_at' => NULL,
            ),
            51 => 
            array (
                'generic_key' => 52,
                'generic_name' => 'Barium Sulfate Powder, Usp Grade',
                'created_at' => '2020-08-12 02:52:44',
                'updated_at' => '2020-08-12 02:52:44',
                'deleted_at' => NULL,
            ),
            52 => 
            array (
                'generic_key' => 53,
                'generic_name' => 'BCG Vaccine',
                'created_at' => '2020-08-12 02:52:59',
                'updated_at' => '2020-08-12 02:52:59',
                'deleted_at' => NULL,
            ),
            53 => 
            array (
                'generic_key' => 54,
                'generic_name' => 'Beclometasone',
                'created_at' => '2020-08-12 02:53:14',
                'updated_at' => '2020-08-12 02:53:14',
                'deleted_at' => NULL,
            ),
            54 => 
            array (
                'generic_key' => 55,
                'generic_name' => 'Benzatropine',
                'created_at' => '2020-08-12 02:53:29',
                'updated_at' => '2020-08-12 02:53:29',
                'deleted_at' => NULL,
            ),
            55 => 
            array (
                'generic_key' => 56,
                'generic_name' => 'Benzoic Acid + Salicylic Acid',
                'created_at' => '2020-08-12 02:53:44',
                'updated_at' => '2020-08-12 02:53:44',
                'deleted_at' => NULL,
            ),
            56 => 
            array (
                'generic_key' => 57,
                'generic_name' => 'Benzoyl Peroxide',
                'created_at' => '2020-08-12 02:53:58',
                'updated_at' => '2020-08-12 02:53:58',
                'deleted_at' => NULL,
            ),
            57 => 
            array (
                'generic_key' => 58,
                'generic_name' => 'Benztropine',
                'created_at' => '2020-08-12 02:54:12',
                'updated_at' => '2020-08-12 02:54:12',
                'deleted_at' => NULL,
            ),
            58 => 
            array (
                'generic_key' => 59,
                'generic_name' => 'Benzyl Benzoate Lotion',
                'created_at' => '2020-08-12 02:54:28',
                'updated_at' => '2020-08-12 02:54:28',
                'deleted_at' => NULL,
            ),
            59 => 
            array (
                'generic_key' => 60,
                'generic_name' => 'Benzyl Penicillin Sodium',
                'created_at' => '2020-08-12 02:54:44',
                'updated_at' => '2020-08-12 02:54:44',
                'deleted_at' => NULL,
            ),
            60 => 
            array (
                'generic_key' => 61,
                'generic_name' => 'Betahistine',
                'created_at' => '2020-08-12 02:54:58',
                'updated_at' => '2020-08-12 02:54:58',
                'deleted_at' => NULL,
            ),
            61 => 
            array (
                'generic_key' => 62,
                'generic_name' => 'Betamethasone',
                'created_at' => '2020-08-12 02:55:13',
                'updated_at' => '2020-08-12 02:55:13',
                'deleted_at' => NULL,
            ),
            62 => 
            array (
                'generic_key' => 63,
                'generic_name' => 'Betaxolol',
                'created_at' => '2020-08-12 02:55:29',
                'updated_at' => '2020-08-12 02:55:29',
                'deleted_at' => NULL,
            ),
            63 => 
            array (
                'generic_key' => 64,
                'generic_name' => 'Biperiden',
                'created_at' => '2020-08-12 02:55:50',
                'updated_at' => '2020-08-12 02:55:50',
                'deleted_at' => NULL,
            ),
            64 => 
            array (
                'generic_key' => 65,
            'generic_name' => 'Bisacodyl (Adult)',
                'created_at' => '2020-08-12 02:56:05',
                'updated_at' => '2020-08-12 02:56:05',
                'deleted_at' => NULL,
            ),
            65 => 
            array (
                'generic_key' => 66,
                'generic_name' => 'Bismuth Subcitrate',
                'created_at' => '2020-08-12 02:56:20',
                'updated_at' => '2020-08-12 02:56:20',
                'deleted_at' => NULL,
            ),
            66 => 
            array (
                'generic_key' => 67,
                'generic_name' => 'Bleomycin',
                'created_at' => '2020-08-12 02:56:35',
                'updated_at' => '2020-08-12 02:56:35',
                'deleted_at' => NULL,
            ),
            67 => 
            array (
                'generic_key' => 68,
                'generic_name' => 'Bromazepam',
                'created_at' => '2020-08-12 02:57:01',
                'updated_at' => '2020-08-12 02:57:01',
                'deleted_at' => NULL,
            ),
            68 => 
            array (
                'generic_key' => 69,
                'generic_name' => 'Bromocriptine',
                'created_at' => '2020-08-12 02:57:17',
                'updated_at' => '2020-08-12 02:57:17',
                'deleted_at' => NULL,
            ),
            69 => 
            array (
                'generic_key' => 70,
                'generic_name' => 'Budesonide',
                'created_at' => '2020-08-12 02:57:35',
                'updated_at' => '2020-08-12 02:57:35',
                'deleted_at' => NULL,
            ),
            70 => 
            array (
                'generic_key' => 71,
                'generic_name' => 'Bumetanide',
                'created_at' => '2020-08-12 02:57:53',
                'updated_at' => '2020-08-12 02:57:53',
                'deleted_at' => NULL,
            ),
            71 => 
            array (
                'generic_key' => 72,
                'generic_name' => 'Bupivacaine',
                'created_at' => '2020-08-12 02:58:10',
                'updated_at' => '2020-08-12 02:58:10',
                'deleted_at' => NULL,
            ),
            72 => 
            array (
                'generic_key' => 73,
                'generic_name' => 'Buserelin',
                'created_at' => '2020-08-12 02:58:26',
                'updated_at' => '2020-08-12 02:58:26',
                'deleted_at' => NULL,
            ),
            73 => 
            array (
                'generic_key' => 74,
                'generic_name' => 'Busulfan',
                'created_at' => '2020-08-12 02:58:42',
                'updated_at' => '2020-08-12 02:58:42',
                'deleted_at' => NULL,
            ),
            74 => 
            array (
                'generic_key' => 75,
                'generic_name' => 'Butamirate Citrate',
                'created_at' => '2020-08-12 02:59:11',
                'updated_at' => '2020-08-12 02:59:11',
                'deleted_at' => NULL,
            ),
            75 => 
            array (
                'generic_key' => 76,
                'generic_name' => 'Butorphanol',
                'created_at' => '2020-08-12 02:59:28',
                'updated_at' => '2020-08-12 02:59:28',
                'deleted_at' => NULL,
            ),
            76 => 
            array (
                'generic_key' => 77,
                'generic_name' => 'Caffeine Citrate',
                'created_at' => '2020-08-12 02:59:46',
                'updated_at' => '2020-08-12 02:59:46',
                'deleted_at' => NULL,
            ),
            77 => 
            array (
                'generic_key' => 78,
                'generic_name' => 'Calamine',
                'created_at' => '2020-08-12 03:00:02',
                'updated_at' => '2020-08-12 03:00:02',
                'deleted_at' => NULL,
            ),
            78 => 
            array (
                'generic_key' => 79,
                'generic_name' => 'Calciferol',
                'created_at' => '2020-08-12 03:00:20',
                'updated_at' => '2020-08-12 03:00:20',
                'deleted_at' => NULL,
            ),
            79 => 
            array (
                'generic_key' => 80,
                'generic_name' => 'Calciferol',
                'created_at' => '2020-08-12 03:04:24',
                'updated_at' => '2020-08-12 03:04:24',
                'deleted_at' => NULL,
            ),
            80 => 
            array (
                'generic_key' => 81,
                'generic_name' => 'Calcitriol',
                'created_at' => '2020-08-12 03:04:39',
                'updated_at' => '2020-08-12 03:04:39',
                'deleted_at' => NULL,
            ),
            81 => 
            array (
                'generic_key' => 82,
                'generic_name' => 'Calcium Carbonate',
                'created_at' => '2020-08-12 03:05:45',
                'updated_at' => '2020-08-12 03:05:45',
                'deleted_at' => NULL,
            ),
            82 => 
            array (
                'generic_key' => 83,
                'generic_name' => 'Calcium Folinate',
                'created_at' => '2020-08-12 03:06:02',
                'updated_at' => '2020-08-12 03:06:02',
                'deleted_at' => NULL,
            ),
            83 => 
            array (
                'generic_key' => 84,
                'generic_name' => 'Calcium Gluconate',
                'created_at' => '2020-08-12 03:06:17',
                'updated_at' => '2020-08-12 03:06:17',
                'deleted_at' => NULL,
            ),
            84 => 
            array (
                'generic_key' => 85,
                'generic_name' => 'Calcium Salt',
                'created_at' => '2020-08-12 03:06:35',
                'updated_at' => '2020-08-12 03:06:35',
                'deleted_at' => NULL,
            ),
            85 => 
            array (
                'generic_key' => 86,
                'generic_name' => 'Captopril',
                'created_at' => '2020-08-12 03:06:51',
                'updated_at' => '2020-08-12 03:06:51',
                'deleted_at' => NULL,
            ),
            86 => 
            array (
                'generic_key' => 87,
                'generic_name' => 'Carbacholl',
                'created_at' => '2020-08-12 03:07:07',
                'updated_at' => '2020-08-12 03:07:07',
                'deleted_at' => NULL,
            ),
            87 => 
            array (
                'generic_key' => 88,
                'generic_name' => 'Carbamazepine',
                'created_at' => '2020-08-12 03:08:17',
                'updated_at' => '2020-08-12 03:08:17',
                'deleted_at' => NULL,
            ),
            88 => 
            array (
                'generic_key' => 89,
                'generic_name' => 'Carbimazole',
                'created_at' => '2020-08-12 03:08:36',
                'updated_at' => '2020-08-12 03:08:36',
                'deleted_at' => NULL,
            ),
            89 => 
            array (
                'generic_key' => 90,
                'generic_name' => 'Carboplatin',
                'created_at' => '2020-08-12 03:08:51',
                'updated_at' => '2020-08-12 03:08:51',
                'deleted_at' => NULL,
            ),
            90 => 
            array (
                'generic_key' => 91,
                'generic_name' => 'Carmustine',
                'created_at' => '2020-08-12 03:09:07',
                'updated_at' => '2020-08-12 03:09:07',
                'deleted_at' => NULL,
            ),
            91 => 
            array (
                'generic_key' => 92,
                'generic_name' => 'Castor Oil',
                'created_at' => '2020-08-12 03:09:28',
                'updated_at' => '2020-08-12 03:09:28',
                'deleted_at' => NULL,
            ),
            92 => 
            array (
                'generic_key' => 93,
                'generic_name' => 'Cefaclor',
                'created_at' => '2020-08-12 03:09:42',
                'updated_at' => '2020-08-12 03:09:42',
                'deleted_at' => NULL,
            ),
            93 => 
            array (
                'generic_key' => 94,
                'generic_name' => 'Cefadroxil',
                'created_at' => '2020-08-12 03:10:03',
                'updated_at' => '2020-08-12 03:10:03',
                'deleted_at' => NULL,
            ),
            94 => 
            array (
                'generic_key' => 95,
                'generic_name' => 'Cefalexin',
                'created_at' => '2020-08-12 03:10:30',
                'updated_at' => '2020-08-12 03:10:30',
                'deleted_at' => NULL,
            ),
            95 => 
            array (
                'generic_key' => 96,
                'generic_name' => 'Cefazolin',
                'created_at' => '2020-08-12 03:10:48',
                'updated_at' => '2020-08-12 03:10:48',
                'deleted_at' => NULL,
            ),
            96 => 
            array (
                'generic_key' => 97,
                'generic_name' => 'Cefixime',
                'created_at' => '2020-08-12 03:11:03',
                'updated_at' => '2020-08-12 03:11:03',
                'deleted_at' => NULL,
            ),
            97 => 
            array (
                'generic_key' => 98,
                'generic_name' => 'Cefotaxime',
                'created_at' => '2020-08-12 03:11:27',
                'updated_at' => '2020-08-12 03:11:27',
                'deleted_at' => NULL,
            ),
            98 => 
            array (
                'generic_key' => 99,
                'generic_name' => 'Cefoxitin',
                'created_at' => '2020-08-12 03:11:43',
                'updated_at' => '2020-08-12 03:11:43',
                'deleted_at' => NULL,
            ),
            99 => 
            array (
                'generic_key' => 100,
                'generic_name' => 'Ceftazidime',
                'created_at' => '2020-08-12 03:11:58',
                'updated_at' => '2020-08-12 03:11:58',
                'deleted_at' => NULL,
            ),
            100 => 
            array (
                'generic_key' => 101,
                'generic_name' => 'Ceftriaxone',
                'created_at' => '2020-08-12 03:12:13',
                'updated_at' => '2020-08-12 03:12:13',
                'deleted_at' => NULL,
            ),
            101 => 
            array (
                'generic_key' => 102,
                'generic_name' => 'Cefuroxime',
                'created_at' => '2020-08-12 03:12:27',
                'updated_at' => '2020-08-12 03:12:27',
                'deleted_at' => NULL,
            ),
            102 => 
            array (
                'generic_key' => 103,
                'generic_name' => 'Chenodeoxycholic Acid',
                'created_at' => '2020-08-12 03:12:42',
                'updated_at' => '2020-08-12 03:12:42',
                'deleted_at' => NULL,
            ),
            103 => 
            array (
                'generic_key' => 104,
                'generic_name' => 'Chlorambucil',
                'created_at' => '2020-08-12 03:12:57',
                'updated_at' => '2020-08-12 03:12:57',
                'deleted_at' => NULL,
            ),
            104 => 
            array (
                'generic_key' => 105,
                'generic_name' => 'Chloramphenicol',
                'created_at' => '2020-08-12 03:13:14',
                'updated_at' => '2020-08-12 03:13:14',
                'deleted_at' => NULL,
            ),
            105 => 
            array (
                'generic_key' => 106,
                'generic_name' => 'Chlorazepate',
                'created_at' => '2020-08-12 03:14:06',
                'updated_at' => '2020-08-12 03:14:06',
                'deleted_at' => NULL,
            ),
            106 => 
            array (
                'generic_key' => 107,
                'generic_name' => 'Chlorhexidine',
                'created_at' => '2020-08-12 03:19:21',
                'updated_at' => '2020-08-12 03:19:21',
                'deleted_at' => NULL,
            ),
            107 => 
            array (
                'generic_key' => 108,
                'generic_name' => 'Chlormethine',
                'created_at' => '2020-08-12 03:19:36',
                'updated_at' => '2020-08-12 03:19:36',
                'deleted_at' => NULL,
            ),
            108 => 
            array (
                'generic_key' => 109,
                'generic_name' => 'Chloroquine',
                'created_at' => '2020-08-12 03:19:53',
                'updated_at' => '2020-08-12 03:19:53',
                'deleted_at' => NULL,
            ),
            109 => 
            array (
                'generic_key' => 110,
                'generic_name' => 'Chlorphenamine',
                'created_at' => '2020-08-12 03:20:08',
                'updated_at' => '2020-08-12 03:20:08',
                'deleted_at' => NULL,
            ),
            110 => 
            array (
                'generic_key' => 111,
                'generic_name' => 'Chlorpheniramine Maleate',
                'created_at' => '2020-08-12 03:20:23',
                'updated_at' => '2020-08-12 03:20:23',
                'deleted_at' => NULL,
            ),
            111 => 
            array (
                'generic_key' => 112,
                'generic_name' => 'Chlorpromazine',
                'created_at' => '2020-08-12 03:20:37',
                'updated_at' => '2020-08-12 03:20:37',
                'deleted_at' => NULL,
            ),
            112 => 
            array (
                'generic_key' => 113,
                'generic_name' => 'Chlorpromazine',
                'created_at' => '2020-08-12 03:21:03',
                'updated_at' => '2020-08-12 03:21:16',
                'deleted_at' => '2020-08-12 03:21:16',
            ),
            113 => 
            array (
                'generic_key' => 114,
                'generic_name' => 'Chlortalidone',
                'created_at' => '2020-08-12 03:21:38',
                'updated_at' => '2020-08-12 03:21:38',
                'deleted_at' => NULL,
            ),
            114 => 
            array (
                'generic_key' => 115,
                'generic_name' => 'Ciclosporin',
                'created_at' => '2020-08-12 03:21:55',
                'updated_at' => '2020-08-12 03:21:55',
                'deleted_at' => NULL,
            ),
            115 => 
            array (
                'generic_key' => 116,
                'generic_name' => 'Cimetidine',
                'created_at' => '2020-08-12 03:22:13',
                'updated_at' => '2020-08-12 03:22:13',
                'deleted_at' => NULL,
            ),
            116 => 
            array (
                'generic_key' => 117,
                'generic_name' => 'Cinnarizine',
                'created_at' => '2020-08-12 03:22:57',
                'updated_at' => '2020-08-12 03:22:57',
                'deleted_at' => NULL,
            ),
            117 => 
            array (
                'generic_key' => 118,
                'generic_name' => 'Ciprofloxacin',
                'created_at' => '2020-08-12 03:23:13',
                'updated_at' => '2020-08-12 03:23:13',
                'deleted_at' => NULL,
            ),
            118 => 
            array (
                'generic_key' => 119,
                'generic_name' => 'Cisapride',
                'created_at' => '2020-08-12 03:23:30',
                'updated_at' => '2020-08-12 03:23:30',
                'deleted_at' => NULL,
            ),
            119 => 
            array (
                'generic_key' => 120,
                'generic_name' => 'Cisplatin',
                'created_at' => '2020-08-12 03:23:47',
                'updated_at' => '2020-08-12 03:23:47',
                'deleted_at' => NULL,
            ),
            120 => 
            array (
                'generic_key' => 121,
                'generic_name' => 'Clarithromycin',
                'created_at' => '2020-08-12 03:24:13',
                'updated_at' => '2020-08-12 03:24:13',
                'deleted_at' => NULL,
            ),
            121 => 
            array (
                'generic_key' => 122,
                'generic_name' => 'Clemastine',
                'created_at' => '2020-08-12 03:24:30',
                'updated_at' => '2020-08-12 03:24:30',
                'deleted_at' => NULL,
            ),
            122 => 
            array (
                'generic_key' => 123,
                'generic_name' => 'Clindamycin',
                'created_at' => '2020-08-12 03:24:52',
                'updated_at' => '2020-08-12 03:24:52',
                'deleted_at' => NULL,
            ),
            123 => 
            array (
                'generic_key' => 124,
                'generic_name' => 'Clobazam',
                'created_at' => '2020-08-12 03:25:08',
                'updated_at' => '2020-08-12 03:25:08',
                'deleted_at' => NULL,
            ),
            124 => 
            array (
                'generic_key' => 125,
                'generic_name' => 'Clofazimine',
                'created_at' => '2020-08-12 03:25:27',
                'updated_at' => '2020-08-12 03:25:27',
                'deleted_at' => NULL,
            ),
            125 => 
            array (
                'generic_key' => 126,
                'generic_name' => 'Clomifene',
                'created_at' => '2020-08-12 03:25:47',
                'updated_at' => '2020-08-12 03:25:47',
                'deleted_at' => NULL,
            ),
            126 => 
            array (
                'generic_key' => 127,
                'generic_name' => 'Chlomiphene',
                'created_at' => '2020-08-12 03:26:02',
                'updated_at' => '2020-08-12 03:26:02',
                'deleted_at' => NULL,
            ),
            127 => 
            array (
                'generic_key' => 128,
                'generic_name' => 'Clonazepam',
                'created_at' => '2020-08-12 03:26:24',
                'updated_at' => '2020-08-12 03:26:24',
                'deleted_at' => NULL,
            ),
            128 => 
            array (
                'generic_key' => 129,
                'generic_name' => 'Clonidine',
                'created_at' => '2020-08-12 03:26:39',
                'updated_at' => '2020-08-12 03:26:39',
                'deleted_at' => NULL,
            ),
            129 => 
            array (
                'generic_key' => 130,
                'generic_name' => 'Cloxacillin',
                'created_at' => '2020-08-12 03:26:56',
                'updated_at' => '2020-08-12 03:26:56',
                'deleted_at' => NULL,
            ),
            130 => 
            array (
                'generic_key' => 131,
                'generic_name' => 'Clozapine',
                'created_at' => '2020-08-12 03:27:22',
                'updated_at' => '2020-08-12 03:27:22',
                'deleted_at' => NULL,
            ),
            131 => 
            array (
                'generic_key' => 132,
                'generic_name' => 'Coal Tar',
                'created_at' => '2020-08-12 03:27:38',
                'updated_at' => '2020-08-12 03:27:38',
                'deleted_at' => NULL,
            ),
            132 => 
            array (
                'generic_key' => 133,
                'generic_name' => 'Co-Amoxiclav',
                'created_at' => '2020-08-12 03:27:52',
                'updated_at' => '2020-08-12 03:27:52',
                'deleted_at' => NULL,
            ),
            133 => 
            array (
                'generic_key' => 134,
                'generic_name' => 'Cobra Antivenom',
                'created_at' => '2020-08-12 03:28:14',
                'updated_at' => '2020-08-12 03:28:14',
                'deleted_at' => NULL,
            ),
            134 => 
            array (
                'generic_key' => 135,
                'generic_name' => 'Colchicine',
                'created_at' => '2020-08-12 03:28:29',
                'updated_at' => '2020-08-12 03:28:29',
                'deleted_at' => NULL,
            ),
            135 => 
            array (
                'generic_key' => 136,
                'generic_name' => 'Colestyramine',
                'created_at' => '2020-08-12 03:29:10',
                'updated_at' => '2020-08-12 03:29:10',
                'deleted_at' => NULL,
            ),
            136 => 
            array (
                'generic_key' => 137,
                'generic_name' => 'Conjugated Estrogens',
                'created_at' => '2020-08-12 03:29:26',
                'updated_at' => '2020-08-12 03:29:26',
                'deleted_at' => NULL,
            ),
            137 => 
            array (
                'generic_key' => 138,
                'generic_name' => 'Cosyntropin',
                'created_at' => '2020-08-12 03:29:41',
                'updated_at' => '2020-08-12 03:29:41',
                'deleted_at' => NULL,
            ),
            138 => 
            array (
                'generic_key' => 139,
                'generic_name' => 'Cotrimoxazole',
                'created_at' => '2020-08-12 03:29:56',
                'updated_at' => '2020-08-12 03:29:56',
                'deleted_at' => NULL,
            ),
            139 => 
            array (
                'generic_key' => 140,
                'generic_name' => 'Creoline',
                'created_at' => '2020-08-12 03:30:11',
                'updated_at' => '2020-08-12 03:30:11',
                'deleted_at' => NULL,
            ),
            140 => 
            array (
                'generic_key' => 141,
                'generic_name' => 'Cromolyn Sodium',
                'created_at' => '2020-08-12 03:30:41',
                'updated_at' => '2020-08-12 03:30:41',
                'deleted_at' => NULL,
            ),
            141 => 
            array (
                'generic_key' => 142,
                'generic_name' => 'Crotamiton',
                'created_at' => '2020-08-12 03:30:57',
                'updated_at' => '2020-08-12 03:30:57',
                'deleted_at' => NULL,
            ),
            142 => 
            array (
                'generic_key' => 143,
                'generic_name' => 'Cyclopentolate',
                'created_at' => '2020-08-12 03:31:16',
                'updated_at' => '2020-08-12 03:31:16',
                'deleted_at' => NULL,
            ),
            143 => 
            array (
                'generic_key' => 144,
                'generic_name' => 'Cyclophosphamide',
                'created_at' => '2020-08-12 03:31:45',
                'updated_at' => '2020-08-12 03:31:45',
                'deleted_at' => NULL,
            ),
            144 => 
            array (
                'generic_key' => 145,
                'generic_name' => 'Cyproterone',
                'created_at' => '2020-08-12 03:32:01',
                'updated_at' => '2020-08-12 03:32:01',
                'deleted_at' => NULL,
            ),
            145 => 
            array (
                'generic_key' => 146,
                'generic_name' => 'Cytarabine',
                'created_at' => '2020-08-12 03:32:22',
                'updated_at' => '2020-08-12 03:32:22',
                'deleted_at' => NULL,
            ),
            146 => 
            array (
                'generic_key' => 147,
                'generic_name' => 'Dacarbazine',
                'created_at' => '2020-08-12 03:36:16',
                'updated_at' => '2020-08-12 03:36:16',
                'deleted_at' => NULL,
            ),
            147 => 
            array (
                'generic_key' => 148,
                'generic_name' => 'Dactinomycin',
                'created_at' => '2020-08-12 03:36:29',
                'updated_at' => '2020-08-12 03:36:29',
                'deleted_at' => NULL,
            ),
            148 => 
            array (
                'generic_key' => 149,
                'generic_name' => 'Dalteparin Sodium PF syringe',
                'created_at' => '2020-08-12 05:20:22',
                'updated_at' => '2020-08-12 05:20:22',
                'deleted_at' => NULL,
            ),
            149 => 
            array (
                'generic_key' => 150,
                'generic_name' => 'Danazol',
                'created_at' => '2020-08-12 05:20:42',
                'updated_at' => '2020-08-12 05:20:42',
                'deleted_at' => NULL,
            ),
            150 => 
            array (
                'generic_key' => 151,
                'generic_name' => 'Dantrolene',
                'created_at' => '2020-08-12 05:20:57',
                'updated_at' => '2020-08-12 05:20:57',
                'deleted_at' => NULL,
            ),
            151 => 
            array (
                'generic_key' => 152,
                'generic_name' => 'Dapsone',
                'created_at' => '2020-08-12 05:21:24',
                'updated_at' => '2020-08-12 05:21:24',
                'deleted_at' => NULL,
            ),
            152 => 
            array (
                'generic_key' => 153,
                'generic_name' => 'Deferoxamine',
                'created_at' => '2020-08-12 05:21:37',
                'updated_at' => '2020-08-12 05:21:37',
                'deleted_at' => NULL,
            ),
            153 => 
            array (
                'generic_key' => 154,
                'generic_name' => 'Dexamethasone Valerate',
                'created_at' => '2020-08-12 05:21:48',
                'updated_at' => '2020-08-12 05:21:48',
                'deleted_at' => NULL,
            ),
            154 => 
            array (
                'generic_key' => 155,
                'generic_name' => 'Dextran, High Molecular Weight',
                'created_at' => '2020-08-12 05:21:59',
                'updated_at' => '2020-08-12 05:21:59',
                'deleted_at' => NULL,
            ),
            155 => 
            array (
                'generic_key' => 156,
                'generic_name' => 'Dextran, Low Molecular Weight',
                'created_at' => '2020-08-12 05:22:12',
                'updated_at' => '2020-08-12 05:22:12',
                'deleted_at' => NULL,
            ),
            156 => 
            array (
                'generic_key' => 157,
                'generic_name' => 'Dextran 40',
                'created_at' => '2020-08-12 05:22:31',
                'updated_at' => '2020-08-12 05:22:31',
                'deleted_at' => NULL,
            ),
            157 => 
            array (
                'generic_key' => 158,
                'generic_name' => 'Dextran 70 High Molecular Wt.',
                'created_at' => '2020-08-12 05:22:52',
                'updated_at' => '2020-08-12 05:22:52',
                'deleted_at' => NULL,
            ),
            158 => 
            array (
                'generic_key' => 159,
                'generic_name' => 'Dextromethorphan',
                'created_at' => '2020-08-12 05:23:08',
                'updated_at' => '2020-08-12 05:23:08',
                'deleted_at' => NULL,
            ),
            159 => 
            array (
                'generic_key' => 160,
                'generic_name' => 'Dextrose 50% Water',
                'created_at' => '2020-08-12 05:23:26',
                'updated_at' => '2020-08-12 05:23:26',
                'deleted_at' => NULL,
            ),
            160 => 
            array (
                'generic_key' => 161,
                'generic_name' => 'D5LR',
                'created_at' => '2020-08-12 05:23:38',
                'updated_at' => '2020-08-12 05:23:38',
                'deleted_at' => NULL,
            ),
            161 => 
            array (
                'generic_key' => 162,
                'generic_name' => 'D5 0.3NACL',
                'created_at' => '2020-08-12 05:23:49',
                'updated_at' => '2020-08-12 05:23:49',
                'deleted_at' => NULL,
            ),
            162 => 
            array (
                'generic_key' => 163,
                'generic_name' => '5% Dextrose In 0.45% Sodium Chloride',
                'created_at' => '2020-08-12 05:24:10',
                'updated_at' => '2020-08-12 05:24:10',
                'deleted_at' => NULL,
            ),
            163 => 
            array (
                'generic_key' => 164,
                'generic_name' => 'D5NSS',
                'created_at' => '2020-08-12 05:24:25',
                'updated_at' => '2020-08-12 05:24:25',
                'deleted_at' => NULL,
            ),
            164 => 
            array (
                'generic_key' => 165,
                'generic_name' => 'D5 Water',
                'created_at' => '2020-08-12 05:24:37',
                'updated_at' => '2020-08-12 05:24:37',
                'deleted_at' => NULL,
            ),
            165 => 
            array (
                'generic_key' => 166,
                'generic_name' => 'D10 Water',
                'created_at' => '2020-08-12 05:24:51',
                'updated_at' => '2020-08-12 05:24:51',
                'deleted_at' => NULL,
            ),
            166 => 
            array (
                'generic_key' => 167,
                'generic_name' => 'Diatrizoate',
                'created_at' => '2020-08-12 05:25:05',
                'updated_at' => '2020-08-12 05:25:05',
                'deleted_at' => NULL,
            ),
            167 => 
            array (
                'generic_key' => 168,
                'generic_name' => 'Diazepam',
                'created_at' => '2020-08-12 05:25:22',
                'updated_at' => '2020-08-12 05:25:22',
                'deleted_at' => NULL,
            ),
            168 => 
            array (
                'generic_key' => 169,
                'generic_name' => 'Dichlorphenamide',
                'created_at' => '2020-08-12 05:26:03',
                'updated_at' => '2020-08-12 05:26:03',
                'deleted_at' => NULL,
            ),
            169 => 
            array (
                'generic_key' => 170,
                'generic_name' => 'Diclofenac Na Eye Drops',
                'created_at' => '2020-08-12 05:26:15',
                'updated_at' => '2020-08-12 05:26:15',
                'deleted_at' => NULL,
            ),
            170 => 
            array (
                'generic_key' => 171,
                'generic_name' => 'Diclofenamide',
                'created_at' => '2020-08-12 05:26:28',
                'updated_at' => '2020-08-12 05:26:28',
                'deleted_at' => NULL,
            ),
            171 => 
            array (
                'generic_key' => 172,
                'generic_name' => 'Dicobalt Edetate',
                'created_at' => '2020-08-12 05:26:44',
                'updated_at' => '2020-08-12 05:26:44',
                'deleted_at' => NULL,
            ),
            172 => 
            array (
                'generic_key' => 173,
            'generic_name' => 'Bisacodyl (Adult)',
                'created_at' => '2020-08-12 05:27:12',
                'updated_at' => '2020-08-12 05:27:12',
                'deleted_at' => NULL,
            ),
            173 => 
            array (
                'generic_key' => 174,
                'generic_name' => 'Dicyclomine',
                'created_at' => '2020-08-12 05:27:33',
                'updated_at' => '2020-08-12 05:27:33',
                'deleted_at' => NULL,
            ),
            174 => 
            array (
                'generic_key' => 175,
                'generic_name' => 'Dicycloverine',
                'created_at' => '2020-08-12 05:27:46',
                'updated_at' => '2020-08-12 05:27:46',
                'deleted_at' => NULL,
            ),
            175 => 
            array (
                'generic_key' => 176,
                'generic_name' => 'Diethylcarbamazine',
                'created_at' => '2020-08-12 05:29:25',
                'updated_at' => '2020-08-12 05:29:25',
                'deleted_at' => NULL,
            ),
            176 => 
            array (
                'generic_key' => 177,
                'generic_name' => 'Digoxin',
                'created_at' => '2020-08-12 05:30:31',
                'updated_at' => '2020-08-12 05:30:31',
                'deleted_at' => NULL,
            ),
            177 => 
            array (
                'generic_key' => 178,
                'generic_name' => 'Diloxanide Furoate',
                'created_at' => '2020-08-12 05:30:47',
                'updated_at' => '2020-08-12 05:30:47',
                'deleted_at' => NULL,
            ),
            178 => 
            array (
                'generic_key' => 179,
                'generic_name' => 'Diltiazem',
                'created_at' => '2020-08-12 05:31:08',
                'updated_at' => '2020-08-12 05:31:08',
                'deleted_at' => NULL,
            ),
            179 => 
            array (
                'generic_key' => 180,
                'generic_name' => 'Dimercaptosuccinic Acid',
                'created_at' => '2020-08-12 05:31:23',
                'updated_at' => '2020-08-12 05:31:23',
                'deleted_at' => NULL,
            ),
            180 => 
            array (
                'generic_key' => 181,
                'generic_name' => 'Dimenhydrinate',
                'created_at' => '2020-08-12 05:31:37',
                'updated_at' => '2020-08-12 05:31:37',
                'deleted_at' => NULL,
            ),
            181 => 
            array (
                'generic_key' => 182,
                'generic_name' => 'Dimercaprol',
                'created_at' => '2020-08-12 05:39:13',
                'updated_at' => '2020-08-12 05:39:13',
                'deleted_at' => NULL,
            ),
            182 => 
            array (
                'generic_key' => 183,
                'generic_name' => 'Diphenhydramine',
                'created_at' => '2020-08-12 05:39:41',
                'updated_at' => '2020-08-12 05:39:41',
                'deleted_at' => NULL,
            ),
            183 => 
            array (
                'generic_key' => 184,
                'generic_name' => 'Diphtheria Antitoxin',
                'created_at' => '2020-08-12 05:39:56',
                'updated_at' => '2020-08-12 05:39:56',
                'deleted_at' => NULL,
            ),
            184 => 
            array (
                'generic_key' => 185,
            'generic_name' => 'Diphtheria - Tetanus Toxoids (DT)',
                'created_at' => '2020-08-12 05:41:16',
                'updated_at' => '2020-08-12 05:41:16',
                'deleted_at' => NULL,
            ),
            185 => 
            array (
                'generic_key' => 186,
            'generic_name' => 'Diphtheria - Tetanus Toxoids (Td)',
                'created_at' => '2020-08-12 05:56:35',
                'updated_at' => '2020-08-12 05:56:35',
                'deleted_at' => NULL,
            ),
            186 => 
            array (
                'generic_key' => 187,
            'generic_name' => 'Diphtheria - Tetanus Toxoids And Pertussis Vaccine (DTP)',
                'created_at' => '2020-08-12 05:56:47',
                'updated_at' => '2020-08-12 05:56:47',
                'deleted_at' => NULL,
            ),
            187 => 
            array (
                'generic_key' => 188,
                'generic_name' => 'Dithranol',
                'created_at' => '2020-08-12 05:57:00',
                'updated_at' => '2020-08-12 05:57:00',
                'deleted_at' => NULL,
            ),
            188 => 
            array (
                'generic_key' => 189,
                'generic_name' => 'Dobutamine',
                'created_at' => '2020-08-12 05:57:14',
                'updated_at' => '2020-08-12 05:57:14',
                'deleted_at' => NULL,
            ),
            189 => 
            array (
                'generic_key' => 190,
                'generic_name' => 'Domperidone',
                'created_at' => '2020-08-12 05:57:51',
                'updated_at' => '2020-08-12 05:57:51',
                'deleted_at' => NULL,
            ),
            190 => 
            array (
                'generic_key' => 191,
                'generic_name' => 'Dopamine HCL in D5 Water',
                'created_at' => '2020-08-12 05:58:03',
                'updated_at' => '2020-08-12 05:58:03',
                'deleted_at' => NULL,
            ),
            191 => 
            array (
                'generic_key' => 192,
                'generic_name' => 'Dosulepin',
                'created_at' => '2020-08-12 05:58:17',
                'updated_at' => '2020-08-12 05:58:17',
                'deleted_at' => NULL,
            ),
            192 => 
            array (
                'generic_key' => 193,
                'generic_name' => 'Dothiepin',
                'created_at' => '2020-08-12 05:58:30',
                'updated_at' => '2020-08-12 05:58:30',
                'deleted_at' => NULL,
            ),
            193 => 
            array (
                'generic_key' => 194,
                'generic_name' => 'Doxorubicin',
                'created_at' => '2020-08-12 05:58:46',
                'updated_at' => '2020-08-12 05:58:46',
                'deleted_at' => NULL,
            ),
            194 => 
            array (
                'generic_key' => 195,
                'generic_name' => 'Doxycycline',
                'created_at' => '2020-08-12 05:59:00',
                'updated_at' => '2020-08-12 05:59:00',
                'deleted_at' => NULL,
            ),
            195 => 
            array (
                'generic_key' => 196,
                'generic_name' => 'Droperidol',
                'created_at' => '2020-08-12 05:59:23',
                'updated_at' => '2020-08-12 05:59:23',
                'deleted_at' => NULL,
            ),
            196 => 
            array (
                'generic_key' => 197,
                'generic_name' => 'DTP + IPV',
                'created_at' => '2020-08-12 05:59:46',
                'updated_at' => '2020-08-12 05:59:46',
                'deleted_at' => NULL,
            ),
            197 => 
            array (
                'generic_key' => 198,
                'generic_name' => 'Duck Embryo Rabies Vaccine',
                'created_at' => '2020-08-12 06:00:07',
                'updated_at' => '2020-08-12 06:00:07',
                'deleted_at' => NULL,
            ),
            198 => 
            array (
                'generic_key' => 199,
                'generic_name' => 'Dydrogesterone',
                'created_at' => '2020-08-12 06:00:22',
                'updated_at' => '2020-08-12 06:00:22',
                'deleted_at' => NULL,
            ),
            199 => 
            array (
                'generic_key' => 200,
                'generic_name' => 'Edrophonium Chloride',
                'created_at' => '2020-08-12 06:00:38',
                'updated_at' => '2020-08-12 06:00:38',
                'deleted_at' => NULL,
            ),
            200 => 
            array (
                'generic_key' => 201,
                'generic_name' => 'Enalapril',
                'created_at' => '2020-08-12 06:00:53',
                'updated_at' => '2020-08-12 06:00:53',
                'deleted_at' => NULL,
            ),
            201 => 
            array (
                'generic_key' => 202,
                'generic_name' => 'Enflurane',
                'created_at' => '2020-08-12 06:01:06',
                'updated_at' => '2020-08-12 06:01:06',
                'deleted_at' => NULL,
            ),
            202 => 
            array (
                'generic_key' => 203,
                'generic_name' => 'Enoxaparin',
                'created_at' => '2020-08-12 06:01:20',
                'updated_at' => '2020-08-12 06:01:20',
                'deleted_at' => NULL,
            ),
            203 => 
            array (
                'generic_key' => 204,
                'generic_name' => 'Ephedrine Sulfate',
                'created_at' => '2020-08-12 06:01:51',
                'updated_at' => '2020-08-12 06:01:51',
                'deleted_at' => NULL,
            ),
            204 => 
            array (
                'generic_key' => 205,
                'generic_name' => 'Epinephrine HCI',
                'created_at' => '2020-08-12 06:02:06',
                'updated_at' => '2020-08-12 06:02:06',
                'deleted_at' => NULL,
            ),
            205 => 
            array (
                'generic_key' => 206,
                'generic_name' => 'Epirubicin',
                'created_at' => '2020-08-12 06:02:33',
                'updated_at' => '2020-08-12 06:02:33',
                'deleted_at' => NULL,
            ),
            206 => 
            array (
                'generic_key' => 207,
                'generic_name' => 'Ergocalciferol',
                'created_at' => '2020-08-12 06:02:46',
                'updated_at' => '2020-08-12 06:02:46',
                'deleted_at' => NULL,
            ),
            207 => 
            array (
                'generic_key' => 208,
                'generic_name' => 'Ergometrine',
                'created_at' => '2020-08-12 06:02:59',
                'updated_at' => '2020-08-12 06:02:59',
                'deleted_at' => NULL,
            ),
            208 => 
            array (
                'generic_key' => 209,
                'generic_name' => 'Ergonovine',
                'created_at' => '2020-08-12 06:03:11',
                'updated_at' => '2020-08-12 06:03:11',
                'deleted_at' => NULL,
            ),
            209 => 
            array (
                'generic_key' => 210,
                'generic_name' => 'Ergotamine',
                'created_at' => '2020-08-12 06:03:25',
                'updated_at' => '2020-08-12 06:03:25',
                'deleted_at' => NULL,
            ),
            210 => 
            array (
                'generic_key' => 211,
                'generic_name' => 'Erythromycin',
                'created_at' => '2020-08-12 06:03:36',
                'updated_at' => '2020-08-12 06:03:36',
                'deleted_at' => NULL,
            ),
            211 => 
            array (
                'generic_key' => 212,
                'generic_name' => 'Esmolol HCl',
                'created_at' => '2020-08-12 06:03:59',
                'updated_at' => '2020-08-12 06:03:59',
                'deleted_at' => NULL,
            ),
            212 => 
            array (
                'generic_key' => 213,
                'generic_name' => 'Estazolam',
                'created_at' => '2020-08-12 06:09:06',
                'updated_at' => '2020-08-12 06:09:06',
                'deleted_at' => NULL,
            ),
            213 => 
            array (
                'generic_key' => 214,
                'generic_name' => 'Estramustine',
                'created_at' => '2020-08-12 06:18:41',
                'updated_at' => '2020-08-12 06:18:41',
                'deleted_at' => NULL,
            ),
            214 => 
            array (
                'generic_key' => 215,
                'generic_name' => 'Ethambutol',
                'created_at' => '2020-08-12 06:18:57',
                'updated_at' => '2020-08-12 06:18:57',
                'deleted_at' => NULL,
            ),
            215 => 
            array (
                'generic_key' => 216,
                'generic_name' => 'Ethinylestradiol',
                'created_at' => '2020-08-12 06:19:09',
                'updated_at' => '2020-08-12 06:19:09',
                'deleted_at' => NULL,
            ),
            216 => 
            array (
                'generic_key' => 217,
                'generic_name' => 'Ethinylestradiol + Levonorgestrel',
                'created_at' => '2020-08-12 06:19:26',
                'updated_at' => '2020-08-12 06:19:26',
                'deleted_at' => NULL,
            ),
            217 => 
            array (
                'generic_key' => 218,
                'generic_name' => 'Ethinylestradiol + Norethisterone',
                'created_at' => '2020-08-12 06:19:44',
                'updated_at' => '2020-08-12 06:19:44',
                'deleted_at' => NULL,
            ),
            218 => 
            array (
                'generic_key' => 219,
                'generic_name' => 'Etodolac',
                'created_at' => '2020-08-12 06:20:08',
                'updated_at' => '2020-08-12 06:20:08',
                'deleted_at' => NULL,
            ),
            219 => 
            array (
                'generic_key' => 220,
                'generic_name' => 'Etoposide',
                'created_at' => '2020-08-12 06:20:28',
                'updated_at' => '2020-08-12 06:20:28',
                'deleted_at' => NULL,
            ),
            220 => 
            array (
                'generic_key' => 221,
                'generic_name' => 'Factor  Viii Concentrate',
                'created_at' => '2020-08-12 06:20:47',
                'updated_at' => '2020-08-12 06:20:47',
                'deleted_at' => NULL,
            ),
            221 => 
            array (
                'generic_key' => 222,
                'generic_name' => 'Factor Ix Complex Concentrate',
                'created_at' => '2020-08-12 06:21:06',
                'updated_at' => '2020-08-12 06:21:06',
                'deleted_at' => NULL,
            ),
            222 => 
            array (
                'generic_key' => 223,
                'generic_name' => 'Famotidine',
                'created_at' => '2020-08-12 06:21:18',
                'updated_at' => '2020-08-12 06:21:18',
                'deleted_at' => NULL,
            ),
            223 => 
            array (
                'generic_key' => 224,
                'generic_name' => 'Felodipine',
                'created_at' => '2020-08-12 06:21:30',
                'updated_at' => '2020-08-12 06:21:30',
                'deleted_at' => NULL,
            ),
            224 => 
            array (
                'generic_key' => 225,
                'generic_name' => 'Fentanyl Citrate',
                'created_at' => '2020-08-12 06:21:43',
                'updated_at' => '2020-08-12 06:21:43',
                'deleted_at' => NULL,
            ),
            225 => 
            array (
                'generic_key' => 226,
                'generic_name' => 'Ferrous Salt',
                'created_at' => '2020-08-12 06:21:58',
                'updated_at' => '2020-08-12 06:21:58',
                'deleted_at' => NULL,
            ),
            226 => 
            array (
                'generic_key' => 227,
                'generic_name' => 'Ferrous Salt + Folic Acid',
                'created_at' => '2020-08-12 06:22:15',
                'updated_at' => '2020-08-12 06:22:15',
                'deleted_at' => NULL,
            ),
            227 => 
            array (
                'generic_key' => 228,
                'generic_name' => 'Fluconazole',
                'created_at' => '2020-08-12 06:22:29',
                'updated_at' => '2020-08-12 06:22:29',
                'deleted_at' => NULL,
            ),
            228 => 
            array (
                'generic_key' => 229,
                'generic_name' => 'Flucytosine',
                'created_at' => '2020-08-12 06:22:51',
                'updated_at' => '2020-08-12 06:22:51',
                'deleted_at' => NULL,
            ),
            229 => 
            array (
                'generic_key' => 230,
                'generic_name' => 'Fludrocortisone',
                'created_at' => '2020-08-12 06:23:33',
                'updated_at' => '2020-08-12 06:23:33',
                'deleted_at' => NULL,
            ),
            230 => 
            array (
                'generic_key' => 231,
                'generic_name' => 'Flumazenil',
                'created_at' => '2020-08-12 06:23:50',
                'updated_at' => '2020-08-12 06:23:50',
                'deleted_at' => NULL,
            ),
            231 => 
            array (
                'generic_key' => 232,
                'generic_name' => 'Flunarizine',
                'created_at' => '2020-08-12 06:24:06',
                'updated_at' => '2020-08-12 06:24:06',
                'deleted_at' => NULL,
            ),
            232 => 
            array (
                'generic_key' => 233,
                'generic_name' => 'Fluocinonide',
                'created_at' => '2020-08-12 06:24:25',
                'updated_at' => '2020-08-12 06:24:25',
                'deleted_at' => NULL,
            ),
            233 => 
            array (
                'generic_key' => 234,
                'generic_name' => 'Fluorescein',
                'created_at' => '2020-08-12 06:24:40',
                'updated_at' => '2020-08-12 06:24:40',
                'deleted_at' => NULL,
            ),
            234 => 
            array (
                'generic_key' => 235,
                'generic_name' => 'Fluoride',
                'created_at' => '2020-08-12 06:26:05',
                'updated_at' => '2020-08-12 06:26:05',
                'deleted_at' => NULL,
            ),
            235 => 
            array (
                'generic_key' => 236,
                'generic_name' => '5-Fluorocytosine',
                'created_at' => '2020-08-12 06:26:21',
                'updated_at' => '2020-08-12 06:26:21',
                'deleted_at' => NULL,
            ),
            236 => 
            array (
                'generic_key' => 237,
                'generic_name' => 'Fluorouracil',
                'created_at' => '2020-08-12 06:26:33',
                'updated_at' => '2020-08-12 06:26:33',
                'deleted_at' => NULL,
            ),
            237 => 
            array (
                'generic_key' => 238,
                'generic_name' => 'Fluoxetine HCI',
                'created_at' => '2020-08-12 06:36:49',
                'updated_at' => '2020-08-12 06:36:49',
                'deleted_at' => NULL,
            ),
            238 => 
            array (
                'generic_key' => 239,
            'generic_name' => 'Flupentixol (Depot)',
                'created_at' => '2020-08-12 06:37:01',
                'updated_at' => '2020-08-12 06:37:01',
                'deleted_at' => NULL,
            ),
            239 => 
            array (
                'generic_key' => 240,
            'generic_name' => 'Fluphenazine (Depot)',
                'created_at' => '2020-08-12 06:37:15',
                'updated_at' => '2020-08-12 06:37:15',
                'deleted_at' => NULL,
            ),
            240 => 
            array (
                'generic_key' => 241,
                'generic_name' => 'Flurazepam',
                'created_at' => '2020-08-12 06:37:40',
                'updated_at' => '2020-08-12 06:37:40',
                'deleted_at' => NULL,
            ),
            241 => 
            array (
                'generic_key' => 242,
                'generic_name' => 'Flutamide',
                'created_at' => '2020-08-12 06:38:05',
                'updated_at' => '2020-08-12 06:38:05',
                'deleted_at' => NULL,
            ),
            242 => 
            array (
                'generic_key' => 243,
                'generic_name' => 'Folic Acid',
                'created_at' => '2020-08-12 06:38:17',
                'updated_at' => '2020-08-12 06:38:17',
                'deleted_at' => NULL,
            ),
            243 => 
            array (
                'generic_key' => 244,
                'generic_name' => 'Framycetin',
                'created_at' => '2020-08-12 06:38:31',
                'updated_at' => '2020-08-12 06:38:31',
                'deleted_at' => NULL,
            ),
            244 => 
            array (
                'generic_key' => 245,
                'generic_name' => 'Furosemide',
                'created_at' => '2020-08-12 06:38:44',
                'updated_at' => '2020-08-12 06:38:44',
                'deleted_at' => NULL,
            ),
            245 => 
            array (
                'generic_key' => 246,
                'generic_name' => 'Fusidate Sodium/Fusidic Acid',
                'created_at' => '2020-08-12 06:38:56',
                'updated_at' => '2020-08-12 06:38:56',
                'deleted_at' => NULL,
            ),
            246 => 
            array (
                'generic_key' => 247,
                'generic_name' => 'Ganciclovir',
                'created_at' => '2020-08-12 06:39:13',
                'updated_at' => '2020-08-12 06:39:13',
                'deleted_at' => NULL,
            ),
            247 => 
            array (
                'generic_key' => 248,
                'generic_name' => 'Gas Forming Agent',
                'created_at' => '2020-08-12 06:39:26',
                'updated_at' => '2020-08-12 06:39:26',
                'deleted_at' => NULL,
            ),
            248 => 
            array (
                'generic_key' => 249,
                'generic_name' => 'Gemfibrozil',
                'created_at' => '2020-08-12 06:39:38',
                'updated_at' => '2020-08-12 06:39:38',
                'deleted_at' => NULL,
            ),
            249 => 
            array (
                'generic_key' => 250,
                'generic_name' => 'Gentamycin',
                'created_at' => '2020-08-12 06:40:24',
                'updated_at' => '2020-08-12 06:40:24',
                'deleted_at' => NULL,
            ),
            250 => 
            array (
                'generic_key' => 251,
                'generic_name' => 'Gentamycin + Betamethasone',
                'created_at' => '2020-08-12 06:40:55',
                'updated_at' => '2020-08-12 06:40:55',
                'deleted_at' => NULL,
            ),
            251 => 
            array (
                'generic_key' => 252,
                'generic_name' => 'Glibenclamide',
                'created_at' => '2020-08-12 06:41:06',
                'updated_at' => '2020-08-12 06:41:06',
                'deleted_at' => NULL,
            ),
            252 => 
            array (
                'generic_key' => 253,
                'generic_name' => 'Gliclazide',
                'created_at' => '2020-08-12 06:41:22',
                'updated_at' => '2020-08-12 06:41:22',
                'deleted_at' => NULL,
            ),
            253 => 
            array (
                'generic_key' => 254,
                'generic_name' => 'Glipizide',
                'created_at' => '2020-08-12 06:41:36',
                'updated_at' => '2020-08-12 06:41:36',
                'deleted_at' => NULL,
            ),
            254 => 
            array (
                'generic_key' => 255,
                'generic_name' => 'Glucagon',
                'created_at' => '2020-08-12 06:42:02',
                'updated_at' => '2020-08-12 06:42:02',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}