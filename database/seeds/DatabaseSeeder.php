<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(GenericNamesTableSeeder::class);
        $this->call(EquipmentTableSeeder::class);
        $this->call(GenericNamesTableSeeder::class);
        $this->call(GenericNamesTableSeeder::class);
        $this->call(GenericNamesTableSeeder::class);
        $this->call(GenericNamesTableSeeder::class);
    }
}
