<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicineAdjustment;

class MedicineAdjustmentApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medicine_adjustments', $medicineAdjustment
        );

        $this->assertApiResponse($medicineAdjustment);
    }

    /**
     * @test
     */
    public function test_read_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/medicine_adjustments/'.$medicineAdjustment->id
        );

        $this->assertApiResponse($medicineAdjustment->toArray());
    }

    /**
     * @test
     */
    public function test_update_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->create();
        $editedMedicineAdjustment = factory(MedicineAdjustment::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medicine_adjustments/'.$medicineAdjustment->id,
            $editedMedicineAdjustment
        );

        $this->assertApiResponse($editedMedicineAdjustment);
    }

    /**
     * @test
     */
    public function test_delete_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medicine_adjustments/'.$medicineAdjustment->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medicine_adjustments/'.$medicineAdjustment->id
        );

        $this->response->assertStatus(404);
    }
}
