<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicineCreditNote;

class MedicineCreditNoteApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medicine_credit_notes', $medicineCreditNote
        );

        $this->assertApiResponse($medicineCreditNote);
    }

    /**
     * @test
     */
    public function test_read_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/medicine_credit_notes/'.$medicineCreditNote->id
        );

        $this->assertApiResponse($medicineCreditNote->toArray());
    }

    /**
     * @test
     */
    public function test_update_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->create();
        $editedMedicineCreditNote = factory(MedicineCreditNote::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medicine_credit_notes/'.$medicineCreditNote->id,
            $editedMedicineCreditNote
        );

        $this->assertApiResponse($editedMedicineCreditNote);
    }

    /**
     * @test
     */
    public function test_delete_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medicine_credit_notes/'.$medicineCreditNote->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medicine_credit_notes/'.$medicineCreditNote->id
        );

        $this->response->assertStatus(404);
    }
}
