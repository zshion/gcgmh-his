<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicineStockbalance;

class MedicineStockbalanceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medicine_stockbalances', $medicineStockbalance
        );

        $this->assertApiResponse($medicineStockbalance);
    }

    /**
     * @test
     */
    public function test_read_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/medicine_stockbalances/'.$medicineStockbalance->id
        );

        $this->assertApiResponse($medicineStockbalance->toArray());
    }

    /**
     * @test
     */
    public function test_update_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->create();
        $editedMedicineStockbalance = factory(MedicineStockbalance::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medicine_stockbalances/'.$medicineStockbalance->id,
            $editedMedicineStockbalance
        );

        $this->assertApiResponse($editedMedicineStockbalance);
    }

    /**
     * @test
     */
    public function test_delete_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medicine_stockbalances/'.$medicineStockbalance->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medicine_stockbalances/'.$medicineStockbalance->id
        );

        $this->response->assertStatus(404);
    }
}
