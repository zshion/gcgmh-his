<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicineDelivery;

class MedicineDeliveryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medicine_deliveries', $medicineDelivery
        );

        $this->assertApiResponse($medicineDelivery);
    }

    /**
     * @test
     */
    public function test_read_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/medicine_deliveries/'.$medicineDelivery->id
        );

        $this->assertApiResponse($medicineDelivery->toArray());
    }

    /**
     * @test
     */
    public function test_update_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->create();
        $editedMedicineDelivery = factory(MedicineDelivery::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medicine_deliveries/'.$medicineDelivery->id,
            $editedMedicineDelivery
        );

        $this->assertApiResponse($editedMedicineDelivery);
    }

    /**
     * @test
     */
    public function test_delete_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medicine_deliveries/'.$medicineDelivery->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medicine_deliveries/'.$medicineDelivery->id
        );

        $this->response->assertStatus(404);
    }
}
