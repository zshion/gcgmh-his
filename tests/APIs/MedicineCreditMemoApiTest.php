<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicineCreditMemo;

class MedicineCreditMemoApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medicine_credit_memos', $medicineCreditMemo
        );

        $this->assertApiResponse($medicineCreditMemo);
    }

    /**
     * @test
     */
    public function test_read_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/medicine_credit_memos/'.$medicineCreditMemo->id
        );

        $this->assertApiResponse($medicineCreditMemo->toArray());
    }

    /**
     * @test
     */
    public function test_update_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->create();
        $editedMedicineCreditMemo = factory(MedicineCreditMemo::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medicine_credit_memos/'.$medicineCreditMemo->id,
            $editedMedicineCreditMemo
        );

        $this->assertApiResponse($editedMedicineCreditMemo);
    }

    /**
     * @test
     */
    public function test_delete_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medicine_credit_memos/'.$medicineCreditMemo->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medicine_credit_memos/'.$medicineCreditMemo->id
        );

        $this->response->assertStatus(404);
    }
}
