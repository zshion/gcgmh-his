<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MedicineExpiredDamage;

class MedicineExpiredDamageApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/medicine_expired_damages', $medicineExpiredDamage
        );

        $this->assertApiResponse($medicineExpiredDamage);
    }

    /**
     * @test
     */
    public function test_read_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->create();

        $this->response = $this->json(
            'GET',
            '/api/medicine_expired_damages/'.$medicineExpiredDamage->id
        );

        $this->assertApiResponse($medicineExpiredDamage->toArray());
    }

    /**
     * @test
     */
    public function test_update_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->create();
        $editedMedicineExpiredDamage = factory(MedicineExpiredDamage::class)->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/medicine_expired_damages/'.$medicineExpiredDamage->id,
            $editedMedicineExpiredDamage
        );

        $this->assertApiResponse($editedMedicineExpiredDamage);
    }

    /**
     * @test
     */
    public function test_delete_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->create();

        $this->response = $this->json(
            'DELETE',
             '/api/medicine_expired_damages/'.$medicineExpiredDamage->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/medicine_expired_damages/'.$medicineExpiredDamage->id
        );

        $this->response->assertStatus(404);
    }
}
