<?php namespace Tests\Repositories;

use App\Models\MedicineAdjustment;
use App\Repositories\MedicineAdjustmentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicineAdjustmentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicineAdjustmentRepository
     */
    protected $medicineAdjustmentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicineAdjustmentRepo = \App::make(MedicineAdjustmentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->make()->toArray();

        $createdMedicineAdjustment = $this->medicineAdjustmentRepo->create($medicineAdjustment);

        $createdMedicineAdjustment = $createdMedicineAdjustment->toArray();
        $this->assertArrayHasKey('id', $createdMedicineAdjustment);
        $this->assertNotNull($createdMedicineAdjustment['id'], 'Created MedicineAdjustment must have id specified');
        $this->assertNotNull(MedicineAdjustment::find($createdMedicineAdjustment['id']), 'MedicineAdjustment with given id must be in DB');
        $this->assertModelData($medicineAdjustment, $createdMedicineAdjustment);
    }

    /**
     * @test read
     */
    public function test_read_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->create();

        $dbMedicineAdjustment = $this->medicineAdjustmentRepo->find($medicineAdjustment->id);

        $dbMedicineAdjustment = $dbMedicineAdjustment->toArray();
        $this->assertModelData($medicineAdjustment->toArray(), $dbMedicineAdjustment);
    }

    /**
     * @test update
     */
    public function test_update_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->create();
        $fakeMedicineAdjustment = factory(MedicineAdjustment::class)->make()->toArray();

        $updatedMedicineAdjustment = $this->medicineAdjustmentRepo->update($fakeMedicineAdjustment, $medicineAdjustment->id);

        $this->assertModelData($fakeMedicineAdjustment, $updatedMedicineAdjustment->toArray());
        $dbMedicineAdjustment = $this->medicineAdjustmentRepo->find($medicineAdjustment->id);
        $this->assertModelData($fakeMedicineAdjustment, $dbMedicineAdjustment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medicine_adjustment()
    {
        $medicineAdjustment = factory(MedicineAdjustment::class)->create();

        $resp = $this->medicineAdjustmentRepo->delete($medicineAdjustment->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicineAdjustment::find($medicineAdjustment->id), 'MedicineAdjustment should not exist in DB');
    }
}
