<?php namespace Tests\Repositories;

use App\Models\MedicineExpiredDamage;
use App\Repositories\MedicineExpiredDamageRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicineExpiredDamageRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicineExpiredDamageRepository
     */
    protected $medicineExpiredDamageRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicineExpiredDamageRepo = \App::make(MedicineExpiredDamageRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->make()->toArray();

        $createdMedicineExpiredDamage = $this->medicineExpiredDamageRepo->create($medicineExpiredDamage);

        $createdMedicineExpiredDamage = $createdMedicineExpiredDamage->toArray();
        $this->assertArrayHasKey('id', $createdMedicineExpiredDamage);
        $this->assertNotNull($createdMedicineExpiredDamage['id'], 'Created MedicineExpiredDamage must have id specified');
        $this->assertNotNull(MedicineExpiredDamage::find($createdMedicineExpiredDamage['id']), 'MedicineExpiredDamage with given id must be in DB');
        $this->assertModelData($medicineExpiredDamage, $createdMedicineExpiredDamage);
    }

    /**
     * @test read
     */
    public function test_read_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->create();

        $dbMedicineExpiredDamage = $this->medicineExpiredDamageRepo->find($medicineExpiredDamage->id);

        $dbMedicineExpiredDamage = $dbMedicineExpiredDamage->toArray();
        $this->assertModelData($medicineExpiredDamage->toArray(), $dbMedicineExpiredDamage);
    }

    /**
     * @test update
     */
    public function test_update_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->create();
        $fakeMedicineExpiredDamage = factory(MedicineExpiredDamage::class)->make()->toArray();

        $updatedMedicineExpiredDamage = $this->medicineExpiredDamageRepo->update($fakeMedicineExpiredDamage, $medicineExpiredDamage->id);

        $this->assertModelData($fakeMedicineExpiredDamage, $updatedMedicineExpiredDamage->toArray());
        $dbMedicineExpiredDamage = $this->medicineExpiredDamageRepo->find($medicineExpiredDamage->id);
        $this->assertModelData($fakeMedicineExpiredDamage, $dbMedicineExpiredDamage->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medicine_expired_damage()
    {
        $medicineExpiredDamage = factory(MedicineExpiredDamage::class)->create();

        $resp = $this->medicineExpiredDamageRepo->delete($medicineExpiredDamage->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicineExpiredDamage::find($medicineExpiredDamage->id), 'MedicineExpiredDamage should not exist in DB');
    }
}
