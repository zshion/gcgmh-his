<?php namespace Tests\Repositories;

use App\Models\MedicineDelivery;
use App\Repositories\MedicineDeliveryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicineDeliveryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicineDeliveryRepository
     */
    protected $medicineDeliveryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicineDeliveryRepo = \App::make(MedicineDeliveryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->make()->toArray();

        $createdMedicineDelivery = $this->medicineDeliveryRepo->create($medicineDelivery);

        $createdMedicineDelivery = $createdMedicineDelivery->toArray();
        $this->assertArrayHasKey('id', $createdMedicineDelivery);
        $this->assertNotNull($createdMedicineDelivery['id'], 'Created MedicineDelivery must have id specified');
        $this->assertNotNull(MedicineDelivery::find($createdMedicineDelivery['id']), 'MedicineDelivery with given id must be in DB');
        $this->assertModelData($medicineDelivery, $createdMedicineDelivery);
    }

    /**
     * @test read
     */
    public function test_read_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->create();

        $dbMedicineDelivery = $this->medicineDeliveryRepo->find($medicineDelivery->id);

        $dbMedicineDelivery = $dbMedicineDelivery->toArray();
        $this->assertModelData($medicineDelivery->toArray(), $dbMedicineDelivery);
    }

    /**
     * @test update
     */
    public function test_update_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->create();
        $fakeMedicineDelivery = factory(MedicineDelivery::class)->make()->toArray();

        $updatedMedicineDelivery = $this->medicineDeliveryRepo->update($fakeMedicineDelivery, $medicineDelivery->id);

        $this->assertModelData($fakeMedicineDelivery, $updatedMedicineDelivery->toArray());
        $dbMedicineDelivery = $this->medicineDeliveryRepo->find($medicineDelivery->id);
        $this->assertModelData($fakeMedicineDelivery, $dbMedicineDelivery->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medicine_delivery()
    {
        $medicineDelivery = factory(MedicineDelivery::class)->create();

        $resp = $this->medicineDeliveryRepo->delete($medicineDelivery->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicineDelivery::find($medicineDelivery->id), 'MedicineDelivery should not exist in DB');
    }
}
