<?php namespace Tests\Repositories;

use App\Models\MedicineStockbalance;
use App\Repositories\MedicineStockbalanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicineStockbalanceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicineStockbalanceRepository
     */
    protected $medicineStockbalanceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicineStockbalanceRepo = \App::make(MedicineStockbalanceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->make()->toArray();

        $createdMedicineStockbalance = $this->medicineStockbalanceRepo->create($medicineStockbalance);

        $createdMedicineStockbalance = $createdMedicineStockbalance->toArray();
        $this->assertArrayHasKey('id', $createdMedicineStockbalance);
        $this->assertNotNull($createdMedicineStockbalance['id'], 'Created MedicineStockbalance must have id specified');
        $this->assertNotNull(MedicineStockbalance::find($createdMedicineStockbalance['id']), 'MedicineStockbalance with given id must be in DB');
        $this->assertModelData($medicineStockbalance, $createdMedicineStockbalance);
    }

    /**
     * @test read
     */
    public function test_read_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->create();

        $dbMedicineStockbalance = $this->medicineStockbalanceRepo->find($medicineStockbalance->id);

        $dbMedicineStockbalance = $dbMedicineStockbalance->toArray();
        $this->assertModelData($medicineStockbalance->toArray(), $dbMedicineStockbalance);
    }

    /**
     * @test update
     */
    public function test_update_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->create();
        $fakeMedicineStockbalance = factory(MedicineStockbalance::class)->make()->toArray();

        $updatedMedicineStockbalance = $this->medicineStockbalanceRepo->update($fakeMedicineStockbalance, $medicineStockbalance->id);

        $this->assertModelData($fakeMedicineStockbalance, $updatedMedicineStockbalance->toArray());
        $dbMedicineStockbalance = $this->medicineStockbalanceRepo->find($medicineStockbalance->id);
        $this->assertModelData($fakeMedicineStockbalance, $dbMedicineStockbalance->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medicine_stockbalance()
    {
        $medicineStockbalance = factory(MedicineStockbalance::class)->create();

        $resp = $this->medicineStockbalanceRepo->delete($medicineStockbalance->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicineStockbalance::find($medicineStockbalance->id), 'MedicineStockbalance should not exist in DB');
    }
}
