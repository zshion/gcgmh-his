<?php namespace Tests\Repositories;

use App\Models\equipment;
use App\Repositories\equipmentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class equipmentRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var equipmentRepository
     */
    protected $equipmentRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->equipmentRepo = \App::make(equipmentRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_equipment()
    {
        $equipment = factory(equipment::class)->make()->toArray();

        $createdequipment = $this->equipmentRepo->create($equipment);

        $createdequipment = $createdequipment->toArray();
        $this->assertArrayHasKey('id', $createdequipment);
        $this->assertNotNull($createdequipment['id'], 'Created equipment must have id specified');
        $this->assertNotNull(equipment::find($createdequipment['id']), 'equipment with given id must be in DB');
        $this->assertModelData($equipment, $createdequipment);
    }

    /**
     * @test read
     */
    public function test_read_equipment()
    {
        $equipment = factory(equipment::class)->create();

        $dbequipment = $this->equipmentRepo->find($equipment->id);

        $dbequipment = $dbequipment->toArray();
        $this->assertModelData($equipment->toArray(), $dbequipment);
    }

    /**
     * @test update
     */
    public function test_update_equipment()
    {
        $equipment = factory(equipment::class)->create();
        $fakeequipment = factory(equipment::class)->make()->toArray();

        $updatedequipment = $this->equipmentRepo->update($fakeequipment, $equipment->id);

        $this->assertModelData($fakeequipment, $updatedequipment->toArray());
        $dbequipment = $this->equipmentRepo->find($equipment->id);
        $this->assertModelData($fakeequipment, $dbequipment->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_equipment()
    {
        $equipment = factory(equipment::class)->create();

        $resp = $this->equipmentRepo->delete($equipment->id);

        $this->assertTrue($resp);
        $this->assertNull(equipment::find($equipment->id), 'equipment should not exist in DB');
    }
}
