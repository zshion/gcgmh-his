<?php namespace Tests\Repositories;

use App\Models\MedicineCreditMemo;
use App\Repositories\MedicineCreditMemoRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicineCreditMemoRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicineCreditMemoRepository
     */
    protected $medicineCreditMemoRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicineCreditMemoRepo = \App::make(MedicineCreditMemoRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->make()->toArray();

        $createdMedicineCreditMemo = $this->medicineCreditMemoRepo->create($medicineCreditMemo);

        $createdMedicineCreditMemo = $createdMedicineCreditMemo->toArray();
        $this->assertArrayHasKey('id', $createdMedicineCreditMemo);
        $this->assertNotNull($createdMedicineCreditMemo['id'], 'Created MedicineCreditMemo must have id specified');
        $this->assertNotNull(MedicineCreditMemo::find($createdMedicineCreditMemo['id']), 'MedicineCreditMemo with given id must be in DB');
        $this->assertModelData($medicineCreditMemo, $createdMedicineCreditMemo);
    }

    /**
     * @test read
     */
    public function test_read_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->create();

        $dbMedicineCreditMemo = $this->medicineCreditMemoRepo->find($medicineCreditMemo->id);

        $dbMedicineCreditMemo = $dbMedicineCreditMemo->toArray();
        $this->assertModelData($medicineCreditMemo->toArray(), $dbMedicineCreditMemo);
    }

    /**
     * @test update
     */
    public function test_update_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->create();
        $fakeMedicineCreditMemo = factory(MedicineCreditMemo::class)->make()->toArray();

        $updatedMedicineCreditMemo = $this->medicineCreditMemoRepo->update($fakeMedicineCreditMemo, $medicineCreditMemo->id);

        $this->assertModelData($fakeMedicineCreditMemo, $updatedMedicineCreditMemo->toArray());
        $dbMedicineCreditMemo = $this->medicineCreditMemoRepo->find($medicineCreditMemo->id);
        $this->assertModelData($fakeMedicineCreditMemo, $dbMedicineCreditMemo->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medicine_credit_memo()
    {
        $medicineCreditMemo = factory(MedicineCreditMemo::class)->create();

        $resp = $this->medicineCreditMemoRepo->delete($medicineCreditMemo->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicineCreditMemo::find($medicineCreditMemo->id), 'MedicineCreditMemo should not exist in DB');
    }
}
