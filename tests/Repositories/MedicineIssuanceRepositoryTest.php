<?php namespace Tests\Repositories;

use App\Models\MedicineIssuance;
use App\Repositories\MedicineIssuanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicineIssuanceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicineIssuanceRepository
     */
    protected $medicineIssuanceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicineIssuanceRepo = \App::make(MedicineIssuanceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medicine_issuance()
    {
        $medicineIssuance = factory(MedicineIssuance::class)->make()->toArray();

        $createdMedicineIssuance = $this->medicineIssuanceRepo->create($medicineIssuance);

        $createdMedicineIssuance = $createdMedicineIssuance->toArray();
        $this->assertArrayHasKey('id', $createdMedicineIssuance);
        $this->assertNotNull($createdMedicineIssuance['id'], 'Created MedicineIssuance must have id specified');
        $this->assertNotNull(MedicineIssuance::find($createdMedicineIssuance['id']), 'MedicineIssuance with given id must be in DB');
        $this->assertModelData($medicineIssuance, $createdMedicineIssuance);
    }

    /**
     * @test read
     */
    public function test_read_medicine_issuance()
    {
        $medicineIssuance = factory(MedicineIssuance::class)->create();

        $dbMedicineIssuance = $this->medicineIssuanceRepo->find($medicineIssuance->id);

        $dbMedicineIssuance = $dbMedicineIssuance->toArray();
        $this->assertModelData($medicineIssuance->toArray(), $dbMedicineIssuance);
    }

    /**
     * @test update
     */
    public function test_update_medicine_issuance()
    {
        $medicineIssuance = factory(MedicineIssuance::class)->create();
        $fakeMedicineIssuance = factory(MedicineIssuance::class)->make()->toArray();

        $updatedMedicineIssuance = $this->medicineIssuanceRepo->update($fakeMedicineIssuance, $medicineIssuance->id);

        $this->assertModelData($fakeMedicineIssuance, $updatedMedicineIssuance->toArray());
        $dbMedicineIssuance = $this->medicineIssuanceRepo->find($medicineIssuance->id);
        $this->assertModelData($fakeMedicineIssuance, $dbMedicineIssuance->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medicine_issuance()
    {
        $medicineIssuance = factory(MedicineIssuance::class)->create();

        $resp = $this->medicineIssuanceRepo->delete($medicineIssuance->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicineIssuance::find($medicineIssuance->id), 'MedicineIssuance should not exist in DB');
    }
}
