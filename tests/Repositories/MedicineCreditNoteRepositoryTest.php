<?php namespace Tests\Repositories;

use App\Models\MedicineCreditNote;
use App\Repositories\MedicineCreditNoteRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MedicineCreditNoteRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MedicineCreditNoteRepository
     */
    protected $medicineCreditNoteRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->medicineCreditNoteRepo = \App::make(MedicineCreditNoteRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->make()->toArray();

        $createdMedicineCreditNote = $this->medicineCreditNoteRepo->create($medicineCreditNote);

        $createdMedicineCreditNote = $createdMedicineCreditNote->toArray();
        $this->assertArrayHasKey('id', $createdMedicineCreditNote);
        $this->assertNotNull($createdMedicineCreditNote['id'], 'Created MedicineCreditNote must have id specified');
        $this->assertNotNull(MedicineCreditNote::find($createdMedicineCreditNote['id']), 'MedicineCreditNote with given id must be in DB');
        $this->assertModelData($medicineCreditNote, $createdMedicineCreditNote);
    }

    /**
     * @test read
     */
    public function test_read_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->create();

        $dbMedicineCreditNote = $this->medicineCreditNoteRepo->find($medicineCreditNote->id);

        $dbMedicineCreditNote = $dbMedicineCreditNote->toArray();
        $this->assertModelData($medicineCreditNote->toArray(), $dbMedicineCreditNote);
    }

    /**
     * @test update
     */
    public function test_update_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->create();
        $fakeMedicineCreditNote = factory(MedicineCreditNote::class)->make()->toArray();

        $updatedMedicineCreditNote = $this->medicineCreditNoteRepo->update($fakeMedicineCreditNote, $medicineCreditNote->id);

        $this->assertModelData($fakeMedicineCreditNote, $updatedMedicineCreditNote->toArray());
        $dbMedicineCreditNote = $this->medicineCreditNoteRepo->find($medicineCreditNote->id);
        $this->assertModelData($fakeMedicineCreditNote, $dbMedicineCreditNote->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_medicine_credit_note()
    {
        $medicineCreditNote = factory(MedicineCreditNote::class)->create();

        $resp = $this->medicineCreditNoteRepo->delete($medicineCreditNote->id);

        $this->assertTrue($resp);
        $this->assertNull(MedicineCreditNote::find($medicineCreditNote->id), 'MedicineCreditNote should not exist in DB');
    }
}
