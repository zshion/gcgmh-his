-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2020 at 08:44 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `allsystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `subject_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'Loggin Something', NULL, NULL, NULL, NULL, '[]', '2020-06-07 09:13:49', '2020-06-07 09:13:49'),
(2, 'default', 'created', 15, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":15,\"equipment_name\":\"sample asdasd\",\"equipment_serial\":null,\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-08T07:24:30.000000Z\",\"updated_at\":\"2020-06-08T07:24:30.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":null,\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":null,\"equipment_supplier\":null,\"equipment_unit_price\":null,\"equipment_location\":null,\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":null,\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null}}', '2020-06-07 23:24:30', '2020-06-07 23:24:30'),
(3, 'default', 'deleted', 9, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":9,\"equipment_name\":\"sample\",\"equipment_serial\":\"sa12837618263\",\"equipment_model\":\"sample\",\"equipment_quantity\":1232,\"created_at\":\"2020-06-07T12:47:42.000000Z\",\"updated_at\":\"2020-06-07T13:42:34.000000Z\",\"deleted_at\":null,\"equipment_description\":\"sample\",\"equipment_brand\":\"samsungs\",\"equipment_country\":\"philippines\",\"equipment_date_delivered\":\"2020-06-07 20:47:00\",\"equipment_date_inspected\":\"2020-06-07 20:46:00\",\"equipment_date_acquired\":\"2020-06-07 20:46:00\",\"equipment_supplier\":\"samplas\",\"equipment_unit_price\":12381,\"equipment_location\":\"Information\",\"equipment_tag_number\":\"Tags\",\"equipment_card_number\":\"card\",\"equipment_memorandum_receipt\":\"Memo\",\"equipment_iar_number\":\"817238712\",\"equipment_asset_categories\":\"Assets\",\"equipment_account_code\":\"123123\",\"equipment_purchase_order\":\"123\"}}', '2020-06-07 23:49:08', '2020-06-07 23:49:08'),
(4, 'default', 'updated', 8, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_name\":\"UpdateName\",\"updated_at\":\"2020-06-08T09:33:29.000000Z\"},\"old\":{\"equipment_name\":\"qweqweqasd\",\"updated_at\":\"2020-06-07T13:53:57.000000Z\"}}', '2020-06-08 01:33:29', '2020-06-08 01:33:29'),
(5, 'default', 'updated', 8, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_name\":\"UpdateNames\",\"updated_at\":\"2020-06-08T14:52:47.000000Z\"},\"old\":{\"equipment_name\":\"UpdateName\",\"updated_at\":\"2020-06-08T09:33:29.000000Z\"}}', '2020-06-08 06:52:47', '2020-06-08 06:52:47'),
(6, 'default', 'deleted', 11, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":11,\"equipment_name\":\"eqName\",\"equipment_serial\":\"1263571623\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-07T13:36:27.000000Z\",\"updated_at\":\"2020-06-07T13:36:27.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":null,\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":null,\"equipment_supplier\":null,\"equipment_unit_price\":null,\"equipment_location\":null,\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":null,\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null}}', '2020-06-08 08:09:19', '2020-06-08 08:09:19'),
(7, 'default', 'deleted', 13, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":13,\"equipment_name\":\"acer Laptop\",\"equipment_serial\":\"612736\",\"equipment_model\":\"maosd\",\"equipment_quantity\":12738,\"created_at\":\"2020-06-08T05:29:22.000000Z\",\"updated_at\":\"2020-06-10T05:27:02.000000Z\",\"deleted_at\":\"2020-06-10T05:27:02.000000Z\",\"equipment_description\":\"aisdyasdjhajsbdja\",\"equipment_brand\":\"samsung\",\"equipment_country\":\"a\",\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":null,\"equipment_supplier\":null,\"equipment_unit_price\":null,\"equipment_location\":null,\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":null,\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null}}', '2020-06-09 21:27:02', '2020-06-09 21:27:02'),
(8, 'default', 'deleted', 14, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":14,\"equipment_name\":\"ace monitor\",\"equipment_serial\":\"1231263\",\"equipment_model\":\"ace 5asdasd\",\"equipment_quantity\":123,\"created_at\":\"2020-06-08T05:46:45.000000Z\",\"updated_at\":\"2020-06-10T05:27:02.000000Z\",\"deleted_at\":\"2020-06-10T05:27:02.000000Z\",\"equipment_description\":\"aisudkjaskjd\",\"equipment_brand\":\"acer\",\"equipment_country\":\"philippines\",\"equipment_date_delivered\":\"2020-06-01 13:46:00\",\"equipment_date_inspected\":null,\"equipment_date_acquired\":null,\"equipment_supplier\":null,\"equipment_unit_price\":null,\"equipment_location\":null,\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":null,\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null}}', '2020-06-09 21:27:02', '2020-06-09 21:27:02'),
(9, 'default', 'created', 21, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":21,\"equipment_name\":\"Desktop Monitor\",\"equipment_serial\":\"123451243\",\"equipment_model\":\"Deviant 24\",\"equipment_quantity\":2,\"created_at\":\"2020-06-10T14:20:53.000000Z\",\"updated_at\":\"2020-06-10T14:20:53.000000Z\",\"deleted_at\":null,\"equipment_description\":\"Monitor for Information Booth\",\"equipment_brand\":\"Devant\",\"equipment_country\":\"Philippines\",\"equipment_date_delivered\":\"2020-06-02 22:19:00\",\"equipment_date_inspected\":\"2020-06-03 22:19:00\",\"equipment_date_acquired\":\"2020-06-07 22:20:00\",\"equipment_supplier\":\"Thinking Tools\",\"equipment_unit_price\":5000,\"equipment_location\":\"Information Booth\",\"equipment_tag_number\":\"dev-24-1\",\"equipment_card_number\":\"12312391872938\",\"equipment_memorandum_receipt\":null,\"equipment_iar_number\":\"0091239123\",\"equipment_asset_categories\":\"Monitors\",\"equipment_account_code\":\"102983091823\",\"equipment_purchase_order\":\"123\"}}', '2020-06-10 06:20:53', '2020-06-10 06:20:53'),
(10, 'default', 'updated', 21, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-10T14:44:11.000000Z\",\"equipment_location\":\"Guard House\"},\"old\":{\"updated_at\":\"2020-06-10T14:20:53.000000Z\",\"equipment_location\":\"Information Booth\"}}', '2020-06-10 06:44:11', '2020-06-10 06:44:11'),
(11, 'default', 'updated', 21, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-10T16:16:54.000000Z\",\"equipment_memorandum_receipt\":\"[{\\\"download_link\\\":\\\"equipment\\\\\\\\June2020\\\\\\\\D8MHGWQjQ8ehItILNLS6.png\\\",\\\"original_name\\\":\\\"medicines.png\\\"}]\"},\"old\":{\"updated_at\":\"2020-06-10T14:44:11.000000Z\",\"equipment_memorandum_receipt\":null}}', '2020-06-10 08:16:54', '2020-06-10 08:16:54'),
(12, 'default', 'created', 22, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":22,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"19975-M16306020017\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-17T12:52:07.000000Z\",\"updated_at\":\"2020-06-17T12:52:07.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Edan\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2016-12-16 08:49:00\",\"equipment_supplier\":null,\"equipment_unit_price\":30000,\"equipment_location\":\"NICU\",\"equipment_tag_number\":\"24ECNIC\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 20:51:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosa Secretaria\"}}', '2020-06-17 04:52:07', '2020-06-17 04:52:07'),
(13, 'default', 'created', 23, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":23,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1702\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T12:56:04.000000Z\",\"updated_at\":\"2020-06-17T12:56:04.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-10 10:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"Pedia\",\"equipment_tag_number\":\"20ECPED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 20:55:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Jozaine Jamila\\/Barbarona\"}}', '2020-06-17 04:56:04', '2020-06-17 04:56:04'),
(14, 'default', 'created', 24, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":24,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1707\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T12:59:00.000000Z\",\"updated_at\":\"2020-06-17T12:59:00.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-17 20:57:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"13ECER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 20:58:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Sarah Jean Ecat\"}}', '2020-06-17 04:59:00', '2020-06-17 04:59:00'),
(15, 'default', 'created', 25, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":25,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1703\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:03:20.000000Z\",\"updated_at\":\"2020-06-17T13:03:20.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-10 09:01:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"OR\",\"equipment_tag_number\":\"15ECOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 21:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Abegail Baliling\"}}', '2020-06-17 05:03:20', '2020-06-17 05:03:20'),
(16, 'default', 'created', 26, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":26,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1711\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:16:14.000000Z\",\"updated_at\":\"2020-06-17T13:16:14.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-10 09:01:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"Med. 1\",\"equipment_tag_number\":\"22ECMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 21:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma Marcelis Andoy\"}}', '2020-06-17 05:16:14', '2020-06-17 05:16:14'),
(17, 'default', 'created', 27, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":27,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1701\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:17:38.000000Z\",\"updated_at\":\"2020-06-17T13:17:38.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-10 09:01:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"23ECMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 21:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Hector Rodriguez\"}}', '2020-06-17 05:17:38', '2020-06-17 05:17:38'),
(18, 'default', 'created', 28, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":28,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1710\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:20:04.000000Z\",\"updated_at\":\"2020-06-17T13:20:04.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-10 09:01:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"17ECMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 21:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Milagros Felias\"}}', '2020-06-17 05:20:04', '2020-06-17 05:20:04'),
(19, 'default', 'created', 29, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":29,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1704\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:23:48.000000Z\",\"updated_at\":\"2020-06-17T13:23:48.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-10 09:01:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"Surg.2\",\"equipment_tag_number\":\"16ECSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 21:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Dinah Chato\"}}', '2020-06-17 05:23:49', '2020-06-17 05:23:49'),
(20, 'default', 'created', 30, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":30,\"equipment_name\":\"02\\/14\\/2020 9:03 PM\",\"equipment_serial\":\"1606-1712\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:31:50.000000Z\",\"updated_at\":\"2020-06-17T13:31:50.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2017-05-10 09:01:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"Surg. 1\",\"equipment_tag_number\":\"11ECSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[{\\\"download_link\\\":\\\"equipment\\\\\\\\June2020\\\\\\\\Tf7K5mWLPlHNPcZFPGiu.jpg\\\",\\\"original_name\\\":\\\"download.jpg\\\"}]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 21:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joziane Jamila\"}}', '2020-06-17 05:31:50', '2020-06-17 05:31:50'),
(21, 'default', 'created', 31, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":31,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"0310-4533\",\"equipment_model\":\"108\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:45:26.000000Z\",\"updated_at\":\"2020-06-17T13:45:26.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 21:45:00\",\"equipment_date_inspected\":\"2020-06-17 21:45:00\",\"equipment_date_acquired\":\"2004-03-20 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":68000,\"equipment_location\":\"OPD\",\"equipment_tag_number\":\"06ECOPD\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 21:45:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"L. Gementiza\"}}', '2020-06-17 05:45:26', '2020-06-17 05:45:26'),
(22, 'default', 'created', 32, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":32,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1596\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T13:57:00.000000Z\",\"updated_at\":\"2020-06-17T13:57:00.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 21:48:00\",\"equipment_date_inspected\":\"2020-06-17 21:48:00\",\"equipment_date_acquired\":\"2020-06-17 21:47:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":60000,\"equipment_location\":\"OPD\",\"equipment_tag_number\":\"21ECOPD\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":null,\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Marcela Acera\"}}', '2020-06-17 05:57:00', '2020-06-17 05:57:00'),
(23, 'default', 'updated', 32, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T13:57:42.000000Z\",\"equipment_date_of_remarks\":\"2020-02-14 21:57:00\"},\"old\":{\"updated_at\":\"2020-06-17T13:57:00.000000Z\",\"equipment_date_of_remarks\":null}}', '2020-06-17 05:57:42', '2020-06-17 05:57:42'),
(24, 'default', 'created', 33, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":33,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"10140522004\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:03:36.000000Z\",\"updated_at\":\"2020-06-17T14:03:36.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Fazinni\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:01:00\",\"equipment_date_inspected\":\"2020-06-17 22:01:00\",\"equipment_date_acquired\":\"2020-06-17 22:00:00\",\"equipment_supplier\":\"Medicotek Inc\",\"equipment_unit_price\":83000,\"equipment_location\":\"Private\",\"equipment_tag_number\":\"09ECPVT\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 22:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Marites Plazos\"}}', '2020-06-17 06:03:36', '2020-06-17 06:03:36'),
(25, 'default', 'updated', 32, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T14:04:36.000000Z\",\"equipment_date_acquired\":\"2015-07-31 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T13:57:42.000000Z\",\"equipment_date_acquired\":\"2020-06-17 21:47:00\"}}', '2020-06-17 06:04:36', '2020-06-17 06:04:36'),
(26, 'default', 'updated', 32, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T14:05:26.000000Z\",\"equipment_date_acquired\":\"2017-02-09 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T14:04:36.000000Z\",\"equipment_date_acquired\":\"2015-07-31 00:00:00\"}}', '2020-06-17 06:05:26', '2020-06-17 06:05:26'),
(27, 'default', 'updated', 33, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T14:05:52.000000Z\",\"equipment_date_acquired\":\"2015-07-31 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T14:03:36.000000Z\",\"equipment_date_acquired\":\"2020-06-17 22:00:00\"}}', '2020-06-17 06:05:52', '2020-06-17 06:05:52'),
(28, 'default', 'updated', 33, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T14:09:08.000000Z\",\"equipment_date_acquired\":\"2017-05-10 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T14:05:52.000000Z\",\"equipment_date_acquired\":\"2015-07-31 00:00:00\"}}', '2020-06-17 06:09:08', '2020-06-17 06:09:08'),
(29, 'default', 'created', 34, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":34,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1309-4726\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:21:49.000000Z\",\"updated_at\":\"2020-06-17T14:21:49.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:21:00\",\"equipment_date_inspected\":\"2020-06-17 22:21:00\",\"equipment_date_acquired\":\"2014-03-27 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":80000,\"equipment_location\":\"HDU\",\"equipment_tag_number\":\"07ECHDU\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 22:21:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Darwin Quevedo\"}}', '2020-06-17 06:21:49', '2020-06-17 06:21:49'),
(30, 'default', 'created', 35, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":35,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"0511-7395\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:25:50.000000Z\",\"updated_at\":\"2020-06-17T14:25:50.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:25:00\",\"equipment_date_inspected\":\"2020-06-17 22:25:00\",\"equipment_date_acquired\":\"2006-03-08 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":70000,\"equipment_location\":\"PICU\",\"equipment_tag_number\":\"01ECPED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 22:25:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma Evelyn Barbarona\"}}', '2020-06-17 06:25:50', '2020-06-17 06:25:50'),
(31, 'default', 'created', 36, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":36,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1601-1265V4\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:32:00.000000Z\",\"updated_at\":\"2020-06-17T14:32:00.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:31:00\",\"equipment_date_inspected\":\"2020-06-17 22:31:00\",\"equipment_date_acquired\":\"2015-05-12 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":65000,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"10ECER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 22:31:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Sarah Jean Ecat\"}}', '2020-06-17 06:32:00', '2020-06-17 06:32:00'),
(32, 'default', 'created', 37, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":37,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1706\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:35:45.000000Z\",\"updated_at\":\"2020-06-17T14:35:45.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:34:00\",\"equipment_date_inspected\":\"2020-06-17 22:34:00\",\"equipment_date_acquired\":\"2017-05-10 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"25ECER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 22:35:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Sarah Jean Ecat\"}}', '2020-06-17 06:35:45', '2020-06-17 06:35:45'),
(33, 'default', 'created', 38, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":38,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1605-1593\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:39:30.000000Z\",\"updated_at\":\"2020-06-17T14:39:30.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:38:00\",\"equipment_date_inspected\":\"2020-06-17 22:38:00\",\"equipment_date_acquired\":\"2017-02-09 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":60000,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"19ECER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 22:39:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Emelly Grace Quipit\"}}', '2020-06-17 06:39:30', '2020-06-17 06:39:30'),
(34, 'default', 'created', 39, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":39,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1605-1595\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:43:14.000000Z\",\"updated_at\":\"2020-06-17T14:43:14.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:42:00\",\"equipment_date_inspected\":\"2020-06-17 22:42:00\",\"equipment_date_acquired\":\"2017-02-09 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":60000,\"equipment_location\":\"Med. 1\",\"equipment_tag_number\":\"12ECMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 22:42:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma Marcelis Andoy\"}}', '2020-06-17 06:43:14', '2020-06-17 06:43:14'),
(35, 'default', 'created', 40, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":40,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1605-1594\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:47:27.000000Z\",\"updated_at\":\"2020-06-17T14:47:27.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:46:00\",\"equipment_date_inspected\":\"2020-06-17 22:46:00\",\"equipment_date_acquired\":\"2017-02-09 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":60000,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"18ECMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 22:47:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Abegail Mercader\"}}', '2020-06-17 06:47:27', '2020-06-17 06:47:27'),
(36, 'default', 'created', 41, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":41,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"0511-7377\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:51:10.000000Z\",\"updated_at\":\"2020-06-17T14:51:10.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:50:00\",\"equipment_date_inspected\":\"2020-06-17 22:50:00\",\"equipment_date_acquired\":\"2006-03-08 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":70000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"02ECMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 22:50:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Loretta Brunidor\"}}', '2020-06-17 06:51:10', '2020-06-17 06:51:10'),
(37, 'default', 'created', 42, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":42,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1108-0794\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:54:54.000000Z\",\"updated_at\":\"2020-06-17T14:54:54.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:54:00\",\"equipment_date_inspected\":\"2020-06-17 22:54:00\",\"equipment_date_acquired\":\"2003-02-15 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":112650,\"equipment_location\":\"Surg. 1\",\"equipment_tag_number\":\"08ECSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 22:54:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joselita Buaquina\"}}', '2020-06-17 06:54:55', '2020-06-17 06:54:55'),
(38, 'default', 'created', 43, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":43,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1606-1709\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T14:58:39.000000Z\",\"updated_at\":\"2020-06-17T14:58:39.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 22:57:00\",\"equipment_date_inspected\":\"2020-06-17 22:57:00\",\"equipment_date_acquired\":\"2017-05-10 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":53750,\"equipment_location\":\"Private\",\"equipment_tag_number\":\"14ECPVT\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 22:58:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Helen Auxtero\"}}', '2020-06-17 06:58:39', '2020-06-17 06:58:39'),
(39, 'default', 'created', 44, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":44,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1804-3227\",\"equipment_model\":\"306\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:07:42.000000Z\",\"updated_at\":\"2020-06-17T15:07:42.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:06:00\",\"equipment_date_inspected\":\"2020-06-17 23:06:00\",\"equipment_date_acquired\":\"2019-04-08 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":165000,\"equipment_location\":\"SICU\",\"equipment_tag_number\":\"26ECSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 23:07:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Karen Cimafranca\"}}', '2020-06-17 07:07:42', '2020-06-17 07:07:42'),
(40, 'default', 'created', 45, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":45,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1902-2565\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:11:25.000000Z\",\"updated_at\":\"2020-06-17T15:11:25.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:10:00\",\"equipment_date_inspected\":\"2020-06-17 23:10:00\",\"equipment_date_acquired\":\"2019-09-27 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":70000,\"equipment_location\":\"OB\",\"equipment_tag_number\":\"27ECOB\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 23:11:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Nemiza Digal\"}}', '2020-06-17 07:11:25', '2020-06-17 07:11:25'),
(41, 'default', 'created', 46, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":46,\"equipment_name\":\"ECG Machine\",\"equipment_serial\":\"1902-2567\",\"equipment_model\":\"305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:18:53.000000Z\",\"updated_at\":\"2020-06-17T15:18:53.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Kenz\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:14:00\",\"equipment_date_inspected\":\"2020-06-17 23:14:00\",\"equipment_date_acquired\":\"2019-10-22 00:00:00\",\"equipment_supplier\":\"RG Meditron Inc\",\"equipment_unit_price\":78000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"29ECMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 23:18:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joel Bioco\"}}', '2020-06-17 07:18:53', '2020-06-17 07:18:53'),
(42, 'default', 'created', 47, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":47,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"11020045\",\"equipment_model\":\"--\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:41:45.000000Z\",\"updated_at\":\"2020-06-17T15:41:45.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Green  Trust\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:40:00\",\"equipment_date_inspected\":\"2020-06-17 23:40:00\",\"equipment_date_acquired\":\"2020-06-17 23:39:00\",\"equipment_supplier\":\"--\",\"equipment_unit_price\":0,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"30SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 23:41:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"--\"}}', '2020-06-17 07:41:45', '2020-06-17 07:41:45'),
(43, 'default', 'created', 48, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":48,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1047405\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:45:42.000000Z\",\"updated_at\":\"2020-06-17T15:45:42.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:44:00\",\"equipment_date_inspected\":\"2020-06-17 23:44:00\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":150000,\"equipment_location\":\"PICU\",\"equipment_tag_number\":\"04SUPED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 23:45:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma Evelyn Barbarona\"}}', '2020-06-17 07:45:42', '2020-06-17 07:45:42'),
(44, 'default', 'created', 49, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":49,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1047404\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:49:26.000000Z\",\"updated_at\":\"2020-06-17T15:49:26.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:48:00\",\"equipment_date_inspected\":\"2020-06-17 23:48:00\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":150000,\"equipment_location\":\"PICU\",\"equipment_tag_number\":\"05SUPED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 23:49:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma Evelyn Barbarona\"}}', '2020-06-17 07:49:26', '2020-06-17 07:49:26'),
(45, 'default', 'created', 50, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":50,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1047403\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:53:09.000000Z\",\"updated_at\":\"2020-06-17T15:53:09.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:52:00\",\"equipment_date_inspected\":\"2020-06-17 23:52:00\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":150000,\"equipment_location\":\"OR\",\"equipment_tag_number\":\"39SUOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 23:52:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Genis Marie Talatala\"}}', '2020-06-17 07:53:09', '2020-06-17 07:53:09'),
(46, 'default', 'created', 51, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":51,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1062915\",\"equipment_model\":\"Vario\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T15:56:53.000000Z\",\"updated_at\":\"2020-06-17T15:56:53.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-17 23:56:00\",\"equipment_date_inspected\":\"2020-06-17 23:56:00\",\"equipment_date_acquired\":\"2020-06-17 23:54:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":50000,\"equipment_location\":\"Private\",\"equipment_tag_number\":\"42SUPVT\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 23:56:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Demetria Alago\"}}', '2020-06-17 07:56:53', '2020-06-17 07:56:53'),
(47, 'default', 'created', 52, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":52,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1060657\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:04:18.000000Z\",\"updated_at\":\"2020-06-17T16:04:18.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:03:00\",\"equipment_date_inspected\":\"2020-06-18 00:03:00\",\"equipment_date_acquired\":\"2003-08-20 00:00:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":150000,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"35SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:04:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ivy Fullido\"}}', '2020-06-17 08:04:18', '2020-06-17 08:04:18'),
(48, 'default', 'created', 53, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":53,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1076262\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:06:42.000000Z\",\"updated_at\":\"2020-06-17T16:06:42.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:06:00\",\"equipment_date_inspected\":\"2020-06-18 00:06:00\",\"equipment_date_acquired\":\"2003-08-20 00:00:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":150000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"14SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:06:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Loreta Brunidor\"}}', '2020-06-17 08:06:42', '2020-06-17 08:06:42'),
(49, 'default', 'created', 54, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":54,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1059926\",\"equipment_model\":\"Vario\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:10:51.000000Z\",\"updated_at\":\"2020-06-17T16:10:51.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:10:00\",\"equipment_date_inspected\":\"2020-06-18 00:10:00\",\"equipment_date_acquired\":\"2003-11-20 00:00:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":50000,\"equipment_location\":\"HDU\",\"equipment_tag_number\":\"29SUHDU\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:10:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Anabelle Ceasar\"}}', '2020-06-17 08:10:51', '2020-06-17 08:10:51'),
(50, 'default', 'updated', 47, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:19:21.000000Z\",\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":null},\"old\":{\"updated_at\":\"2020-06-17T15:41:45.000000Z\",\"equipment_date_delivered\":\"2020-06-17 23:40:00\",\"equipment_date_inspected\":\"2020-06-17 23:40:00\",\"equipment_date_acquired\":\"2020-06-17 23:39:00\"}}', '2020-06-17 08:19:22', '2020-06-17 08:19:22'),
(51, 'default', 'updated', 48, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:20:06.000000Z\",\"equipment_date_acquired\":\"2003-06-02 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T15:45:42.000000Z\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\"}}', '2020-06-17 08:20:07', '2020-06-17 08:20:07'),
(52, 'default', 'updated', 49, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:20:45.000000Z\",\"equipment_date_acquired\":\"2003-06-02 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T15:49:26.000000Z\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\"}}', '2020-06-17 08:20:45', '2020-06-17 08:20:45'),
(53, 'default', 'updated', 50, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:22:04.000000Z\",\"equipment_date_acquired\":\"2003-06-02 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T15:53:09.000000Z\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\"}}', '2020-06-17 08:22:04', '2020-06-17 08:22:04'),
(54, 'default', 'updated', 51, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:23:00.000000Z\",\"equipment_date_acquired\":\"2003-12-11 23:54:00\"},\"old\":{\"updated_at\":\"2020-06-17T15:56:53.000000Z\",\"equipment_date_acquired\":\"2020-06-17 23:54:00\"}}', '2020-06-17 08:23:00', '2020-06-17 08:23:00');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(55, 'default', 'updated', 51, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:23:39.000000Z\",\"equipment_date_acquired\":\"2003-07-29 23:54:00\"},\"old\":{\"updated_at\":\"2020-06-17T16:23:00.000000Z\",\"equipment_date_acquired\":\"2003-12-11 23:54:00\"}}', '2020-06-17 08:23:39', '2020-06-17 08:23:39'),
(56, 'default', 'updated', 52, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:24:26.000000Z\",\"equipment_date_acquired\":\"2003-12-08 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T16:04:18.000000Z\",\"equipment_date_acquired\":\"2003-08-20 00:00:00\"}}', '2020-06-17 08:24:26', '2020-06-17 08:24:26'),
(57, 'default', 'updated', 53, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:25:08.000000Z\",\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2003-12-08 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T16:06:42.000000Z\",\"equipment_date_delivered\":\"2020-06-18 00:06:00\",\"equipment_date_inspected\":\"2020-06-18 00:06:00\",\"equipment_date_acquired\":\"2003-08-20 00:00:00\"}}', '2020-06-17 08:25:08', '2020-06-17 08:25:08'),
(58, 'default', 'updated', 54, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"updated_at\":\"2020-06-17T16:26:10.000000Z\",\"equipment_date_delivered\":null,\"equipment_date_inspected\":null,\"equipment_date_acquired\":\"2003-12-11 00:00:00\"},\"old\":{\"updated_at\":\"2020-06-17T16:10:51.000000Z\",\"equipment_date_delivered\":\"2020-06-18 00:10:00\",\"equipment_date_inspected\":\"2020-06-18 00:10:00\",\"equipment_date_acquired\":\"2003-11-20 00:00:00\"}}', '2020-06-17 08:26:10', '2020-06-17 08:26:10'),
(59, 'default', 'created', 55, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":55,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1058705\",\"equipment_model\":\"Vario\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:28:36.000000Z\",\"updated_at\":\"2020-06-17T16:28:36.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:28:00\",\"equipment_date_inspected\":\"2020-06-18 00:28:00\",\"equipment_date_acquired\":\"2003-12-30 00:00:00\",\"equipment_supplier\":\"Dispo Philippines Inc.\",\"equipment_unit_price\":50000,\"equipment_location\":\"HDU\",\"equipment_tag_number\":\"36SUHDU\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:28:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Anabelle Ceasar\"}}', '2020-06-17 08:28:36', '2020-06-17 08:28:36'),
(60, 'default', 'created', 56, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":56,\"equipment_name\":\"\\\"Suction Machine \\\"\",\"equipment_serial\":\"9021722\",\"equipment_model\":null,\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:30:11.000000Z\",\"updated_at\":\"2020-06-17T16:30:11.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Doctors  F.\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:29:00\",\"equipment_date_inspected\":\"2020-06-18 00:29:00\",\"equipment_date_acquired\":\"2006-01-26 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":113000,\"equipment_location\":\"\\\"Surg. 1 \\\"\",\"equipment_tag_number\":\"33SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:30:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosalina Salisid\"}}', '2020-06-17 08:30:11', '2020-06-17 08:30:11'),
(61, 'default', 'created', 57, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":57,\"equipment_name\":\"Thoracic Pump\",\"equipment_serial\":\"9021724\",\"equipment_model\":\"DF500\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:31:59.000000Z\",\"updated_at\":\"2020-06-17T16:31:59.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Doctors F.\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:31:00\",\"equipment_date_inspected\":\"2020-06-18 00:31:00\",\"equipment_date_acquired\":\"2008-08-29 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":113000,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"02THMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:31:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ivy Fullido\"}}', '2020-06-17 08:31:59', '2020-06-17 08:31:59'),
(62, 'default', 'created', 58, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":58,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"19552-101-57\",\"equipment_model\":\"V7Plus AC\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:34:07.000000Z\",\"updated_at\":\"2020-06-17T16:34:07.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Hersill\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:33:00\",\"equipment_date_inspected\":\"2020-06-18 00:33:00\",\"equipment_date_acquired\":\"2009-04-24 00:00:00\",\"equipment_supplier\":\"Fernando Medical Interprises\",\"equipment_unit_price\":75000,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"12SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:34:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ivy Fullido\"}}', '2020-06-17 08:34:07', '2020-06-17 08:34:07'),
(63, 'default', 'created', 59, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":59,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"8600004\",\"equipment_model\":\"D-3000\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:36:00.000000Z\",\"updated_at\":\"2020-06-17T16:36:00.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Blue  Cross\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:35:00\",\"equipment_date_inspected\":\"2020-06-18 00:35:00\",\"equipment_date_acquired\":\"2009-05-06 00:00:00\",\"equipment_supplier\":\"Berovan Marketing\",\"equipment_unit_price\":82000,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"20SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:35:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joseph Redulla\"}}', '2020-06-17 08:36:00', '2020-06-17 08:36:00'),
(64, 'default', 'created', 60, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":60,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"953540001\",\"equipment_model\":\"D-3000\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:42:10.000000Z\",\"updated_at\":\"2020-06-17T16:42:10.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Blue  Cross\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:41:00\",\"equipment_date_inspected\":\"2020-06-18 00:41:00\",\"equipment_date_acquired\":\"2009-12-29 00:00:00\",\"equipment_supplier\":\"Berovan Marketing\",\"equipment_unit_price\":75000,\"equipment_location\":\"Med. 1\",\"equipment_tag_number\":\"10SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:42:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ivy Fullido\"}}', '2020-06-17 08:42:10', '2020-06-17 08:42:10'),
(65, 'default', 'created', 61, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":61,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"953540008\",\"equipment_model\":\"D-3000\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:43:46.000000Z\",\"updated_at\":\"2020-06-17T16:43:46.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Blue  Cross\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:43:00\",\"equipment_date_inspected\":\"2020-06-18 00:43:00\",\"equipment_date_acquired\":\"2010-04-07 00:00:00\",\"equipment_supplier\":\"Berovan Marketing\",\"equipment_unit_price\":78570,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"13SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:43:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ivy Fullido\"}}', '2020-06-17 08:43:46', '2020-06-17 08:43:46'),
(66, 'default', 'created', 62, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":62,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9028798\",\"equipment_model\":\"DF-600\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:53:31.000000Z\",\"updated_at\":\"2020-06-17T16:53:31.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Doctors F.\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:53:00\",\"equipment_date_inspected\":\"2020-06-18 00:53:00\",\"equipment_date_acquired\":\"2011-05-06 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76800,\"equipment_location\":\"DR\",\"equipment_tag_number\":\"07SUDR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"DR\",\"equipment_date_of_remarks\":\"2020-02-14 00:53:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosa Secretaria\"}}', '2020-06-17 08:53:31', '2020-06-17 08:53:31'),
(67, 'default', 'created', 63, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":63,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"100004\",\"equipment_model\":\"2110\\/120\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:55:36.000000Z\",\"updated_at\":\"2020-06-17T16:55:36.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Sorensen\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:55:00\",\"equipment_date_inspected\":\"2020-06-18 00:55:00\",\"equipment_date_acquired\":\"2011-05-13 00:00:00\",\"equipment_supplier\":\"Technomed International Inc\",\"equipment_unit_price\":83900,\"equipment_location\":\"Pvt. 1 & 2\",\"equipment_tag_number\":\"03SUPVT\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:55:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Milagros Felias\"}}', '2020-06-17 08:55:36', '2020-06-17 08:55:36'),
(68, 'default', 'created', 64, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":64,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9035018\",\"equipment_model\":\"DF-650A\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T16:57:29.000000Z\",\"updated_at\":\"2020-06-17T16:57:29.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Doctors  F.\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 00:57:00\",\"equipment_date_inspected\":\"2020-06-18 00:57:00\",\"equipment_date_acquired\":\"2013-07-18 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76000,\"equipment_location\":\"Pvt. 1 & 2\",\"equipment_tag_number\":\"01SUPVT\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 00:57:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Demetria Alago\"}}', '2020-06-17 08:57:29', '2020-06-17 08:57:29'),
(69, 'default', 'created', 65, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":65,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9035020\",\"equipment_model\":\"DF-650A\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:06:25.000000Z\",\"updated_at\":\"2020-06-17T17:06:25.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Doctors  F.\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:06:00\",\"equipment_date_inspected\":\"2020-06-18 01:06:00\",\"equipment_date_acquired\":\"2013-07-18 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76000,\"equipment_location\":\"Pvt. 1 & 2\",\"equipment_tag_number\":\"02SUPVT\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:06:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Demetria Alago\"}}', '2020-06-17 09:06:25', '2020-06-17 09:06:25'),
(70, 'default', 'created', 66, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":66,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9035023\",\"equipment_model\":\"DF-650A\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:08:00.000000Z\",\"updated_at\":\"2020-06-17T17:08:00.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Doctors  F.\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:07:00\",\"equipment_date_inspected\":\"2020-06-18 01:07:00\",\"equipment_date_acquired\":\"2013-09-27 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76000,\"equipment_location\":\"NICU\",\"equipment_tag_number\":\"06SUNIC\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:07:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosa Secretaria\"}}', '2020-06-17 09:08:00', '2020-06-17 09:08:00'),
(71, 'default', 'created', 67, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":67,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9033245\",\"equipment_model\":\"DF-760\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:09:36.000000Z\",\"updated_at\":\"2020-06-17T17:09:36.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Doctors F.\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:09:00\",\"equipment_date_inspected\":\"2020-06-18 01:09:00\",\"equipment_date_acquired\":\"2014-04-21 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76000,\"equipment_location\":\"Surg. 2\",\"equipment_tag_number\":\"25SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:09:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosalina Salisid\"}}', '2020-06-17 09:09:36', '2020-06-17 09:09:36'),
(72, 'default', 'created', 68, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":68,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9033244\",\"equipment_model\":\"DF-760\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:11:26.000000Z\",\"updated_at\":\"2020-06-17T17:11:26.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Doctors  F.\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:11:00\",\"equipment_date_inspected\":\"2020-06-18 01:11:00\",\"equipment_date_acquired\":\"2014-04-21 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76000,\"equipment_location\":\"SICU\",\"equipment_tag_number\":\"26SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:11:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosalina Salisid\"}}', '2020-06-17 09:11:26', '2020-06-17 09:11:26'),
(73, 'default', 'created', 69, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":69,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9033246\",\"equipment_model\":\"DF-760\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:15:09.000000Z\",\"updated_at\":\"2020-06-17T17:15:09.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Doctors  F.\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:14:00\",\"equipment_date_inspected\":\"2020-06-18 01:14:00\",\"equipment_date_acquired\":\"2014-04-21 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76000,\"equipment_location\":\"Surg. 2\",\"equipment_tag_number\":\"27SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:15:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosalina Salisid\"}}', '2020-06-17 09:15:09', '2020-06-17 09:15:09'),
(74, 'default', 'created', 70, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":70,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9033280\",\"equipment_model\":\"DF-760\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:24:23.000000Z\",\"updated_at\":\"2020-06-17T17:24:23.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"\\\"Doctors  F.\\\"\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:24:00\",\"equipment_date_inspected\":\"2020-06-18 01:24:00\",\"equipment_date_acquired\":\"2014-04-21 00:00:00\",\"equipment_supplier\":\"Medical Center Trading Corp\",\"equipment_unit_price\":76000,\"equipment_location\":\"Surg. 2\",\"equipment_tag_number\":\"28SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:24:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosalina Salisid\"}}', '2020-06-17 09:24:23', '2020-06-17 09:24:23'),
(75, 'default', 'created', 71, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":71,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"14003\",\"equipment_model\":\"RSU 346\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:38:36.000000Z\",\"updated_at\":\"2020-06-17T17:38:36.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Rexmed\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:38:00\",\"equipment_date_inspected\":\"2020-06-18 01:38:00\",\"equipment_date_acquired\":\"2014-06-16 00:00:00\",\"equipment_supplier\":\"Blue Sky Trading Corp.\",\"equipment_unit_price\":75000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"23SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:38:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Loreta Brunidor\"}}', '2020-06-17 09:38:36', '2020-06-17 09:38:36'),
(76, 'default', 'created', 72, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":72,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"14004\",\"equipment_model\":\"RSU 346\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:40:13.000000Z\",\"updated_at\":\"2020-06-17T17:40:13.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Rexmed\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:39:00\",\"equipment_date_inspected\":\"2020-06-18 01:39:00\",\"equipment_date_acquired\":\"2014-06-16 00:00:00\",\"equipment_supplier\":\"Blue Sky Trading Corp.\",\"equipment_unit_price\":75000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"24SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:40:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Loreta Brunidor\"}}', '2020-06-17 09:40:13', '2020-06-17 09:40:13'),
(77, 'default', 'created', 73, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":73,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"14005\",\"equipment_model\":\"RSU-346\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:41:51.000000Z\",\"updated_at\":\"2020-06-17T17:41:51.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Rexmed\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:41:00\",\"equipment_date_inspected\":\"2020-06-18 01:41:00\",\"equipment_date_acquired\":\"2014-06-16 00:00:00\",\"equipment_supplier\":\"Blue Sky Trading Corp.\",\"equipment_unit_price\":75000,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"31SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:41:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joseph Wood Fullido\"}}', '2020-06-17 09:41:52', '2020-06-17 09:41:52'),
(78, 'default', 'created', 74, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":74,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"14007\",\"equipment_model\":\"RSU-346\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:43:49.000000Z\",\"updated_at\":\"2020-06-17T17:43:49.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Rexmed\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:43:00\",\"equipment_date_inspected\":\"2020-06-18 01:43:00\",\"equipment_date_acquired\":\"2014-06-16 00:00:00\",\"equipment_supplier\":\"Blue Sky Trading Corp.\",\"equipment_unit_price\":75000,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"32SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Non-Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:43:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joseph Wood Fullido\"}}', '2020-06-17 09:43:49', '2020-06-17 09:43:49'),
(79, 'default', 'created', 75, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":75,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"S-7161\",\"equipment_model\":\"--\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T17:58:00.000000Z\",\"updated_at\":\"2020-06-17T17:58:00.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Gomco\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:57:00\",\"equipment_date_inspected\":\"2020-06-18 01:57:00\",\"equipment_date_acquired\":\"2005-07-08 00:00:00\",\"equipment_supplier\":\"--\",\"equipment_unit_price\":119000,\"equipment_location\":\"OR\",\"equipment_tag_number\":\"22SUOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"11\\/20\\/2019-not functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:57:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Tita Roman\"}}', '2020-06-17 09:58:01', '2020-06-17 09:58:01'),
(80, 'default', 'created', 76, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":76,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9045454\",\"equipment_model\":\"DF-650A\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:00:04.000000Z\",\"updated_at\":\"2020-06-17T18:00:04.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Doctors F.\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 01:59:00\",\"equipment_date_inspected\":\"2020-06-18 01:59:00\",\"equipment_date_acquired\":\"2016-02-05 00:00:00\",\"equipment_supplier\":\"Endure Medical Inc.\",\"equipment_unit_price\":76984,\"equipment_location\":\"OR\",\"equipment_tag_number\":\"44SUOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 01:59:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Araceli Mercado\"}}', '2020-06-17 10:00:04', '2020-06-17 10:00:04'),
(81, 'default', 'created', 77, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":77,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"9045455\",\"equipment_model\":\"DF-650A\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:02:51.000000Z\",\"updated_at\":\"2020-06-17T18:02:51.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Doctors F.\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 02:02:00\",\"equipment_date_inspected\":\"2020-06-18 02:02:00\",\"equipment_date_acquired\":\"2016-05-05 00:00:00\",\"equipment_supplier\":\"Endure Medical Inc.\",\"equipment_unit_price\":76984,\"equipment_location\":\"PICU\",\"equipment_tag_number\":\"40SUPED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 02:02:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Alona Arao-Arao\"}}', '2020-06-17 10:02:51', '2020-06-17 10:02:51'),
(82, 'default', 'created', 78, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":78,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"2016062000409\",\"equipment_model\":\"--\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:04:46.000000Z\",\"updated_at\":\"2020-06-17T18:04:46.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Indoplas\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 02:04:00\",\"equipment_date_inspected\":\"2020-06-18 02:04:00\",\"equipment_date_acquired\":\"2016-09-20 00:00:00\",\"equipment_supplier\":\"Donated: East meets W\",\"equipment_unit_price\":16000,\"equipment_location\":\"DR\",\"equipment_tag_number\":\"46SUDR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 02:04:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Janet Marie Araneta\"}}', '2020-06-17 10:04:47', '2020-06-17 10:04:47'),
(83, 'default', 'created', 79, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":79,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"201606202041020\",\"equipment_model\":\"--\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:23:48.000000Z\",\"updated_at\":\"2020-06-17T18:23:48.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Indoplas\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 02:23:00\",\"equipment_date_inspected\":\"2020-06-18 02:23:00\",\"equipment_date_acquired\":\"2016-09-20 00:00:00\",\"equipment_supplier\":\"Donated: East meets W\",\"equipment_unit_price\":16000,\"equipment_location\":\"OB-OR\",\"equipment_tag_number\":\"78SUOBOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 02:23:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Janet Marie Araneta\"}}', '2020-06-17 10:23:48', '2020-06-17 10:23:48'),
(84, 'default', 'created', 80, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":80,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1502515\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:27:30.000000Z\",\"updated_at\":\"2020-06-17T18:27:30.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 02:27:00\",\"equipment_date_inspected\":\"2020-06-18 02:27:00\",\"equipment_date_acquired\":\"2017-04-11 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Main OR\",\"equipment_tag_number\":\"47SUOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 02:27:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Merlyn Pierras\"}}', '2020-06-17 10:27:30', '2020-06-17 10:27:30'),
(85, 'default', 'created', 81, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":81,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1501588\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:31:32.000000Z\",\"updated_at\":\"2020-06-17T18:31:32.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 02:29:00\",\"equipment_date_inspected\":\"2020-06-18 02:29:00\",\"equipment_date_acquired\":\"2017-04-11 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Private\",\"equipment_tag_number\":\"70SUPVT\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 02:31:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma. Theresa Genalde\"}}', '2020-06-17 10:31:32', '2020-06-17 10:31:32'),
(86, 'default', 'created', 82, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":82,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1502512\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:33:16.000000Z\",\"updated_at\":\"2020-06-17T18:33:16.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 02:32:00\",\"equipment_date_inspected\":\"2020-06-18 02:32:00\",\"equipment_date_acquired\":\"2017-04-11 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"\\\"Surg. 1 \\\"\",\"equipment_tag_number\":\"49SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 02:33:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Gem Branaldez\"}}', '2020-06-17 10:33:16', '2020-06-17 10:33:16'),
(87, 'default', 'created', 83, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":83,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1211080\",\"equipment_model\":\"7E-A\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T18:44:41.000000Z\",\"updated_at\":\"2020-06-17T18:44:41.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Union\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 02:44:00\",\"equipment_date_inspected\":\"2020-06-18 02:44:00\",\"equipment_date_acquired\":\"2017-05-20 00:00:00\",\"equipment_supplier\":\"Donated: Bong Castro\",\"equipment_unit_price\":35000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"47SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 02:44:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Emmanuel Chris Arcay\"}}', '2020-06-17 10:44:41', '2020-06-17 10:44:41'),
(88, 'default', 'created', 84, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":84,\"equipment_name\":\"Portable Suction Machine\",\"equipment_serial\":\"DB1105\",\"equipment_model\":\"Dynamic II\",\"equipment_quantity\":null,\"created_at\":\"2020-06-17T19:06:00.000000Z\",\"updated_at\":\"2020-06-17T19:06:00.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Cheiron\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 03:05:00\",\"equipment_date_inspected\":\"2020-06-18 03:05:00\",\"equipment_date_acquired\":\"2017-06-19 00:00:00\",\"equipment_supplier\":\"Saviour MeDevices\",\"equipment_unit_price\":75800,\"equipment_location\":\"Foton\",\"equipment_tag_number\":\"71SUAMB\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 03:05:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"John Melchor Namoc\"}}', '2020-06-17 11:06:00', '2020-06-17 11:06:00'),
(89, 'default', 'created', 85, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":85,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1661926\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:14:39.000000Z\",\"updated_at\":\"2020-06-18T05:14:39.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:14:00\",\"equipment_date_inspected\":\"2020-06-18 13:14:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"HDU\",\"equipment_tag_number\":\"56SUHDU\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 13:14:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Cerlita Balo\"}}', '2020-06-17 21:14:40', '2020-06-17 21:14:40'),
(90, 'default', 'created', 86, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":86,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1661924\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:19:18.000000Z\",\"updated_at\":\"2020-06-18T05:19:18.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:19:00\",\"equipment_date_inspected\":\"2020-06-18 13:19:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"HDU\",\"equipment_tag_number\":\"55SUHDU\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 13:19:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Cerlita Balo\"}}', '2020-06-17 21:19:18', '2020-06-17 21:19:18'),
(91, 'default', 'created', 87, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":87,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1663818\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:26:18.000000Z\",\"updated_at\":\"2020-06-18T05:26:18.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:26:00\",\"equipment_date_inspected\":\"2020-06-18 13:26:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Pedia\",\"equipment_tag_number\":\"60SUPED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 13:26:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Alona Arao-Arao\"}}', '2020-06-17 21:26:18', '2020-06-17 21:26:18'),
(92, 'default', 'created', 88, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":88,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1662391\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:29:40.000000Z\",\"updated_at\":\"2020-06-18T05:29:40.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:29:00\",\"equipment_date_inspected\":\"2020-06-18 13:29:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"53SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 13:29:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Maria Liza Balonda\"}}', '2020-06-17 21:29:40', '2020-06-17 21:29:40'),
(93, 'default', 'created', 89, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":89,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1661923\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:32:09.000000Z\",\"updated_at\":\"2020-06-18T05:32:09.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:31:00\",\"equipment_date_inspected\":\"2020-06-18 13:31:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"52SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 13:32:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Maria Liza Balonda\"}}', '2020-06-17 21:32:09', '2020-06-17 21:32:09'),
(94, 'default', 'created', 90, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":90,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1663814\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:34:37.000000Z\",\"updated_at\":\"2020-06-18T05:34:37.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:34:00\",\"equipment_date_inspected\":\"2020-06-18 13:34:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"OB\",\"equipment_tag_number\":\"54SUOB\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 13:34:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Gloria Ancog\"}}', '2020-06-17 21:34:37', '2020-06-17 21:34:37'),
(95, 'default', 'created', 91, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":91,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1661922\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:42:37.000000Z\",\"updated_at\":\"2020-06-18T05:42:37.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:42:00\",\"equipment_date_inspected\":\"2020-06-18 13:42:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Med. 1\",\"equipment_tag_number\":\"46SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 13:42:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Maria Marcelis Andoy\"}}', '2020-06-17 21:42:37', '2020-06-17 21:42:37'),
(96, 'default', 'created', 92, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":92,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1662390\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:46:19.000000Z\",\"updated_at\":\"2020-06-18T05:46:19.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:46:00\",\"equipment_date_inspected\":\"2020-06-18 13:46:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Med. 1\",\"equipment_tag_number\":\"65SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 13:46:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Maria Marcelis Andoy\"}}', '2020-06-17 21:46:19', '2020-06-17 21:46:19'),
(97, 'default', 'created', 93, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":93,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1661919\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:49:12.000000Z\",\"updated_at\":\"2020-06-18T05:49:12.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:48:00\",\"equipment_date_inspected\":\"2020-06-18 13:48:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"63SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 13:49:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Hector Rodriguez\"}}', '2020-06-17 21:49:12', '2020-06-17 21:49:12'),
(98, 'default', 'created', 94, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":94,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1661925\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:52:05.000000Z\",\"updated_at\":\"2020-06-18T05:52:05.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:51:00\",\"equipment_date_inspected\":\"2020-06-18 13:51:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"62SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 13:51:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Hector Rodriguez\"}}', '2020-06-17 21:52:05', '2020-06-17 21:52:05');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_id`, `subject_type`, `causer_id`, `causer_type`, `properties`, `created_at`, `updated_at`) VALUES
(99, 'default', 'created', 95, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":95,\"equipment_name\":null,\"equipment_serial\":\"1662387\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:54:44.000000Z\",\"updated_at\":\"2020-06-18T05:54:44.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:54:00\",\"equipment_date_inspected\":\"2020-06-18 13:54:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"\\\"Surg. 1 \\\"\",\"equipment_tag_number\":\"57SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 13:54:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joziane Jamila\"}}', '2020-06-17 21:54:44', '2020-06-17 21:54:44'),
(100, 'default', 'created', 96, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":96,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1661927\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T05:57:37.000000Z\",\"updated_at\":\"2020-06-18T05:57:37.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 13:57:00\",\"equipment_date_inspected\":\"2020-06-18 13:57:00\",\"equipment_date_acquired\":\"2017-07-07 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"\\\"Surg. 1 \\\"\",\"equipment_tag_number\":\"58SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"1\\/31\\/2020-not functional-defective plate holder\",\"equipment_date_of_remarks\":\"2020-02-14 13:57:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joziane Jamila\"}}', '2020-06-17 21:57:37', '2020-06-17 21:57:37'),
(101, 'default', 'created', 97, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":97,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1663815\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:00:29.000000Z\",\"updated_at\":\"2020-06-18T06:00:29.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:00:00\",\"equipment_date_inspected\":\"2020-06-18 14:00:00\",\"equipment_date_acquired\":\"2017-07-28 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Med. 1\",\"equipment_tag_number\":\"74SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 14:00:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Maria Marcelis Andoy\"}}', '2020-06-17 22:00:29', '2020-06-17 22:00:29'),
(102, 'default', 'created', 98, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":98,\"equipment_name\":null,\"equipment_serial\":\"1663817\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:03:09.000000Z\",\"updated_at\":\"2020-06-18T06:03:09.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:02:00\",\"equipment_date_inspected\":\"2020-06-18 14:02:00\",\"equipment_date_acquired\":\"2017-07-28 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Med. 2\",\"equipment_tag_number\":\"66SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:03:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Hector Rodriguez\"}}', '2020-06-17 22:03:09', '2020-06-17 22:03:09'),
(103, 'default', 'created', 99, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":99,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1663821\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:06:02.000000Z\",\"updated_at\":\"2020-06-18T06:06:02.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:05:00\",\"equipment_date_inspected\":\"2020-06-18 14:05:00\",\"equipment_date_acquired\":\"2017-07-28 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"68SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:05:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Milagros Felias\"}}', '2020-06-17 22:06:02', '2020-06-17 22:06:02'),
(104, 'default', 'created', 100, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":100,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1663816\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:11:34.000000Z\",\"updated_at\":\"2020-06-18T06:11:34.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:11:00\",\"equipment_date_inspected\":\"2020-06-18 14:11:00\",\"equipment_date_acquired\":\"2017-07-28 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"MICU\",\"equipment_tag_number\":\"76SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:11:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Milagros Felias\"}}', '2020-06-17 22:11:34', '2020-06-17 22:11:34'),
(105, 'default', 'created', 101, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":101,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1663813\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:14:28.000000Z\",\"updated_at\":\"2020-06-18T06:14:28.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:14:00\",\"equipment_date_inspected\":\"2020-06-18 14:14:00\",\"equipment_date_acquired\":\"2017-07-28 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Surg. 2\",\"equipment_tag_number\":\"83SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:14:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Virginia Tan\"}}', '2020-06-17 22:14:28', '2020-06-17 22:14:28'),
(106, 'default', 'created', 102, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":102,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1663820\",\"equipment_model\":\"Basic 30\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:17:20.000000Z\",\"updated_at\":\"2020-06-18T06:17:20.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:17:00\",\"equipment_date_inspected\":\"2020-06-18 14:17:00\",\"equipment_date_acquired\":\"2017-07-28 00:00:00\",\"equipment_supplier\":\"HealthSolution Inc.\",\"equipment_unit_price\":195000,\"equipment_location\":\"Surg. 2\",\"equipment_tag_number\":\"84SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:17:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Virginia Tan\"}}', '2020-06-17 22:17:20', '2020-06-17 22:17:20'),
(107, 'default', 'created', 103, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":103,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"DB1100\",\"equipment_model\":\"Dynamic II\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:22:03.000000Z\",\"updated_at\":\"2020-06-18T06:22:03.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Cheiron\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:21:00\",\"equipment_date_inspected\":\"2020-06-18 14:21:00\",\"equipment_date_acquired\":\"2017-08-11 00:00:00\",\"equipment_supplier\":\"Saviour MeDevices\",\"equipment_unit_price\":75800,\"equipment_location\":\"NICU\",\"equipment_tag_number\":\"77SUNIC\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 14:21:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Rosa Secretaria\"}}', '2020-06-17 22:22:03', '2020-06-17 22:22:03'),
(108, 'default', 'created', 104, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":104,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"DB1106\",\"equipment_model\":\"Dynamic II\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:24:56.000000Z\",\"updated_at\":\"2020-06-18T06:24:56.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Cheiron\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:24:00\",\"equipment_date_inspected\":\"2020-06-18 14:24:00\",\"equipment_date_acquired\":\"2017-08-11 00:00:00\",\"equipment_supplier\":\"Saviour MeDevices\",\"equipment_unit_price\":75800,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"79SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:24:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Sarah Jane Ecat\"}}', '2020-06-17 22:24:56', '2020-06-17 22:24:56'),
(109, 'default', 'created', 105, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":105,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"DB1107\",\"equipment_model\":\"Dynamic II\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:27:47.000000Z\",\"updated_at\":\"2020-06-18T06:27:47.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Cheiron\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:27:00\",\"equipment_date_inspected\":\"2020-06-18 14:27:00\",\"equipment_date_acquired\":\"2017-08-11 00:00:00\",\"equipment_supplier\":\"Saviour MeDevices\",\"equipment_unit_price\":75800,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"80SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:27:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Sarah Jane Ecat\"}}', '2020-06-17 22:27:47', '2020-06-17 22:27:47'),
(110, 'default', 'created', 106, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":106,\"equipment_name\":null,\"equipment_serial\":\"DB1108\",\"equipment_model\":\"Dynamic II\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:30:28.000000Z\",\"updated_at\":\"2020-06-18T06:30:28.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Cheiron\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:30:00\",\"equipment_date_inspected\":\"2020-06-18 14:30:00\",\"equipment_date_acquired\":\"2017-08-11 00:00:00\",\"equipment_supplier\":\"Saviour MeDevices\",\"equipment_unit_price\":75800,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"81SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:30:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Sarah Jane Ecat\"}}', '2020-06-17 22:30:28', '2020-06-17 22:30:28'),
(111, 'default', 'created', 107, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":107,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"DB1109\",\"equipment_model\":\"Dynamic II\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:33:22.000000Z\",\"updated_at\":\"2020-06-18T06:33:22.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Cheiron\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:33:00\",\"equipment_date_inspected\":\"2020-06-18 14:33:00\",\"equipment_date_acquired\":\"2017-08-11 00:00:00\",\"equipment_supplier\":\"Saviour MeDevices\",\"equipment_unit_price\":75800,\"equipment_location\":\"ER\",\"equipment_tag_number\":\"82SUER\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"New Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:33:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Sarah Jane Ecat\"}}', '2020-06-17 22:33:22', '2020-06-17 22:33:22'),
(112, 'default', 'created', 108, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":108,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"DB1103\",\"equipment_model\":\"Dynamic II\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:36:14.000000Z\",\"updated_at\":\"2020-06-18T06:36:14.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Cheiron\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:35:00\",\"equipment_date_inspected\":\"2020-06-18 14:35:00\",\"equipment_date_acquired\":\"2017-08-11 00:00:00\",\"equipment_supplier\":\"Saviour MeDevices\",\"equipment_unit_price\":75800,\"equipment_location\":\"Medical 2\",\"equipment_tag_number\":\"85SUMED\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 14:36:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Abegail Baliling\"}}', '2020-06-17 22:36:14', '2020-06-17 22:36:14'),
(113, 'default', 'created', 109, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":109,\"equipment_name\":null,\"equipment_serial\":\"1735076\",\"equipment_model\":\"Vario 8\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:38:55.000000Z\",\"updated_at\":\"2020-06-18T06:38:55.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:38:00\",\"equipment_date_inspected\":\"2020-06-18 14:38:00\",\"equipment_date_acquired\":\"2019-04-17 00:00:00\",\"equipment_supplier\":\"Luzon Quick\",\"equipment_unit_price\":374800,\"equipment_location\":\"\\\"Surg. 1 \\\"\",\"equipment_tag_number\":\"86SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:38:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Richelle Decasa\"}}', '2020-06-17 22:38:55', '2020-06-17 22:38:55'),
(114, 'default', 'created', 110, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":110,\"equipment_name\":\"Thoracic Pump\",\"equipment_serial\":\"1735077\",\"equipment_model\":\"Vario 8\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:41:48.000000Z\",\"updated_at\":\"2020-06-18T06:41:48.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:41:00\",\"equipment_date_inspected\":\"2020-06-18 14:41:00\",\"equipment_date_acquired\":\"2019-04-17 00:00:00\",\"equipment_supplier\":\"Luzon Quick\",\"equipment_unit_price\":374800,\"equipment_location\":\"\\\"Surg. 1 \\\"\",\"equipment_tag_number\":\"87SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:41:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma. Marcelis Andoy\"}}', '2020-06-17 22:41:48', '2020-06-17 22:41:48'),
(115, 'default', 'created', 111, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":111,\"equipment_name\":\"Thoracic Pump\",\"equipment_serial\":\"1735074\",\"equipment_model\":\"Vario 8\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:44:40.000000Z\",\"updated_at\":\"2020-06-18T06:44:40.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:44:00\",\"equipment_date_inspected\":\"2020-06-18 14:44:00\",\"equipment_date_acquired\":\"2019-04-17 00:00:00\",\"equipment_supplier\":\"Luzon Quick\",\"equipment_unit_price\":374800,\"equipment_location\":\"Surg. 2\",\"equipment_tag_number\":\"88SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:44:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Joanne Dela Torre\"}}', '2020-06-17 22:44:40', '2020-06-17 22:44:40'),
(116, 'default', 'created', 112, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":112,\"equipment_name\":null,\"equipment_serial\":\"1735078\",\"equipment_model\":\"Vario 8\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:47:21.000000Z\",\"updated_at\":\"2020-06-18T06:47:21.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medela\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:47:00\",\"equipment_date_inspected\":\"2020-06-18 14:47:00\",\"equipment_date_acquired\":\"2019-04-17 00:00:00\",\"equipment_supplier\":\"Luzon Quick\",\"equipment_unit_price\":374800,\"equipment_location\":\"Surg. 2\",\"equipment_tag_number\":\"89SUSUR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:47:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Ma Heidi Corbita\"}}', '2020-06-17 22:47:21', '2020-06-17 22:47:21'),
(117, 'default', 'created', 113, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":113,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1807022\",\"equipment_model\":\"SU-305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:50:13.000000Z\",\"updated_at\":\"2020-06-18T06:50:13.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Gemmyco\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:49:00\",\"equipment_date_inspected\":\"2020-06-18 14:49:00\",\"equipment_date_acquired\":\"2019-09-25 00:00:00\",\"equipment_supplier\":\"Iraseth Pharma\",\"equipment_unit_price\":195000,\"equipment_location\":\"OB-OR\",\"equipment_tag_number\":\"91SUOBOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:50:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Karen Supieza\"}}', '2020-06-17 22:50:14', '2020-06-17 22:50:14'),
(118, 'default', 'created', 114, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":114,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"1906400\",\"equipment_model\":\"SU-305\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T06:53:06.000000Z\",\"updated_at\":\"2020-06-18T06:53:06.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Gemmyco\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 14:52:00\",\"equipment_date_inspected\":\"2020-06-18 14:52:00\",\"equipment_date_acquired\":\"2019-09-25 00:00:00\",\"equipment_supplier\":\"Iraseth Pharma\",\"equipment_unit_price\":195000,\"equipment_location\":\"OB-OR\",\"equipment_tag_number\":\"90SUOBOR\",\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Under Warranty\",\"equipment_date_of_remarks\":\"2020-02-14 14:52:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Karen Supieza\"}}', '2020-06-17 22:53:06', '2020-06-17 22:53:06'),
(119, 'default', 'created', 115, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":115,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"SS 975611\",\"equipment_model\":\"C55F\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T12:21:49.000000Z\",\"updated_at\":\"2020-06-18T12:21:49.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medica\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 20:21:00\",\"equipment_date_inspected\":\"2020-06-18 20:21:00\",\"equipment_date_acquired\":\"2019-10-16 00:00:00\",\"equipment_supplier\":\"Mular Medical\",\"equipment_unit_price\":238000,\"equipment_location\":\"Main OR\",\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 20:21:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Angeli Apayor\"}}', '2020-06-18 04:21:49', '2020-06-18 04:21:49'),
(120, 'default', 'created', 116, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":116,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"SS 975603\",\"equipment_model\":\"C55F\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T12:24:48.000000Z\",\"updated_at\":\"2020-06-18T12:24:48.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medica\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 20:24:00\",\"equipment_date_inspected\":\"2020-06-18 20:24:00\",\"equipment_date_acquired\":\"2019-10-16 00:00:00\",\"equipment_supplier\":\"Mular Medical\",\"equipment_unit_price\":238000,\"equipment_location\":\"Main OR\",\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 20:24:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Angeli Apayor\"}}', '2020-06-18 04:24:48', '2020-06-18 04:24:48'),
(121, 'default', 'created', 117, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":117,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"SS 975561\",\"equipment_model\":\"C55F\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T12:27:42.000000Z\",\"updated_at\":\"2020-06-18T12:27:42.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medica\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 20:27:00\",\"equipment_date_inspected\":\"2020-06-18 20:27:00\",\"equipment_date_acquired\":\"2019-10-16 00:00:00\",\"equipment_supplier\":\"Mular Medical\",\"equipment_unit_price\":238000,\"equipment_location\":\"Main OR\",\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 20:27:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Glecello Marzo Policianos\"}}', '2020-06-18 04:27:42', '2020-06-18 04:27:42'),
(122, 'default', 'created', 118, 'App\\Equipment', 1, 'App\\User', '{\"attributes\":{\"equipment_key\":118,\"equipment_name\":\"Suction Machine\",\"equipment_serial\":\"SS 975602\",\"equipment_model\":\"C55F\",\"equipment_quantity\":null,\"created_at\":\"2020-06-18T12:31:02.000000Z\",\"updated_at\":\"2020-06-18T12:31:02.000000Z\",\"deleted_at\":null,\"equipment_description\":null,\"equipment_brand\":\"Medica\",\"equipment_country\":null,\"equipment_date_delivered\":\"2020-06-18 20:30:00\",\"equipment_date_inspected\":\"2020-06-18 20:30:00\",\"equipment_date_acquired\":\"2019-10-16 00:00:00\",\"equipment_supplier\":\"Mular Medical\",\"equipment_unit_price\":238000,\"equipment_location\":\"Main OR\",\"equipment_tag_number\":null,\"equipment_card_number\":null,\"equipment_memorandum_receipt\":\"[]\",\"equipment_iar_number\":null,\"equipment_asset_categories\":null,\"equipment_account_code\":null,\"equipment_purchase_order\":null,\"equipment_remarks\":\"Functional\",\"equipment_date_of_remarks\":\"2020-02-14 20:30:00\",\"equipment_status\":null,\"equipment_date_of_status\":null,\"equipment_par\":\"Glecello Marzo Policianos\"}}', '2020-06-18 04:31:02', '2020-06-18 04:31:02');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(10) UNSIGNED NOT NULL,
  `country_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `country_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Afghanistan', NULL, NULL, NULL),
(2, 'Albania', NULL, NULL, NULL),
(3, 'Algeria', NULL, NULL, NULL),
(4, 'Andorra', NULL, NULL, NULL),
(5, 'Angola', NULL, NULL, NULL),
(6, 'Anguilla', NULL, NULL, NULL),
(7, 'Antigua & Barbuda', NULL, NULL, NULL),
(8, 'Argentina', NULL, NULL, NULL),
(9, 'Armenia', NULL, NULL, NULL),
(10, 'Australia', NULL, NULL, NULL),
(11, 'Austria', NULL, NULL, NULL),
(12, 'Azerbaijan', NULL, NULL, NULL),
(13, 'Bahamas', NULL, NULL, NULL),
(14, 'Bahrain', NULL, NULL, NULL),
(15, 'Bangladesh', NULL, NULL, NULL),
(16, 'Barbados', NULL, NULL, NULL),
(17, 'Belarus', NULL, NULL, NULL),
(18, 'Belgium', NULL, NULL, NULL),
(19, 'Belize', NULL, NULL, NULL),
(20, 'Benin', NULL, NULL, NULL),
(21, 'Bermuda', NULL, NULL, NULL),
(22, 'Bhutan', NULL, NULL, NULL),
(23, 'Bolivia', NULL, NULL, NULL),
(24, 'Bosnia & Herzegovina', NULL, NULL, NULL),
(25, 'Botswana', NULL, NULL, NULL),
(26, 'Brazil', NULL, NULL, NULL),
(27, 'Brunei Darussalam', NULL, NULL, NULL),
(28, 'Bulgaria', NULL, NULL, NULL),
(29, 'Burkina Faso', NULL, NULL, NULL),
(30, 'Myanmar/Burma', NULL, NULL, NULL),
(31, 'Burundi', NULL, NULL, NULL),
(32, 'Cambodia', NULL, NULL, NULL),
(33, 'Cameroon', NULL, NULL, NULL),
(34, 'Canada', NULL, NULL, NULL),
(35, 'Cape Verde', NULL, NULL, NULL),
(36, 'Cayman Islands', NULL, NULL, NULL),
(37, 'Central African Republic', NULL, NULL, NULL),
(38, 'Chad', NULL, NULL, NULL),
(39, 'Chile', NULL, NULL, NULL),
(40, 'China', NULL, NULL, NULL),
(41, 'Colombia', NULL, NULL, NULL),
(42, 'Comoros', NULL, NULL, NULL),
(43, 'Congo', NULL, NULL, NULL),
(44, 'Costa Rica', NULL, NULL, NULL),
(45, 'Croatia', NULL, NULL, NULL),
(46, 'Cuba', NULL, NULL, NULL),
(47, 'Cyprus', NULL, NULL, NULL),
(48, 'Czech Republic', NULL, NULL, NULL),
(49, 'Democratic Republic of the Congo', NULL, NULL, NULL),
(50, 'Denmark', NULL, NULL, NULL),
(51, 'Djibouti', NULL, NULL, NULL),
(52, 'Dominican Republic', NULL, NULL, NULL),
(53, 'Dominica', NULL, NULL, NULL),
(54, 'Ecuador', NULL, NULL, NULL),
(55, 'Egypt', NULL, NULL, NULL),
(56, 'El Salvador', NULL, NULL, NULL),
(57, 'Equatorial Guinea', NULL, NULL, NULL),
(58, 'Eritrea', NULL, NULL, NULL),
(59, 'Estonia', NULL, NULL, NULL),
(60, 'Ethiopia', NULL, NULL, NULL),
(61, 'Fiji', NULL, NULL, NULL),
(62, 'Finland', NULL, NULL, NULL),
(63, 'France', NULL, NULL, NULL),
(64, 'French Guiana', NULL, NULL, NULL),
(65, 'Gabon', NULL, NULL, NULL),
(66, 'Gambia', NULL, NULL, NULL),
(67, 'Georgia', NULL, NULL, NULL),
(68, 'Germany', NULL, NULL, NULL),
(69, 'Ghana', NULL, NULL, NULL),
(70, 'Great Britain', NULL, NULL, NULL),
(71, 'Greece', NULL, NULL, NULL),
(72, 'Grenada', NULL, NULL, NULL),
(73, 'Guadeloupe', NULL, NULL, NULL),
(74, 'Guatemala', NULL, NULL, NULL),
(75, 'Guinea', NULL, NULL, NULL),
(76, 'Guinea-Bissau', NULL, NULL, NULL),
(77, 'Guyana', NULL, NULL, NULL),
(78, 'Haiti', NULL, NULL, NULL),
(79, 'Honduras', NULL, NULL, NULL),
(80, 'Hungary', NULL, NULL, NULL),
(81, 'Iceland', NULL, NULL, NULL),
(82, 'India', NULL, NULL, NULL),
(83, 'Indonesia', NULL, NULL, NULL),
(84, 'Iran', NULL, NULL, NULL),
(85, 'Iraq', NULL, NULL, NULL),
(86, 'Israel and the Occupied Territories', NULL, NULL, NULL),
(87, 'Italy', NULL, NULL, NULL),
(88, 'Ivory Coast (Cote d\'Ivoire)', NULL, NULL, NULL),
(89, 'Jamaica', NULL, NULL, NULL),
(90, 'Japan', NULL, NULL, NULL),
(91, 'Jordan', NULL, NULL, NULL),
(92, 'Kazakhstan', NULL, NULL, NULL),
(93, 'Kenya', NULL, NULL, NULL),
(94, 'Kosovo', NULL, NULL, NULL),
(95, 'Kuwait', NULL, NULL, NULL),
(96, 'Kyrgyz Republic (Kyrgyzstan)', NULL, NULL, NULL),
(97, 'Laos', NULL, NULL, NULL),
(98, 'Latvia', NULL, NULL, NULL),
(99, 'Lebanon', NULL, NULL, NULL),
(100, 'Lesotho', NULL, NULL, NULL),
(101, 'Liberia', NULL, NULL, NULL),
(102, 'Libya', NULL, NULL, NULL),
(103, 'Liechtenstein', NULL, NULL, NULL),
(104, 'Lithuania', NULL, NULL, NULL),
(105, 'Luxembourg', NULL, NULL, NULL),
(106, 'Republic of Macedonia', NULL, NULL, NULL),
(107, 'Madagascar', NULL, NULL, NULL),
(108, 'Malawi', NULL, NULL, NULL),
(109, 'Malaysia', NULL, NULL, NULL),
(110, 'Maldives', NULL, NULL, NULL),
(111, 'Mali', NULL, NULL, NULL),
(112, 'Malta', NULL, NULL, NULL),
(113, 'Martinique', NULL, NULL, NULL),
(114, 'Mauritania', NULL, NULL, NULL),
(115, 'Mauritius', NULL, NULL, NULL),
(116, 'Mayotte', NULL, NULL, NULL),
(117, 'Mexico', NULL, NULL, NULL),
(118, 'Moldova, Republic of', NULL, NULL, NULL),
(119, 'Monaco', NULL, NULL, NULL),
(120, 'Mongolia', NULL, NULL, NULL),
(121, 'Montenegro', NULL, NULL, NULL),
(122, 'Montserrat', NULL, NULL, NULL),
(123, 'Morocco', NULL, NULL, NULL),
(124, 'Mozambique', NULL, NULL, NULL),
(125, 'Namibia', NULL, NULL, NULL),
(126, 'Nepal', NULL, NULL, NULL),
(127, 'Netherlands', NULL, NULL, NULL),
(128, 'New Zealand', NULL, NULL, NULL),
(129, 'Nicaragua', NULL, NULL, NULL),
(130, 'Niger', NULL, NULL, NULL),
(131, 'Nigeria', NULL, NULL, NULL),
(132, 'Korea, Democratic Republic of (North Korea)', NULL, NULL, NULL),
(133, 'Norway', NULL, NULL, NULL),
(134, 'Oman', NULL, NULL, NULL),
(135, 'Pacific Islands', NULL, NULL, NULL),
(136, 'Pakistan', NULL, NULL, NULL),
(137, 'Panama', NULL, NULL, NULL),
(138, 'Papua New Guinea', NULL, NULL, NULL),
(139, 'Paraguay', NULL, NULL, NULL),
(140, 'Peru', NULL, NULL, NULL),
(141, 'Philippines', NULL, NULL, NULL),
(142, 'Poland', NULL, NULL, NULL),
(143, 'Portugal', NULL, NULL, NULL),
(144, 'Puerto Rico', NULL, NULL, NULL),
(145, 'Qatar', NULL, NULL, NULL),
(146, 'Reunion', NULL, NULL, NULL),
(147, 'Romania', NULL, NULL, NULL),
(148, 'Russian Federation', NULL, NULL, NULL),
(149, 'Rwanda', NULL, NULL, NULL),
(150, 'Saint Kitts and Nevis', NULL, NULL, NULL),
(151, 'Saint Lucia', NULL, NULL, NULL),
(152, 'Saint Vincent\'s & Grenadines', NULL, NULL, NULL),
(153, 'Samoa', NULL, NULL, NULL),
(154, 'Sao Tome and Principe', NULL, NULL, NULL),
(155, 'Saudi Arabia', NULL, NULL, NULL),
(156, 'Senegal', NULL, NULL, NULL),
(157, 'Serbia', NULL, NULL, NULL),
(158, 'Seychelles', NULL, NULL, NULL),
(159, 'Sierra Leone', NULL, NULL, NULL),
(160, 'Singapore', NULL, NULL, NULL),
(161, 'Slovak Republic (Slovakia)', NULL, NULL, NULL),
(162, 'Slovenia', NULL, NULL, NULL),
(163, 'Solomon Islands', NULL, NULL, NULL),
(164, 'Somalia', NULL, NULL, NULL),
(165, 'South Africa', NULL, NULL, NULL),
(166, 'Korea, Republic of (South Korea)', NULL, NULL, NULL),
(167, 'South Sudan', NULL, NULL, NULL),
(168, 'Spain', NULL, NULL, NULL),
(169, 'Sri Lanka', NULL, NULL, NULL),
(170, 'Sudan', NULL, NULL, NULL),
(171, 'Suriname', NULL, NULL, NULL),
(172, 'Swaziland', NULL, NULL, NULL),
(173, 'Sweden', NULL, NULL, NULL),
(174, 'Switzerland', NULL, NULL, NULL),
(175, 'Syria', NULL, NULL, NULL),
(176, 'Tajikistan', NULL, NULL, NULL),
(177, 'Tanzania', NULL, NULL, NULL),
(178, 'Thailand', NULL, NULL, NULL),
(179, 'Timor Leste', NULL, NULL, NULL),
(180, 'Togo', NULL, NULL, NULL),
(181, 'Trinidad & Tobago', NULL, NULL, NULL),
(182, 'Tunisia', NULL, NULL, NULL),
(183, 'Turkey', NULL, NULL, NULL),
(184, 'Turkmenistan', NULL, NULL, NULL),
(185, 'Turks & Caicos Islands', NULL, NULL, NULL),
(186, 'Uganda', NULL, NULL, NULL),
(187, 'Ukraine', NULL, NULL, NULL),
(188, 'United Arab Emirates', NULL, NULL, NULL),
(189, 'United States of America (USA)', NULL, NULL, NULL),
(190, 'Uruguay', NULL, NULL, NULL),
(191, 'Uzbekistan', NULL, NULL, NULL),
(192, 'Venezuela', NULL, NULL, NULL),
(193, 'Vietnam', NULL, NULL, NULL),
(194, 'Virgin Islands (UK)', NULL, NULL, NULL),
(195, 'Virgin Islands (US)', NULL, NULL, NULL),
(196, 'Yemen', NULL, NULL, NULL),
(197, 'Zambia', NULL, NULL, NULL),
(198, 'Zimbabwe', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '{}', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '{}', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '{}', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '{}', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '{}', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '{}', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '{}', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\",\"taggable\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '{}', 12),
(12, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(13, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(14, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(15, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(16, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(17, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(18, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(19, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(20, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(21, 1, 'role_id', 'text', 'Role', 0, 1, 1, 1, 1, 1, '{}', 9),
(22, 4, 'purchase_order_key', 'text', 'Purchase Order Key', 1, 0, 0, 0, 0, 0, '{}', 1),
(23, 4, 'purchase_order_number', 'text', 'PO Number', 0, 1, 1, 1, 1, 1, '{}', 2),
(24, 4, 'purchase_order_pr_number', 'text', 'PR Number', 0, 1, 1, 1, 1, 1, '{}', 3),
(25, 4, 'purchase_order_mode_of_procurement', 'text', 'Mode Of Procurement', 0, 1, 1, 1, 1, 1, '{}', 4),
(26, 4, 'purchase_order_date_awarded', 'timestamp', 'Date Awarded', 0, 1, 1, 1, 1, 1, '{}', 5),
(27, 4, 'purchase_order_date_of_po_received', 'timestamp', 'Date Of Po Received', 0, 1, 1, 1, 1, 1, '{}', 6),
(28, 4, 'purchase_order_date_delivered', 'timestamp', 'Date Delivered', 0, 1, 1, 1, 1, 1, '{}', 7),
(29, 4, 'purchase_order_inspected', 'timestamp', 'Inspected', 0, 1, 1, 1, 1, 1, '{}', 8),
(30, 4, 'purchase_order_date_acquired', 'timestamp', 'Date Acquired', 0, 1, 1, 1, 1, 1, '{}', 9),
(31, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 10),
(32, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 11),
(33, 4, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 12),
(34, 5, 'equipment_key', 'text', 'Equipment Key', 1, 0, 0, 0, 0, 0, '{}', 1),
(35, 5, 'equipment_name', 'text', 'Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(36, 5, 'equipment_serial', 'text', 'Serial', 0, 1, 1, 1, 1, 1, '{}', 4),
(37, 5, 'equipment_model', 'text', 'Model', 0, 1, 1, 1, 1, 1, '{}', 6),
(38, 5, 'equipment_quantity', 'text', 'Quantity', 0, 1, 1, 1, 1, 1, '{}', 8),
(39, 5, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '{}', 26),
(40, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 24),
(41, 5, 'deleted_at', 'timestamp', 'Deleted At', 0, 0, 0, 0, 0, 0, '{}', 25),
(42, 5, 'equipment_description', 'text', 'Description', 0, 1, 1, 1, 1, 1, '{}', 3),
(43, 5, 'equipment_brand', 'text', 'Brand', 0, 1, 1, 1, 1, 1, '{}', 5),
(44, 5, 'equipment_country', 'text', 'Country', 0, 1, 1, 1, 1, 1, '{}', 14),
(45, 5, 'equipment_date_delivered', 'timestamp', 'Date Delivered', 0, 1, 1, 1, 1, 1, '{}', 17),
(46, 5, 'equipment_date_inspected', 'timestamp', 'Date Inspected', 0, 1, 1, 1, 1, 1, '{}', 18),
(47, 5, 'equipment_date_acquired', 'timestamp', 'Date Acquired', 0, 1, 1, 1, 1, 1, '{}', 9),
(48, 5, 'equipment_supplier', 'text', 'Supplier', 0, 1, 1, 1, 1, 1, '{}', 12),
(49, 5, 'equipment_unit_price', 'number', 'Unit Price', 0, 1, 1, 1, 1, 1, '{}', 13),
(50, 5, 'equipment_location', 'text', 'Location', 0, 1, 1, 1, 1, 1, '{}', 10),
(51, 5, 'equipment_tag_number', 'text', 'Tag Number', 0, 1, 1, 1, 1, 1, '{}', 7),
(52, 5, 'equipment_card_number', 'text', 'Card Number', 0, 1, 1, 1, 1, 1, '{}', 19),
(53, 5, 'equipment_memorandum_receipt', 'file', 'Memorandum Receipt', 0, 1, 1, 1, 1, 1, '{\"display\":{\"width\":\"3\",\"id\":\"custom_id\"}}', 31),
(54, 5, 'equipment_iar_number', 'number', 'IAR Number', 0, 1, 1, 1, 1, 1, '{}', 23),
(55, 5, 'equipment_asset_categories', 'text', 'Asset Categories', 0, 1, 1, 1, 1, 1, '{}', 20),
(56, 5, 'equipment_account_code', 'text', 'Account Code', 0, 1, 1, 1, 1, 1, '{}', 21),
(57, 5, 'equipment_purchase_order', 'text', 'Purchase Order', 0, 1, 1, 1, 1, 1, '{}', 22),
(58, 1, 'email_verified_at', 'timestamp', 'Email Verified At', 0, 1, 1, 1, 1, 1, '{}', 6),
(59, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(60, 7, 'log_name', 'text', 'Log Name', 0, 1, 1, 1, 1, 1, '{}', 2),
(61, 7, 'description', 'text', 'Description', 1, 1, 1, 1, 1, 1, '{}', 3),
(62, 7, 'subject_id', 'text', 'Subject Id', 0, 1, 1, 1, 1, 1, '{}', 4),
(63, 7, 'subject_type', 'text', 'Subject Type', 0, 1, 1, 1, 1, 1, '{}', 5),
(64, 7, 'causer_id', 'text', 'Causer Id', 0, 1, 1, 1, 1, 1, '{}', 6),
(65, 7, 'causer_type', 'text', 'Causer Type', 0, 1, 1, 1, 1, 1, '{}', 7),
(66, 7, 'properties', 'text', 'Properties', 0, 1, 1, 1, 1, 1, '{}', 8),
(67, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(68, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(69, 5, 'equipment_belongsto_purchase_order_relationship', 'relationship', 'PO Number', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\PurchaseOrder\",\"table\":\"purchase_orders\",\"type\":\"belongsTo\",\"column\":\"equipment_purchase_order\",\"key\":\"purchase_order_number\",\"label\":\"purchase_order_number\",\"pivot_table\":\"activity_log\",\"pivot\":\"0\",\"taggable\":\"0\"}', 16),
(70, 5, 'equipment_belongsto_country_relationship', 'relationship', 'Country', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"equipment_country\",\"key\":\"country_name\",\"label\":\"country_name\",\"pivot_table\":\"activity_log\",\"pivot\":\"0\",\"taggable\":\"0\"}', 15),
(71, 5, 'equipment_remarks', 'text', 'Remarks', 0, 1, 1, 1, 1, 1, '{}', 27),
(72, 5, 'equipment_date_of_remarks', 'timestamp', 'Date Of Remarks', 0, 1, 1, 1, 1, 1, '{}', 28),
(73, 5, 'equipment_status', 'text', 'Status', 0, 1, 1, 1, 1, 1, '{}', 29),
(74, 5, 'equipment_date_of_status', 'timestamp', 'Date Of Status', 0, 1, 1, 1, 1, 1, '{}', 30),
(75, 5, 'equipment_par', 'text', 'PAR', 0, 1, 1, 1, 1, 1, '{}', 11);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"desc\",\"default_search_key\":null,\"scope\":null}', '2020-06-07 02:36:13', '2020-06-07 22:48:48'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2020-06-07 02:36:13', '2020-06-07 02:36:13'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController', '', 1, 0, NULL, '2020-06-07 02:36:13', '2020-06-07 02:36:13'),
(4, 'purchase_orders', 'purchase-orders', 'Purchase Order', 'Purchase Orders', NULL, 'App\\PurchaseOrder', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-07 02:40:45', '2020-06-09 20:34:01'),
(5, 'equipment', 'equipment', 'Equipment', 'Equipment', NULL, 'App\\Equipment', NULL, 'App\\Http\\Controllers\\EquipmentController', NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null,\"scope\":null}', '2020-06-07 04:34:58', '2020-06-17 05:15:47'),
(7, 'activity_log', 'activity-log', 'Activity Log', 'Activity Logs', NULL, 'App\\Activity', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-06-07 23:41:01', '2020-06-07 23:41:01'),
(8, 'medicine_name', 'medicine-name', 'Medicine Name', 'Medicine Names', NULL, 'App\\MedicineName', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2020-07-12 22:09:33', '2020-07-12 22:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `equipment`
--

CREATE TABLE `equipment` (
  `equipment_key` int(10) UNSIGNED NOT NULL,
  `equipment_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_serial` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_quantity` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `equipment_description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_date_delivered` timestamp NULL DEFAULT NULL,
  `equipment_date_inspected` timestamp NULL DEFAULT NULL,
  `equipment_date_acquired` timestamp NULL DEFAULT NULL,
  `equipment_supplier` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_unit_price` int(11) DEFAULT NULL,
  `equipment_location` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_tag_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_card_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_memorandum_receipt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_iar_number` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_asset_categories` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_account_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_purchase_order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_remarks` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_date_of_remarks` timestamp NULL DEFAULT NULL,
  `equipment_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `equipment_date_of_status` timestamp NULL DEFAULT NULL,
  `equipment_par` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `equipment`
--

INSERT INTO `equipment` (`equipment_key`, `equipment_name`, `equipment_serial`, `equipment_model`, `equipment_quantity`, `created_at`, `updated_at`, `deleted_at`, `equipment_description`, `equipment_brand`, `equipment_country`, `equipment_date_delivered`, `equipment_date_inspected`, `equipment_date_acquired`, `equipment_supplier`, `equipment_unit_price`, `equipment_location`, `equipment_tag_number`, `equipment_card_number`, `equipment_memorandum_receipt`, `equipment_iar_number`, `equipment_asset_categories`, `equipment_account_code`, `equipment_purchase_order`, `equipment_remarks`, `equipment_date_of_remarks`, `equipment_status`, `equipment_date_of_status`, `equipment_par`) VALUES
(22, 'ECG Machine', '19975-M16306020017', NULL, NULL, '2020-06-17 04:52:07', '2020-06-17 04:52:07', NULL, NULL, 'Edan', NULL, NULL, NULL, '2016-12-16 00:49:00', NULL, 30000, 'NICU', '24ECNIC', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 12:51:00', NULL, NULL, 'Rosa Secretaria'),
(23, 'ECG Machine', '1606-1702', '305', NULL, '2020-06-17 04:56:04', '2020-06-17 04:56:04', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-10 02:00:00', 'RG Meditron Inc', 53750, 'Pedia', '20ECPED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 12:55:00', NULL, NULL, 'Jozaine Jamila/Barbarona'),
(24, 'ECG Machine', '1606-1707', '305', NULL, '2020-06-17 04:59:00', '2020-06-17 04:59:00', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-17 12:57:00', 'RG Meditron Inc', 53750, 'ER', '13ECER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 12:58:00', NULL, NULL, 'Sarah Jean Ecat'),
(25, 'ECG Machine', '1606-1703', '305', NULL, '2020-06-17 05:03:20', '2020-06-17 05:03:20', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-10 01:01:00', 'RG Meditron Inc', 53750, 'OR', '15ECOR', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 13:03:00', NULL, NULL, 'Abegail Baliling'),
(26, 'ECG Machine', '1606-1711', '305', NULL, '2020-06-17 05:16:14', '2020-06-17 05:16:14', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-10 01:01:00', 'RG Meditron Inc', 53750, 'Med. 1', '22ECMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 13:03:00', NULL, NULL, 'Ma Marcelis Andoy'),
(27, 'ECG Machine', '1606-1701', '305', NULL, '2020-06-17 05:17:38', '2020-06-17 05:17:38', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-10 01:01:00', 'RG Meditron Inc', 53750, 'Med. 2', '23ECMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 13:03:00', NULL, NULL, 'Hector Rodriguez'),
(28, 'ECG Machine', '1606-1710', '305', NULL, '2020-06-17 05:20:04', '2020-06-17 05:20:04', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-10 01:01:00', 'RG Meditron Inc', 53750, 'MICU', '17ECMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 13:03:00', NULL, NULL, 'Milagros Felias'),
(29, 'ECG Machine', '1606-1704', '305', NULL, '2020-06-17 05:23:48', '2020-06-17 05:23:48', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-10 01:01:00', 'RG Meditron Inc', 53750, 'Surg.2', '16ECSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 13:03:00', NULL, NULL, 'Dinah Chato'),
(30, '02/14/2020 9:03 PM', '1606-1712', '305', NULL, '2020-06-17 05:31:50', '2020-06-17 05:31:50', NULL, NULL, 'Kenz', NULL, NULL, NULL, '2017-05-10 01:01:00', 'RG Meditron Inc', 53750, 'Surg. 1', '11ECSUR', NULL, '[{\"download_link\":\"equipment\\\\June2020\\\\Tf7K5mWLPlHNPcZFPGiu.jpg\",\"original_name\":\"download.jpg\"}]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 13:03:00', NULL, NULL, 'Joziane Jamila'),
(31, 'ECG Machine', '0310-4533', '108', NULL, '2020-06-17 05:45:26', '2020-06-17 05:45:26', NULL, NULL, 'Kenz', NULL, '2020-06-17 13:45:00', '2020-06-17 13:45:00', '2004-03-19 16:00:00', 'RG Meditron Inc', 68000, 'OPD', '06ECOPD', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 13:45:00', NULL, NULL, 'L. Gementiza'),
(32, 'ECG Machine', '1606-1596', '305', NULL, '2020-06-17 05:57:00', '2020-06-17 06:05:26', NULL, NULL, 'Kenz', NULL, '2020-06-17 13:48:00', '2020-06-17 13:48:00', '2017-02-08 16:00:00', 'RG Meditron Inc', 60000, 'OPD', '21ECOPD', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 13:57:00', NULL, NULL, 'Marcela Acera'),
(33, 'ECG Machine', '10140522004', NULL, NULL, '2020-06-17 06:03:36', '2020-06-17 06:09:08', NULL, NULL, 'Fazinni', NULL, '2020-06-17 14:01:00', '2020-06-17 14:01:00', '2017-05-09 16:00:00', 'Medicotek Inc', 83000, 'Private', '09ECPVT', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 14:03:00', NULL, NULL, 'Marites Plazos'),
(34, 'ECG Machine', '1309-4726', NULL, NULL, '2020-06-17 06:21:49', '2020-06-17 06:21:49', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:21:00', '2020-06-17 14:21:00', '2014-03-26 16:00:00', 'RG Meditron Inc', 80000, 'HDU', '07ECHDU', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 14:21:00', NULL, NULL, 'Darwin Quevedo'),
(35, 'ECG Machine', '0511-7395', NULL, NULL, '2020-06-17 06:25:50', '2020-06-17 06:25:50', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:25:00', '2020-06-17 14:25:00', '2006-03-07 16:00:00', 'RG Meditron Inc', 70000, 'PICU', '01ECPED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 14:25:00', NULL, NULL, 'Ma Evelyn Barbarona'),
(36, 'ECG Machine', '1601-1265V4', '305', NULL, '2020-06-17 06:32:00', '2020-06-17 06:32:00', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:31:00', '2020-06-17 14:31:00', '2015-05-11 16:00:00', 'RG Meditron Inc', 65000, 'ER', '10ECER', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 14:31:00', NULL, NULL, 'Sarah Jean Ecat'),
(37, 'ECG Machine', '1606-1706', '305', NULL, '2020-06-17 06:35:45', '2020-06-17 06:35:45', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:34:00', '2020-06-17 14:34:00', '2017-05-09 16:00:00', 'RG Meditron Inc', 53750, 'ER', '25ECER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 14:35:00', NULL, NULL, 'Sarah Jean Ecat'),
(38, 'ECG Machine', '1605-1593', '305', NULL, '2020-06-17 06:39:30', '2020-06-17 06:39:30', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:38:00', '2020-06-17 14:38:00', '2017-02-08 16:00:00', 'RG Meditron Inc', 60000, 'ER', '19ECER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 14:39:00', NULL, NULL, 'Emelly Grace Quipit'),
(39, 'ECG Machine', '1605-1595', '305', NULL, '2020-06-17 06:43:14', '2020-06-17 06:43:14', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:42:00', '2020-06-17 14:42:00', '2017-02-08 16:00:00', 'RG Meditron Inc', 60000, 'Med. 1', '12ECMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 14:42:00', NULL, NULL, 'Ma Marcelis Andoy'),
(40, 'ECG Machine', '1605-1594', '305', NULL, '2020-06-17 06:47:27', '2020-06-17 06:47:27', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:46:00', '2020-06-17 14:46:00', '2017-02-08 16:00:00', 'RG Meditron Inc', 60000, 'Med. 2', '18ECMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 14:47:00', NULL, NULL, 'Abegail Mercader'),
(41, 'ECG Machine', '0511-7377', NULL, NULL, '2020-06-17 06:51:10', '2020-06-17 06:51:10', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:50:00', '2020-06-17 14:50:00', '2006-03-07 16:00:00', 'RG Meditron Inc', 70000, 'MICU', '02ECMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 14:50:00', NULL, NULL, 'Loretta Brunidor'),
(42, 'ECG Machine', '1108-0794', NULL, NULL, '2020-06-17 06:54:54', '2020-06-17 06:54:54', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:54:00', '2020-06-17 14:54:00', '2003-02-14 16:00:00', 'RG Meditron Inc', 112650, 'Surg. 1', '08ECSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 14:54:00', NULL, NULL, 'Joselita Buaquina'),
(43, 'ECG Machine', '1606-1709', '305', NULL, '2020-06-17 06:58:39', '2020-06-17 06:58:39', NULL, NULL, 'Kenz', NULL, '2020-06-17 14:57:00', '2020-06-17 14:57:00', '2017-05-09 16:00:00', 'RG Meditron Inc', 53750, 'Private', '14ECPVT', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 14:58:00', NULL, NULL, 'Helen Auxtero'),
(44, 'ECG Machine', '1804-3227', '306', NULL, '2020-06-17 07:07:42', '2020-06-17 07:07:42', NULL, NULL, 'Kenz', NULL, '2020-06-17 15:06:00', '2020-06-17 15:06:00', '2019-04-07 16:00:00', 'RG Meditron Inc', 165000, 'SICU', '26ECSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Under Warranty', '2020-02-14 15:07:00', NULL, NULL, 'Karen Cimafranca'),
(45, 'ECG Machine', '1902-2565', '305', NULL, '2020-06-17 07:11:25', '2020-06-17 07:11:25', NULL, NULL, 'Kenz', NULL, '2020-06-17 15:10:00', '2020-06-17 15:10:00', '2019-09-26 16:00:00', 'RG Meditron Inc', 70000, 'OB', '27ECOB', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 15:11:00', NULL, NULL, 'Nemiza Digal'),
(46, 'ECG Machine', '1902-2567', '305', NULL, '2020-06-17 07:18:53', '2020-06-17 07:18:53', NULL, NULL, 'Kenz', NULL, '2020-06-17 15:14:00', '2020-06-17 15:14:00', '2019-10-21 16:00:00', 'RG Meditron Inc', 78000, 'MICU', '29ECMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 15:18:00', NULL, NULL, 'Joel Bioco'),
(47, 'Suction Machine', '11020045', '--', NULL, '2020-06-17 07:41:45', '2020-06-17 08:19:21', NULL, NULL, '\"Green  Trust\"', NULL, NULL, NULL, NULL, '--', 0, 'ER', '30SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 15:41:00', NULL, NULL, '--'),
(48, 'Suction Machine', '1047405', 'Basic 30', NULL, '2020-06-17 07:45:42', '2020-06-17 08:20:06', NULL, NULL, 'Medela', NULL, '2020-06-17 15:44:00', '2020-06-17 15:44:00', '2003-06-01 16:00:00', 'Dispo Philippines Inc.', 150000, 'PICU', '04SUPED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 15:45:00', NULL, NULL, 'Ma Evelyn Barbarona'),
(49, 'Suction Machine', '1047404', 'Basic 30', NULL, '2020-06-17 07:49:26', '2020-06-17 08:20:45', NULL, NULL, 'Medela', NULL, '2020-06-17 15:48:00', '2020-06-17 15:48:00', '2003-06-01 16:00:00', 'Dispo Philippines Inc.', 150000, 'PICU', '05SUPED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 15:49:00', NULL, NULL, 'Ma Evelyn Barbarona'),
(50, 'Suction Machine', '1047403', 'Basic 30', NULL, '2020-06-17 07:53:09', '2020-06-17 08:22:04', NULL, NULL, 'Medela', NULL, '2020-06-17 15:52:00', '2020-06-17 15:52:00', '2003-06-01 16:00:00', 'Dispo Philippines Inc.', 150000, 'OR', '39SUOR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 15:52:00', NULL, NULL, 'Genis Marie Talatala'),
(51, 'Suction Machine', '1062915', 'Vario', NULL, '2020-06-17 07:56:53', '2020-06-17 08:23:39', NULL, NULL, 'Medela', NULL, '2020-06-17 15:56:00', '2020-06-17 15:56:00', '2003-07-29 15:54:00', 'Dispo Philippines Inc.', 50000, 'Private', '42SUPVT', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 15:56:00', NULL, NULL, 'Demetria Alago'),
(52, 'Suction Machine', '1060657', 'Basic 30', NULL, '2020-06-17 08:04:18', '2020-06-17 08:24:26', NULL, NULL, 'Medela', NULL, '2020-06-17 16:03:00', '2020-06-17 16:03:00', '2003-12-07 16:00:00', 'Dispo Philippines Inc.', 150000, 'Med. 2', '35SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:04:00', NULL, NULL, 'Ivy Fullido'),
(53, 'Suction Machine', '1076262', 'Basic 30', NULL, '2020-06-17 08:06:42', '2020-06-17 08:25:08', NULL, NULL, 'Medela', NULL, NULL, NULL, '2003-12-07 16:00:00', 'Dispo Philippines Inc.', 150000, 'MICU', '14SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:06:00', NULL, NULL, 'Loreta Brunidor'),
(54, 'Suction Machine', '1059926', 'Vario', NULL, '2020-06-17 08:10:51', '2020-06-17 08:26:10', NULL, NULL, 'Medela', NULL, NULL, NULL, '2003-12-10 16:00:00', 'Dispo Philippines Inc.', 50000, 'HDU', '29SUHDU', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:10:00', NULL, NULL, 'Anabelle Ceasar'),
(55, 'Suction Machine', '1058705', 'Vario', NULL, '2020-06-17 08:28:36', '2020-06-17 08:28:36', NULL, NULL, 'Medela', NULL, '2020-06-17 16:28:00', '2020-06-17 16:28:00', '2003-12-29 16:00:00', 'Dispo Philippines Inc.', 50000, 'HDU', '36SUHDU', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:28:00', NULL, NULL, 'Anabelle Ceasar'),
(56, '\"Suction Machine \"', '9021722', NULL, NULL, '2020-06-17 08:30:11', '2020-06-17 08:30:11', NULL, NULL, '\"Doctors  F.\"', NULL, '2020-06-17 16:29:00', '2020-06-17 16:29:00', '2006-01-25 16:00:00', 'Medical Center Trading Corp', 113000, '\"Surg. 1 \"', '33SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:30:00', NULL, NULL, 'Rosalina Salisid'),
(57, 'Thoracic Pump', '9021724', 'DF500', NULL, '2020-06-17 08:31:59', '2020-06-17 08:31:59', NULL, NULL, 'Doctors F.', NULL, '2020-06-17 16:31:00', '2020-06-17 16:31:00', '2008-08-28 16:00:00', 'Medical Center Trading Corp', 113000, 'Med. 2', '02THMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:31:00', NULL, NULL, 'Ivy Fullido'),
(58, 'Suction Machine', '19552-101-57', 'V7Plus AC', NULL, '2020-06-17 08:34:07', '2020-06-17 08:34:07', NULL, NULL, 'Hersill', NULL, '2020-06-17 16:33:00', '2020-06-17 16:33:00', '2009-04-23 16:00:00', 'Fernando Medical Interprises', 75000, 'Med. 2', '12SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:34:00', NULL, NULL, 'Ivy Fullido'),
(59, 'Suction Machine', '8600004', 'D-3000', NULL, '2020-06-17 08:36:00', '2020-06-17 08:36:00', NULL, NULL, '\"Blue  Cross\"', NULL, '2020-06-17 16:35:00', '2020-06-17 16:35:00', '2009-05-05 16:00:00', 'Berovan Marketing', 82000, 'ER', '20SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:35:00', NULL, NULL, 'Joseph Redulla'),
(60, 'Suction Machine', '953540001', 'D-3000', NULL, '2020-06-17 08:42:10', '2020-06-17 08:42:10', NULL, NULL, '\"Blue  Cross\"', NULL, '2020-06-17 16:41:00', '2020-06-17 16:41:00', '2009-12-28 16:00:00', 'Berovan Marketing', 75000, 'Med. 1', '10SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:42:00', NULL, NULL, 'Ivy Fullido'),
(61, 'Suction Machine', '953540008', 'D-3000', NULL, '2020-06-17 08:43:46', '2020-06-17 08:43:46', NULL, NULL, '\"Blue  Cross\"', NULL, '2020-06-17 16:43:00', '2020-06-17 16:43:00', '2010-04-06 16:00:00', 'Berovan Marketing', 78570, 'Med. 2', '13SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:43:00', NULL, NULL, 'Ivy Fullido'),
(62, 'Suction Machine', '9028798', 'DF-600', NULL, '2020-06-17 08:53:31', '2020-06-17 08:53:31', NULL, NULL, 'Doctors F.', NULL, '2020-06-17 16:53:00', '2020-06-17 16:53:00', '2011-05-05 16:00:00', 'Medical Center Trading Corp', 76800, 'DR', '07SUDR', NULL, '[]', NULL, NULL, NULL, NULL, 'DR', '2020-02-13 16:53:00', NULL, NULL, 'Rosa Secretaria'),
(63, 'Suction Machine', '100004', '2110/120', NULL, '2020-06-17 08:55:36', '2020-06-17 08:55:36', NULL, NULL, 'Sorensen', NULL, '2020-06-17 16:55:00', '2020-06-17 16:55:00', '2011-05-12 16:00:00', 'Technomed International Inc', 83900, 'Pvt. 1 & 2', '03SUPVT', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:55:00', NULL, NULL, 'Milagros Felias'),
(64, 'Suction Machine', '9035018', 'DF-650A', NULL, '2020-06-17 08:57:29', '2020-06-17 08:57:29', NULL, NULL, '\"Doctors  F.\"', NULL, '2020-06-17 16:57:00', '2020-06-17 16:57:00', '2013-07-17 16:00:00', 'Medical Center Trading Corp', 76000, 'Pvt. 1 & 2', '01SUPVT', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 16:57:00', NULL, NULL, 'Demetria Alago'),
(65, 'Suction Machine', '9035020', 'DF-650A', NULL, '2020-06-17 09:06:25', '2020-06-17 09:06:25', NULL, NULL, '\"Doctors  F.\"', NULL, '2020-06-17 17:06:00', '2020-06-17 17:06:00', '2013-07-17 16:00:00', 'Medical Center Trading Corp', 76000, 'Pvt. 1 & 2', '02SUPVT', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:06:00', NULL, NULL, 'Demetria Alago'),
(66, 'Suction Machine', '9035023', 'DF-650A', NULL, '2020-06-17 09:08:00', '2020-06-17 09:08:00', NULL, NULL, '\"Doctors  F.\"', NULL, '2020-06-17 17:07:00', '2020-06-17 17:07:00', '2013-09-26 16:00:00', 'Medical Center Trading Corp', 76000, 'NICU', '06SUNIC', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:07:00', NULL, NULL, 'Rosa Secretaria'),
(67, 'Suction Machine', '9033245', 'DF-760', NULL, '2020-06-17 09:09:36', '2020-06-17 09:09:36', NULL, NULL, 'Doctors F.', NULL, '2020-06-17 17:09:00', '2020-06-17 17:09:00', '2014-04-20 16:00:00', 'Medical Center Trading Corp', 76000, 'Surg. 2', '25SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:09:00', NULL, NULL, 'Rosalina Salisid'),
(68, 'Suction Machine', '9033244', 'DF-760', NULL, '2020-06-17 09:11:26', '2020-06-17 09:11:26', NULL, NULL, '\"Doctors  F.\"', NULL, '2020-06-17 17:11:00', '2020-06-17 17:11:00', '2014-04-20 16:00:00', 'Medical Center Trading Corp', 76000, 'SICU', '26SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:11:00', NULL, NULL, 'Rosalina Salisid'),
(69, 'Suction Machine', '9033246', 'DF-760', NULL, '2020-06-17 09:15:09', '2020-06-17 09:15:09', NULL, NULL, '\"Doctors  F.\"', NULL, '2020-06-17 17:14:00', '2020-06-17 17:14:00', '2014-04-20 16:00:00', 'Medical Center Trading Corp', 76000, 'Surg. 2', '27SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:15:00', NULL, NULL, 'Rosalina Salisid'),
(70, 'Suction Machine', '9033280', 'DF-760', NULL, '2020-06-17 09:24:23', '2020-06-17 09:24:23', NULL, NULL, '\"Doctors  F.\"', NULL, '2020-06-17 17:24:00', '2020-06-17 17:24:00', '2014-04-20 16:00:00', 'Medical Center Trading Corp', 76000, 'Surg. 2', '28SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:24:00', NULL, NULL, 'Rosalina Salisid'),
(71, 'Suction Machine', '14003', 'RSU 346', NULL, '2020-06-17 09:38:36', '2020-06-17 09:38:36', NULL, NULL, 'Rexmed', NULL, '2020-06-17 17:38:00', '2020-06-17 17:38:00', '2014-06-15 16:00:00', 'Blue Sky Trading Corp.', 75000, 'MICU', '23SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:38:00', NULL, NULL, 'Loreta Brunidor'),
(72, 'Suction Machine', '14004', 'RSU 346', NULL, '2020-06-17 09:40:13', '2020-06-17 09:40:13', NULL, NULL, 'Rexmed', NULL, '2020-06-17 17:39:00', '2020-06-17 17:39:00', '2014-06-15 16:00:00', 'Blue Sky Trading Corp.', 75000, 'MICU', '24SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:40:00', NULL, NULL, 'Loreta Brunidor'),
(73, 'Suction Machine', '14005', 'RSU-346', NULL, '2020-06-17 09:41:51', '2020-06-17 09:41:51', NULL, NULL, 'Rexmed', NULL, '2020-06-17 17:41:00', '2020-06-17 17:41:00', '2014-06-15 16:00:00', 'Blue Sky Trading Corp.', 75000, 'ER', '31SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:41:00', NULL, NULL, 'Joseph Wood Fullido'),
(74, 'Suction Machine', '14007', 'RSU-346', NULL, '2020-06-17 09:43:49', '2020-06-17 09:43:49', NULL, NULL, 'Rexmed', NULL, '2020-06-17 17:43:00', '2020-06-17 17:43:00', '2014-06-15 16:00:00', 'Blue Sky Trading Corp.', 75000, 'ER', '32SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'Non-Functional', '2020-02-13 17:43:00', NULL, NULL, 'Joseph Wood Fullido'),
(75, 'Suction Machine', 'S-7161', '--', NULL, '2020-06-17 09:58:00', '2020-06-17 09:58:00', NULL, NULL, 'Gomco', NULL, '2020-06-17 17:57:00', '2020-06-17 17:57:00', '2005-07-07 16:00:00', '--', 119000, 'OR', '22SUOR', NULL, '[]', NULL, NULL, NULL, NULL, '11/20/2019-not functional', '2020-02-13 17:57:00', NULL, NULL, 'Tita Roman'),
(76, 'Suction Machine', '9045454', 'DF-650A', NULL, '2020-06-17 10:00:04', '2020-06-17 10:00:04', NULL, NULL, 'Doctors F.', NULL, '2020-06-17 17:59:00', '2020-06-17 17:59:00', '2016-02-04 16:00:00', 'Endure Medical Inc.', 76984, 'OR', '44SUOR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 17:59:00', NULL, NULL, 'Araceli Mercado'),
(77, 'Suction Machine', '9045455', 'DF-650A', NULL, '2020-06-17 10:02:51', '2020-06-17 10:02:51', NULL, NULL, 'Doctors F.', NULL, '2020-06-17 18:02:00', '2020-06-17 18:02:00', '2016-05-04 16:00:00', 'Endure Medical Inc.', 76984, 'PICU', '40SUPED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 18:02:00', NULL, NULL, 'Alona Arao-Arao'),
(78, 'Suction Machine', '2016062000409', '--', NULL, '2020-06-17 10:04:46', '2020-06-17 10:04:46', NULL, NULL, 'Indoplas', NULL, '2020-06-17 18:04:00', '2020-06-17 18:04:00', '2016-09-19 16:00:00', 'Donated: East meets W', 16000, 'DR', '46SUDR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 18:04:00', NULL, NULL, 'Janet Marie Araneta'),
(79, 'Suction Machine', '201606202041020', '--', NULL, '2020-06-17 10:23:48', '2020-06-17 10:23:48', NULL, NULL, 'Indoplas', NULL, '2020-06-17 18:23:00', '2020-06-17 18:23:00', '2016-09-19 16:00:00', 'Donated: East meets W', 16000, 'OB-OR', '78SUOBOR', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-13 18:23:00', NULL, NULL, 'Janet Marie Araneta'),
(80, 'Suction Machine', '1502515', 'Basic 30', NULL, '2020-06-17 10:27:30', '2020-06-17 10:27:30', NULL, NULL, 'Medela', NULL, '2020-06-17 18:27:00', '2020-06-17 18:27:00', '2017-04-10 16:00:00', 'HealthSolution Inc.', 195000, 'Main OR', '47SUOR', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-13 18:27:00', NULL, NULL, 'Merlyn Pierras'),
(81, 'Suction Machine', '1501588', 'Basic 30', NULL, '2020-06-17 10:31:32', '2020-06-17 10:31:32', NULL, NULL, 'Medela', NULL, '2020-06-17 18:29:00', '2020-06-17 18:29:00', '2017-04-10 16:00:00', 'HealthSolution Inc.', 195000, 'Private', '70SUPVT', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-13 18:31:00', NULL, NULL, 'Ma. Theresa Genalde'),
(82, 'Suction Machine', '1502512', 'Basic 30', NULL, '2020-06-17 10:33:16', '2020-06-17 10:33:16', NULL, NULL, 'Medela', NULL, '2020-06-17 18:32:00', '2020-06-17 18:32:00', '2017-04-10 16:00:00', 'HealthSolution Inc.', 195000, '\"Surg. 1 \"', '49SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-13 18:33:00', NULL, NULL, 'Gem Branaldez'),
(83, 'Suction Machine', '1211080', '7E-A', NULL, '2020-06-17 10:44:41', '2020-06-17 10:44:41', NULL, NULL, 'Union', NULL, '2020-06-17 18:44:00', '2020-06-17 18:44:00', '2017-05-19 16:00:00', 'Donated: Bong Castro', 35000, 'MICU', '47SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-13 18:44:00', NULL, NULL, 'Emmanuel Chris Arcay'),
(84, 'Portable Suction Machine', 'DB1105', 'Dynamic II', NULL, '2020-06-17 11:06:00', '2020-06-17 11:06:00', NULL, NULL, 'Cheiron', NULL, '2020-06-17 19:05:00', '2020-06-17 19:05:00', '2017-06-18 16:00:00', 'Saviour MeDevices', 75800, 'Foton', '71SUAMB', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-13 19:05:00', NULL, NULL, 'John Melchor Namoc'),
(85, 'Suction Machine', '1661926', 'Basic 30', NULL, '2020-06-17 21:14:39', '2020-06-17 21:14:39', NULL, NULL, 'Medela', NULL, '2020-06-18 05:14:00', '2020-06-18 05:14:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'HDU', '56SUHDU', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 05:14:00', NULL, NULL, 'Cerlita Balo'),
(86, 'Suction Machine', '1661924', 'Basic 30', NULL, '2020-06-17 21:19:18', '2020-06-17 21:19:18', NULL, NULL, 'Medela', NULL, '2020-06-18 05:19:00', '2020-06-18 05:19:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'HDU', '55SUHDU', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 05:19:00', NULL, NULL, 'Cerlita Balo'),
(87, 'Suction Machine', '1663818', 'Basic 30', NULL, '2020-06-17 21:26:18', '2020-06-17 21:26:18', NULL, NULL, 'Medela', NULL, '2020-06-18 05:26:00', '2020-06-18 05:26:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'Pedia', '60SUPED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 05:26:00', NULL, NULL, 'Alona Arao-Arao'),
(88, 'Suction Machine', '1662391', 'Basic 30', NULL, '2020-06-17 21:29:40', '2020-06-17 21:29:40', NULL, NULL, 'Medela', NULL, '2020-06-18 05:29:00', '2020-06-18 05:29:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'ER', '53SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 05:29:00', NULL, NULL, 'Maria Liza Balonda'),
(89, 'Suction Machine', '1661923', 'Basic 30', NULL, '2020-06-17 21:32:09', '2020-06-17 21:32:09', NULL, NULL, 'Medela', NULL, '2020-06-18 05:31:00', '2020-06-18 05:31:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'ER', '52SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 05:32:00', NULL, NULL, 'Maria Liza Balonda'),
(90, 'Suction Machine', '1663814', 'Basic 30', NULL, '2020-06-17 21:34:37', '2020-06-17 21:34:37', NULL, NULL, 'Medela', NULL, '2020-06-18 05:34:00', '2020-06-18 05:34:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'OB', '54SUOB', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 05:34:00', NULL, NULL, 'Gloria Ancog'),
(91, 'Suction Machine', '1661922', 'Basic 30', NULL, '2020-06-17 21:42:37', '2020-06-17 21:42:37', NULL, NULL, 'Medela', NULL, '2020-06-18 05:42:00', '2020-06-18 05:42:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'Med. 1', '46SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 05:42:00', NULL, NULL, 'Maria Marcelis Andoy'),
(92, 'Suction Machine', '1662390', 'Basic 30', NULL, '2020-06-17 21:46:19', '2020-06-17 21:46:19', NULL, NULL, 'Medela', NULL, '2020-06-18 05:46:00', '2020-06-18 05:46:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'Med. 1', '65SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 05:46:00', NULL, NULL, 'Maria Marcelis Andoy'),
(93, 'Suction Machine', '1661919', 'Basic 30', NULL, '2020-06-17 21:49:12', '2020-06-17 21:49:12', NULL, NULL, 'Medela', NULL, '2020-06-18 05:48:00', '2020-06-18 05:48:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'Med. 2', '63SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 05:49:00', NULL, NULL, 'Hector Rodriguez'),
(94, 'Suction Machine', '1661925', 'Basic 30', NULL, '2020-06-17 21:52:05', '2020-06-17 21:52:05', NULL, NULL, 'Medela', NULL, '2020-06-18 05:51:00', '2020-06-18 05:51:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, 'Med. 2', '62SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 05:51:00', NULL, NULL, 'Hector Rodriguez'),
(95, NULL, '1662387', 'Basic 30', NULL, '2020-06-17 21:54:44', '2020-06-17 21:54:44', NULL, NULL, 'Medela', NULL, '2020-06-18 05:54:00', '2020-06-18 05:54:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, '\"Surg. 1 \"', '57SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 05:54:00', NULL, NULL, 'Joziane Jamila'),
(96, 'Suction Machine', '1661927', 'Basic 30', NULL, '2020-06-17 21:57:37', '2020-06-17 21:57:37', NULL, NULL, 'Medela', NULL, '2020-06-18 05:57:00', '2020-06-18 05:57:00', '2017-07-06 16:00:00', 'HealthSolution Inc.', 195000, '\"Surg. 1 \"', '58SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, '1/31/2020-not functional-defective plate holder', '2020-02-14 05:57:00', NULL, NULL, 'Joziane Jamila'),
(97, 'Suction Machine', '1663815', 'Basic 30', NULL, '2020-06-17 22:00:29', '2020-06-17 22:00:29', NULL, NULL, 'Medela', NULL, '2020-06-18 06:00:00', '2020-06-18 06:00:00', '2017-07-27 16:00:00', 'HealthSolution Inc.', 195000, 'Med. 1', '74SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 06:00:00', NULL, NULL, 'Maria Marcelis Andoy'),
(98, NULL, '1663817', 'Basic 30', NULL, '2020-06-17 22:03:09', '2020-06-17 22:03:09', NULL, NULL, 'Medela', NULL, '2020-06-18 06:02:00', '2020-06-18 06:02:00', '2017-07-27 16:00:00', 'HealthSolution Inc.', 195000, 'Med. 2', '66SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:03:00', NULL, NULL, 'Hector Rodriguez'),
(99, 'Suction Machine', '1663821', 'Basic 30', NULL, '2020-06-17 22:06:02', '2020-06-17 22:06:02', NULL, NULL, 'Medela', NULL, '2020-06-18 06:05:00', '2020-06-18 06:05:00', '2017-07-27 16:00:00', 'HealthSolution Inc.', 195000, 'MICU', '68SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:05:00', NULL, NULL, 'Milagros Felias'),
(100, 'Suction Machine', '1663816', 'Basic 30', NULL, '2020-06-17 22:11:34', '2020-06-17 22:11:34', NULL, NULL, 'Medela', NULL, '2020-06-18 06:11:00', '2020-06-18 06:11:00', '2017-07-27 16:00:00', 'HealthSolution Inc.', 195000, 'MICU', '76SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:11:00', NULL, NULL, 'Milagros Felias'),
(101, 'Suction Machine', '1663813', 'Basic 30', NULL, '2020-06-17 22:14:28', '2020-06-17 22:14:28', NULL, NULL, 'Medela', NULL, '2020-06-18 06:14:00', '2020-06-18 06:14:00', '2017-07-27 16:00:00', 'HealthSolution Inc.', 195000, 'Surg. 2', '83SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:14:00', NULL, NULL, 'Virginia Tan'),
(102, 'Suction Machine', '1663820', 'Basic 30', NULL, '2020-06-17 22:17:20', '2020-06-17 22:17:20', NULL, NULL, 'Medela', NULL, '2020-06-18 06:17:00', '2020-06-18 06:17:00', '2017-07-27 16:00:00', 'HealthSolution Inc.', 195000, 'Surg. 2', '84SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:17:00', NULL, NULL, 'Virginia Tan'),
(103, 'Suction Machine', 'DB1100', 'Dynamic II', NULL, '2020-06-17 22:22:03', '2020-06-17 22:22:03', NULL, NULL, 'Cheiron', NULL, '2020-06-18 06:21:00', '2020-06-18 06:21:00', '2017-08-10 16:00:00', 'Saviour MeDevices', 75800, 'NICU', '77SUNIC', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 06:21:00', NULL, NULL, 'Rosa Secretaria'),
(104, 'Suction Machine', 'DB1106', 'Dynamic II', NULL, '2020-06-17 22:24:56', '2020-06-17 22:24:56', NULL, NULL, 'Cheiron', NULL, '2020-06-18 06:24:00', '2020-06-18 06:24:00', '2017-08-10 16:00:00', 'Saviour MeDevices', 75800, 'ER', '79SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:24:00', NULL, NULL, 'Sarah Jane Ecat'),
(105, 'Suction Machine', 'DB1107', 'Dynamic II', NULL, '2020-06-17 22:27:47', '2020-06-17 22:27:47', NULL, NULL, 'Cheiron', NULL, '2020-06-18 06:27:00', '2020-06-18 06:27:00', '2017-08-10 16:00:00', 'Saviour MeDevices', 75800, 'ER', '80SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:27:00', NULL, NULL, 'Sarah Jane Ecat'),
(106, NULL, 'DB1108', 'Dynamic II', NULL, '2020-06-17 22:30:28', '2020-06-17 22:30:28', NULL, NULL, 'Cheiron', NULL, '2020-06-18 06:30:00', '2020-06-18 06:30:00', '2017-08-10 16:00:00', 'Saviour MeDevices', 75800, 'ER', '81SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:30:00', NULL, NULL, 'Sarah Jane Ecat'),
(107, 'Suction Machine', 'DB1109', 'Dynamic II', NULL, '2020-06-17 22:33:22', '2020-06-17 22:33:22', NULL, NULL, 'Cheiron', NULL, '2020-06-18 06:33:00', '2020-06-18 06:33:00', '2017-08-10 16:00:00', 'Saviour MeDevices', 75800, 'ER', '82SUER', NULL, '[]', NULL, NULL, NULL, NULL, 'New Under Warranty', '2020-02-14 06:33:00', NULL, NULL, 'Sarah Jane Ecat'),
(108, 'Suction Machine', 'DB1103', 'Dynamic II', NULL, '2020-06-17 22:36:14', '2020-06-17 22:36:14', NULL, NULL, 'Cheiron', NULL, '2020-06-18 06:35:00', '2020-06-18 06:35:00', '2017-08-10 16:00:00', 'Saviour MeDevices', 75800, 'Medical 2', '85SUMED', NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 06:36:00', NULL, NULL, 'Abegail Baliling'),
(109, NULL, '1735076', 'Vario 8', NULL, '2020-06-17 22:38:55', '2020-06-17 22:38:55', NULL, NULL, 'Medela', NULL, '2020-06-18 06:38:00', '2020-06-18 06:38:00', '2019-04-16 16:00:00', 'Luzon Quick', 374800, '\"Surg. 1 \"', '86SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Under Warranty', '2020-02-14 06:38:00', NULL, NULL, 'Richelle Decasa'),
(110, 'Thoracic Pump', '1735077', 'Vario 8', NULL, '2020-06-17 22:41:48', '2020-06-17 22:41:48', NULL, NULL, 'Medela', NULL, '2020-06-18 06:41:00', '2020-06-18 06:41:00', '2019-04-16 16:00:00', 'Luzon Quick', 374800, '\"Surg. 1 \"', '87SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Under Warranty', '2020-02-14 06:41:00', NULL, NULL, 'Ma. Marcelis Andoy'),
(111, 'Thoracic Pump', '1735074', 'Vario 8', NULL, '2020-06-17 22:44:40', '2020-06-17 22:44:40', NULL, NULL, 'Medela', NULL, '2020-06-18 06:44:00', '2020-06-18 06:44:00', '2019-04-16 16:00:00', 'Luzon Quick', 374800, 'Surg. 2', '88SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Under Warranty', '2020-02-14 06:44:00', NULL, NULL, 'Joanne Dela Torre'),
(112, NULL, '1735078', 'Vario 8', NULL, '2020-06-17 22:47:21', '2020-06-17 22:47:21', NULL, NULL, 'Medela', NULL, '2020-06-18 06:47:00', '2020-06-18 06:47:00', '2019-04-16 16:00:00', 'Luzon Quick', 374800, 'Surg. 2', '89SUSUR', NULL, '[]', NULL, NULL, NULL, NULL, 'Under Warranty', '2020-02-14 06:47:00', NULL, NULL, 'Ma Heidi Corbita'),
(113, 'Suction Machine', '1807022', 'SU-305', NULL, '2020-06-17 22:50:13', '2020-06-17 22:50:13', NULL, NULL, 'Gemmyco', NULL, '2020-06-18 06:49:00', '2020-06-18 06:49:00', '2019-09-24 16:00:00', 'Iraseth Pharma', 195000, 'OB-OR', '91SUOBOR', NULL, '[]', NULL, NULL, NULL, NULL, 'Under Warranty', '2020-02-14 06:50:00', NULL, NULL, 'Karen Supieza'),
(114, 'Suction Machine', '1906400', 'SU-305', NULL, '2020-06-17 22:53:06', '2020-06-17 22:53:06', NULL, NULL, 'Gemmyco', NULL, '2020-06-18 06:52:00', '2020-06-18 06:52:00', '2019-09-24 16:00:00', 'Iraseth Pharma', 195000, 'OB-OR', '90SUOBOR', NULL, '[]', NULL, NULL, NULL, NULL, 'Under Warranty', '2020-02-14 06:52:00', NULL, NULL, 'Karen Supieza'),
(115, 'Suction Machine', 'SS 975611', 'C55F', NULL, '2020-06-18 04:21:49', '2020-06-18 04:21:49', NULL, NULL, 'Medica', NULL, '2020-06-18 12:21:00', '2020-06-18 12:21:00', '2019-10-15 16:00:00', 'Mular Medical', 238000, 'Main OR', NULL, NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 12:21:00', NULL, NULL, 'Angeli Apayor'),
(116, 'Suction Machine', 'SS 975603', 'C55F', NULL, '2020-06-18 04:24:48', '2020-06-18 04:24:48', NULL, NULL, 'Medica', NULL, '2020-06-18 12:24:00', '2020-06-18 12:24:00', '2019-10-15 16:00:00', 'Mular Medical', 238000, 'Main OR', NULL, NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 12:24:00', NULL, NULL, 'Angeli Apayor'),
(117, 'Suction Machine', 'SS 975561', 'C55F', NULL, '2020-06-18 04:27:42', '2020-06-18 04:27:42', NULL, NULL, 'Medica', NULL, '2020-06-18 12:27:00', '2020-06-18 12:27:00', '2019-10-15 16:00:00', 'Mular Medical', 238000, 'Main OR', NULL, NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 12:27:00', NULL, NULL, 'Glecello Marzo Policianos'),
(118, 'Suction Machine', 'SS 975602', 'C55F', NULL, '2020-06-18 04:31:02', '2020-06-18 04:31:02', NULL, NULL, 'Medica', NULL, '2020-06-18 12:30:00', '2020-06-18 12:30:00', '2019-10-15 16:00:00', 'Mular Medical', 238000, 'Main OR', NULL, NULL, '[]', NULL, NULL, NULL, NULL, 'Functional', '2020-02-14 12:30:00', NULL, NULL, 'Glecello Marzo Policianos');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log_activities`
--

CREATE TABLE `log_activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `logs` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `method` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `log_activities`
--

INSERT INTO `log_activities` (`id`, `logs`, `url`, `method`, `ip`, `agent`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'My Testing Add To Log.', 'http://localhost:8000/add-to-log', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-07 05:10:03', '2020-06-07 05:10:03'),
(2, 'My Testing Add To Log.', 'http://localhost:8000/admin/add-to-log', 'GET', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-07 05:47:36', '2020-06-07 05:47:36');

-- --------------------------------------------------------

--
-- Table structure for table `medicine_names`
--

CREATE TABLE `medicine_names` (
  `id` int(10) UNSIGNED NOT NULL,
  `medicine_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2020-06-07 02:36:15', '2020-06-07 02:36:15');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2020-06-07 02:36:15', '2020-06-07 02:36:15', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 7, '2020-06-07 02:36:15', '2020-06-16 08:18:06', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 5, '2020-06-07 02:36:15', '2020-06-09 20:26:02', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 4, '2020-06-07 02:36:15', '2020-06-09 20:26:02', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2020-06-07 02:36:15', '2020-06-16 08:18:06', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2020-06-07 02:36:15', '2020-06-16 08:18:06', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2020-06-07 02:36:15', '2020-06-16 08:18:06', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2020-06-07 02:36:15', '2020-06-16 08:18:06', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2020-06-07 02:36:15', '2020-06-16 08:18:06', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 9, '2020-06-07 02:36:15', '2020-06-16 08:18:06', 'voyager.settings.index', NULL),
(11, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2020-06-07 02:36:32', '2020-06-16 08:18:06', 'voyager.hooks', NULL),
(12, 1, 'Add / Update', '', '_self', 'voyager-edit', '#000000', 20, 2, '2020-06-07 02:40:45', '2020-06-09 21:30:28', 'voyager.purchase-orders.index', 'null'),
(13, 1, 'Add / Update', '', '_self', 'voyager-edit', '#000000', 17, 2, '2020-06-07 04:34:59', '2020-06-09 21:30:19', 'voyager.equipment.index', 'null'),
(16, 1, 'Activity Logs', '', '_self', 'voyager-logbook', '#000000', NULL, 6, '2020-06-08 06:38:20', '2020-06-16 08:18:05', 'voyager.model_log.index', 'null'),
(17, 1, 'Equipmment', '#', '_self', 'voyager-polaroid', '#000000', NULL, 2, '2020-06-09 19:21:30', '2020-06-09 21:29:20', NULL, ''),
(19, 1, 'Reports', 'admin/equipment/reports', '_self', 'voyager-pie-graph', '#000000', 17, 1, '2020-06-09 19:55:16', '2020-06-09 21:29:45', NULL, ''),
(20, 1, 'Purchase Order', '#', '_self', 'voyager-documentation', '#000000', NULL, 3, '2020-06-09 20:25:50', '2020-06-09 22:05:11', NULL, ''),
(21, 1, 'Reports', 'admin/purchase-orders/reports', '_self', 'voyager-pie-graph', '#000000', 20, 1, '2020-06-09 20:27:15', '2020-06-09 21:29:58', NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(37, '2014_10_12_000000_create_users_table', 1),
(38, '2016_01_01_000000_add_voyager_user_fields', 1),
(39, '2016_01_01_000000_create_data_types_table', 1),
(40, '2016_01_01_000000_create_pages_table', 1),
(41, '2016_01_01_000000_create_posts_table', 1),
(42, '2016_02_15_204651_create_categories_table', 1),
(43, '2016_05_19_173453_create_menu_table', 1),
(44, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(45, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(46, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(47, '2016_06_01_000004_create_oauth_clients_table', 1),
(48, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(49, '2016_10_21_190000_create_roles_table', 1),
(50, '2016_10_21_190000_create_settings_table', 1),
(51, '2016_11_30_135954_create_permission_table', 1),
(52, '2016_11_30_141208_create_permission_role_table', 1),
(53, '2016_12_26_201236_data_types__add__server_side', 1),
(54, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(55, '2017_01_14_005015_create_translations_table', 1),
(56, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(57, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(58, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(59, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(60, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(61, '2017_08_05_000000_add_group_to_settings_table', 1),
(62, '2017_11_26_013050_add_user_role_relationship', 1),
(63, '2017_11_26_015000_create_user_roles_table', 1),
(64, '2018_03_11_000000_add_user_settings', 1),
(65, '2018_03_14_000000_add_details_to_data_types_table', 1),
(66, '2018_03_16_000000_make_settings_value_nullable', 1),
(67, '2019_08_19_000000_create_failed_jobs_table', 1),
(68, '2020_06_07_080301_create_purchase_order_activity_table', 2),
(69, '2020_06_07_104451_create_categories_table', 0),
(70, '2020_06_07_104451_create_countries_table', 0),
(71, '2020_06_07_104451_create_data_rows_table', 0),
(72, '2020_06_07_104451_create_data_types_table', 0),
(73, '2020_06_07_104451_create_equipment_backup_table', 0),
(74, '2020_06_07_104451_create_failed_jobs_table', 0),
(75, '2020_06_07_104451_create_menu_items_table', 0),
(76, '2020_06_07_104451_create_menus_table', 0),
(77, '2020_06_07_104451_create_oauth_access_tokens_table', 0),
(78, '2020_06_07_104451_create_oauth_auth_codes_table', 0),
(79, '2020_06_07_104451_create_oauth_clients_table', 0),
(80, '2020_06_07_104451_create_oauth_personal_access_clients_table', 0),
(81, '2020_06_07_104451_create_oauth_refresh_tokens_table', 0),
(82, '2020_06_07_104451_create_pages_table', 0),
(83, '2020_06_07_104451_create_permission_role_table', 0),
(84, '2020_06_07_104451_create_permissions_table', 0),
(85, '2020_06_07_104451_create_posts_table', 0),
(86, '2020_06_07_104451_create_purchase_order_activity_table', 0),
(87, '2020_06_07_104451_create_purchase_orders_table', 0),
(88, '2020_06_07_104451_create_roles_table', 0),
(89, '2020_06_07_104451_create_settings_table', 0),
(90, '2020_06_07_104451_create_translations_table', 0),
(91, '2020_06_07_104451_create_user_roles_table', 0),
(92, '2020_06_07_104451_create_users_table', 0),
(93, '2020_06_07_104456_add_foreign_keys_to_categories_table', 0),
(94, '2020_06_07_104456_add_foreign_keys_to_data_rows_table', 0),
(95, '2020_06_07_104456_add_foreign_keys_to_menu_items_table', 0),
(96, '2020_06_07_104456_add_foreign_keys_to_permission_role_table', 0),
(97, '2020_06_07_104456_add_foreign_keys_to_user_roles_table', 0),
(98, '2020_06_07_104456_add_foreign_keys_to_users_table', 0),
(99, '2020_06_07_104956_create_equipment_table', 0),
(100, '2020_06_07_162932_create_activity_log_table', 3),
(101, '2020_06_07_174734_create_activities_table', 4);

-- --------------------------------------------------------

--
-- Table structure for table `model_log`
--

CREATE TABLE `model_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `row_id` bigint(20) UNSIGNED DEFAULT NULL,
  `event` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `before` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `after` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_log`
--

INSERT INTO `model_log` (`id`, `table_name`, `row_id`, `event`, `before`, `after`, `ip_address`, `user_agent`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:24:46', '2020-06-08 07:24:46'),
(2, 'equipment', NULL, 'deleted', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:26:18', '2020-06-08 07:26:18'),
(3, 'equipment', NULL, 'deleted', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:26:19', '2020-06-08 07:26:19'),
(4, 'equipment', NULL, 'deleted', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:26:19', '2020-06-08 07:26:19'),
(5, 'equipment', NULL, 'deleted', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:26:19', '2020-06-08 07:26:19'),
(6, 'equipment', NULL, 'deleted', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:26:19', '2020-06-08 07:26:19'),
(7, 'equipment', NULL, 'deleted', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:27:24', '2020-06-08 07:27:24'),
(8, 'equipment', NULL, 'updated', '{\"equipment_name\":\"UpdateNames\",\"created_at\":\"\\\"2020-06-03T06:03:39.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-08T14:52:47.000000Z\\\"\",\"equipment_date_inspected\":\"2020-06-01 23:17:00\"}', '{\"equipment_name\":\"UpdateNamed\",\"created_at\":\"2020-06-03T06:03:39.000000Z\",\"updated_at\":\"2020-06-08T14:52:47.000000Z\",\"equipment_date_inspected\":\"\\\"2020-06-01T23:17:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:29:21', '2020-06-08 07:29:21'),
(9, 'equipment', NULL, 'deleted', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 07:50:30', '2020-06-08 07:50:30'),
(10, 'equipment', NULL, 'deleted', '{\"created_at\":\"\\\"2020-06-03T06:03:39.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-08T15:29:21.000000Z\\\"\"}', '{\"created_at\":\"2020-06-03T06:03:39.000000Z\",\"updated_at\":\"2020-06-08T15:29:21.000000Z\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 08:04:53', '2020-06-08 08:04:53'),
(11, 'equipment', NULL, 'deleted', '{\"created_at\":\"\\\"2020-06-07T13:36:27.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-07T13:36:27.000000Z\\\"\"}', '{\"created_at\":\"2020-06-07T13:36:27.000000Z\",\"updated_at\":\"2020-06-07T13:36:27.000000Z\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-08 08:09:19', '2020-06-08 08:09:19'),
(12, 'equipment', NULL, 'deleted', '{\"created_at\":\"\\\"2020-06-08T05:29:22.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-10T05:27:02.000000Z\\\"\",\"deleted_at\":\"\\\"2020-06-10T05:27:02.000000Z\\\"\"}', '{\"created_at\":\"2020-06-08T05:29:22.000000Z\",\"updated_at\":\"2020-06-10T05:27:02.000000Z\",\"deleted_at\":\"2020-06-10T05:27:02.000000Z\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-09 21:27:02', '2020-06-09 21:27:02'),
(13, 'equipment', NULL, 'deleted', '{\"created_at\":\"\\\"2020-06-08T05:46:45.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-10T05:27:02.000000Z\\\"\",\"deleted_at\":\"\\\"2020-06-10T05:27:02.000000Z\\\"\"}', '{\"created_at\":\"2020-06-08T05:46:45.000000Z\",\"updated_at\":\"2020-06-10T05:27:02.000000Z\",\"deleted_at\":\"2020-06-10T05:27:02.000000Z\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-09 21:27:02', '2020-06-09 21:27:02'),
(14, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-10 06:20:53', '2020-06-10 06:20:53'),
(15, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-10T14:20:53.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-10T14:20:53.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-02 22:19:00\",\"equipment_date_inspected\":\"2020-06-03 22:19:00\",\"equipment_date_acquired\":\"2020-06-07 22:20:00\",\"equipment_location\":\"Information Booth\"}', '{\"created_at\":\"2020-06-10T14:20:53.000000Z\",\"updated_at\":\"2020-06-10T14:20:53.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-02T22:19:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-03T22:19:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2020-06-07T22:20:00.000000Z\\\"\",\"equipment_location\":\"Guard House\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-10 06:44:11', '2020-06-10 06:44:11'),
(16, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-10T14:20:53.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-10T14:44:11.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-02 22:19:00\",\"equipment_date_inspected\":\"2020-06-03 22:19:00\",\"equipment_date_acquired\":\"2020-06-07 22:20:00\",\"equipment_memorandum_receipt\":null}', '{\"created_at\":\"2020-06-10T14:20:53.000000Z\",\"updated_at\":\"2020-06-10T14:44:11.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-02T22:19:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-03T22:19:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2020-06-07T22:20:00.000000Z\\\"\",\"equipment_memorandum_receipt\":\"[{\\\"download_link\\\":\\\"equipment\\\\\\\\June2020\\\\\\\\D8MHGWQjQ8ehItILNLS6.png\\\",\\\"original_name\\\":\\\"medicines.png\\\"}]\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-10 08:16:54', '2020-06-10 08:16:54'),
(17, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 04:52:08', '2020-06-17 04:52:08'),
(18, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 04:56:04', '2020-06-17 04:56:04'),
(19, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 04:59:00', '2020-06-17 04:59:00'),
(20, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:03:20', '2020-06-17 05:03:20'),
(21, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:16:14', '2020-06-17 05:16:14'),
(22, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:17:38', '2020-06-17 05:17:38'),
(23, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:20:04', '2020-06-17 05:20:04'),
(24, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:23:49', '2020-06-17 05:23:49'),
(25, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:31:50', '2020-06-17 05:31:50'),
(26, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:45:26', '2020-06-17 05:45:26'),
(27, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:57:01', '2020-06-17 05:57:01'),
(28, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T13:57:00.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T13:57:00.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 21:48:00\",\"equipment_date_inspected\":\"2020-06-17 21:48:00\",\"equipment_date_acquired\":\"2020-06-17 21:47:00\",\"equipment_date_of_remarks\":null}', '{\"created_at\":\"2020-06-17T13:57:00.000000Z\",\"updated_at\":\"2020-06-17T13:57:00.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T21:48:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T21:48:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2020-06-17T21:47:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T21:57:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 05:57:42', '2020-06-17 05:57:42'),
(29, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:03:36', '2020-06-17 06:03:36'),
(30, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T13:57:00.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T13:57:42.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 21:48:00\",\"equipment_date_inspected\":\"2020-06-17 21:48:00\",\"equipment_date_acquired\":\"2020-06-17 21:47:00\",\"equipment_date_of_remarks\":\"2020-02-14 21:57:00\"}', '{\"created_at\":\"2020-06-17T13:57:00.000000Z\",\"updated_at\":\"2020-06-17T13:57:42.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T21:48:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T21:48:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2015-07-31T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T21:57:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:04:36', '2020-06-17 06:04:36'),
(31, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T13:57:00.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T14:04:36.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 21:48:00\",\"equipment_date_inspected\":\"2020-06-17 21:48:00\",\"equipment_date_acquired\":\"2015-07-31 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 21:57:00\"}', '{\"created_at\":\"2020-06-17T13:57:00.000000Z\",\"updated_at\":\"2020-06-17T14:04:36.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T21:48:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T21:48:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2017-02-09T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T21:57:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:05:26', '2020-06-17 06:05:26'),
(32, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T14:03:36.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T14:03:36.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 22:01:00\",\"equipment_date_inspected\":\"2020-06-17 22:01:00\",\"equipment_date_acquired\":\"2020-06-17 22:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 22:03:00\"}', '{\"created_at\":\"2020-06-17T14:03:36.000000Z\",\"updated_at\":\"2020-06-17T14:03:36.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T22:01:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T22:01:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2015-07-31T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T22:03:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:05:51', '2020-06-17 06:05:51'),
(33, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T14:03:36.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T14:05:52.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 22:01:00\",\"equipment_date_inspected\":\"2020-06-17 22:01:00\",\"equipment_date_acquired\":\"2015-07-31 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 22:03:00\"}', '{\"created_at\":\"2020-06-17T14:03:36.000000Z\",\"updated_at\":\"2020-06-17T14:05:52.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T22:01:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T22:01:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2017-05-10T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T22:03:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:09:08', '2020-06-17 06:09:08'),
(34, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:21:49', '2020-06-17 06:21:49'),
(35, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:25:50', '2020-06-17 06:25:50'),
(36, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:32:01', '2020-06-17 06:32:01'),
(37, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:35:45', '2020-06-17 06:35:45'),
(38, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:39:30', '2020-06-17 06:39:30'),
(39, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:43:15', '2020-06-17 06:43:15'),
(40, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:47:27', '2020-06-17 06:47:27'),
(41, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:51:11', '2020-06-17 06:51:11'),
(42, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:54:55', '2020-06-17 06:54:55'),
(43, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 06:58:39', '2020-06-17 06:58:39'),
(44, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:07:42', '2020-06-17 07:07:42'),
(45, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:11:25', '2020-06-17 07:11:25'),
(46, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:18:53', '2020-06-17 07:18:53'),
(47, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:41:45', '2020-06-17 07:41:45'),
(48, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:45:42', '2020-06-17 07:45:42'),
(49, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:49:26', '2020-06-17 07:49:26'),
(50, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:53:09', '2020-06-17 07:53:09'),
(51, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 07:56:53', '2020-06-17 07:56:53'),
(52, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:04:18', '2020-06-17 08:04:18'),
(53, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:06:42', '2020-06-17 08:06:42'),
(54, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:10:51', '2020-06-17 08:10:51'),
(55, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T15:41:45.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T15:41:45.000000Z\\\"\",\"equipment_date_of_remarks\":\"2020-02-14 23:41:00\"}', '{\"created_at\":\"2020-06-17T15:41:45.000000Z\",\"updated_at\":\"2020-06-17T15:41:45.000000Z\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T23:41:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:19:21', '2020-06-17 08:19:21'),
(56, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T15:45:42.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T15:45:42.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 23:44:00\",\"equipment_date_inspected\":\"2020-06-17 23:44:00\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 23:45:00\"}', '{\"created_at\":\"2020-06-17T15:45:42.000000Z\",\"updated_at\":\"2020-06-17T15:45:42.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T23:44:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T23:44:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2003-06-02T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T23:45:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:20:06', '2020-06-17 08:20:06'),
(57, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T15:49:26.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T15:49:26.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 23:48:00\",\"equipment_date_inspected\":\"2020-06-17 23:48:00\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 23:49:00\"}', '{\"created_at\":\"2020-06-17T15:49:26.000000Z\",\"updated_at\":\"2020-06-17T15:49:26.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T23:48:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T23:48:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2003-06-02T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T23:49:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:20:45', '2020-06-17 08:20:45'),
(58, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T15:53:09.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T15:53:09.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 23:52:00\",\"equipment_date_inspected\":\"2020-06-17 23:52:00\",\"equipment_date_acquired\":\"2003-02-20 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 23:52:00\"}', '{\"created_at\":\"2020-06-17T15:53:09.000000Z\",\"updated_at\":\"2020-06-17T15:53:09.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T23:52:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T23:52:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2003-06-02T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T23:52:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:22:04', '2020-06-17 08:22:04'),
(59, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T15:56:53.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T15:56:53.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 23:56:00\",\"equipment_date_inspected\":\"2020-06-17 23:56:00\",\"equipment_date_acquired\":\"2020-06-17 23:54:00\",\"equipment_date_of_remarks\":\"2020-02-14 23:56:00\"}', '{\"created_at\":\"2020-06-17T15:56:53.000000Z\",\"updated_at\":\"2020-06-17T15:56:53.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T23:56:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T23:56:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2003-12-11T23:54:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T23:56:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:23:00', '2020-06-17 08:23:00'),
(60, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T15:56:53.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T16:23:00.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-17 23:56:00\",\"equipment_date_inspected\":\"2020-06-17 23:56:00\",\"equipment_date_acquired\":\"2003-12-11 23:54:00\",\"equipment_date_of_remarks\":\"2020-02-14 23:56:00\"}', '{\"created_at\":\"2020-06-17T15:56:53.000000Z\",\"updated_at\":\"2020-06-17T16:23:00.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-17T23:56:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-17T23:56:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2003-07-29T23:54:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T23:56:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:23:39', '2020-06-17 08:23:39'),
(61, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T16:04:18.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T16:04:18.000000Z\\\"\",\"equipment_date_delivered\":\"2020-06-18 00:03:00\",\"equipment_date_inspected\":\"2020-06-18 00:03:00\",\"equipment_date_acquired\":\"2003-08-20 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 00:04:00\"}', '{\"created_at\":\"2020-06-17T16:04:18.000000Z\",\"updated_at\":\"2020-06-17T16:04:18.000000Z\",\"equipment_date_delivered\":\"\\\"2020-06-18T00:03:00.000000Z\\\"\",\"equipment_date_inspected\":\"\\\"2020-06-18T00:03:00.000000Z\\\"\",\"equipment_date_acquired\":\"\\\"2003-12-08T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T00:04:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:24:26', '2020-06-17 08:24:26'),
(62, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T16:06:42.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T16:06:42.000000Z\\\"\",\"equipment_date_acquired\":\"2003-08-20 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 00:06:00\"}', '{\"created_at\":\"2020-06-17T16:06:42.000000Z\",\"updated_at\":\"2020-06-17T16:06:42.000000Z\",\"equipment_date_acquired\":\"\\\"2003-12-08T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T00:06:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:25:08', '2020-06-17 08:25:08'),
(63, 'equipment', NULL, 'updated', '{\"created_at\":\"\\\"2020-06-17T16:10:51.000000Z\\\"\",\"updated_at\":\"\\\"2020-06-17T16:10:51.000000Z\\\"\",\"equipment_date_acquired\":\"2003-11-20 00:00:00\",\"equipment_date_of_remarks\":\"2020-02-14 00:10:00\"}', '{\"created_at\":\"2020-06-17T16:10:51.000000Z\",\"updated_at\":\"2020-06-17T16:10:51.000000Z\",\"equipment_date_acquired\":\"\\\"2003-12-11T00:00:00.000000Z\\\"\",\"equipment_date_of_remarks\":\"\\\"2020-02-14T00:10:00.000000Z\\\"\"}', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:26:10', '2020-06-17 08:26:10'),
(64, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:28:36', '2020-06-17 08:28:36'),
(65, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:30:11', '2020-06-17 08:30:11'),
(66, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:31:59', '2020-06-17 08:31:59'),
(67, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:34:07', '2020-06-17 08:34:07'),
(68, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:36:00', '2020-06-17 08:36:00'),
(69, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:42:10', '2020-06-17 08:42:10'),
(70, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:43:47', '2020-06-17 08:43:47'),
(71, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:53:31', '2020-06-17 08:53:31'),
(72, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:55:36', '2020-06-17 08:55:36'),
(73, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 08:57:29', '2020-06-17 08:57:29'),
(74, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:06:25', '2020-06-17 09:06:25'),
(75, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:08:00', '2020-06-17 09:08:00'),
(76, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:09:36', '2020-06-17 09:09:36'),
(77, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:11:26', '2020-06-17 09:11:26'),
(78, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:15:09', '2020-06-17 09:15:09'),
(79, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:24:23', '2020-06-17 09:24:23'),
(80, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:38:36', '2020-06-17 09:38:36'),
(81, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:40:13', '2020-06-17 09:40:13'),
(82, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:41:52', '2020-06-17 09:41:52'),
(83, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:43:49', '2020-06-17 09:43:49'),
(84, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 09:58:01', '2020-06-17 09:58:01'),
(85, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:00:04', '2020-06-17 10:00:04'),
(86, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:02:51', '2020-06-17 10:02:51'),
(87, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:04:47', '2020-06-17 10:04:47'),
(88, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:23:48', '2020-06-17 10:23:48'),
(89, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:27:30', '2020-06-17 10:27:30'),
(90, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:31:32', '2020-06-17 10:31:32'),
(91, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:33:16', '2020-06-17 10:33:16'),
(92, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 10:44:41', '2020-06-17 10:44:41'),
(93, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 11:06:00', '2020-06-17 11:06:00'),
(94, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:14:40', '2020-06-17 21:14:40'),
(95, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:19:18', '2020-06-17 21:19:18'),
(96, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:26:18', '2020-06-17 21:26:18'),
(97, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:29:40', '2020-06-17 21:29:40'),
(98, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:32:09', '2020-06-17 21:32:09'),
(99, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:34:37', '2020-06-17 21:34:37'),
(100, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:42:37', '2020-06-17 21:42:37'),
(101, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:46:19', '2020-06-17 21:46:19'),
(102, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:49:12', '2020-06-17 21:49:12'),
(103, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:52:05', '2020-06-17 21:52:05'),
(104, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:54:44', '2020-06-17 21:54:44'),
(105, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 21:57:37', '2020-06-17 21:57:37'),
(106, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:00:29', '2020-06-17 22:00:29'),
(107, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:03:09', '2020-06-17 22:03:09'),
(108, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:06:02', '2020-06-17 22:06:02'),
(109, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:11:34', '2020-06-17 22:11:34'),
(110, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:14:29', '2020-06-17 22:14:29'),
(111, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:17:20', '2020-06-17 22:17:20'),
(112, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:22:03', '2020-06-17 22:22:03'),
(113, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:24:56', '2020-06-17 22:24:56'),
(114, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:27:47', '2020-06-17 22:27:47'),
(115, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:30:28', '2020-06-17 22:30:28'),
(116, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:33:22', '2020-06-17 22:33:22'),
(117, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:36:14', '2020-06-17 22:36:14'),
(118, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:38:55', '2020-06-17 22:38:55'),
(119, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:41:48', '2020-06-17 22:41:48'),
(120, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:44:40', '2020-06-17 22:44:40'),
(121, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:47:21', '2020-06-17 22:47:21'),
(122, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:50:14', '2020-06-17 22:50:14'),
(123, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-17 22:53:06', '2020-06-17 22:53:06'),
(124, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-18 04:21:50', '2020-06-18 04:21:50'),
(125, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-18 04:24:48', '2020-06-18 04:24:48'),
(126, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-18 04:27:42', '2020-06-18 04:27:42'),
(127, 'equipment', NULL, 'created', NULL, NULL, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.97 Safari/537.36', 1, '2020-06-18 04:31:02', '2020-06-18 04:31:02');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(2, 'browse_bread', NULL, '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(3, 'browse_database', NULL, '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(4, 'browse_media', NULL, '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(5, 'browse_compass', NULL, '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(6, 'browse_menus', 'menus', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(7, 'read_menus', 'menus', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(8, 'edit_menus', 'menus', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(9, 'add_menus', 'menus', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(10, 'delete_menus', 'menus', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(11, 'browse_roles', 'roles', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(12, 'read_roles', 'roles', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(13, 'edit_roles', 'roles', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(14, 'add_roles', 'roles', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(15, 'delete_roles', 'roles', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(16, 'browse_users', 'users', '2020-06-07 02:36:16', '2020-06-07 02:36:16'),
(17, 'read_users', 'users', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(18, 'edit_users', 'users', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(19, 'add_users', 'users', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(20, 'delete_users', 'users', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(21, 'browse_settings', 'settings', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(22, 'read_settings', 'settings', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(23, 'edit_settings', 'settings', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(24, 'add_settings', 'settings', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(25, 'delete_settings', 'settings', '2020-06-07 02:36:17', '2020-06-07 02:36:17'),
(26, 'browse_hooks', NULL, '2020-06-07 02:36:32', '2020-06-07 02:36:32'),
(27, 'browse_purchase_orders', 'purchase_orders', '2020-06-07 02:40:45', '2020-06-07 02:40:45'),
(28, 'read_purchase_orders', 'purchase_orders', '2020-06-07 02:40:45', '2020-06-07 02:40:45'),
(29, 'edit_purchase_orders', 'purchase_orders', '2020-06-07 02:40:45', '2020-06-07 02:40:45'),
(30, 'add_purchase_orders', 'purchase_orders', '2020-06-07 02:40:45', '2020-06-07 02:40:45'),
(31, 'delete_purchase_orders', 'purchase_orders', '2020-06-07 02:40:45', '2020-06-07 02:40:45'),
(32, 'browse_equipment', 'equipment', '2020-06-07 04:34:59', '2020-06-07 04:34:59'),
(33, 'read_equipment', 'equipment', '2020-06-07 04:34:59', '2020-06-07 04:34:59'),
(34, 'edit_equipment', 'equipment', '2020-06-07 04:34:59', '2020-06-07 04:34:59'),
(35, 'add_equipment', 'equipment', '2020-06-07 04:34:59', '2020-06-07 04:34:59'),
(36, 'delete_equipment', 'equipment', '2020-06-07 04:34:59', '2020-06-07 04:34:59'),
(37, 'browse_activity_log', 'activity_log', '2020-06-07 23:41:01', '2020-06-07 23:41:01'),
(38, 'read_activity_log', 'activity_log', '2020-06-07 23:41:01', '2020-06-07 23:41:01'),
(39, 'edit_activity_log', 'activity_log', '2020-06-07 23:41:01', '2020-06-07 23:41:01'),
(40, 'add_activity_log', 'activity_log', '2020-06-07 23:41:01', '2020-06-07 23:41:01'),
(41, 'delete_activity_log', 'activity_log', '2020-06-07 23:41:01', '2020-06-07 23:41:01'),
(42, 'browse_model_log', 'model_log', '2020-06-08 06:38:20', '2020-06-08 06:38:20'),
(43, 'clear_model_log', 'model_log', '2020-06-08 06:38:21', '2020-06-08 06:38:21'),
(44, 'browse_medicine_name', 'medicine_name', '2020-07-12 22:09:33', '2020-07-12 22:09:33'),
(45, 'read_medicine_name', 'medicine_name', '2020-07-12 22:09:33', '2020-07-12 22:09:33'),
(46, 'edit_medicine_name', 'medicine_name', '2020-07-12 22:09:33', '2020-07-12 22:09:33'),
(47, 'add_medicine_name', 'medicine_name', '2020-07-12 22:09:33', '2020-07-12 22:09:33'),
(48, 'delete_medicine_name', 'medicine_name', '2020-07-12 22:09:33', '2020-07-12 22:09:33');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(27, 2),
(28, 1),
(28, 2),
(29, 1),
(29, 2),
(30, 1),
(30, 2),
(31, 1),
(31, 2),
(32, 1),
(32, 2),
(33, 1),
(33, 2),
(34, 1),
(34, 2),
(35, 1),
(35, 2),
(36, 1),
(36, 2),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_orders`
--

CREATE TABLE `purchase_orders` (
  `purchase_order_key` int(10) UNSIGNED NOT NULL,
  `purchase_order_number` int(11) DEFAULT NULL,
  `purchase_order_pr_number` int(11) DEFAULT NULL,
  `purchase_order_mode_of_procurement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `purchase_order_date_awarded` timestamp NULL DEFAULT NULL,
  `purchase_order_date_of_po_received` timestamp NULL DEFAULT NULL,
  `purchase_order_date_delivered` timestamp NULL DEFAULT NULL,
  `purchase_order_inspected` timestamp NULL DEFAULT NULL,
  `purchase_order_date_acquired` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2020-06-07 02:35:11', '2020-06-07 02:35:11'),
(2, 'user', 'Normal User', '2020-06-07 02:36:16', '2020-06-07 02:36:16');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'GCGMH', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', NULL, '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings\\June2020\\LOXeeZhcnHbOAZ95johr.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', 'settings\\June2020\\h5facbgwUQqJE68GjSMQ.jpg', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'GCGMH', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome To GCGMH', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings\\June2020\\Afad6hPiHdAtuYZnsu9t.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'admin.admin_loader', 'Admin Loader', 'settings\\June2020\\G6PCN5rCMeJmp6sTuUmg.gif', NULL, 'image', 6, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'admin', 'admin@admin.com', 'users/default.png', NULL, '$2y$10$lMaBRTH.Y9Hz2gx0mh18TuO40E0QFK/JeZI0opfzcHaJ4tLF4NBf2', NULL, NULL, '2020-06-07 02:35:11', '2020-06-07 02:35:11'),
(2, 2, 'Ms. Julie Cole I', 'vmitchell@example.com', 'users/default.png', '2020-06-07 09:26:40', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'yQ8eCrfWP6', NULL, '2020-06-07 09:26:40', '2020-06-07 09:26:40'),
(3, 2, 'Anahi Legros', 'ygoyette@example.com', 'users/default.png', '2020-06-07 09:27:48', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'umgpCsC7xR', NULL, '2020-06-07 09:27:48', '2020-06-07 09:27:48'),
(4, 2, 'Gerry Ritchie', 'meggie.hand@example.net', 'users/default.png', '2020-06-07 09:30:11', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'q7u5wEgeqt', NULL, '2020-06-07 09:30:11', '2020-06-07 09:30:11'),
(5, 2, 'Sibyl Carroll', 'schaden.juwan@example.com', 'users/default.png', '2020-06-07 09:45:39', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', '5ho52T23Cu', NULL, '2020-06-07 09:45:39', '2020-06-07 09:45:39'),
(6, 2, 'user1', 'user@user.com', 'users/default.png', NULL, '$2y$10$Y2ZcHhF6PFLI6pJ72cqd9.5LW990Zd./FnvC3QbVJQ4muhrCvSn2.', NULL, '{\"locale\":\"en\"}', '2020-06-16 06:33:51', '2020-06-16 06:48:50');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_id`) VALUES
(6, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activity_log_log_name_index` (`log_name`),
  ADD KEY `subject` (`subject_id`,`subject_type`),
  ADD KEY `causer` (`causer_id`,`causer_type`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `equipment`
--
ALTER TABLE `equipment`
  ADD PRIMARY KEY (`equipment_key`),
  ADD UNIQUE KEY `equipment_equipment_serial_unique` (`equipment_serial`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_activities`
--
ALTER TABLE `log_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicine_names`
--
ALTER TABLE `medicine_names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_log`
--
ALTER TABLE `model_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `model_log_row_id_index` (`row_id`),
  ADD KEY `model_log_event_index` (`event`),
  ADD KEY `model_log_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  ADD PRIMARY KEY (`purchase_order_key`),
  ADD UNIQUE KEY `purchase_order_purchase_order_number_unique` (`purchase_order_number`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=123;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=199;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `equipment`
--
ALTER TABLE `equipment`
  MODIFY `equipment_key` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=119;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `log_activities`
--
ALTER TABLE `log_activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `medicine_names`
--
ALTER TABLE `medicine_names`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `model_log`
--
ALTER TABLE `model_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_orders`
--
ALTER TABLE `purchase_orders`
  MODIFY `purchase_order_key` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
