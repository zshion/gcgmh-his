<script src="https://unpkg.com/ag-grid-enterprise/dist/ag-grid-enterprise.min.noStyle.js"></script>
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-grid.css">
<link rel="stylesheet" href="https://unpkg.com/ag-grid-community/dist/styles/ag-theme-alpine.css">
<div id="myGrid" style="height: 600px;width:100%;" class="ag-theme-alpine"></div>

  <script type="text/javascript" charset="utf-8">


      var columnDefs = [
          {headerName:'Name', field: 'equipment_name', filter: 'agTextColumnFilter', minWidth: 200},
          {headerName:'Desc', field: 'equipment_description', filter: 'agTextColumnFilter' },
          {headerName:'Brand', field: 'equipment_brand', minWidth: 180,filter: 'agTextColumnFilter' },
          {headerName:'Serial', field: 'equipment_serial',filter: 'agTextColumnFilter' },
          {headerName:'Model', field: 'equipment_model',filter: 'agTextColumnFilter' },
          {headerName:'Country', field: 'equipment_country' },
          {headerName:'Inspected', field: 'equipment_date_inspected' },
          {headerName:'Acquired', field: 'equipment_date_acquired' },
          {headerName:'Supplier', field: 'equipment_supplier' },
          {headerName:'Price', field: 'equipment_unit_price' },
          {headerName:'Location', field: 'equipment_location' },
          {headerName:'Tag', field: 'equipment_tag_number' },
          {headerName:'Card', field: 'equipment_card_number' },
          {headerName:'Memo', field: 'equipment_memorandum_receipt' },
          {headerName:'IAR', field: 'equipment_iar_number' },
          {headerName:'Category', field: 'equipment_asset_categories' },
          {headerName:'AccountCode', field: 'equipment_account_code' },
          {headerName:'PurchaseOrder', field: 'equipment_purchase_order' },


      ];

      var gridOptions = {
    /*columnDefs: [
      { field: 'name', filter: 'agTextColumnFilter', minWidth: 200, checkboxSelection: true },
      { field: 'serial' },
      { field: 'sku', minWidth: 180 },
      { field: 'quantity' },

    ],*/
    columnDefs: columnDefs,
          rowSelection: 'multiple',
          defaultColDef: {
        rezizable:true,
              flex: 1,
              minWidth: 100,
              // allow every column to be aggregated
              enableValue: true,
              // allow every column to be grouped
              enableRowGroup: true,
              // allow every column to be pivoted
              enablePivot: true,
              sortable: true,
              filter: true,
              floatingFilter: true,
          },
          sideBar: true,

      };

      // setup the grid after the page has finished loading
      document.addEventListener('DOMContentLoaded', function() {
          var gridDiv = document.querySelector('#myGrid');
          new agGrid.Grid(gridDiv, gridOptions);

          agGrid
          .simpleHttpRequest({
                  url:
                      'http://localhost:8000/',
              })
              .then(function(data) {
              gridOptions.api.setRowData(data);
          });
      });

      function getSelectedRows() {
          gridOptions.api.setRowData(data);
      }

  </script>
