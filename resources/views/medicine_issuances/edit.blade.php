@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Medicine Issuance
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($medicineIssuance, ['route' => ['medicineIssuances.update', $medicineIssuance->id], 'method' => 'patch']) !!}

                        @include('medicine_issuances.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection