@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Medicine Issuance
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::open(['route' => 'medicineIssuances.store']) !!}

                        @include('medicine_issuances.fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
