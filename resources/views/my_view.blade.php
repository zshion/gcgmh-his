<html>
<head>
    <meta charset="utf-8">
    <title>Laravel Dependent Dropdown  Tutorial With Example</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>
<div class="container">
    <div class="form-group">
        <label for="country">Purchase Order:</label>
        <select name="country" class="form-control" style="width:250px">
            <option value="">--- Select Purchase Order---</option>
            @foreach ($countries as $key => $value)
                <option value="{{ $key }}">{{ $value }}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group">
        <label for="state">Select State:</label>
        <input name="states"/>
        <select name="state" class="form-control"style="width:250px">
            <option>--State--</option>
        </select>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function ()
    {
        jQuery('select[name="country"]').on('change',function(){
            var countryID = jQuery(this).val();

            //alert(countryID);
            if(countryID)
            {
               /* jQuery.ajax({
                    url : '/admin/equipment/drop/' +countryID,
                    type : "GET",
                    dataType : "json",
                    success:function(data)
                    {
                        console.log(data);
                        jQuery('select[name="state"]').empty();
                        jQuery.each(data, function(key,value){
                            $('select[name="state"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });

                    }
                });*/


                jQuery.ajax({
                    url : '/admin/equipment/drop/' +countryID,
                    type : "GET",
                    dataType : "json",
                    success:function(data)
                    {
                       // var obj = JSON.parse(data);

                        console.log(data);

                       /* jQuery('select[name="state"]').empty();
                        jQuery.each(data, function(key,value){
                            $('select[name="state"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });*/

                        for (var x = 0; x < data.length; x++) {

                            $('input[name="states"]').val($('input[name="states"]').val() + data[x].purchase_order_mode_of_procurement);
                            // updateListing(data[x]);
                        }


                       /* jQuery('input[name="states"]').empty();
                        jQuery.each(data, function(key,value){
                            $('input[name="states"]').val($('input[name="states"]').val() + value);
                        });*/
                    }
                });
            }
            else
            {
                $('select[name="state"]').empty();
            }
        });
    });
</script>
</body>
</html>
