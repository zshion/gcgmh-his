<div class="table-responsive">
    <table class="table" id="equipment-table">
        <thead>
            <tr>
                <th>Equipment Name</th>
        <th>Equipment Serial</th>
        <th>Equipment Model</th>
        <th>Equipment Quantity</th>
        <th>Equipment Description</th>
        <th>Equipment Brand</th>
        <th>Equipment Country</th>
        <th>Equipment Date Delivered</th>
        <th>Equipment Date Inspected</th>
        <th>Equipment Date Acquired</th>
        <th>Equipment Supplier</th>
        <th>Equipment Unit Price</th>
        <th>Equipment Location</th>
        <th>Equipment Tag Number</th>
        <th>Equipment Card Number</th>
        <th>Equipment Memorandum Receipt</th>
        <th>Equipment Iar Number</th>
        <th>Equipment Asset Categories</th>
        <th>Equipment Account Code</th>
        <th>Equipment Purchase Order</th>
        <th>Equipment Remarks</th>
        <th>Equipment Date Of Remarks</th>
        <th>Equipment Status</th>
        <th>Equipment Date Of Status</th>
        <th>Equipment Par</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($equipment as $equipment)
            <tr>
                <td>{{ $equipment->equipment_name }}</td>
            <td>{{ $equipment->equipment_serial }}</td>
            <td>{{ $equipment->equipment_model }}</td>
            <td>{{ $equipment->equipment_quantity }}</td>
            <td>{{ $equipment->equipment_description }}</td>
            <td>{{ $equipment->equipment_brand }}</td>
            <td>{{ $equipment->equipment_country }}</td>
            <td>{{ $equipment->equipment_date_delivered }}</td>
            <td>{{ $equipment->equipment_date_inspected }}</td>
            <td>{{ $equipment->equipment_date_acquired }}</td>
            <td>{{ $equipment->equipment_supplier }}</td>
            <td>{{ $equipment->equipment_unit_price }}</td>
            <td>{{ $equipment->equipment_location }}</td>
            <td>{{ $equipment->equipment_tag_number }}</td>
            <td>{{ $equipment->equipment_card_number }}</td>
            <td>{{ $equipment->equipment_memorandum_receipt }}</td>
            <td>{{ $equipment->equipment_iar_number }}</td>
            <td>{{ $equipment->equipment_asset_categories }}</td>
            <td>{{ $equipment->equipment_account_code }}</td>
            <td>{{ $equipment->equipment_purchase_order }}</td>
            <td>{{ $equipment->equipment_remarks }}</td>
            <td>{{ $equipment->equipment_date_of_remarks }}</td>
            <td>{{ $equipment->equipment_status }}</td>
            <td>{{ $equipment->equipment_date_of_status }}</td>
            <td>{{ $equipment->equipment_par }}</td>
                <td>
                    {!! Form::open(['route' => ['equipment.destroy', $equipment->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('equipment.show', [$equipment->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('equipment.edit', [$equipment->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
