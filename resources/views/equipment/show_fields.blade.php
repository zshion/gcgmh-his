<!-- Equipment Name Field -->
<div class="form-group">
    {!! Form::label('equipment_name', 'Equipment Name:') !!}
    <p>{{ $equipment->equipment_name }}</p>
</div>

<!-- Equipment Serial Field -->
<div class="form-group">
    {!! Form::label('equipment_serial', 'Equipment Serial:') !!}
    <p>{{ $equipment->equipment_serial }}</p>
</div>

<!-- Equipment Model Field -->
<div class="form-group">
    {!! Form::label('equipment_model', 'Equipment Model:') !!}
    <p>{{ $equipment->equipment_model }}</p>
</div>

<!-- Equipment Quantity Field -->
<div class="form-group">
    {!! Form::label('equipment_quantity', 'Equipment Quantity:') !!}
    <p>{{ $equipment->equipment_quantity }}</p>
</div>

<!-- Equipment Description Field -->
<div class="form-group">
    {!! Form::label('equipment_description', 'Equipment Description:') !!}
    <p>{{ $equipment->equipment_description }}</p>
</div>

<!-- Equipment Brand Field -->
<div class="form-group">
    {!! Form::label('equipment_brand', 'Equipment Brand:') !!}
    <p>{{ $equipment->equipment_brand }}</p>
</div>

<!-- Equipment Country Field -->
<div class="form-group">
    {!! Form::label('equipment_country', 'Equipment Country:') !!}
    <p>{{ $equipment->equipment_country }}</p>
</div>

<!-- Equipment Date Delivered Field -->
<div class="form-group">
    {!! Form::label('equipment_date_delivered', 'Equipment Date Delivered:') !!}
    <p>{{ $equipment->equipment_date_delivered }}</p>
</div>

<!-- Equipment Date Inspected Field -->
<div class="form-group">
    {!! Form::label('equipment_date_inspected', 'Equipment Date Inspected:') !!}
    <p>{{ $equipment->equipment_date_inspected }}</p>
</div>

<!-- Equipment Date Acquired Field -->
<div class="form-group">
    {!! Form::label('equipment_date_acquired', 'Equipment Date Acquired:') !!}
    <p>{{ $equipment->equipment_date_acquired }}</p>
</div>

<!-- Equipment Supplier Field -->
<div class="form-group">
    {!! Form::label('equipment_supplier', 'Equipment Supplier:') !!}
    <p>{{ $equipment->equipment_supplier }}</p>
</div>

<!-- Equipment Unit Price Field -->
<div class="form-group">
    {!! Form::label('equipment_unit_price', 'Equipment Unit Price:') !!}
    <p>{{ $equipment->equipment_unit_price }}</p>
</div>

<!-- Equipment Location Field -->
<div class="form-group">
    {!! Form::label('equipment_location', 'Equipment Location:') !!}
    <p>{{ $equipment->equipment_location }}</p>
</div>

<!-- Equipment Tag Number Field -->
<div class="form-group">
    {!! Form::label('equipment_tag_number', 'Equipment Tag Number:') !!}
    <p>{{ $equipment->equipment_tag_number }}</p>
</div>

<!-- Equipment Card Number Field -->
<div class="form-group">
    {!! Form::label('equipment_card_number', 'Equipment Card Number:') !!}
    <p>{{ $equipment->equipment_card_number }}</p>
</div>

<!-- Equipment Memorandum Receipt Field -->
<div class="form-group">
    {!! Form::label('equipment_memorandum_receipt', 'Equipment Memorandum Receipt:') !!}
    <p>{{ $equipment->equipment_memorandum_receipt }}</p>
</div>

<!-- Equipment Iar Number Field -->
<div class="form-group">
    {!! Form::label('equipment_iar_number', 'Equipment Iar Number:') !!}
    <p>{{ $equipment->equipment_iar_number }}</p>
</div>

<!-- Equipment Asset Categories Field -->
<div class="form-group">
    {!! Form::label('equipment_asset_categories', 'Equipment Asset Categories:') !!}
    <p>{{ $equipment->equipment_asset_categories }}</p>
</div>

<!-- Equipment Account Code Field -->
<div class="form-group">
    {!! Form::label('equipment_account_code', 'Equipment Account Code:') !!}
    <p>{{ $equipment->equipment_account_code }}</p>
</div>

<!-- Equipment Purchase Order Field -->
<div class="form-group">
    {!! Form::label('equipment_purchase_order', 'Equipment Purchase Order:') !!}
    <p>{{ $equipment->equipment_purchase_order }}</p>
</div>

<!-- Equipment Remarks Field -->
<div class="form-group">
    {!! Form::label('equipment_remarks', 'Equipment Remarks:') !!}
    <p>{{ $equipment->equipment_remarks }}</p>
</div>

<!-- Equipment Date Of Remarks Field -->
<div class="form-group">
    {!! Form::label('equipment_date_of_remarks', 'Equipment Date Of Remarks:') !!}
    <p>{{ $equipment->equipment_date_of_remarks }}</p>
</div>

<!-- Equipment Status Field -->
<div class="form-group">
    {!! Form::label('equipment_status', 'Equipment Status:') !!}
    <p>{{ $equipment->equipment_status }}</p>
</div>

<!-- Equipment Date Of Status Field -->
<div class="form-group">
    {!! Form::label('equipment_date_of_status', 'Equipment Date Of Status:') !!}
    <p>{{ $equipment->equipment_date_of_status }}</p>
</div>

<!-- Equipment Par Field -->
<div class="form-group">
    {!! Form::label('equipment_par', 'Equipment Par:') !!}
    <p>{{ $equipment->equipment_par }}</p>
</div>

