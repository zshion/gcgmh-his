<!-- Equipment Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_name', 'Equipment Name:') !!}
    {!! Form::text('equipment_name', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Serial Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_serial', 'Equipment Serial:') !!}
    {!! Form::text('equipment_serial', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Model Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_model', 'Equipment Model:') !!}
    {!! Form::text('equipment_model', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Quantity Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_quantity', 'Equipment Quantity:') !!}
    {!! Form::number('equipment_quantity', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_description', 'Equipment Description:') !!}
    {!! Form::text('equipment_description', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Brand Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_brand', 'Equipment Brand:') !!}
    {!! Form::text('equipment_brand', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Country Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_country', 'Equipment Country:') !!}
    {!! Form::text('equipment_country', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Date Delivered Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_date_delivered', 'Equipment Date Delivered:') !!}
    {!! Form::text('equipment_date_delivered', null, ['class' => 'form-control','id'=>'equipment_date_delivered']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#equipment_date_delivered').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Equipment Date Inspected Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_date_inspected', 'Equipment Date Inspected:') !!}
    {!! Form::text('equipment_date_inspected', null, ['class' => 'form-control','id'=>'equipment_date_inspected']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#equipment_date_inspected').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Equipment Date Acquired Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_date_acquired', 'Equipment Date Acquired:') !!}
    {!! Form::text('equipment_date_acquired', null, ['class' => 'form-control','id'=>'equipment_date_acquired']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#equipment_date_acquired').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Equipment Supplier Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_supplier', 'Equipment Supplier:') !!}
    {!! Form::text('equipment_supplier', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Unit Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_unit_price', 'Equipment Unit Price:') !!}
    {!! Form::number('equipment_unit_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Location Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_location', 'Equipment Location:') !!}
    {!! Form::text('equipment_location', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Tag Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_tag_number', 'Equipment Tag Number:') !!}
    {!! Form::text('equipment_tag_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Card Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_card_number', 'Equipment Card Number:') !!}
    {!! Form::text('equipment_card_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Memorandum Receipt Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_memorandum_receipt', 'Equipment Memorandum Receipt:') !!}
    {!! Form::text('equipment_memorandum_receipt', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Iar Number Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_iar_number', 'Equipment Iar Number:') !!}
    {!! Form::text('equipment_iar_number', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Asset Categories Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_asset_categories', 'Equipment Asset Categories:') !!}
    {!! Form::text('equipment_asset_categories', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Account Code Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_account_code', 'Equipment Account Code:') !!}
    {!! Form::text('equipment_account_code', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Purchase Order Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_purchase_order', 'Equipment Purchase Order:') !!}
    {!! Form::text('equipment_purchase_order', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Remarks Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_remarks', 'Equipment Remarks:') !!}
    {!! Form::text('equipment_remarks', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Date Of Remarks Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_date_of_remarks', 'Equipment Date Of Remarks:') !!}
    {!! Form::text('equipment_date_of_remarks', null, ['class' => 'form-control','id'=>'equipment_date_of_remarks']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#equipment_date_of_remarks').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Equipment Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_status', 'Equipment Status:') !!}
    {!! Form::text('equipment_status', null, ['class' => 'form-control']) !!}
</div>

<!-- Equipment Date Of Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_date_of_status', 'Equipment Date Of Status:') !!}
    {!! Form::text('equipment_date_of_status', null, ['class' => 'form-control','id'=>'equipment_date_of_status']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#equipment_date_of_status').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Equipment Par Field -->
<div class="form-group col-sm-6">
    {!! Form::label('equipment_par', 'Equipment Par:') !!}
    {!! Form::text('equipment_par', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('equipment.index') }}" class="btn btn-default">Cancel</a>
</div>
