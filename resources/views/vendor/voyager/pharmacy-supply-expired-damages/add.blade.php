@extends('voyager::master')

@section('css')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@stop

@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Add Expired Damaged Supply
        </h1>

    </div>
@stop

@section('content')

    <div class="panel panel-bordered">
        <!-- form start -->
        <form role="form" class="form-edit-add" action="/create" method="POST" enctype="multipart/form-data">
            <!-- PUT Method if we are editing -->

            <!-- CSRF TOKEN -->
            <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                <div class="panel-body">

                    <!-- Adding / Editing -->
                    <!-- GET THE DISPLAY OPTIONS -->
                                                                
                        <div class="form-group float-left  col-md-4 " >
                                    
                            <label class="control-label" for="name">Classification</label>
                                <div class="mr">
                                    <select class="form-control select2-ajax" name="pharmacy_item_type_key" data-get-items-route="http://127.0.0.1:8000/admin/medicine-expired-damages/relation" data-get-items-field="medicine_expired_damage_belongsto_pharmacy_item_type_relationship" data-method="add">
                                        <option value="">None</option>
                                    </select>
                                </div>
                        </div>
                    <!-- GET THE DISPLAY OPTIONS -->
                                                                
                        <div class="form-group float-left  col-md-4 " >
                                    
                            <label class="control-label" for="name">Item Description</label>
                                <div class="mr">
                                    <select class="form-control select2-ajax" name="pharmacy_supply_key" data-get-items-route="http://127.0.0.1:8000/admin/supply-expired-damages/relation" data-get-items-field="supply_expired_damage_belongsto_pharmacy_supply_relationship" data-method="add">
                                        <option value="">None</option>
                                    </select>
                                </div>
                        </div>
                                                        
                    <!-- GET THE DISPLAY OPTIONS -->
                                                                
                        <div class="form-group float-left  col-md-4 " >
                                    
                            <label class="control-label" for="name">Qty</label>
                                <div class="mr">
                                    <input  type="text" class="form-control" name="supply_expired_damaged_qty" placeholder="Qty" value="">
                                </div>
                        </div>
                                                        
                    <!-- GET THE DISPLAY OPTIONS -->
                                                                
                        <div class="form-group float-left  col-md-4 " >
                                    
                            <label class="control-label" for="name">Expired Date</label>
                                <div class="mr">
                                    <input  type="text" class="form-control" name="supply_expired_damaged_date" placeholder="Expired Date" value="">
                                </div>
                        </div>
                                                        
                    <!-- GET THE DISPLAY OPTIONS -->
                                                                
                        <div class="form-group float-left  col-md-4 " >
                                    
                            <label class="control-label" for="name">Location</label>
                                <div class="mr">
                                    <input  type="text" class="form-control" name="supply_expired_damaged_location" placeholder="Location" value="">
                                </div>
                        </div>
                            
                </div><!-- panel-body -->

                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary save">Save</button>
                    </div>
        </form>
    </div>                                   

@stop

