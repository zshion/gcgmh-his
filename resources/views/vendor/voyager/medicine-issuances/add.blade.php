@extends('voyager::master')


@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Add Medicine Issuance
        </h1>

    </div>
@stop

@section('content')
    <div class="page-content edit-add container-fluid">
        <div class="row">
            <div class="col-md-12">

                <div class="panel panel-bordered">
                    <!-- form start -->
                    <form role="form" class="form-edit-add form-horizontal" action="http://localhost:8000/admin/medicine-issuances" method="POST" enctype="multipart/form-data">
                        <!-- PUT Method if we are editing -->

                        <!-- CSRF TOKEN -->
                        <input type="hidden" name="_token" value="x1O8Nb1FKi06ZRYUd82ne0FTUxv614I5TcxYLFGh">

                        <div class="panel-body">


                            <!-- Adding / Editing -->

                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group float-left  col-md-4 ">

                                <label class="control-label" for="name">Issued Date</label>
                                <div class="mr">
                                    <input type="date" class="form-control" name="medicine_issuance_date" placeholder="Issued Date" value="">
                                </div>
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group float-left  col-md-4 ">

                                <label class="control-label" for="name">Charge Slip No.</label>
                                <div class="mr">
                                    <input type="text" class="form-control" name="medicine_issuance_charge_slip" placeholder="Charge Slip No." value="">
                                </div>
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group float-left  col-md-4 ">

                                <label class="control-label" for="name">Hospital No.</label>
                                <div class="mr">
                                    <input type="text" class="form-control" name="patient_hospital_no" placeholder="Hospital No." value="">
                                </div>
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group float-left  col-md-4 ">
                                <label class="control-label" for="name">Item Description</label>
                                <div class="mr">
                                    <select class="form-control select2-ajax select2-hidden-accessible" name="pharmacy_medicine_key" data-get-items-route="http://localhost:8000/admin/medicine-issuances/relation" data-get-items-field="medicine_issuance_belongsto_pharmacy_medicine_relationship" data-method="add" data-select2-id="1" tabindex="-1" aria-hidden="true">
                                        <option value="" data-select2-id="3">None</option>
                                    </select>

                                </div>
                            </div>






                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group float-left  col-md-4 ">

                                <label class="control-label" for="name">Qty</label>
                                <div class="mr">
                                    <input type="text" class="form-control" name="medicine_issuance_qty" placeholder="Qty" value="">





                                </div>
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group float-left  col-md-4 ">

                                <label class="control-label" for="name">Issued By</label>
                                <div class="mr">
                                    <input type="text" class="form-control" name="employee_name" placeholder="Issued By" value="">





                                </div>
                            </div>
                            <!-- GET THE DISPLAY OPTIONS -->

                            <div class="form-group float-left  col-md-4 ">
                                <label class="control-label" for="name">Remarks</label>
                                <div class="mr">
                                    <input type="text" class="form-control" name="medicine_issuance_remarks" placeholder="Remarks" value="">
                                </div>
                            </div>
                            {{--   <div class="form-group">
                                   <input type="text" name="country_name" id="country_name" class="form-control input-lg dcountry" placeholder="Enter Country Name" />
                                   <div id="countryList"> </div>
                               </div>


--}}
                            <div id="dynamictable">
                            </div>






                        </div><!-- panel-body -->

                        <div class="panel-footer">
                            <button type="submit" class="btn btn-primary save">Save</button>
                        </div>
                    </form>

                    <iframe id="form_target" name="form_target" style="display:none"></iframe>
                    <form id="my_form" action="http://localhost:8000/admin/upload" target="form_target" method="post" enctype="multipart/form-data" style="width:0;height:0;overflow:hidden">
                        <input name="image" id="upload_file" type="file" onchange="$('#my_form').submit();this.value='';">
                        <input type="hidden" name="type_slug" id="type_slug" value="medicine-issuances">
                        <input type="hidden" name="_token" value="x1O8Nb1FKi06ZRYUd82ne0FTUxv614I5TcxYLFGh">
                    </form>

                </div>
            </div>
        </div>
    </div>

    <table class="table table-bordered" id="dynamicTables">
        <tr>
            <th>Name</th>
            <th>Qty</th>
            <th>Price</th>
            <th>Action</th>
        </tr>
        <tr>
            <td>
                <div class="mr">
                    <select class="form-control select2-ajax select2-hidden-accessible" name="pharmacy_medicine_key" data-get-items-route="http://127.0.0.1:8000/admin/medicine-issuances/relation" data-get-items-field="medicine_issuance_belongsto_pharmacy_medicine_relationship" data-method="add" data-select2-id="1" tabindex="-1" aria-hidden="true">
                            <option value="" data-select2-id="3">None</option>
                    </select>
                </div>
            </td>
            <td><input type="text" name="addmore[0][name]" placeholder="Enter your Name" class="form-control" /></td>
            <td><input type="text" name="addmore[0][qty]" placeholder="Enter your Qty" class="form-control" /></td>
            <td><input type="text" name="addmore[0][price]" placeholder="Enter your Price" class="form-control" /></td>
            <td><button type="button" name="add" id="add" class="btn btn-success">Add More</button></td>
        </tr>
    </table>

    <button type="submit" class="btn btn-success">Save</button>


    <script type="text/javascript">

        var i = 0;

        $("#add").click(function(){
            ++i;
            $("#dynamictable").append('' +
                '<div id="dynamic'+i+'">'+
                '<input type="text" name="addmore['+i+'][name]" placeholder="Enter your Name" class="form-control" />' +
                '<input type="text" name="addmore['+i+'][qty]" placeholder="Enter your Qty" class="form-control" />' +
                '<input type="text" name="addmore['+i+'][price]" placeholder="Enter your Price" class="form-control" />' +

                ' <div class="form-group"><input type="text" name="country_name" id="medicine_'+i+'" class="form-control input-lg dcountry" placeholder="Enter Country Name" /> <div id="medicine_'+i+'" class="countryList"> </div> </div>'+
                '<button type="button" class="btn btn-danger remove-tr" onClick="removeDiv(this)">Remove</button>')+
            '</div>'
            ;
        });
        /* '<td> {{--{!! Form::open() !!}{!! Form::select('pharmacy_medicine_key', $medicine, null, ['class' => 'form-control', 'id'=>'1']) !!}{!! Form::close() !!}--}}  </td>'+*/

        function removeDiv(elem){
            --i;
            $(elem).parent('div').remove();
        }
        /*  $(document).on('click', '.remove-tr', function(){

              alert('clicked');

              var id_arr = $(this).attr('id');
              $('#'+id_arr).remove();
          });*/






    </script>
    <script>

        $(function() {




            $(document).delegate('.dcountry','focus',function(){

                var  id_arr = $(this).attr('id');

               // alert('#'+id_arr+'.countryList');
                $('#'+id_arr).keyup(function(){

                    var query = $(this).val();
                    if(query != '')
                    {
                        var _token = $('input[name="_token"]').val();
                        $.ajax({
                            url: "{{ url('admin/medicine-issuances/fetch') }}",
                            method:"GET",
                            data:{query:query, _token:_token},
                            success:function(data){
                                $('#'+id_arr+'.countryList').fadeIn();
                                $('#'+id_arr+'.countryList').html(data);
                            }
                        });
                    }
                });
            });

            $(document).mouseup(function(e)
            {
                id_arr = $(this).attr('id');

                var container = $('#'+id_arr+'.countryList');

                // if the target of the click isn't the container nor a descendant of the container
                if (!container.is(e.target) && container.has(e.target).length === 0)
                {
                    container.fadeOut();
                }
            });

            $(document).delegate('click', 'li', function(){

                $('#'+id_arr).val($(this).text());
                $('#'+id_arr+'.countryList').fadeOut();
            });
        });






    </script>
    <div class="modal fade modal-danger" id="confirm_delete_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"
                            aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><i class="voyager-warning"></i> {{ __('voyager::generic.are_you_sure') }}</h4>
                </div>

                <div class="modal-body">
                    <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'</h4>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                    <button type="button" class="btn btn-danger" id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <h1>Laravel 5 - Autocomplete Mutiple Fields Using jQuery, Ajax and MySQL</h1>
        {!! Form::open() !!}

        <table class="table table-bordered" id="bottom">
            <tr>
                <th><input class='check_all' type='checkbox' onclick="select_all()"/></th>
                <th>S. No</th>
                <th>Country Name</th>
            </tr>
            <tr>
                <td><input type='checkbox' class='chkbox'/></td>
                <td><span id='sn'>1.</span></td>
                <td><input class="form-control autocomplete_txt" type='text' data-type="countryname" id='countryname_1' name='countryname[]'/></td>
            </tr>
        </table>
        <button type="button" class='btn btn-danger delete'>- Delete</button>
        <button type="button" class='btn btn-success addbtn'>+ Add More</button>
        {!! Form::close() !!}
    </div>


    <script type="text/javascript">

        $(".delete").on('click', function() {
            $('.chkbox:checkbox:checked').parents("tr").remove();
            $('.check_all').prop("checked", false);
            updateSerialNo();
        });
        var i=$('table#bottom tr').length;
        $(".addbtn").on('click',function(){
            count=$('table#bottom tr').length;

            var data="<tr><td><input type='checkbox' class='chkbox'/></td>";
            data+="<td><span id='sn"+i+"'>"+count+".</span></td>";
            data+="<td><input class='form-control autocomplete_txt' type='text' data-type='countryname' id='countryname_"+i+"' name='countryname[]'/></td>";
            $('table').append(data);
            i++;
        });

        function select_all() {
            $('input[class=chkbox]:checkbox').each(function(){
                if($('input[class=check_all]:checkbox:checked').length == 0){
                    $(this).prop("checked", false);
                } else {
                    $(this).prop("checked", true);
                }
            });
        }
        function updateSerialNo(){
            obj=$('table tr').find('span');
            $.each( obj, function( key, value ) {
                id=value.id;
                $('#'+id).html(key+1);
            });
        }
        //autocomplete script
        $(document).on('focus','.autocomplete_txt',function(){
            type = 'countryname';

            autoType='pharmacy_medicine_name';


            $(this).autocomplete({
                minLength: 0,
                source: function( request, response ) {
                    $.ajax({
                        url: "{{ url('admin/medicine-issuances/searchajax') }}",
                        method:'GET',
                        dataType: "json",
                        data: {
                            term : request.term,
                            type : type,
                        },
                        success: function(data) {
                            var array = $.map(data, function (item) {
                                return {
                                    label: item['pharmacy_medicine_name'],
                                    value: item['pharmacy_medicine_key'],
                                    data : item
                                }
                            });
                            response(array)
                        }
                    });
                },
                select: function( event, ui ) {
                    var data = ui.item.data;
                    id_arr = $(this).attr('id');
                    id = id_arr.split("_");
                    elementId = id[id.length-1];
                    $('#countryname_'+elementId).val(data.name);

                }
            });


        });
    </script>



    <!-- End Delete File Modal -->
@stop


