@extends('voyager::master')


@section('page_header')


    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Monthly Reports
        </h1>

    </div>
@stop

@section('content')
    <div class="example-error inactive">
        <span class="message"></span>
    </div>

    <table class="table" id="table">
        <thead>
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">First Name</th>

            <th class="text-center">Actions</th>
        </tr>
        </thead>
        <tbody>
        <tbody>
        @foreach($data as $item)
            <tr class="item{{$item->medicine_issuance_key}}">
                <td>{{$item->medicine_issuance_key}}</td>
                <td>{{$item->pharmacy_medicine_name}}</td>

                <td><button class="edit-modal btn btn-info"
                            data-info="{{$item->medicine_issuance_key}},{{$item->pharmacy_medicine_name}}">
                        <span class="glyphicon glyphicon-edit"></span> Edit
                    </button>
                    <button class="delete-modal btn btn-danger"
                            data-info="{{$item->medicine_issuance_key}},{{$item->pharmacy_medicine_name}}">
                        <span class="glyphicon glyphicon-trash"></span> Delete
                    </button></td>
            </tr>
        @endforeach
        </tbody>
        </tbody>
    </table>
    <script>
        $(document).ready(function() {
            $('#table').DataTable( {
                dom: 'Blfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ]
            } );
        } );
    </script>
    <div class="page-wrapper">


       {{-- <div style="margin: 10px 0;">

            <button onclick="onBtnExportDataAsExcel()" class="btn btn-success btn-add-new" style="padding: 5px 20px;">
                Export Report
            </button>
        </div>

        <div id="myGrid" style="height: 600px;width:100%;" class="ag-theme-alpine"></div>--}}


        <!--Custom reports -->
        <div class="container">
            <div class="form-group col-md-3">
                <h5>Type <span class="text-danger"></span></h5>
                <div class="controls">
                    <select id="type" name="type">
                        <option value="1" >Regular</option>
                        <option value="2" >Donated</option>
                    </select>   </div>
            </div>
            <div class="form-group col-md-3">
                <h5>Start Date (Every 27 of the month) <span class="text-danger"></span></h5>
                <div class="controls">
                    <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date"> <div class="help-block"></div></div>
            </div>
            <div class="form-group col-md-3">
                <h5>End Date <span class="text-danger"></span></h5>
                <div class="controls">
                    <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date"> <div class="help-block"></div></div>
            </div>
            <div class="text-left" style="margin-left: 15px;">
                <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Submit</button>
            </div>
        </div>

            <div class="table-responsive col-md-12" style="width: 100%;">

            <table class="table table-bordered table-striped" id="laravel_datatable">
                <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Beginning Balance</th>
                    <th>Delivery</th>
                    <th>Issuance</th>
                    <th colspan="7">Adjustment</th>
                    <th colspan="2"></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>

                <tr>
                    <th>SKU</th>
                    <th>Medicines</th>
                    <th>QTY</th>
                    <th>QTY</th>
                    <th>QTY</th>
                    <th>Unrecorded IAR</th>
                    <th>Cancelled IAR</th>
                    <th>OverIssued</th>
                    <th>UnIssued</th>
                    <th>Returned from Ward</th>
                    <th>Issued to Employees</th>
                    <th>Replacement of Expired Medicines from Supplier</th>
                    <th>Expired</th>
                    <th>Damaged</th>
                    <th>RMA</th>
                    <th>Credit Note</th>
                    <th>Balance End</th>
                </tr>
                </thead>
            </table>
            </div>


        <script>

            $(document).ready(function() {

                jQuery.fn.DataTable.Api.register( 'buttons.exportData()', function ( options ) {
                    if ( this.context.length ) {
                        var jsonResult = $.ajax({
                            url: '/admin/issuances_reports?page=all',
                            data: function (d) {
                                d.start_date = $('#start_date').val();
                                d.end_date = $('#end_date').val();
                                d.type = $('#type').val();
                            },
                            success: function (result) {
                                //Do nothing
                            },
                            async: false
                        });

                        return {body: jsonResult.responseJSON.data.map(el => Object.keys(el).map(key => el[key])), header: $("#laravel_datatable thead tr th").map(function() { return this.innerHTML; }).get()};
                    }
                } );

                $("#laravel_datatable ").DataTable(
                    {
                        "dom": 'lBrtip',

                        "buttons":[{"extend":"create"},
                            {"extend":"export"},
                            {"extend":"print"},
                            {"extend":"reset"},
                            {"extend":"reload"}],
                        "processing": true,
                        "serverSide": true,
                        ajax: {
                            url: "{{ url('admin/issuances_reports') }}",
                            type: 'GET',
                            data: function (d) {
                                d.start_date = $('#start_date').val();
                                d.end_date = $('#end_date').val();
                                d.type = $('#type').val();
                            }
                        },
                        columns: [
                            { data: 'pharmacy_medicine_sku', name: 'pharmacy_medicine_sku' },
                            { data: 'pharmacy_medicine_name', name: 'pharmacy_medicine_name'},
                            { data: 'beginning', name: 'beginning' },
                            { data: 'delivery', name: 'delivery' },
                            { data: 'issuance', name: 'issuance' },
                            { data: 'unrecorded', name: 'unrecorded' },
                            { data: 'cancelled', name: 'cancelled' },
                            { data: 'overissued', name: 'overissued' },
                            { data: 'unissued', name: 'unissued' },
                            { data: 'ward', name: 'ward' },
                            { data: 'employee', name: 'employee' },
                            { data: 'replacement', name: 'replacement' },
                            { data: 'expired', name: 'expired' },
                            { data: 'damaged', name: 'damaged' },
                            { data: 'memo', name: 'memo' },
                            { data: 'note', name: 'note' },
                            { data: 'ending', name: 'ending' },
                            { data: 'action', name: 'action' },
                            { data: 'delete', name: 'delete' },

                        ],
                        order: [[0, 'asc']]
                    });

            });
            $('#btnFiterSubmitSearch').click(function(){
                $('#laravel_datatable').DataTable().draw(true);
            });

          /*  $(document).ready( function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#laravel_datatable').DataTable({
                    dom: 'lBfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                   /!* processing: true,
                    serverSide: true,
                    responsive: true,*!/


                    ajax: {
                        url: "{{ url('admin/issuances_reports') }}",
                        type: 'GET',
                        data: function (d) {
                            d.start_date = $('#start_date').val();
                            d.end_date = $('#end_date').val();
                            d.type = $('#type').val();
                        }
                    },
                dom: 'Bfrtip',
                buttons: [
                    $.extend( true, {}, buttonCommon, {
                        extend: 'copyHtml5'
                    } ),
                    $.extend( true, {}, buttonCommon, {
                        extend: 'excelHtml5'
                    } ),
                    $.extend( true, {}, buttonCommon, {
                        extend: 'pdfHtml5'
                    } )
                ]

                    columns: [
                        { data: 'pharmacy_medicine_sku', name: 'pharmacy_medicine_sku' },
                        { data: 'pharmacy_medicine_name', name: 'pharmacy_medicine_name'},
                        { data: 'beginning', name: 'beginning' },
                        { data: 'delivery', name: 'delivery' },
                        { data: 'issuance', name: 'issuance' },
                        { data: 'unrecorded', name: 'unrecorded' },
                        { data: 'cancelled', name: 'cancelled' },
                        { data: 'overissued', name: 'overissued' },
                        { data: 'unissued', name: 'unissued' },
                        { data: 'ward', name: 'ward' },
                        { data: 'employee', name: 'employee' },
                        { data: 'replacement', name: 'replacement' },
                        { data: 'expired', name: 'expired' },
                        { data: 'damaged', name: 'damaged' },
                        { data: 'memo', name: 'memo' },
                        { data: 'note', name: 'note' },
                        { data: 'ending', name: 'ending' },

                    ],
                    order: [[0, 'asc']]
                });

            });

            $('#btnFiterSubmitSearch').click(function(){
                $('#laravel_datatable').DataTable().draw(true);
            });*/

        </script>

    <style>
        /*.ag-header-group-cell {
            border-right:1px solid
        }*/
        .ag-header-group-cell-label {
            justify-content: center;
        }

        .columns {
            display: flex;
            flex-direction: row;
            margin-right: 20px;
        }

        label {
            box-sizing: border-box;
            display: block;
            padding: 2px;
        }

        button {
            display: inline-block;
            margin: 5px;
            font-weight: bold;
        }

        .page-wrapper {
            height: 100%;
            display: flex;
            flex-direction: column;
        }

        .grid-wrapper {
            flex-grow: 1;
        }
        .example-error {
            box-sizing: border-box;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 30px;
            background-color: #dc3a3a;
            color: white;
            transition: top 0.2s;
            white-space: nowrap;
            overflow: hidden;
            padding: 5px;
        }

        .example-error.inactive {
            top: -30px;
        }
    </style>

   {{-- <script type="text/javascript" charset="utf-8">


        var columnDefs = [
            {headerName:'Description', field: 'pharmacy_medicine_name' },
            {headerName:'Unit', field: 'medicine_key' },
            { headerName: 'Beginning Balance',
                children: [
                    { headerName: 'Qty', field: 'medicine_stockbalance_beginning' },
                    { headerName: 'Unit Cost', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]},
            { headerName: 'Deliveries',
                children: [
                    { headerName: 'Qty', field: 'totalqty' },
                    { headerName: 'Unit Cost', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]}



        ];

        var gridOptions = {
            /*columnDefs: [
              { field: 'name', filter: 'agTextColumnFilter', minWidth: 200, checkboxSelection: true },
              { field: 'serial' },
              { field: 'sku', minWidth: 180 },
              { field: 'quantity' },

            ],*/

            columnDefs: columnDefs,
            rowSelection: 'multiple',


            defaultColDef: {
                columnGroups:true,
                rezizable:true,
                flex: 1,
                minWidth: 100,
                // allow every column to be aggregated
                enableValue: true,
                // allow every column to be grouped
                enableRowGroup: true,
                // allow every column to be pivoted
                enablePivot: true,
                sortable: true,
                filter: 'agTextColumnFilter',
                floatingFilter: true,

            },
            sideBar: false,

        };

        // setup the grid after the page has finished loading
        document.addEventListener('DOMContentLoaded', function() {
            var gridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(gridDiv, gridOptions);

            agGrid
                .simpleHttpRequest({
                    url:
                        'http://localhost:8000/api/issuances_report',
                })
                .then(function(data) {
                    gridOptions.api.setRowData(data);
                });
        });

        function getSelectedRows() {
            gridOptions.api.setRowData(data);

        }

    </script>--}}
  {{--  <script>
        var columnDefs = [
            {headerName:'Description', field: 'pharmacy_medicine_name' },
            {headerName:'Unit', field: 'medicine_key' },
            { headerName: 'Beginning Balance',
                children: [
                    { headerName: 'Qty', field: 'medicine_stockbalance_beginning' },
                    { headerName: 'Unit Cost', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]},
            { headerName: 'Deliveries',
                children: [
                    { headerName: 'Qty', field: 'totalqty' },
                    { headerName: 'Unit Cost', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]},
            { headerName: 'Adjustment',
                children: [
                    { headerName: 'Add: unrecorded IAR', field: '' },
                    { headerName: 'Total Cost', field: '' },
                    { headerName: 'Less: Cancelled IAR', field: '' },
                    { headerName: 'Total Cost', field: '' },
                    { headerName: 'Add: Over Issued', field: '' },
                    { headerName: 'Total Cost', field: '' },
                    { headerName: 'Less: unissued', field: '' },
                    { headerName: 'Total Cost', field: '' },
                    { headerName: 'Add/Less: error', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]},
            { headerName: 'expired/damages',
                children: [
                    { headerName: 'Qty', field: 'damagesum' },
                    { headerName: 'Unit Cost', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]},
            { headerName: 'Total Issued',
                children: [
                    { headerName: 'Qty', field: 'issued'  },
                    { headerName: 'Unit Cost', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]},
            { headerName: 'Balance End',
                children: [
                    { headerName: 'Qty', field: 'medicine_stockbalance_current' },
                    { headerName: 'Unit Cost', field: '' },
                    { headerName: 'Total Cost', field: '' }
                ]}



        ];

        var gridOptions = {
            defaultColDef: {
                sortable: true,
                filter: true,
                resizable: true,
                minWidth: 100,
            },

            autoGroupColumnDef: {
                flex: 1,
                minWidth: 250,
            },

            columnDefs: columnDefs,
            sideBar: true,
            rowSelection: 'multiple',
            groupIncludeFooter: true,
            groupIncludeTotalFooter: true,


           /* onGridReady: function (params) {
                document.querySelector('#columnGroups').checked = true;
            },*/
        };

        function getBooleanValue(checkboxSelector) {
            return document.querySelector(checkboxSelector).checked === true;
        }


        function getParams() {
            return {

                columnGroups: true,
                onlySelected: true,


            };
        }
        /*function getParams() {
            return {
                allColumns: getBooleanValue('#allColumns'),
                columnGroups: getBooleanValue('#columnGroups'),
                columnKeys: getBooleanValue('#columnKeys') && ['country', 'bronze'],
                onlySelected: getBooleanValue('#onlySelected'),
                onlySelectedAllPages: getBooleanValue('#onlySelectedAllPages'),
                shouldRowBeSkipped:
                    getBooleanValue('#shouldRowBeSkipped') && only20YearOlds,
                skipFooters: getBooleanValue('#skipFooters'),
                skipGroups: getBooleanValue('#skipGroups'),
                skipHeader: getBooleanValue('#skipHeader'),
                skipPinnedTop: getBooleanValue('#skipPinnedTop'),
                skipPinnedBottom: getBooleanValue('#skipPinnedBottom'),
            };
        }*/

        function validateSelection(params) {
            var message = '';
            var errorDiv = document.querySelector('.example-error');
            var messageDiv = errorDiv.querySelector('.message');

            if (params.onlySelected || params.onlySelectedAllPages) {
                message += params.onlySelected ? 'onlySelected' : 'onlySelectedAllPages';
                message += ' is checked, please selected a row.';

                if (!gridOptions.api.getSelectedNodes().length) {
                    errorDiv.classList.remove('inactive');
                    messageDiv.innerHTML = message;
                    window.setTimeout(function () {
                        errorDiv.classList.add('inactive');
                        messageDiv.innerHTML = '';
                    }, 2000);
                    return true;
                }
            }

            return false;
        }

        function onBtnExportDataAsCsv() {
            var params = getParams();
            if (validateSelection(params)) {
                return;
            }
            gridOptions.api.exportDataAsCsv(params);
        }

        function onBtnExportDataAsExcel() {
            var params = getParams();
            if (validateSelection(params)) {
                return;
            }
            gridOptions.api.exportDataAsExcel(params);
        }

        // setup the grid after the page has finished loading
        document.addEventListener('DOMContentLoaded', function () {
            var gridDiv = document.querySelector('#myGrid');
            new agGrid.Grid(gridDiv, gridOptions);

            agGrid
                .simpleHttpRequest({
                    url:
                        'http://localhost:8000/admin/issuances_reports',
                })
                .then(function (data) {
                    gridOptions.api.setRowData(data);
                    gridOptions.api.forEachNode(function (node) {
                        node.expanded = true;
                    });
                    gridOptions.api.onGroupExpandedOrCollapsed();
                });
        });
    </script>--}}


@stop
