<!DOCTYPE html>
<html>
<head>
    <title>Laravel Yajra Datatables Export to Excel Button Example - ItSolutionStuff.com</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
    <script src="/vendor/datatables/buttons.server-side.js"></script> 

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>



<body>

<div class="container">
    <div class="form-group col-md-3">
        <h5>Type <span class="text-danger"></span></h5>
        <div class="controls">
            <select id="type" name="type">
                <option value="1" >Regular</option>
                <option value="2" >Donated</option>
            </select>   </div>
    </div>
    <div class="form-group col-md-3">
        <h5>Start Date <span class="text-danger"></span></h5>
        <div class="controls">
            <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date"> <div class="help-block"></div></div>
    </div>
    <div class="form-group col-md-3">
        <h5>End Date <span class="text-danger"></span></h5>
        <div class="controls">
            <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date"> <div class="help-block"></div></div>
    </div>
  {{--  <label for="from">From</label>
    <input type="text" id="start_date" name="start_date">
    <label for="to">to</label>
    <input type="text" id="end_date" name="end_date">
--}}

    <div class="text-left" style="
    margin-left: 15px;
    ">
        <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Submit</button>
    </div>
    <br>

    {!! $dataTable->table() !!}


  {{--  <script>
        $( function() {
            var dateFormat = "mm/dd/yy",
                from = $( "#start_date" )
                    .datepicker({
                        defaultDate: "+0",
                        changeMonth: true,
                        numberOfMonths: 1
                    })
                    .on( "change", function() {
                        to.datepicker( "option", "minDate", getDate( this ) );
                    }),
                to = $( "#end_date" ).datepicker({
                    defaultDate: "+1",
                    changeMonth: true,
                    numberOfMonths: 1
                })
                    .on( "change", function() {
                        from.datepicker( "option", "maxDate", getDate( this ) );
                    });

            function getDate( element ) {
                var date;
                try {
                    date = $.datepicker.parseDate( dateFormat, element.value );
                } catch( error ) {
                    date = null;
                }

                return date;
            }
        } );
    </script>--}}
    <script>

       // $(document).ready(function() {

       /* $("#issuancesreports-table ").DataTable(
            {   dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                "processing": true,
                "serverSide": true,
                ajax: {
                    url: "{{ url('admin/issuances_reports') }}",
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                        d.type = $('#type').val();
                    }
                },
                columns: [
                    { data: 'action', name: 'action' },
                    { data: 'pharmacy_medicine_sku', name: 'pharmacy_medicine_key' },


                ],
                order: [[0, 'asc']]
            });

        });*/

        $('#btnFiterSubmitSearch').click(function(){
            $('#issuancesreports-table').DataTable().draw(true);
               /* {   dom: 'lBfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    "processing": true,
                    "serverSide": true,
                    ajax: {
                        url: "{{ url('admin/issuances_reports') }}",
                        type: 'GET',
                        data: function (d) {
                            d.start_date = $('#start_date').val();
                            d.end_date = $('#end_date').val();
                            d.type = $('#type').val();
                        }
                    },

                }).draw(true);*/
        });
    </script>
</div>

</body>

{!! $dataTable->scripts() !!}

</html>
