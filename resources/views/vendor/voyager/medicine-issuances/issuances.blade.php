@extends('voyager::master')


@section('page_header')


    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Medicine Issuances
        </h1>
        <a class="btn btn-success" href="medicine-issuances/create" role="button">Add New</a>
    </div>
@stop
@section('content')
    <div class="example-error inactive">
        <span class="message"></span>
    </div>
    <div class="page-wrapper">


        <!--Custom reports -->
        <div class="container">

            <br>
            <div class="table-responsive col-md-12" style="width: 100%;">
            <table class="table table-bordered table-striped" id="laravel_datatable">
                <thead>
                    <tr>
                        <th>Issued Date</th>
                        <th>Type</th>
                        <th>Charge Slip No.</th>
                        <th>Hospital No.</th>
                        <th>Item Description</th>
                        <th>Qty</th>
                        <th>Issued By</th>
                        <th>Remarks</th>
                        <th>Created At</th>
                        <th>Actions</th>

                    </tr>
                </thead>
            </table>
            </div>
        </div>

        <script>
            $(document).ready( function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#laravel_datatable').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    searching: true,

                    ajax: {
                        url: "{{ url('admin/view_issuances') }}",
                        type: 'GET',
                        data: function (d) {
                            d.start_date = $('#start_date').val();
                            d.end_date = $('#end_date').val();
                            d.type = $('#type').val();
                        }
                    },
                    /*exporting */
                    dom: 'lBfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ],
                    columns: [
                        { data: 'medicine_issuance_date', name: 'medicine_issuance_date' },
                        { data: 'pharmacy_item_type_name', name: 'pharmacy_item_type_name' },
                        { data: 'medicine_issuance_charge_slip', name: 'medicine_issuance_charge_slip' },
                        { data: 'patient_hospital_no', name: 'patient_hospital_no'},
                        { data: 'pharmacy_medicine_name', name: 'pharmacy_medicine_name'},
                        { data: 'medicine_issuance_qty', name: 'medicine_issuance_qty'},
                        { data: 'employee_name', name: 'employee_name'},
                        { data: 'medicine_issuance_remarks', name: 'medicine_issuance_remarks'},
                        { data: 'created_at', name: 'created_at'},
                        { data: 'action', name: 'action'},

                    ],
                    order: [[0, 'asc']]
                });

            });

            $('#btnFiterSubmitSearch').click(function(){
                $('#laravel_datatable').DataTable().draw(true);
            });

        </script>

    <style>
        /*.ag-header-group-cell {
            border-right:1px solid
        }*/
        .ag-header-group-cell-label {
            justify-content: center;
        }

        .columns {
            display: flex;
            flex-direction: row;
            margin-right: 20px;
        }

        label {
            box-sizing: border-box;
            display: block;
            padding: 2px;
        }

        button {
            display: inline-block;
            margin: 5px;
            font-weight: bold;
        }

        .page-wrapper {
            height: 100%;
            display: flex;
            flex-direction: column;
        }

        .grid-wrapper {
            flex-grow: 1;
        }
        .example-error {
            box-sizing: border-box;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 30px;
            background-color: #dc3a3a;
            color: white;
            transition: top 0.2s;
            white-space: nowrap;
            overflow: hidden;
            padding: 5px;
        }

        .example-error.inactive {
            top: -30px;
        }
    </style>

@stop
