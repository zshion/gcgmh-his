@extends('voyager::master')


@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i>Return Merchandise Authorization
        </h1>
        <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
        </a>
    </div>
@stop

@section('content')
    <div class="example-error inactive">
        <span class="message"></span>
    </div>
    <div class="page-wrapper">

        <!--Custom Issuances Table -->

            <div class="table-responsive col-md-12" style="width: 100%;">
                <table class="table table-bordered table-striped" id="laravel_datatable">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>RMA No.</th>
                            <th>RMA Date</th>
                            <th>Supplier</th>
                            <th>Item Description</th>
                            <th>Qty</th>
                            <th>Unit Cost</th>
                            <th>Total Cost</th>
                            <th>Remarks</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <script>
                $(document).ready( function () {
                    var response_data;
    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $('#laravel_datatable').DataTable({
                        processing: true,
                        serverSide: true,
                        responsive: true,
    
                        ajax: {
                            url: "{{ url('api/medicine_credit_memos') }}",
                            type: 'GET',
                            dataType: 'json',
                        },
                        "drawCallback": function (settings) { 
                            var response = settings.json;
                            response_data = response.data;
                        },
                            /*exporting */
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ],
                        columns: [
                            { data: 'medicine_credit_memo_date', name: 'medicine_credit_memo_date' },
                            { data: 'medicine_credit_memo_docno', name: 'medicine_credit_memo_docno'},
                            { data: 'medicine_credit_memo_doc_date', name: 'medicine_credit_memo_doc_date'},
                            { data: 'supplier_name', name: 'supplier_name'},
                            { data: 'pharmacy_medicine_name', name: 'pharmacy_medicine_name'},
                            { data: 'medicine_credit_memo_qty', name: 'medicine_credit_memo_qty'},
                            { data: 'medicine_credit_memo_unit_cost', name: 'medicine_credit_memo_unit_cost'},
                            { data: 'medicine_credit_memo_total_cost', name: 'medicine_credit_memo_total_cost'},
                            { data: 'medicine_credit_memo_remarks', name: 'medicine_credit_memo_remarks'},
                            { data: 'created_date', name: 'created_date'},
                            { data: 'action', name: 'action'},
                        ],
                        order: [[0, 'desc']]
                    });
    
                });
    
                $('#btnFiterSubmitSearch').click(function(){
                    $('#laravel_datatable').DataTable().draw(true);
                });
    
            </script>
    
        <style>
            /*.ag-header-group-cell {
                border-right:1px solid
            }*/
            .ag-header-group-cell-label {
                justify-content: center;
            }
    
            .columns {
                display: flex;
                flex-direction: row;
                margin-right: 20px;
            }
    
            label {
                box-sizing: border-box;
                display: block;
                padding: 2px;
            }
    
            button {
                display: inline-block;
                margin: 5px;
                font-weight: bold;
            }
    
            .page-wrapper {
                height: 100%;
                display: flex;
                flex-direction: column;
            }
    
            .grid-wrapper {
                flex-grow: 1;
            }
            .example-error {
                box-sizing: border-box;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 30px;
                background-color: #dc3a3a;
                color: white;
                transition: top 0.2s;
                white-space: nowrap;
                overflow: hidden;
                padding: 5px;
            }
    
            .example-error.inactive {
                top: -30px;
            }
        </style>
    
@stop