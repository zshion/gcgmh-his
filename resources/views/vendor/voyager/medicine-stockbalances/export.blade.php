@extends('voyager::template')

@section('content')

    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Medicine Stock Balances
        </h1>
    </div>

    <div class="example-error inactive">
        <span class="message"></span>
    </div>

    <div class="container">
        <div class="table-responsive col-md-12" style="width: 100%;">
            <table class="table table-bordered table-striped" id="laravel_datatable">
                <thead>
                    <tr>
                        <th>SKU</th>
                        <th>Item Description</th>
                        <th>Beginning Qty</th>
                        <th>Current Qty</th>
                        <th>Re-Order Level</th>
                        <th>Status</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

    <script>
        $(document).ready( function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: true,

                ajax: {
                    url: "{{ url('api/medicine_stockbalances') }}",
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                        d.type = $('#type').val();
                    }
                },
                /*exporting */
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    { data: 'pharmacy_medicine_sku', name: 'pharmacy_medicine_sku' },
                    { data: 'pharmacy_medicine_name', name: 'pharmacy_medicine_name'},
                    { data: 'medicine_stockbalance_beginning', name: 'medicine_stockbalance_beginning'},  
                    { data: 'medicine_stockbalance_current', name: 'medicine_stockbalance_current'},  
                    { data: 'medicine_stockbalance_reorder', name: 'medicine_stockbalance_reorder'},
                    { data: 'medicine_stockbalance_status', name: 'medicine_stockbalance_status'}, 
                ],
                order: [[0, 'asc']]
            });

        });

        $('#btnFiterSubmitSearch').click(function(){
            $('#laravel_datatable').DataTable().draw(true);
        });

    </script>

    <style>
        /*.ag-header-group-cell {
            border-right:1px solid
        }*/
        .ag-header-group-cell-label {
            justify-content: center;
        }

        .columns {
            display: flex;
            flex-direction: row;
            margin-right: 20px;
        }

        label {
            box-sizing: border-box;
            display: block;
            padding: 2px;
        }

        button {
            display: inline-block;
            margin: 5px;
            font-weight: bold;
        }

        .page-wrapper {
            height: 100%;
            display: flex;
            flex-direction: column;
        }

        .grid-wrapper {
            flex-grow: 1;
        }
        .example-error {
            box-sizing: border-box;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 30px;
            background-color: #dc3a3a;
            color: white;
            transition: top 0.2s;
            white-space: nowrap;
            overflow: hidden;
            padding: 5px;
        }

        .example-error.inactive {
            top: -30px;
        }
    </style>

@stop