@extends('voyager::template')

@section('content')

    <h1 class="page-title">
        <i class=""></i> Monthly Report
    </h1>

    <div class="example-error inactive">
        <span class="message"></span>
    </div>
    <div class="page-wrapper">

    <div class="container">
        <div class="form-group col-md-3">
            <h5>Type <span class="text-danger"></span></h5>
            <div class="controls">
                <select id="type" name="type">
                    <option value="1" >Regular</option>
                    <option value="2" >Donated</option>
                </select>   </div>
        </div>
        <div class="form-group col-md-3">
            <h5>Start Date <span class="text-danger"></span></h5>
            <div class="controls">
                <input type="date" name="start_date" id="start_date" class="form-control datepicker-autoclose" placeholder="Please select start date"> <div class="help-block"></div></div>
        </div>
        <div class="form-group col-md-3">
            <h5>End Date <span class="text-danger"></span></h5>
            <div class="controls">
                <input type="date" name="end_date" id="end_date" class="form-control datepicker-autoclose" placeholder="Please select end date"> <div class="help-block"></div></div>
        </div>


        <div class="text-left" style="margin-left: 15px;">
            <button type="text" id="btnFiterSubmitSearch" class="btn btn-info">Submit</button>
        </div>
        <div class="table-responsive col-md-12" style="width: 100%;">
            <table class="table table-bordered table-striped" id="laravel_datatable">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th>Delivery</th>
                        <th>Issuance</th>
                        <th colspan="7">Adjustment</th>
                        <th colspan="2"></th>
                        <th></th>
                        <th></th>
                    </tr>
    
                    <tr>
                        <th>SKU</th>
                        <th>Medicines</th>
                        <th>QTY</th>
                        <th>QTY</th>
                        <th>Unrecorded IAR</th>
                        <th>Cancelled IAR</th>
                        <th>OverIssued</th>
                        <th>UnIssued</th>
                        <th>Returned from Ward</th>
                        <th>Issued to Employees</th>
                        <th>Replacement of Expired Medicines from Supplier</th>
                        <th>Expired</th>
                        <th>Damaged</th>
                        <th>RMA</th>
                        <th>Credit Note</th>
                    </tr>
                </thead>
            </table>
            </div>
    </div>
    <script>
        $(document).ready( function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#laravel_datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                searching: true,

                ajax: {
                    url: "{{ url('admin/pharmacy-medicine-transaction-report') }}",
                    type: 'GET',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                        d.type = $('#type').val();
                    }
                },
                /*exporting */
                dom: 'lBfrtip',
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                columns: [
                    { data: 'pharmacy_medicine_sku', name: 'pharmacy_medicine_sku' },
                            { data: 'pharmacy_medicine_name', name: 'pharmacy_medicine_name'},
                            { data: 'delivery', name: 'delivery' },
                            { data: 'issuance', name: 'issuance' },
                            { data: 'unrecorded', name: 'unrecorded' },
                            { data: 'cancelled', name: 'cancelled' },
                            { data: 'overissued', name: 'overissued' },
                            { data: 'unissued', name: 'unissued' },
                            { data: 'ward', name: 'ward' },
                            { data: 'employee', name: 'employee' },
                            { data: 'replacement', name: 'replacement' },
                            { data: 'expired', name: 'expired' },
                            { data: 'damaged', name: 'damaged' },
                            { data: 'memo', name: 'memo' },
                            { data: 'note', name: 'note' },   
                ],
                order: [[0, 'asc']]
            });

        });

        $('#btnFiterSubmitSearch').click(function(){
            $('#laravel_datatable').DataTable().draw(true);
        });

    </script>

    <style>
        /*.ag-header-group-cell {
            border-right:1px solid
        }*/
        .ag-header-group-cell-label {
            justify-content: center;
        }

        .columns {
            display: flex;
            flex-direction: row;
            margin-right: 20px;
        }

        label {
            box-sizing: border-box;
            display: block;
            padding: 2px;
        }

        button {
            display: inline-block;
            margin: 5px;
            font-weight: bold;
        }

        .page-wrapper {
            height: 100%;
            display: flex;
            flex-direction: column;
        }

        .grid-wrapper {
            flex-grow: 1;
        }
        .example-error {
            box-sizing: border-box;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 30px;
            background-color: #dc3a3a;
            color: white;
            transition: top 0.2s;
            white-space: nowrap;
            overflow: hidden;
            padding: 5px;
        }

        .example-error.inactive {
            top: -30px;
        }
    </style>

@stop