@extends('voyager::master')


@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class=""></i> Equipment Reports
    </h1>

</div>
@stop
@section('content')
<div id="myGrid" style="height: 600px;width:100%;" class="ag-theme-alpine"></div>

<script type="text/javascript" charset="utf-8">


    var columnDefs = [
        {headerName:'Name', field: 'equipment_name', filter: 'agTextColumnFilter', minWidth: 200},
        {headerName:'Desc', field: 'equipment_description', filter: 'agTextColumnFilter' },
        {headerName:'Brand', field: 'equipment_brand', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Serial', field: 'equipment_serial',filter: 'agTextColumnFilter' },
        {headerName:'Model', field: 'equipment_model',filter: 'agTextColumnFilter' },
        {headerName:'Country', field: 'equipment_country', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Inspected', field: 'equipment_date_inspected', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Acquired', field: 'equipment_date_acquired', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'PAR', field: 'equipment_par', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Supplier', field: 'equipment_supplier', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Price', field: 'equipment_unit_price', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Location', field: 'equipment_location', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Tag', field: 'equipment_tag_number', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Card', field: 'equipment_card_number', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Memo', field: 'equipment_memorandum_receipt', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'IAR', field: 'equipment_iar_number', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Category', field: 'equipment_asset_categories', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'AccountCode', field: 'equipment_account_code', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'PurchaseOrder', field: 'equipment_purchase_order', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Warranty Period', field: 'equipment_warranty_period', minWidth: 180,filter: 'agTextColumnFilter' },
        {headerName:'Preventive Maintenance Frequency', field: 'equipment_maintenance_frequency', minWidth: 180,filter: 'agTextColumnFilter' },


    ];

    var gridOptions = {
        /*columnDefs: [
          { field: 'name', filter: 'agTextColumnFilter', minWidth: 200, checkboxSelection: true },
          { field: 'serial' },
          { field: 'sku', minWidth: 180 },
          { field: 'quantity' },

        ],*/
        columnDefs: columnDefs,
        rowSelection: 'multiple',
        defaultColDef: {
            rezizable:true,
            flex: 1,
            minWidth: 100,
            // allow every column to be aggregated
            enableValue: true,
            // allow every column to be grouped
            enableRowGroup: true,
            // allow every column to be pivoted
            enablePivot: true,
            sortable: true,
            filter: true,
            floatingFilter: true,
        },
        sideBar: true,

    };

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);

        agGrid
            .simpleHttpRequest({
                url:
                    'http://localhost:8000/api/equipment',
            })
            .then(function(data) {
                gridOptions.api.setRowData(data);
            });
    });

    function getSelectedRows() {
        gridOptions.api.setRowData(data);
    }

</script>
@stop
