@extends('voyager::master')


@section('page_header')
    <div class="container-fluid">
        <h1 class="page-title">
            <i class=""></i> Medicine Adjustments
        </h1>
        <a href="{{ route('voyager.'.$dataType->slug.'.create') }}" class="btn btn-success btn-add-new">
            <i class="voyager-plus"></i> <span>{{ __('voyager::generic.add_new') }}</span>
        </a>
    </div>
@stop

@section('content')
    <div class="example-error inactive">
        <span class="message"></span>
    </div>
    <div class="page-wrapper">

        <!--Custom Issuances Table -->

            <div class="table-responsive col-md-12" style="width: 100%;">
                <table class="table table-bordered table-striped" id="laravel_datatable">
                    <thead>
                        <tr>
                            <th>Adjustment Date</th>
                            <th>Type</th>
                            <th>Document No.</th>
                            <th>Adjustment</th>
                            <th>Item Description</th>
                            <th>Qty</th>
                            <th>Unit Cost</th>
                            <th>Total Cost</th>
                            <th>Remarks</th>
                            <th>Created At</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <script>
                $(document).ready( function () {
                    var response_data;
    
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
    
                    $('#laravel_datatable').DataTable({
                        processing: true,
                        serverSide: true,
                        responsive: true,
    
                        ajax: {
                            url: "{{ url('api/medicine_adjustments') }}",
                            type: 'GET',
                            dataType: 'json',
                        },
                        "drawCallback": function (settings) { 
                            var response = settings.json;
                            response_data = response.data;
                        },
                            /*exporting */
                        dom: 'lBfrtip',
                        buttons: [
                            'copy', 'csv', 'excel', 'pdf', 'print'
                        ],
                        columns: [
                            { data: 'medicine_adjustment_date', name: 'medicine_adjustment_date' },
                            { data: 'pharmacy_item_type_name', name: 'pharmacy_item_type_name'},
                            { data: 'medicine_adjustment_docno', name: 'medicine_adjustment_docno'},
                            { data: 'medicine_adjustment_list_name', name: 'medicine_adjustment_list_name'},
                            { data: 'pharmacy_medicine_name', name: 'pharmacy_medicine_name'},
                            { data: 'medicine_adjustment_qty', name: 'medicine_adjustment_qty' },
                            { data: 'medicine_adjustment_unit_cost', name: 'medicine_adjustment_unit_cost'},
                            { data: 'medicine_adjustment_total_cost', name: 'medicine_adjustment_total_cost'},
                            { data: 'medicine_adjustment_remarks', name: 'medicine_adjustment_remarks'},
                            { data: 'created_date', name: 'created_date'},
                            { data: 'action', name: 'action'},
                        ],
                        order: [[0, 'desc']]
                    });
    
                });
    
                $('#btnFiterSubmitSearch').click(function(){
                    $('#laravel_datatable').DataTable().draw(true);
                });
    
            </script>
    
        <style>
            /*.ag-header-group-cell {
                border-right:1px solid
            }*/
            .ag-header-group-cell-label {
                justify-content: center;
            }
    
            .columns {
                display: flex;
                flex-direction: row;
                margin-right: 20px;
            }
    
            label {
                box-sizing: border-box;
                display: block;
                padding: 2px;
            }
    
            button {
                display: inline-block;
                margin: 5px;
                font-weight: bold;
            }
    
            .page-wrapper {
                height: 100%;
                display: flex;
                flex-direction: column;
            }
    
            .grid-wrapper {
                flex-grow: 1;
            }
            .example-error {
                box-sizing: border-box;
                position: absolute;
                top: 0;
                left: 0;
                width: 100%;
                height: 30px;
                background-color: #dc3a3a;
                color: white;
                transition: top 0.2s;
                white-space: nowrap;
                overflow: hidden;
                padding: 5px;
            }
    
            .example-error.inactive {
                top: -30px;
            }
        </style>    
@stop