@extends('voyager::master')


@section('page_header')
<div class="container-fluid">
    <h1 class="page-title">
        <i class=""></i> Purchase Order Reports
    </h1>

</div>
@stop
@section('content')
<!-- ag grid -->

<div id="myGrid" style="height: 600px;width:100%;" class="ag-theme-alpine"></div>

<script type="text/javascript" charset="utf-8">
    var columnDefs = [
        {headerName:'PONumber', field: 'purchase_order_number', filter: 'agTextColumnFilter', minWidth: 200},
        {headerName:'PRNumber', field: 'purchase_order_pr_number', filter: 'agTextColumnFilter' },
        {headerName:'Procurement', field: 'purchase_order_mode_of_procurement', minWidth: 180,filter: 'agTextColumnFilter'

        },
        {headerName:'DateAwarded', field: 'purchase_order_date_awarded',filter: 'agTextColumnFilter',
            filterParams: {
                comparator: function(filterLocalDateAtMidnight, cellValue) {
                    var dateAsString = cellValue;
                    if (dateAsString == null) return -1;
                    var dateParts = dateAsString.split('/');
                    var cellDate = new Date(
                        Number(dateParts[2]),
                        Number(dateParts[1]) - 1,
                        Number(dateParts[0])
                    );

                    if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) {
                        return 0;
                    }

                    if (cellDate < filterLocalDateAtMidnight) {
                        return -1;
                    }

                    if (cellDate > filterLocalDateAtMidnight) {
                        return 1;
                    }
                },
                browserDatePicker: true,
            }
        },
        {headerName:'DateReceived', field: 'purchase_order_date_of_po_received',filter: 'agTextColumnFilter' },
        {headerName:'DateDelivered', field: 'purchase_order_date_delivered' },
        {headerName:'DateInspected', field: 'purchase_order_inspected' },
        {headerName:'DateAcquired', field: 'purchase_order_date_acquired' },




    ];

    var gridOptions = {
        /*columnDefs: [
          { field: 'name', filter: 'agTextColumnFilter', minWidth: 200, checkboxSelection: true },
          { field: 'serial' },
          { field: 'sku', minWidth: 180 },
          { field: 'quantity' },

        ],*/
        columnDefs: columnDefs,
        rowSelection: 'multiple',
        defaultColDef: {
            rezizable:true,
            flex: 1,
            minWidth: 100,
            // allow every column to be aggregated
            enableValue: true,
            // allow every column to be grouped
            enableRowGroup: true,
            // allow every column to be pivoted
            enablePivot: true,
            sortable: true,
            filter: true,
            floatingFilter: true,
        },
        sideBar: true,

    };

    // setup the grid after the page has finished loading
    document.addEventListener('DOMContentLoaded', function() {
        var gridDiv = document.querySelector('#myGrid');
        new agGrid.Grid(gridDiv, gridOptions);

        agGrid
            .simpleHttpRequest({
                url:
                    'http://localhost:8000/api/purchase_orders',
            })
            .then(function(data) {
                gridOptions.api.setRowData(data);
            });
    });

    function getSelectedRows() {
        gridOptions.api.setRowData(data);
    }

</script>
<!--end agrid -->
@stop
